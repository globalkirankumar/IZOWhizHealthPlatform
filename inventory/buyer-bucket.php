<?php 
	include("include/config.php");
	include("include/functions.php");
	if($_SESSION['sess_username']==''){
		
		header("Location:".SITE_URL);	
	}
?>
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>Izohealth</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>
<script>
  
	function checkall(objForm)
    {
	len = objForm.elements.length;
	var i=0;
	for( i=0 ; i<len ; i++){
		if (objForm.elements[i].type=='checkbox') 
		objForm.elements[i].checked=objForm.check_all.checked;
	}
   }
	function del_prompt(frmobj,comb)
		{
		//alert(comb);
			if(comb=='Activate'){
				if(confirm ("Are you sure you want to Assign Medician(s)"))
				{
					frmobj.action = "bucket-blank.php";
					frmobj.what.value="Assign";
					frmobj.submit();
					
				}
				else{ 
				return false;
				}
		}
	}
</script>
</head>
<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms "  data-smooth-scrolling="1">
<div class="vd_body">
  <header class="header-1" id="header">
    <?php include("header.php");?>
  </header>
  <!-- Header Ends --> 
  
  <!---------sidebar---->
  <div class="content">
    <div class="container">
      <?php include("sidebar.php");?>
      
      <!-- Middle Content Start -->
      <div class="vd_content-wrapper">
        <div class="vd_container">
          <div class="vd_content clearfix">
            <div class="vd_head-section clearfix">
              <div class="vd_panel-inner-part">
                <div class="vd_panel-inner-part-header">
                  <ul class="breadcrumb">
                    <li><a href="welcome.php">Home</a> </li>
                    <li><a href="buyer-bucket.php">Buyer Bucket</a> </li>
                  </ul>
                </div>
                <?php if($_SESSION['sess_msg']){ ?>
                <div class="vd_panel-inner-part-content"> <span class="successful-message" style="color:red;"> <?php echo $_SESSION['sess_msg'];$_SESSION['sess_msg']='';?></span> </div>
                <?php }?>
                <div class="vd_panel-inner-part-content"> <span  style="float:right; padding-right:10px;">
                  <input type="button" name="add" value="Back"  class="btn btn-fright" onclick="location.href='assign-store.php'" />
                  </span>
                  <form name="frm" method="post" action="bucket-blank.php" enctype="multipart/form-data">
                    <div class="search_formbox listing_formbox">
                      <div class="clearfix"></div>
                      <div class="panel panel-default table-responsive">
                        <?php 
                
                $where='';
                
                $ses_id = session_id();
                
                $where.=" and ses_id='".$ses_id."' and store_id='".$_SESSION['store_id']."' and store_status='0'";		
                
                $start=0;
                
                if(isset($_GET['start'])) $start=$_GET['start'];
                
                $pagesize=500;
                
                if(isset($_GET['pagesize'])) $pagesize=$_GET['pagesize'];
                
                $order_by='id';
                
                if(isset($_GET['order_by'])) $order_by=$_GET['order_by'];
                
                $order_by2='desc';
                
                if(isset($_GET['order_by2'])) $order_by2=$_GET['order_by2'];
                
                $sql = $obj->query("select * from  inv_bucket_inventory where 1=1 $where order by $order_by $order_by2 limit $start, $pagesize",$debug=-1);
                
                $sql2 = $obj->query("select * from  inv_bucket_inventory where 1=1 $where order by $order_by $order_by2");
                
                $reccnt=$obj->numRows($sql2);
                
                if($reccnt==0){?>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td align="center" valign="middle"><font face="Arial, Helvetica, sans-serif"   color="#FF0000" size="+1">No Record</font></td>
                          </tr>
                        </table>
                        <?php } else { ?>
                        <table class="table table-bordered table-striped">
                          <thead>
                            <tr style="background:#6C9465; color:#fff;">
                              <th>No.</th>
                              <th>Category</th>
                              <th>Brand</th>
                              <th>Generic</th>
                              <th>Form Medicine</th>
                              <th>Strength</th>
                              <th>Usage</th>
                              <th>Quantity</th>
                              <th>Expire Date</th>
                              <th>Price</th>
                              <th>Total Price</th>
                              <th>Status</th>
                              <th></th>
                            </tr>
                          </thead>
                          <?php 
			 			$i=0;
						$total=0;
						while($line=$obj->fetchNextObject($sql)){$i++;
						if($i%2==0){
									$bgcolor = "success";
								}else{
									$bgcolor = "warning";
							}?>
                          <tbody>
                             <tr class="<?php echo $bgcolor;?>">
                              <td><?php echo $i+$start;?></td>
                              <td><?php echo getField('name','inv_category',$line->category_id);?></td>
                              <td><?php echo getField('name','inv_brand',$line->brand_id);?></td>
                              <td><?php echo getField('generic_name','inv_generic_name',$line->generic_id);?></td>
                              <td><?php echo getField('form_of_medicine_name','inv_form_of_medicine',$line->formof_id);?></td>
                              <td><?php echo getField('strength_of_medicine_name','inv_strength_of_medicine',$line->strength_id);?></td>
                              <td><?php echo getField('usage_category_name','inv_usage_category',$line->usage_id);?></td>
                              <td><?php echo $line->qty;?></td>
                              <td><?php echo date('m/d/Y', strtotime($line->expire_date));?></td>
                              <td><?php echo $line->price;?></td>
                              <td><?php echo $line->qty*$line->price;?></td>
                              <?php if($line->store_status=='1'){?>
                              <td>Assign</td>
                              <?php }else{ ?>
                              <td>Not Assign</td>
                              <?php } ?>
                              <td><a href="buyer-bucket-del.php?id=<?php echo $line->id;?>" style="text-decoration:none;">Delete</a>
                              <input type="hidden" name="ids[]" value="<?php echo $line->id;?>" />
                              </td>
                             
                            </tr>
                            <?php $total += $line->qty*$line->price;?>
                          </tbody>
                          <?php } ?>
                          <tr>
                            <td colspan="10"><span style="padding-left:570px;"><b>Grand Total : <?php echo $total; ?> </b></span></td>
                          </tr>
                        </table>
                      </div>
                      <ul class="pagination">
                        <?php include("include/paging.inc.php"); ?>
                      </ul>
                      <div class="custom-edit">
                        <input type="hidden" name="what" value="what" />
                        <input type="submit" name="Submit" value="Assign" class="btn btn-fright" onclick="return del_prompt(this.form,this.value)" />
                      </div>
                    </div>
                    <?php }?>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer Start -->
  <?php include("footer.php");?>
<!-- Footer END -->
<body>
</html>
