<?php 
	
	include("include/config.php");
	include("include/functions.php");
	
	if($_SESSION['sess_username']==''){
		
		header("Location:".SITE_URL);	
	}
	
	if($_REQUEST['submitFrom']=='Yes'){
		
			$store_id = mysql_real_escape_string($_REQUEST['store_id']);
			
			 header("Location:services-sold-for-store.php?id=$store_id");
				
			exit();
       }
?>
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>Inventory</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>
<script type="text/javascript" language="javascript">
function validate(obj)
{
if(obj.store.value==''){
alert("Please enter store");
obj.store.focus();
return false;
}
}
</script>
</head>

<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms "  data-smooth-scrolling="1">
<div class="vd_body">
  <header class="header-1" id="header">
    <?php include("header.php");?>
   
  </header>
  <!-- Header Ends --> 
  
  <!---------sidebar---->
  <div class="content">
    <div class="container">
      <?php include("sidebar.php");?>
      <!-- Middle Content Start -->
      <div class="vd_content-wrapper">
        <div class="vd_container">
          <div class="vd_content clearfix">
            <div class="vd_head-section clearfix">
              <div class="vd_panel-inner-part">
                <div class="vd_panel-inner-part-header">
                  <ul class="breadcrumb">
                    <li><a href="welcome.php">Home</a> </li>
					<li><a href="services-sold-for-store.php">Select Store</a> </li>
                  </ul>
                </div>
				
                <div class="vd_panel-inner-part-content"> 
                  <form name="frm" class="search_formbox more_search_formbox" action="" method="POST" onsubmit="return validate(this)">
                    <input type="hidden" name="submitFrom" value="Yes">
                    <input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>" />
                    <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12">
                      <div class="form-group form-group2">
                        <div class="controls">
                          <select name="store_id" id="store" required>
                            <option value="">Select Store</option>
                            <?php $storArr = $obj->query("select * from inv_stores where status=1");
				  				while($storResult = $obj->fetchNextobject($storArr)){?>
                            <option value="<?php echo $storResult->id;?>"> <?php echo stripslashes($storResult->name);?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="controls">
                        <input type="submit" class="btn greenbutton submit-btn-first" value="Next">
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer Start -->
  <?php include("footer.php");?>
<!-- Footer END --> 
<body>
</html>
