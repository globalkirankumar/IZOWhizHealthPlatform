<?php 
	include("include/config.php");
	include("include/functions.php");
	if($_SESSION['sess_username']==''){
		
		header("Location:".SITE_URL);	
	}
?>
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>Izohealth</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>
<script>
	function checkall(objForm)

    {

	len = objForm.elements.length;

	var i=0;

	for( i=0 ; i<len ; i++){

		if (objForm.elements[i].type=='checkbox') 

		objForm.elements[i].checked=objForm.check_all.checked;

	}

   }

	function del_prompt(frmobj,comb)

		{
			if(comb=='Delete'){


				if(confirm ("Are you sure you want to delete record(s)"))

				{

					frmobj.action = "brand-del.php";

					frmobj.what.value="Delete";

					frmobj.submit();

				}

				else{ 

				return false;

				}

			}

		else if(comb=='Deactivate'){
			frmobj.action = "brand-del.php";
			frmobj.what.value="Deactivate";

			frmobj.submit();

		}

		else if(comb=='Activate'){

			frmobj.action = "brand-del.php";

			frmobj.what.value="Activate";

			frmobj.submit();

		}
	}
</script>
<script type="text/javascript" language="javascript">
function validate(obj)
{
if(obj.brand.value==''){
alert("Please enter brand name");
obj.brand.focus();
return false;
}
}
</script>
</head>

<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms"  data-smooth-scrolling="1">
<div class="vd_body">
  <header class="header-1" id="header">
    <?php include("header.php");?>
  </header>
  <!-- Header Ends --> 
  
  <!---------sidebar---->
  <div class="content">
    <div class="container">
      <?php include("sidebar.php");?>
      
      <!-- Middle Content Start -->
      <div class="vd_content-wrapper">
        <div class="vd_container">
          <div class="vd_content clearfix">
            <div class="vd_head-section clearfix">
              <div class="vd_panel-inner-part">
                <div class="vd_panel-inner-part-header">
                  <ul class="breadcrumb">
                    <li><a href="welcome.php">Home</a> </li>
                    <li><a href="store-inventory-list.php">Inventory List</a> </li>
                  </ul>
                </div>
                <div class="vd_panel-inner-part-content">
                  <form name="searchForm" method="post" action="store-inventory-list.php" onSubmit="return validate(this)">
                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                      <div class="search_formbox">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <div class="form-group form-group2">
                            <div class="controls">
                              <input type="text" name="name" Placeholder="Generic Name" id="inventory" value="<?php echo $_REQUEST['name'];?>" required>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input type="submit" name="search" class="btn greenbutton fright" value="Search">
                        </div>
                        <!--<div class="col-md-4 col-sm-4 col-xs-12"> <a href="store-inventory-new-list.php">New Inventory</a> </div>-->
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </form>
                  <div class="clearfix"></div>
                </div>
                <?php if($_SESSION['sess_msg']){ ?>
                <div class="vd_panel-inner-part-content"> <span class="successful-message" style="color:red;"> <?php echo $_SESSION['sess_msg'];$_SESSION['sess_msg']='';?></span> </div>
                <?php }?>
                <div class="vd_panel-inner-part-content">
                  <div class="search_formbox listing_formbox">
                  <!--<input type="button" name="add" value="Add Brand" class="btn btn-fright" onclick="location.href='brand-addf.php'"> -->
                  <div class="clearfix"></div>
                  <form name="frm" method="post" action="brand-del.php" enctype="multipart/form-data">
                    <div class="panel panel-default table-responsive">
	<?php 

$where='';

if($_REQUEST['name']!=''){
$cat_query=$obj->Query("select id from inv_generic_name where generic_name like '%".addslashes($_REQUEST['name'])."%' ");
while($cat_query_res=$obj->fetchNextObject($cat_query))
{
	$generic_id_arr[] = $cat_query_res->id;
}

if(count($generic_id_arr)>0)
{
$where.=" and generic_id in (".implode(" , ",$generic_id_arr).") ";		
}
}


$start=0;


if(isset($_GET['start'])) $start=$_GET['start'];


$pagesize=5;


if(isset($_GET['pagesize'])) $pagesize=$_GET['pagesize'];


$order_by='id';


if(isset($_GET['order_by'])) $order_by=$_GET['order_by'];


$order_by2='desc';


if(isset($_GET['order_by2'])) $order_by2=$_GET['order_by2'];
if(isset($_SESSION['sess_username']))
{
$table_middle_name = $_SESSION['name'];
$table_name = 'inv_'.$table_middle_name.'_inventory';

}
$sql=$obj->Query("select * from $table_name where 1=1 and status='1' $where order by $order_by $order_by2 limit $start, $pagesize");
//echo "select * from $table_name where 1=1 $where order by $order_by $order_by2 limit $start, $pagesize";


$sql2=$obj->query("select * from $table_name where 1=1 $where order by $order_by $order_by2",$debug=-1);


$reccnt=$obj->numRows($sql2);

if($reccnt==0){?>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td align="center" valign="middle"><font face="Arial, Helvetica, sans-serif"   color="#FF0000" size="+1">No Record</font></td>
                        </tr>
                      </table>
                      <?php } else { ?>
                      <table class="table table-bordered table-striped">
                        <thead>
                          <tr class="item">
                            <th>No.</th>
                            <th>Category</th>
							<th>Brand</th>
                            <th>Generic Name</th>
                            <th>Form of Medicine</th>
                            <th>Strength of Medicine</th>
                             <th>Expiry Date</th>
                            <!--<th><input name="check_all" type="checkbox"  id="check_all" onclick="checkall(this.form)" value="check_all" /></th>-->
                          </tr>
                        </thead>
                     <?php 
						$i=1;while($line=$obj->fetchNextObject($sql)){?>
                        <tbody>
                          <tr>
                            <td><?php echo $i++;?></td>
                            <td><?php if($line->category_id=='0'){ echo "Open for all"; } else { echo stripslashes(getField('name','inv_category',$line->category_id)); } ?></td>
							<td><?php echo  stripslashes(getField('name','inv_brand',$line->brand_id)); ?></td>
                            <td><?php echo  stripslashes(getField('generic_name','inv_generic_name',$line->generic_id)); ?></td>
                             <td><?php echo  stripslashes(getField('form_of_medicine_name','inv_form_of_medicine',$line->formof_id)); ?></td>
                             <td><?php echo  stripslashes(getField('strength_of_medicine_name','inv_strength_of_medicine',$line->strength_id)); ?></td>
                             <td><?php echo  $line->expire_date; ?></td>
                            <!--<th><a class="editfile" href="brand-addf.php?id=<?php echo $line->id;?>"><i class="fa fa-pencil"></i></a></th>-->
                            <!--<th><input type="checkbox" name="ids[]" value="<?php echo $line->id;?>" /></th>-->
                          </tr>
                        </tbody>
                        <?php } ?>
                      </table>
                    </div>
                    <div class="custom-edit">
                      <input type="hidden" name="what" value="what" />
                      <!--<input type="submit" name="Submit" value="Activate" class="edit-btn" onclick="return del_prompt(this.form,this.value)" />
                      <input type="submit" name="Submit" value="Deactivate" class="edit-btn" onclick="return del_prompt(this.form,this.value)" />
                      <input type="submit" name="Submit" value="Delete" class="edit-btn" onclick="return del_prompt(this.form,this.value)" />-->
                    </div>
                    <ul class="pagination">
                      <?php include("include/paging.inc.php"); ?>
                    </ul>
                    </div>
                    <?php }?>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Footer Start -->
  <?php include("footer.php");?>
<!-- Footer END -->
<body>
</html>
