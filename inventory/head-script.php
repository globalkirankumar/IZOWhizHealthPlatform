<!-- CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
	  <link href="css/style.css" rel="stylesheet" type="text/css">
    <link href="css/theme.css" rel="stylesheet" type="text/css">
    <link href="css/chrome.css" rel="stylesheet" type="text/chrome">
    <link href="css/theme-responsive.min.css" rel="stylesheet" type="text/css">
    <link href="custom/custom.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="colorbox/colorbox.css"/>		<!-- Head SCRIPTS -->
    <script type="text/javascript" src="js/modernizr.js"></script>	