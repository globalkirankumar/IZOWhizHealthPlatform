<?php 
	include("include/config.php");
	include("include/functions.php");
	if($_SESSION['sess_username']==''){
		header("Location:".SITE_URL);	
	}
?>
<!DOCTYPE html>
<!--[if IE 8]>	<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>	<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>Izohealth</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>
<script type="text/javascript" language="javascript">
	function validate(obj)
	{
	if(obj.category.value==''){
	alert("Please enter Category");
	obj.category.focus();
	return false;
	}
	}
</script>
<script type="text/javascript">
	function addBucket(id,qty){
		var qty =$('#qty'+id).val();
		$.ajax({
		url:"assignstore-confirm.php",
		data:{id:id,qty:qty},
		success:function(data){
		$("#assign"+id).val("Assigned");	
	}	
  })
}
</script>
</head>

<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms"  data-smooth-scrolling="1">
<div class="vd_body">
<header class="header-1" id="header">
  <?php include("header.php");?>
</header>
<!-- Header Ends --> 

<!---------sidebar---->
<div class="content">
  <div class="container">
    <?php include("sidebar.php");?>
    
    <!-- Middle Content Start -->
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_head-section clearfix">
            <div class="vd_panel-inner-part">
              <div class="vd_panel-inner-part-header">
                <ul class="breadcrumb">
                  <li><a href="welcome.php">Home</a> </li>
                  <li><a href="assign-store.php">Assign to store</a> </li>
                </ul>
              </div>
              <div class="vd_panel-inner-part-content"> <span style="color:#F36">
                <?php if(isset($_COOKIE['suc_mes'])) { echo $_COOKIE['suc_mes']; }?>
                </span>
                <form name="searchForm" method="post" action="assign-store.php" onSubmit="return validate(this)">
                  <div class="col-lg-12 col-md-12  col-sm-12 col-xs-12">
                    <div class="search_formbox">
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group form-group2">
                          <div class="controls">
                            <select name="category_id" id="category_id">
                              <option value="">Select Medicine Category</option>
                              <?php $catArr = $obj->query("select * from inv_category where status=1");
						  		while($catResult = $obj->fetchNextObject($catArr)){?>
                              <option value="<?php echo $catResult->id?>" <?php if($_REQUEST['category_id']==$catResult->id){?>selected<?php }?>> <?php echo $catResult->name;?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group form-group2">
                          <div class="controls">
                            <select name="brand_id" id="brand_id">
                              <option value="">Select Brand</option>
                              <?php $brandArr = $obj->query("select * from inv_brand where status=1");
				 				while($brandResult = $obj->fetchNextObject($brandArr)){?>
                              <option value="<?php echo $brandResult->id?>" <?php if($_REQUEST['brand_id']==$brandResult->id){?>selected<?php }?>><?php echo $brandResult->name;?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group form-group2">
                          <div class="controls">
                            <select name="generic_id" id="generic_id">
                              <option value="">Select Generic Name</option>
                              <?php $genericArr = $obj->query("select * from inv_generic_name where status=1");
								 while($genericResult = $obj->fetchNextObject($genericArr)){?>
                              <option value="<?php echo $genericResult->id;?>" <?php if($_REQUEST['generic_id']==$genericResult->id){?>selected<?php }?>><?php echo $genericResult->generic_name;?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group form-group2">
                          <div class="controls">
                            <select name="form_id" id="form_id">
                              <option value="">Select Form of Medicine</option>
                              <?php $formQuery = $obj->query("select * from inv_form_of_medicine where status=1");
				 				while($formResult = $obj->fetchNextObject($formQuery)){?>
                              <option value="<?php echo $formResult->id;?>" <?php if($_REQUEST['form_id']==$formResult->id){?>selected<?php }?>><?php echo $formResult->form_of_medicine_name;?></option>
                              <?php } ?>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group form-group2">
                          <div class="controls">
                            <input type="text" name="expiary_date" id="datepicker" placeholder="Expiary Date" value="<?php echo $_REQUEST['expiary_date'];?>">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <input type="submit" name="search" class="btn greenbutton fright" value="Search">
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-12"> <a href="assign-store.php">View All</a> </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                </form>
                <div class="clearfix"></div>
              </div>
              <?php if($_SESSION['sess_msg']){ ?>
              <div class="vd_panel-inner-part-content"> <span class="successful-message"> <?php echo $_SESSION['sess_msg'];$_SESSION['sess_msg']='';?></span> </div>
              <?php }?>
              <div class="vd_panel-inner-part-content"><span  style="float:right; padding-right:10px;">
                <input type="button" name="add" value="View Bucket"  class="btn btn-fright" onclick="location.href='buyer-bucket.php'" />
                </span>
                <div class="search_formbox listing_formbox">
                  <div class="clearfix"></div>
                  <div class="panel panel-default table-responsive">
                    <?php 

$where='';

if($_REQUEST['category_id']!=''){

$where.=" and category_id like '%".$_REQUEST['category_id']."%'";		

}

if($_REQUEST['brand_id']!=''){

$where.=" and brand_id like '%".$_REQUEST['brand_id']."%'";		

}

if($_REQUEST['generic_id']!=''){

$where.=" and generic_id like '%".$_REQUEST['generic_id']."%'";		

}

if($_REQUEST['form_id']!=''){

$where.=" and formof_id like '%".$_REQUEST['form_id']."%'";		

}

if($_REQUEST['expiary_date']!=''){

$where.=" and expire_date like '%".$_REQUEST['expiary_date']."%'";		

}

//$where.=" and qty!=0";

$start=0;


if(isset($_GET['start'])) $start=$_GET['start'];


$pagesize=25;


if(isset($_GET['pagesize'])) $pagesize=$_GET['pagesize'];


$order_by='id';


if(isset($_GET['order_by'])) $order_by=$_GET['order_by'];


$order_by2='desc';


if(isset($_GET['order_by2'])) $order_by2=$_GET['order_by2'];


$sql = $obj->query("select * from inv_buyer_inventory where 1=1 $where order by $order_by $order_by2 limit $start, $pagesize");


$sql2 = $obj->query("select * from inv_buyer_inventory where 1=1 $where order by $order_by $order_by2");


$reccnt=$obj->numRows($sql2);

if($reccnt==0){?>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td align="center" valign="middle"><font face="Arial, Helvetica, sans-serif"  color="#FF0000" size="+1">No Record</font></td>
                      </tr>
                    </table>
                    <?php } else { ?>
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr style="background:#6C9465; color:#fff;">
                          <th>No.</th>
                          <th>Category</th>
                          <th>Brand</th>
                          <th>Generic</th>
                          <th>Form Medicine</th>
                          <th>Strength</th>
                          <th>Usage</th>
                          <th>Quantity</th>
                          <th>Expire Date</th>
                          <th>Price</th>
                          <th>Qty Assign</th>
                          <th></th>
                        </tr>
                      </thead>
                      <?php 
			 			    $i=0;
					    while($line=$obj->fetchNextObject($sql)){
							$i++;
							if($i%2==0){
									$bgcolor = "success";
								}else{
									$bgcolor = "warning";
							}?>
                      <tbody>
                        <tr class="<?php echo $bgcolor;?>">
                          <td><?php echo $i+$start;?></td>
                          <td><?php echo getField('name','inv_category',$line->category_id);?></td>
                          <td><?php echo getField('name','inv_brand',$line->brand_id);?></td>
                          <td><?php echo getField('generic_name','inv_generic_name',$line->generic_id);?></td>
                          <td><?php echo getField('form_of_medicine_name','inv_form_of_medicine',$line->formof_id);?></td>
                          <td><?php echo getField('strength_of_medicine_name','inv_strength_of_medicine',$line->strength_id);?></td>
                          <td><?php echo getField('usage_category_name','inv_usage_category',$line->usage_id);?></td>
                          <td><?php echo $line->qty;?></td>
                          <td><?php echo date('m/d/Y', strtotime($line->expire_date));?></td>
                          <td><?php echo $line->price;?></td>
                          <td><input type="text" name="qty" id="qty<?php echo $line->id;?>"></td>
                          <td><input type="button" value="Assign" class="btn btn-fright" id="assign<?php echo $line->id;?>" onclick="return addBucket('<?php echo $line->id;?>')"></td>
                            </form>
                        </tr>
                      </tbody>
                      <?php } ?>
                    </table>
                  </div>
                  <ul class="pagination">
                    <?php include("include/paging.inc.php"); ?>
                  </ul>
                </div>
                <?php }?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- Footer Start -->
<?php include("footer.php");?>
<!-- Footer END -->
<body>
</html>
