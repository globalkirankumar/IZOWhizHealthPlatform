<?php 

	include("include/config.php");
	include("include/functions.php");

	if($_SESSION['sess_username']==''){
		header("Location:".SITE_URL);	
	}
?>

<!DOCTYPE html>

<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->

<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->

<!--[if gt IE 9]><!-->

<html>

<!--<![endif]-->

<head>
<meta charset="utf-8" />
<title>Izohealth</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>
<script type="text/javascript" language="javascript">

function validate(obj)

{

if(obj.qty.value==''){

alert("Please Enter Quantity");

obj.qty.focus();

return false;

}

}

</script>
</head>

<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms" data-smooth-scrolling="1">
<div class="vd_body">
<header class="header-1" id="header">
  <?php include("header.php");?>
</header>

<!-- Header Ends --> 

<!---------sidebar---->

<div class="content">
<div class="container">
<?php include("sidebar.php");?>

<!-- Middle Content Start -->

<div class="vd_content-wrapper">
<div class="vd_container">
<div class="vd_content clearfix">
<div class="vd_head-section clearfix">
<div class="vd_panel-inner-part">
<div class="vd_panel-inner-part-header">
  <ul class="breadcrumb">
    <li><a href="welcome.php">Home</a> </li>
    <li><a href="low-stock.php">Low Stock Reports</a> </li>
  </ul>
</div>
<div class="vd_panel-inner-part-content">
  <form name="searchForm" method="post" action="low-stock.php" onSubmit="return validate(this)">
    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
      <div class="search_formbox">
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="form-group form-group2">
            <div class="controls">
              <input type="text" name="qty" Placeholder="Stock Less Then (X)" id="qty" value="<?php echo $_REQUEST['qty'];?>" required>
            </div>
          </div>
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12">
          <input type="submit" name="search" class="btn greenbutton fright" value="Search">
        </div>
        <div class="col-md-2 col-sm-2 col-xs-12"> <a href="low-stock.php">View All</a> </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </form>
  <div class="clearfix"></div>
</div>
<?php if($_SESSION['sess_msg']){ ?>
<div class="vd_panel-inner-part-content"> <span class="successful-message" style="color:red;"> <?php echo $_SESSION['sess_msg'];$_SESSION['sess_msg']='';?></span> </div>
<?php }?>
<div class="vd_panel-inner-part-content">
<div class="search_formbox listing_formbox">
<div class="clearfix"></div>
<form name="frm" method="post" action="" enctype="multipart/form-data">
<div class="panel panel-default table-responsive">
<?php 
	$where='';
	if($_REQUEST['qty']!=''){
	$where.=" and qty < '".mysql_real_escape_string($_REQUEST['qty'])."'";		
	}else { $where.=" and qty='0'";	}

	$start=0;

  if(isset($_GET['start'])) $start=$_GET['start'];
  $pagesize=20;
  if(isset($_GET['pagesize'])) $pagesize=$_GET['pagesize'];
  $order_by='id';
  if(isset($_GET['order_by'])) $order_by=$_GET['order_by'];
   $order_by2='desc';
  if(isset($_GET['order_by2'])) $order_by2=$_GET['order_by2'];
  //echo "select * from  inv_buyer_inventory where 1=1 $where order by $order_by $order_by2 limit $start, $pagesize";
  $sql = $obj->query("select * from  inv_buyer_inventory where 1=1 $where order by $order_by $order_by2 limit $start, $pagesize",$debug=-1);
  $sql2 = $obj->query("select * from  inv_buyer_inventory where 1=1 $where order by $order_by $order_by2");
  $reccnt=$obj->numRows($sql2);
if($reccnt==0){?>
  <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
      <td align="center" valign="middle"><font face="Arial, Helvetica, sans-serif"   color="#FF0000" size="+1">No Record</font></td>
    </tr>
  </table>
  <?php } else { ?>
  <table class="table table-bordered table-striped">
    <thead>
      <tr class="item">
        <th>No.</th>
        <th>Category</th>
        <th>Brand</th>
        <th>Generic</th>
        <th>Form Medicine</th>
        <th>Strength</th>
        <th>Usage</th>
        <th>Expire Date</th>
        <th>Quantity</th>
        <th>Price</th>
      </tr>
    </thead>
    <?php 
		$i=0;
		while($line=$obj->fetchNextObject($sql)){
		$i++;
		?>
    <tbody>
      <tr>
        <td><?php echo $i+$start;?></td>
        <td><?php echo getField('name','inv_category',$line->category_id);?></td>
        <td><?php echo getField('name','inv_brand',$line->brand_id);?></td>
        <td><?php echo getField('generic_name','inv_generic_name',$line->generic_id);?></td>
        <td><?php echo getField('form_of_medicine_name','inv_form_of_medicine',$line->formof_id);?></td>
        <td><?php echo getField('strength_of_medicine_name','inv_strength_of_medicine',$line->strength_id);?></td>
        <td><?php echo getField('usage_category_name','inv_usage_category',$line->usage_id);?></td>
        <td><?php echo date('m/d/Y', strtotime($line->expire_date));?></td>
        <td><?php echo $line->qty;?></td>
        <td><?php echo $line->price;?></td>
      </tr>
    </tbody>
    <?php } ?>
  </table>
</div>
<ul class="pagination">
  <?php include("include/paging.inc.php"); ?>
</ul>
<?php }?>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<!-- Footer Start -->

<?php include("footer.php");?>

<!-- Footer END -->

<body>
</html>
