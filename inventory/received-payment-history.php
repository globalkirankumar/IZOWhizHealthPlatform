<?php 

	include("include/config.php");

	include("include/functions.php");

	if($_SESSION['sess_username']==''){
		
		header("Location:".SITE_URL);	

}
?>

<!DOCTYPE html>

<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->

<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->

<!--[if gt IE 9]><!-->

<html>

<!--<![endif]-->

<head>
<meta charset="utf-8" />
<title>Inventory</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>
<script>

	function checkall(objForm)



    {



	len = objForm.elements.length;



	var i=0;



	for( i=0 ; i<len ; i++){



		if (objForm.elements[i].type=='checkbox') 



		objForm.elements[i].checked=objForm.check_all.checked;



	}



   }



	function del_prompt(frmobj,comb)



		{

			if(comb=='Delete'){





				if(confirm ("Are you sure you want to delete record(s)"))



				{



					frmobj.action = "genericname-del.php";



					frmobj.what.value="Delete";



					frmobj.submit();



				}



				else{ 



				return false;



				}



			}



		else if(comb=='Deactivate'){

			

			frmobj.action = "genericname-del.php";

			

			frmobj.what.value="Deactivate";



			frmobj.submit();



		}



		else if(comb=='Activate'){



			frmobj.action = "genericname-del.php";



			frmobj.what.value="Activate";



			frmobj.submit();



		}

	}

</script>
<script type="text/javascript" language="javascript">

function validate(obj)

{

if(obj.generic.value==''){

alert("Please enter generic name");

obj.generic.focus();

return false;

}

}

</script>
</head>

<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms"  data-smooth-scrolling="1">
<div class="vd_body">
<header class="header-1" id="header">
  <?php include("header.php");?>
</header>

<!-- Header Ends --> 

<!---------sidebar---->

<div class="content">
  <div class="container">
    <?php include("sidebar.php");?>
    
    <!-- Middle Content Start -->
    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_head-section clearfix">
            <div class="vd_panel-inner-part">
              <div class="vd_panel-inner-part-header">
                <ul class="breadcrumb">
                  <li><a href="welcome.php">Home</a> </li>
                  <li><a href="credit-debit-history.php">Received Payment History for Store "<?php echo ucwords($_SESSION['store_name']); ?>"</a> </li>
                </ul>
              </div>
              <div class="vd_panel-inner-part-content">
                <div class="clearfix"></div>
              </div>
              <?php if($_SESSION['sess_msg']){ ?>
              <div class="vd_panel-inner-part-content"> <span class="successful-message" style="color:red;"> <?php echo $_SESSION['sess_msg'];$_SESSION['sess_msg']='';?></span> </div>
              <?php }?>
              <div class="vd_panel-inner-part-content">
                <div class="search_formbox listing_formbox">
                
                <!-- <input type="button" name="add" value="Add Service" class="btn btn-fright" onclick="location.href='service-addf.php'">-->
                
                <div class="clearfix"></div>
                <form name="frm" method="post" action="genericname-del.php" enctype="multipart/form-data">
                  <div class="panel panel-default table-responsive">
                    <?php 

if($_GET['id'])
{
	 $sql=" update inv_payments set status='1',modified_on=now()";

	       $sql.=" where id='".$_REQUEST['id']."'";

	       $obj->query($sql);

	       $_SESSION['sess_msg']='Payment history updated successfully';   
}

$sql3=$obj->query("select * from inv_payments WHERE store_id='$_SESSION[store_id]' order by id desc");
		  
//print_r($line4);
?>
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr class="item">
                          <th>Date</th>
                          <th>Amount</th>
                          <th>Payment Type</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <?php while($line3=$obj->fetchNextObject($sql3)){?>
                      <tbody>
                        <tr>
                          <td><?php echo date('d-m-Y',strtotime($line3->date));?></td>
                          <td><?php echo $line3->amount;?></td>
                          <td><?php if($line3->payment_type ==1){?>
                            Payment Against Medicine
                            <?php } else if($line3->payment_type ==2){?>
                            Payment Against Subscription
                            <?php } else if($line3->payment_type ==3){?>
                            Payment Against Services
                            <?php }?></td>
                          <?php if($line3->status==1){?>
                          <td>Approved </td>
                          <?php }else{ ?>
                          <td>Unapproved</td>
                          <?php } ?>
                          <th><a class="editfile" href="received-payment-history.php?id=<?php echo $line3->id;?>"><i  <?php if($line3->status=='0'){?>class="fa fa-pencil" <?php } ?>></i></a></th>
                        </tr>
                      </tbody>
                      <?php } ?>
                    </table>
                  </div>
                  <div class="custom-edit">
                    <input type="hidden" name="what" value="what" />
                    
                    <!--   <input type="submit" name="Submit" value="Activate" class="edit-btn" onclick="return del_prompt(this.form,this.value)" />

                      <input type="submit" name="Submit" value="Deactivate" class="edit-btn" onclick="return del_prompt(this.form,this.value)" />

                      <input type="submit" name="Submit" value="Delete" class="edit-btn" onclick="return del_prompt(this.form,this.value)" />--> 
                    
                  </div>
                  <ul class="pagination">
                    <?php include("include/paging.inc.php"); ?>
                  </ul>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Footer Start -->

<?php include("footer.php");?>

<!-- Footer END -->

<body>
</html>
