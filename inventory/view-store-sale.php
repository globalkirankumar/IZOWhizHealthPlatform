<?php 

	include("include/config.php");

	include("include/functions.php");

	if($_SESSION['sess_username']==''){

		header("Location:".SITE_URL);	

	}
?>

<!DOCTYPE html>

<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<html>

<!--<![endif]-->

<head>
<meta charset="utf-8" />
<title>Izohealth</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>
<script type="text/javascript" language="javascript">

function validate(obj)

{

if(obj.from_date.value==''){

alert("Please Select Form Data");

obj.from_date.focus();

return false;

}

if(obj.from_to.value==''){

alert("Please Select To Data");

obj.from_to.focus();

return false;

}

}

</script>
</head>

<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms"  data-smooth-scrolling="1">
<div class="vd_body">
<header class="header-1" id="header">
  <?php include("header.php");?>
</header>

<!-- Header Ends --> 

<!---------sidebar---->

<div class="content">
  <div class="container">
    <?php include("sidebar.php");?>
    
    <!-- Middle Content Start -->
    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_head-section clearfix">
            <div class="vd_panel-inner-part">
              <div class="vd_panel-inner-part-header">
                <ul class="breadcrumb">
                  <li><a href="welcome.php">Home</a> </li>
                  <li><a href="view-store-sale.php">Store Report</a> </li>
                </ul>
              </div>
              <div class="vd_panel-inner-part-content">
                <form name="searchForm" method="post" action="" onSubmit="return validate(this)">
                  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="search_formbox">
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group form-group2">
                          <div class="controls">
                            <input type="text" name="from_date" id="datepicker" Placeholder="From Date" value="<?php echo $_REQUEST['from_date'];?>">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-4 col-sm-4 col-xs-12">
                        <div class="form-group form-group2">
                          <div class="controls">
                            <input type="text" name="from_to" id="datepicker1" Placeholder="From Date" value="<?php echo $_REQUEST['from_to'];?>">
                          </div>
                        </div>
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-12">
                        <input type="submit" name="search" class="btn greenbutton fright" value="Search">
                      </div>
                      <div class="col-md-2 col-sm-2 col-xs-12"> <a href="">View All</a> </div>
                      <div class="clearfix"></div>
                    </div>
                  </div>
                </form>
                <div class="clearfix"></div>
              </div>
              <div class="vd_panel-inner-part-content">
                <div class="search_formbox listing_formbox">
                <div class="clearfix"></div>
                <form name="frm" method="post" action="" enctype="multipart/form-data">
                  <div class="panel panel-default table-responsive">
                    <?php 

$where='';

if($_REQUEST['from_date'] && $_REQUEST['from_to'] ){
	
	$vald=explode("/",$_REQUEST['from_date']);	

	$valid_from=$vald[2]."-".$vald[0]."-".$vald[1];
		
	$vald1=explode("/",$_REQUEST['from_to']);	

	$valid_to=$vald1[2]."-".$vald1[0]."-".$vald1[1];	
	
	$where.=" and is_sale_done='1' and created_on BETWEEN '$valid_from' AND '$valid_to'";
	
	}
	
	$where.=" and store_id=$_SESSION[store_id] and is_sale_done='1'";
	
$start=0;

if(isset($_GET['start'])) $start=$_GET['start'];


$pagesize=10;

if(isset($_GET['pagesize'])) $pagesize=$_GET['pagesize'];


$order_by='id';

if(isset($_GET['order_by'])) $order_by=$_GET['order_by'];

$order_by2='desc';

if(isset($_GET['order_by2'])) $order_by2=$_GET['order_by2'];

$sql=$obj->Query("select * from inv_sales where 1=1 $where order by $order_by $order_by2 limit $start, $pagesize",$debug=-1);

$sql2=$obj->query("select * from inv_sales where 1=1 $where order by $order_by $order_by2",$debug=-1);

$reccnt=$obj->numRows($sql2);

if($reccnt==0){?>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td align="center" valign="middle"><font face="Arial, Helvetica, sans-serif" color="#FF0000" size="+1">No Record</font></td>
                      </tr>
                    </table>
                    <?php } else { ?>
                    <table class="table table-bordered table-striped">
                      <thead>
                        <tr class="item">
                          <th>No.</th>
                          <th>Patient Name</th>
                          <th>Date</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <?php 
						$i=0;
						while($line=$obj->fetchNextObject($sql)){
						  $i++;
						$pactName = $obj->query("select name,patient_id from crm_patients where pid='".$line->patient_id."'",$debug=-1);
						$pactResult = $obj->fetchNextObject($pactName);?>
                      <tbody>
                        <tr>
                          <td><?php echo $i+$start;?></td>
                          <td><?php echo stripslashes($pactResult->name);?> (<?php echo stripslashes($pactResult->patient_id);?>)</td>
                          <td><?php echo date('d-m-Y',strtotime($line->created_on));?></td>
                          <td><a href="view-detail.php?pid=<?php echo $line->patient_id;?>&ses_id=<?php echo $line->ses_id;?>" style="text-decoration:none;" class="iframeenq">View</a></td>
                        </tr>
                      </tbody>
                      <?php } ?>
                    </table>
                  </div>
                  <ul class="pagination">
                    <?php include("include/paging.inc.php"); ?>
                  </ul>
                  </div>
                  <?php }?>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Footer Start -->

<?php include("footer.php");?>
<script src="colorbox/jquery.colorbox.js"></script> 
<script>
	$(document).ready(function(){
	$(".iframeenq").colorbox({iframe:true, width:"1000px;", height:"500px;", frameborder:"0",scrolling:true}); 
	});
</script> 

<!-- Footer END --> 
<script>

  $(function() {

    $( "#datepicker1" ).datepicker();

  });

  </script>
<body>
</html>
