<?php 
	
	include("include/config.php");
	include("include/functions.php");
	
	if($_SESSION['sess_username']==''){
		
		header("Location:".SITE_URL);	
	}
		
	
	if($_REQUEST['submitFrom']=='Yes'){
		
		    $amount_taken = $_REQUEST['amout_taken'];
		    $status = $_REQUEST['status'];
		
			$obj->query("insert into  inv_sale_service SET amount_taken='$amount_taken',patcient_id='$_SESSION[patient_id]',service_id='$_REQUEST[id]',store_id='$_SESSION[store_id]',status='".$status."',created_by='$_SESSION[user_type]',created_on=now()");
		
			$_SESSION['sess_msg']="Service Sold successfully";
			
		  header("Location:service-taken.php");

		exit();
	 
}
	
if($_REQUEST['id']!=''){

$sql=$obj->query("select * from inv_services where id=".$_REQUEST['id']);

$result=$obj->fetchNextObject($sql);

}
?>
<!DOCTYPE html>
<!--[if IE 8]><html class="ie ie8"> <![endif]-->
<!--[if IE 9]><html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>Izohealth</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>
<script type="text/javascript" language="javascript">
function validate(obj)
{
if(obj.generic.value==''){
alert("Please enter generic name");
obj.generic.focus();
return false;
}
if(obj.status.value==''){
alert("Please Select Status");
obj.status.focus();
return false;
}

}
</script>
</head>

<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms "  data-smooth-scrolling="1">
<div class="vd_body">
  <header class="header-1" id="header">
    <?php include("header.php");?>
   
  </header>
  <!-- Header Ends --> 
  
  <!---------sidebar---->
  <div class="content">
    <div class="container">
      <?php include("sidebar.php");?>
      <!-- Middle Content Start -->
      <div class="vd_content-wrapper">
        <div class="vd_container">
          <div class="vd_content clearfix">
            <div class="vd_head-section clearfix">
              <div class="vd_panel-inner-part">
                <div class="vd_panel-inner-part-header">
                  <ul class="breadcrumb">
                    <li><a href="welcome.php">Home</a> </li>
					<li><a href="">Service Taken</a> </li>
                  </ul>
                </div>
                
				<?php if($_SESSION['sess_msg']){ ?>
                <div class="vd_panel-inner-part-content"> <span class="successful-message" style="color:red;"> 
				<?php echo $_SESSION['sess_msg'];$_SESSION['sess_msg']='';?></span> </div>
                <?php }?>
				
                <div class="vd_panel-inner-part-content"> <span  style="float:right; padding-right:10px;">
                 
                  </span>
                  <form name="frm" class="search_formbox more_search_formbox" action="" method="POST" onsubmit="return validate(this)">
                    <input type="hidden" name="submitFrom" value="Yes">
                    <input type="hidden" name="id" value="<?php echo $_REQUEST['id'];?>" />
					
                    <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3 col-xs-12">
					 
					 <div class="form-group form-group2">
                        <div class="controls">
                          <input type="text" name="name" Placeholder="Service" id="name" value="<?php echo stripslashes($result->name);?>" readonly>
                        </div>
                      </div>
					  
					  <div class="form-group form-group2">
						<div class="controls">
						<select name="validity_duration" disabled="true">
							<option value="">-------------------------Validity----------------------------------</option>
							<?php for($i=1; $i<=10; $i++)
							{?>
							<option value="<?php echo $i; ?>" <?php if($i==$result->validity_duration){ ?>selected<?php } ?>><?php echo $i; ?></option>
							<?php } ?>
							
                        </select>
						</div>
						</div>
						
						
						<div class="form-group form-group2">
						<div class="controls">
						<select name="validity_type" disabled="true">
							<option value="">--------------------------Duration--------------------------------</option>
							
							<option value="month" <?php if($result->validity_type == 'month'){ ?>selected<?php } ?>>Month</option>
							<option value="year" <?php if($result->validity_type == 'year'){ ?>selected<?php } ?>>Year</option>
							
							
                        </select>
						</div>
						</div>
						
						 <div class="form-group form-group2">
                        <div class="controls">
                          <!-- <input type="text" name="name" Placeholder="Subscription Name" id="name" value="<?php echo stripslashes($result->name);?>" required>-->
						  <textarea name="description" Placeholder="Description" readonly><?php echo $result->description;?></textarea>
                        </div>
                      </div>
						
						
						 <div class="form-group form-group2">
                        <div class="controls">
                          <input type="number" required min="1" name="subscription_price" Placeholder="Price" id="subscription_price" value="<?php echo stripslashes($result->subscription_price);?>" readonly>
                        </div>
						</div>
						
						 <div class="form-group form-group2">
                        <div class="controls">
                          <input type="number" required min="1" name="amout_taken" Placeholder="Amount Taken" required>
                        </div>
						</div>
						
					  
                      <div class="col-lg-6 col-md-offset-3 col-md-6 col-sm-6 col-xs-12">
					  
                      <div class="form-group form-group2">
                        <div class="controls">
                          <select name="status" id="status" required>
                            <option value="">Status</option>                           
                            <option value="1" <?php if($result->status=='1'){?>selected<?php } ?>>Active</option>
                            <option value="0" <?php if($result->status=='0'){?>selected<?php } ?>>Deactive</option>
                          </select>
                        </div>
                      </div>
                    </div>
                      <div class="controls">
                        <input type="submit" class="btn greenbutton submit-btn-first" value="Submit">
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Footer Start -->
  <?php include("footer.php");?>
<!-- Footer END --> 

<script>
	$("#a").change(function()
		{
		var val=$(this).val();
		if(val=='1')
		{		
		$('#c').show(); 
		}
		});
		
	$("#b").change(function()
		{
		var val=$(this).val();
		if(val=='0')
		{
		$('#c').hide();
		}
	});
</script>
<body>
</html>
