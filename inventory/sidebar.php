				<div class="vd_navbar vd_nav-width vd_navbar-tabs-menu vd_navbar-left  ">
				<div class="navbar-menu clearfix">
				<h3 class="menu-title hide-nav-medium hide-nav-small">INVENTORY</h3>
				<div class="vd_menu">
				<ul>
				
				<li>
				<a href="welcome.php" data-action="">
				<span class="menu-icon"><i class="fa fa-home"></i></span>
				<span class="menu-text">HOME</span>
				</a>
				</li>
                
				<?php if($_SESSION['user_type']=='admin'){?>
				<li>
				<a href="notification_list.php" data-action="">
				<span class="menu-icon"><i class="fa fa-bell"></i></span>
				<span class="menu-text">Notification</span>
				</a>
				</li>
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Masters</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu" data-action="click-target">
				<ul>
				
					<li>
					<a href="category-list.php">
					<span class="menu-text">Medicine Category</span>
					</a>
					</li>
					
					<li>
					<a href="brand-list.php">
					<span class="menu-text">Medicine Brand</span>
					</a>
					</li>
					
					<li>
					<a href="genericname-list.php">
					<span class="menu-text">Medicine Generic Name</span>
					</a>
					</li>
					
					<li>
					<a href="formofmadic-list.php">
					<span class="menu-text">Form Of Medicine</span>
					</a>
					</li>
					
					<li>
					<a href="strengthofmdic-list.php">
					<span class="menu-text">Strength of Medicine</span>
					</a>
					</li>
					
					<li>
					<a href="usage-list.php">
					<span class="menu-text">Usage Category</span>
					</a>
					</li>
					
					
				</ul>
				</div>
				</li>
                
				
               				
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Subscription Plan</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu" data-action="click-target">
				<ul>
				<li>
				<a href="subscription-addf.php">
				<span class="menu-text">Add New Subscription </span>
				</a>
				</li>
				<li>
				<a href="subscription-list.php">
				<span class="menu-text">Subscription Plan View</span>
				</a>
				</li>
				</ul>
				</div>
				</li>
				
				
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Services</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu" data-action="click-target">
				<ul>
				<li>
				<a href="service-addf.php">
				<span class="menu-text">Add New Service</span>
				</a>
				</li>
				<li>
				<a href="service-list.php">
				<span class="menu-text">Services List View</span>
				</a>
				</li>
				</ul>
				</div>
				</li>
				
				
				
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Store and Staff</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				
				<li>
				<a href="store-list.php">
				<span class="menu-text">Stores</span>
				</a>
				</li>
				
				<li>
				<a href="staff-list.php">
				<span class="menu-text">Staffs</span>
				</a>
				</li>
				
				</ul>
				</div>
				</li>

				
				
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">My Inventory</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				<li>
				<a href="inventory-addf.php">
				<span class="menu-text">Add Inventory</span>
				</a>
				</li>
				<li>
				<a href="inventory-list.php">
				<span class="menu-text">Inventory List</span>
				</a>
				</li>
				</ul>
				</div>
				</li>
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Assign Inventory</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				<li>
				<a href="select-store.php">
				<span class="menu-text">Assign to store</span>
				</a>
				</li>
				
				</ul>
				</div>
				</li>
                
                <li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Reports</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				<li>
				<a href="current_inv_stores.php">
				<span class="menu-text">Current Inventory of Stores</span>
				</a>
				</li>
				
				<li>
				<a href="credit-debit.php">
				<span class="menu-text">Credit & Debit Notes</span>
				</a>
				</li>
				
				<li>
				<a href="received-payment.php">
				<span class="menu-text">Received Payment History</span>
				</a>
				</li>
				
				<li>
				<a href="assigned-inventory-history.php">
				<span class="menu-text">Assigned Inventory History</span>
				</a>
				</li>
				
                <li>
				<a href="store-name.php">
				<span class="menu-text">Store Medicine Sales History</span>
				</a>
				</li>
                
				<li>
				<a href="subscription-plan-sold.php">
				<span class="menu-text">Subscription Plan Sold</span>
				</a>
				</li>
				
				<li>
				<a href="services-sold.php">
				<span class="menu-text">Services Sold</span>
				</a>
				</li>
                
                <li>
				<a href="current_inv_stores_subscription.php">
				<span class="menu-text">Subscription Plan Sold for Store</span>
				</a>
				</li>
                
                <li>
				<a href="current_inv_stores_service.php">
				<span class="menu-text">Services Plan Sold for Store</span>
				</a>
				</li>
				
				</ul>
				</div>
				</li>
                
                <li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Low Stocks</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				<li>
				<a href="low-stock.php">
				<span class="menu-text">My Low Stock Reports</span>
				</a>
				</li>
				
				<li>
				<a href="stores-low-stock.php">
				<span class="menu-text">Store Low Stock Reports</span>
				</a>
				</li>
				
				
				</ul>
				</div>
				</li>
                
				<?php }else{ ?>
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Inventory Received</span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				<li>
				<a href="store-inventory-new-list.php">
				<span class="menu-text">History of received Inventory</span>
				</a>
				</li>
				</ul>
				</div>
				</li>
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text"> Sale</span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				<li>
				<a href="sale-addf.php">
				<span class="menu-text">Medicine Sale</span>
				</a>
				</li>
                <li>
				<a href="subscription-sale.php">
				<span class="menu-text">Subscription Sale</span>
				</a>
				</li>
                <li>
				<a href="service-sale.php">
				<span class="menu-text">Services Sale</span>
				</a>
				</li>
				</ul>
				</div>
				</li>
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Current Inventory</span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				<li>
				<a href="current-sale-inventroy-store.php">
				<span class="menu-text">Current Inventory </span>
				</a>
				</li>
				</ul>
				</div>
				</li>
				
                <li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Subscription Plan</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu" data-action="click-target">
				<ul>
				
				<li>
				<a href="subscription-list.php">
				<span class="menu-text">Subscription Plan View</span>
				</a>
				</li>
				</ul>
				</div>
				</li>
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Payments</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu" data-action="click-target">
				<ul>
				
				<li>
				<a href="make-payment.php">
				<span class="menu-text">Make a payment</span>
				</a>
				</li>
				<li>
				<a href="payment-list.php">
				<span class="menu-text">View payment list</span>
				</a>
				</li>
				</ul>
				</div>
				</li>
				
				
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Services</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu" data-action="click-target">
				<ul>
				
				<li>
				<a href="service-list.php">
				<span class="menu-text">Services List View</span>
				</a>
				</li>
				</ul>
				</div>
				</li>
				
				<li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Sale Report</span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				<li>
				<a href="view-store-sale.php">
				<span class="menu-text">Medicine Sale Report </span>
				</a>
				</li>
				<li>
				<a href="subscription-report.php">
				<span class="menu-text">Subscription Sale Report </span>
				</a>
				</li>
				<li>
				<a href="service-report.php">
				<span class="menu-text">Service Sale Report </span>
				</a>
				</li>
				</ul>
				</div>
				</li>
                
                <li>
				<a href="javascript:void(0);" data-action="click-trigger">
				<span class="menu-icon"><i class="fa fa-dashboard"></i></span>
				<span class="menu-text">Low Stock Reports</span>
				<span class="menu-badge"><span class="badge vd_bg-black-30"><i class="fa fa-angle-down"></i></span></span>
				</a>
				<div class="child-menu"  data-action="click-target">
				<ul>
				<li>
				<a href="store-low-stock.php">
				<span class="menu-text">Low Stock Reports</span>
				</a>
				</li>
				
				
				
				</ul>
				</div>
				</li>

				<?php } ?>

				</ul>
				</div>
				</div>
				<div class="navbar-spacing clearfix">
				</div>
				</div>