<?php 
	include("include/config.php");
	include("include/functions.php");
	if($_SESSION['sess_username']==''){
		
		header("Location:".SITE_URL);	
	}
?>
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->
<html>
<!--<![endif]-->
<head>
<meta charset="utf-8" />
<title>Izohealth</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include("head-script.php");?>
<script type="text/javascript" language="javascript">
function validate(obj)
{
if(obj.name.value==''){
alert("Please Enter Patient Name");
obj.name.focus();
return false;
}
}
</script>
</head>

<body id="forms" class="full-layout  nav-right-hide nav-right-start-hide nav-top-fixed responsive clearfix" data-active="forms"  data-smooth-scrolling="1">
<div class="vd_body">
  <header class="header-1" id="header">
    <?php include("header.php");?>
  </header>
  <!-- Header Ends --> 
  
  <!---------sidebar---->
  <div class="content">
    <div class="container">
      <?php include("sidebar.php");?>
      
      <!-- Middle Content Start -->
      <div class="vd_content-wrapper">
        <div class="vd_container">
          <div class="vd_content clearfix">
            <div class="vd_head-section clearfix">
              <div class="vd_panel-inner-part">
                <div class="vd_panel-inner-part-header">
                  <ul class="breadcrumb">
                    <li><a href="welcome.php">Home</a> </li>
                    <li><a href="subscription-report.php">Subscription Report</a> </li>
                  </ul>
                </div>
                <div class="vd_panel-inner-part-content">
                  <form name="searchForm" method="post" action="subscription-report.php" onSubmit="return validate(this)">
                    <div class="col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-12 col-xs-12">
                      <div class="search_formbox">
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <div class="form-group form-group2">
                            <div class="controls">
                              <input type="text" name="name" Placeholder="Patient Name" id="name" value="<?php echo $_REQUEST['name'];?>" required>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12">
                          <input type="submit" name="search" class="btn greenbutton fright" value="Search">
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-12"> <a href="subscription-report.php">View All</a> </div>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                  </form>
                  <div class="clearfix"></div>
                </div>
                <?php if($_SESSION['sess_msg']){ ?>
                <div class="vd_panel-inner-part-content"> <span class="successful-message" style="color:red;"> <?php echo $_SESSION['sess_msg'];$_SESSION['sess_msg']='';?></span> </div>
                <?php }?>
                <div class="vd_panel-inner-part-content">
                  <div class="search_formbox listing_formbox">
                  
                  <form name="frm" method="post" action="" enctype="multipart/form-data">
                    <div class="panel panel-default table-responsive">
<?php 

$where='';

if($_REQUEST['name']!=''){

$where.=" and patcient_id='".getPid($_REQUEST['name'])."' and store_id='$_SESSION[store_id]'";	

}
	
$where.=" and store_id='$_SESSION[store_id]'";

$start=0;


if(isset($_GET['start'])) $start=$_GET['start'];


$pagesize=10;


if(isset($_GET['pagesize'])) $pagesize=$_GET['pagesize'];


$order_by='id';


if(isset($_GET['order_by'])) $order_by=$_GET['order_by'];


$order_by2='desc';


if(isset($_GET['order_by2'])) $order_by2=$_GET['order_by2'];


$sql=$obj->query("select * from inv_sale_subscription where 1=1 $where order by $order_by $order_by2 limit $start, $pagesize",$debug=-1);


$sql2=$obj->query("select * from inv_sale_subscription where 1=1 $where order by $order_by $order_by2",$debug=-1);


$reccnt=$obj->numRows($sql2);

if($reccnt==0){?>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td align="center" valign="middle"><font face="Arial, Helvetica, sans-serif"   color="#FF0000" size="+1">No Record</font></td>
                        </tr>
                      </table>
                      <?php } else { ?>
                      <table class="table table-bordered table-striped">
                        <thead>
                          <tr class="item">
                            <th>No.</th>
                            <th>Patient Name</th>
                            <th>Subscription Plan</th>
                            <th>Validity</th>
                            <th>Amount Taken</th>
							<th>Plan Date</th>
                            <th>Plan Expire Date</th>
                          </tr>
                        </thead>
                   <?php 
						$i=0;
						while($line=$obj->fetchNextObject($sql)){
							$i++;
							$pactName = $obj->query("select name,patient_id from crm_patients where pid='".$line->patcient_id."'",$debug=-1);
							$pactResult = $obj->fetchNextObject($pactName);?>
                        <tbody>
                          <tr>
                            <td><?php echo $i+$start;?></td>
                            <td><?php echo stripslashes($pactResult->name);?> (<?php echo stripslashes($pactResult->patient_id);?>)</td>
                            <td><?php 
							$where = 'id';
							echo getFieldWhere('name','inv_subscription_plan',$where,$line->subscription_id);?></td>
                            <?php $validityArr = $obj->query("select * from inv_subscription_plan where id='".$line->subscription_id."'",$debug=-1);
							$validResult = $obj->fetchNextObject($validityArr);?>
                            <td><?php echo $validResult->validity_duration;?>&nbsp;&nbsp;<?php echo $validResult->validity_type;?></td>
							<td><?php echo $line->amount_taken;?></td>
                            <td><?php echo $line->created_on;?></td>
                            <td>
							 <?php 
							    if($validResult->validity_type=='month')
								{
								    	$day =$validResult->validity_duration*30;
								}else  if($validResult->validity_type=='year'){
									    $day =$validResult->validity_duration*365;
								}
								echo date('Y-m-d',strtotime("+".$day." day", strtotime($line->created_on)));?>
                               </td>
                          </tr>
                        </tbody>
                        <?php } ?>
                      </table>
                    </div>
                    <div class="custom-edit">
                      <input type="hidden" name="what" value="what" />
                   <!--<input type="submit" name="Submit" value="Activate" class="edit-btn" onclick="return del_prompt(this.form,this.value)" />
                      <input type="submit" name="Submit" value="Deactivate" class="edit-btn" onclick="return del_prompt(this.form,this.value)" />
                      <input type="submit" name="Submit" value="Delete" class="edit-btn" onclick="return del_prompt(this.form,this.value)" />-->
                    </div>
                    <ul class="pagination">
                      <?php include("include/paging.inc.php"); ?>
                    </ul>
                    </div>
                    <?php }?>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  <!-- Footer Start -->
  <?php include("footer.php");?>
<!-- Footer END -->
<body>
</html>
