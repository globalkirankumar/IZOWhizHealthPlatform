<?php
include('lib/open.inc.php');
include("login.inc.php");
$LOGU = new LoginUser();
if($_SESSION["AMD"][0])
	{
		$BSC->redir(SITE_PATH."home.php");
		exit;		
	}
if($BSC->is_post_back())
{
  
  $flag = $LOGU->login($_POST);
   if($flag==1)
	{
		unset($_SESSION['security_code']);
		$BSC->redir(SITE_PATH_ADM."index.php");
		exit;
	}	
	
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title>Log In</title>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/login.css">
	<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="validation/css/validationEngine.jquery.css" rel="stylesheet" type="text/css">
</head>
<body class="gp-background">
<style>
.facebooklink{
	float:right;
	color:#3663A2 !important;
	padding:10px 0px;
}
.fa.fa-facebook {
    padding: 8px 11px;
    height: 30px !important;
    background: #3663A2 none repeat scroll 0% 0%;
    color: #FFF !important;
    border-radius: 23px;
    font-size: 14px;
}
.logo{
	width:100%;
	height:auto;
	padding-top:30px;
}
.logo img {
    display: block;
    margin: 0 auto;
	max-width:100%;
}
</style>

<div ui-view="">	
	
<div class="login-page ng-scope">
	<div class="img-container">
		<div class="text-center pull-right photo"> <img src="img/logo-login.png" class="user-avatar img-circle img-responsive">
			<h1><small></small><br> &nbsp;</h1> </div>
	</div>
	<div class="form-content">
		<?=$ADMIN->alert()?>
			                 <?php //if(@$_SESSION['sessmsg']!=''){echo $_SESSION['sessmsg'];}?>
		<form role="form" class="bottom-75 ng-pristine ng-valid" ng-submit="submit()" method="post" name="login" id="login">
			<div class="table-form">
				<div class="form-groups">
					<div class="form-group">  <input type="text" class="validate[required] form-control customlogin input-lg" placeholder="Email" name="email" data-errormessage-value-missing="Email is required!"> </div>
					
					<div class="form-group"> <input type="password" class="validate[required] form-control customlogin input-lg" placeholder="Password" name="password"  data-errormessage-value-missing="Password is required!"> </div>
				</div>
				<div class="button-container"> <button type="submit" class="btn btn-default login" name="login" value="Log In"><img src="img/arrow.png"></button> </div>
			</div>
			<div class="remember"> <label class="checkbox1" for="Option"> <input id="Option" type="checkbox"> <span></span> </label> remember me <span class="pass"> forgot password?</span> </div>
		</form>
	</div>
</div>
	
</div>	
	
	
	
	
	
	
	
	
	
	
	
	
<script src="<?=SITE_PATH?>validation/js/jquery-1.8.2.min.js"></script>
<script src="<?=SITE_PATH?>validation/js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=SITE_PATH?>validation/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script>
        $.noConflict();
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			//jQuery("#login").validationEngine({promptPosition: 'inline'});
			jQuery("#login").validationEngine();
		});
</script>

</body>
</html>