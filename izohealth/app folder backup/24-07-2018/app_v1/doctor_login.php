<?php
// error_reporting ( - 1 );
include("../lib/open.inc.php");
include 'class/all_class_files.php';
include_once 'notification/GCM.php';
$gcm = new GCM ();
$m = new MyMail ();
$json = array();
$flag = mysql_real_escape_string($_POST ['flag']);
// initiate doctor object
$doctor_function = new doctor ();
date_default_timezone_set('Asia/Kolkata');

switch ($flag) {

    case 'login' :
        $gsm_tocken = mysql_real_escape_string($_POST ['gsm_tocken']);
        $email = mysql_real_escape_string($_POST ['email']);
        $country_code = mysql_real_escape_string($_POST ['country_code']);
        $password = mysql_real_escape_string($_POST ['password']);
        $password = md5($password);
        $password = base64_encode($password);
        $login_type = 0; // 0 email, 1 phone
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // valid address
            $login_type = 0;
        } else {
            if (is_numeric($email)) {
                $login_type = 1;
            }
        }
        $is_ios_device = 0;
        if (array_key_exists('is_ios_device', $_POST)) {
            $is_ios_device = 1;
        }
        if ($login_type == 1) {
            $email_from_db = '';
            $phone_check_query = $PDO->db_query("select * from #_doctors  where phone='" . $email . "' and country_code='" . $country_code . "' ");
            if (mysql_num_rows($phone_check_query) > 0) {

                $email_from_db = $PDO->getSingleResult("select email from #_doctors where phone='" . $email . "' and country_code='" . $country_code . "' ");
            }
            if ($email_from_db != '') {
                $email = $email_from_db;
                $query = $PDO->db_query("select * from #_users_login  where  email ='" . $email_from_db . "' and password ='" . $password . "'");
            } else {
                $json ['status'] = 'false';
                $json ['msg'] = 'Phone number and password not matched';
                $doctor_function->send_res($json);
                exit ();
            }
        } else {
            $query = $PDO->db_query("select * from #_users_login  where  email ='" . $email . "' and password ='" . $password . "'");
        }

        $query_status = $PDO->db_query("select * from #_doctors  where email ='" . $email . "'");
        $row = $PDO->db_fetch_array($query_status);
        $doc_status = $row ['status'];
        $doc_log_status = $row ['log_status'];
        if ($doc_status == 0) {
            $json ['status'] = 'false';
            $json ['msg'] = 'Your account is inactive.Please contact admin.';
            $doctor_function->send_res($json);
            exit ();
        }

        /*  if ($doc_log_status == 1) {
            $json ['status'] = 'false';
            $json ['msg'] = 'Your account is logged with some other device.Please logout from that device and login here.';
            $doctor_function->send_res ( $json );
            exit ();
        }*/

        // $query = $PDO->db_query("select ul.* from #_users_login as ul left join #_doctors as d on d.user_id=ul.pid where ul.email ='" . $email . "' and ul.password ='" . $password . "' AND d.is_verified=1");
        $json ['doctor_detail'] = array();
        $json ['user_type'] = 'doctor';
        if (mysql_num_rows($query) > 0) {

            $query_verify = $PDO->db_query("select * from #_doctors  where email ='" . $email . "' and is_verified =1");
            if (mysql_num_rows($query_verify) > 0) {

                $row = $PDO->db_fetch_array($query);
                $table == '';
                if ($row ['user_type'] == "clinics") {
                    $table = "clinics";
                } else if ($row ['user_type'] == "labs") {
                    $table = "labs";
                } else if ($row ['user_type'] == "doctors") {
                    $table = "doctors";
                } else if ($row ['user_type'] == "hospital") {
                    $table = "hospitals";
                } else if ($row ['user_type'] == "healthcare_organization") {
                    $table = "healthcare_organization";
                }

                if ($table != '') {

                    $doctor_query = $PDO->db_query("select * from #_" . $table . " where user_id='" . $row ['pid'] . "' and status =1 ");
                    if (mysql_num_rows($doctor_query) > 0) {

                        $json ['user_type'] = $row ['user_type'];

                        $doctor_data = $PDO->db_fetch_array($doctor_query);
                        $gsm_user = $PDO->db_query("select * from #_gcmuser  where  user_id ='" . $doctor_data ['pid'] . "' and gsm_tocken ='" . $gsm_tocken . "' and user_type ='doctor'");
                        $doctor_query = $PDO->db_query("update #_doctors SET live_status ='1' where pid='" . $doctor_data ['pid'] . "'");

                        if (mysql_num_rows($gsm_user) == 0) {
                            $gsm_user = $PDO->db_query("insert into #_gcmuser  set  user_id ='" . $doctor_data ['pid'] . "', gsm_tocken ='" . $gsm_tocken . "' ,user_type ='doctor',flag = '1',created_on='" . date('Y-m-d H:i:s') . "'");
                        } else {

                            $gsm_user = $PDO->db_query("Update #_gcmuser  set  flag ='1', modified_on='" . date('Y-m-d H:i:s') . "' where user_id ='" . $doctor_data ['pid'] . "' and gsm_tocken ='" . $gsm_tocken . "' and user_type ='doctor' ");
                        }

                        if ($json ['user_type'] == 'doctors') {

                            $data ['doctor_id'] = $doctor_data ['pid'];
                            $data ['hospital_id'] = $doctor_data ['hospital_id'];
                            $data ['hospital'] = $PDO->getSingleResult("select name from #_hospitals where pid='" . $doctor_data ['hospital_id'] . "'");
                            $data ['clinic_id'] = $doctor_data ['clinic_id'];
                            $data ['clinic'] = $PDO->getSingleResult("select name from #_clinics where pid='" . $doctor_data ['clinic_id'] . "'");
                            $data ['user_id'] = $doctor_data ['user_id'];
                            $data ['speciality'] = $doctor_data ['speciality'];

                            if (($doctor_data ['speciality']) != 0) {
                                $data ['speciality_name'] = $PDO->getSingleResult("select name from #_specialities where pid='" . $doctor_data ['speciality'] . "'");
                            } else {
                                $data ['speciality_name'] = 'General Practitioner';
                            }

                            $data ['name'] = $doctor_data ['name'];
                            $data ['email'] = $doctor_data ['email'];
                            $data ['phone'] = $doctor_data ['phone'];
                            $data ['sex'] = $doctor_data ['sex'];
                            $data ['date_of_birth'] = $doctor_data ['date_of_birth'];
                            $data ['doctor_type'] = $doctor_data ['doctor_type'];
                            $data ['rs_number'] = $doctor_data ['rs_number'];
                            $data ['fees'] = $doctor_data ['fees'];
                            $data ['education'] = $doctor_data ['education'];
                            $data ['location'] = $doctor_data ['location'];
                            $data ['area_code'] = $doctor_data ['area_code'];
                            $data ['dates'] = json_decode($doctor_data ['dates']);
                            $data ['address'] = $doctor_data ['address'];
                            $data ['township_id'] = $doctor_data ['township'];
                            $data ['division_id'] = $doctor_data ['division'];
                            $data ['township'] = $PDO->getSingleResult("select name from #_township where pid='" . $doctor_data ['township'] . "'");
                            $data ['division'] = $PDO->getSingleResult("select name from #_division where pid='" . $doctor_data ['division'] . "'");
                            $data ['is_verified'] = $doctor_data ['is_verified'];
                            $data ['isHomeservice'] = $doctor_data ['isHomeservice'];
                            $data ['is_voip'] = $doctor_data ['is_voip'];
                            $data ['isChatNeeded'] = $doctor_data ['isChatNeeded'];
                            $data ['busy_free'] = $doctor_data ['busy_free'];
                            $data ['years_of_experience'] = $doctor_data ['years_of_experience'];
                            $data ['status'] = $doctor_data ['status'];

                            if ($doctor_data ['id_card'] == NULL) {
                                $data ['id_card'] = '';
                            } else {
                                $data ['id_card'] = SITE_PATH_ADM . "uploaded_files/doctors/idcard/" . $doctor_data ['id_card'];
                            }
                            if ($doctor_data ['profile_image'] == NULL) {
                                $data ['profile_image'] = '';
                            } else {
                                $data ['profile_image'] = SITE_PATH_ADM . "uploaded_files/doctors/profile/" . $doctor_data ['profile_image'];
                            }
                            if ($doctor_data ['registration_image'] == NULL) {
                                $data ['registration_image'] = '';
                            } else {
                                $data ['registration_image'] = SITE_PATH_ADM . "uploaded_files/doctors/registration/" . $doctor_data ['registration_image'];
                            }

                            $json ['doctor_detail'] = $data;
                            if ($doctor_data ['status'] == 0) {
                                $json ['status'] = 'false';
                            } else {
                                // $json['status'] = 2;
                                $json ['status'] = 'true';
                            }
                            $modified_on = date('Y-m-d H:i:s');
                            // update gsm_tocken
                            $gsm_tocken_details = array(
                                'gsm_tocken' => $gsm_tocken,
                                'user_id' => $doctor_data ['user_id'],
                                'flag' => 1,
                                'modified_on' => $modified_on
                            );

                            $log_details = array(
                                'is_ios_device' => $is_ios_device,
                                'log_status' => 1,
                                'modified_on' => $modified_on
                            );

                            $updated_id = $PDO->sqlquery("rs", 'gcmuser', $gsm_tocken_details, $update = 'user_id', $id = $doctor_data ['user_id']);
                            $updated_log_status = $PDO->sqlquery("rs", 'doctors', $log_details, $update = 'pid', $id = $data ['doctor_id']);

                        } else {

                            $json ['doctor_detail'] = $doctor_data;
                            $json ['doctor_detail'] ['left_services'] = json_decode($doctor_data ['left_services']);
                            $json ['doctor_detail'] ['imagin_services'] = json_decode($doctor_data ['imagin_services']);
                            // $json['status'] = 2;
                            $json ['status'] = 'true';
                        }
                    } else {
                        $json ['status'] = 'false';
                    }
                } else {
                    $json ['status'] = 'false';
                }
            } else {
                $json ['status'] = 'false';
                $json ['msg'] = 'Account is not activated.Please wait until admin to activate your account.';
            }
            // end of main if
        } else {
            $json ['status'] = 'false';
            $json ['msg'] = 'Login details not matched';
        }

        break;

    case 'logout' :
        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $gsm_tocken = mysql_real_escape_string($_POST ['gsm_tocken']);
        $doctor_query = $PDO->db_query("update #_doctors SET live_status ='0' where pid='" . $doctor_id . "'");

        $user_id = $PDO->getSingleResult("select user_id from #_doctors where pid='" . $doctor_id . "'");

        $PDO->db_query("update #_gcmuser  set flag ='0',gsm_tocken ='' where user_id ='" . $user_id . "' and gsm_tocken ='" . $gsm_tocken . "' and user_type ='doctor'");
        $modified_on = date('Y-m-d H:i:s');

        $log_details = array(
            'is_ios_device' => 0,
            'log_status' => 0,
            'modified_on' => $modified_on
        );
        $updated_log_status = $PDO->sqlquery("rs", 'doctors', $log_details, $update = 'pid', $id = $doctor_id);
        $json ['status'] = 'true';
        $json ['msg'] = 'You logout successfully';
        break;


    default :
        break;
}

/* Output header */
// header('Content-type: application/json');
// echo json_encode($_POST);
echo json_encode($json);
?>