<?php
include("../lib/open.inc.php");
include 'class/all_class_files.php';
$flag = mysql_real_escape_string($_POST ['flag']);
$json = array();
include_once 'notification/GCM.php';
$gcm = new GCM ();
date_default_timezone_set("Asia/Kolkata");

// initiate doctor object
$doctor_function = new doctor ();

switch ($flag) {

    case 'booking_app' :
        sleep(2);
        $appointmentdata = array();
        $appointmentBookMapping = array();
        $title = '';
        $sender_id = 0;
        $sender_type = '';
        $book_type_name = '';

        $calldata ['book_type'] = mysql_real_escape_string($_POST ['book_type']);
        $calldata ['service_type'] = mysql_real_escape_string($_POST ['service_type']);
        $calldata ['book_id'] = mysql_real_escape_string($_POST ['book_id']);
        $calldata ['gsm_tocken'] = mysql_real_escape_string($_POST ['gsm_tocken']);
        $lab_test = mysql_real_escape_string($_POST ['lab_test']);
        $lab_test = explode(",", $lab_test);
        $calldata ['lab_test'] = json_encode($lab_test);
        $calldata ['book_date'] = date('Y-m-d', strtotime(mysql_real_escape_string($_POST ['book_date'])));
        $calldata ['book_time'] = mysql_real_escape_string($_POST ['book_time']);
        $appointmentdata ['app_date'] = date('Y-m-d', strtotime(mysql_real_escape_string($_POST ['book_date'])));

        $appointmentdata ['app_time'] = mysql_real_escape_string($_POST ['book_time']);
        $appointmentdata ['comments'] = mysql_real_escape_string($_POST ['booking_comments']);
        $calldata ['booking_comments'] = mysql_real_escape_string($_POST ['booking_comments']);
        $calldata ['home_visit_service'] = mysql_real_escape_string($_POST ['home_visit_service']);

        if ($calldata ['book_type'] == 'Lab') {
            $title = 'Appointment with Lab';
            $sender_id = $calldata ['book_id'];
            $sender_type = 'L';
            $appointmentdata ['appointment_with'] = 'L';
            $calldata ['book_id'] = $PDO->getSingleResult("select pid from #_labs where pid='" . $calldata ['book_id'] . "'");
            $book_type_name = $PDO->getSingleResult("select name from #_labs where pid='" . $calldata ['book_id'] . "'");
        } else if ($calldata ['book_type'] == 'Hospital') {
            $calldata ['book_id'] = $PDO->getSingleResult("select pid from #_hospitals where pid='" . $calldata ['book_id'] . "'");
            $book_type_name = $PDO->getSingleResult("select name from #_hospitals where pid='" . $calldata ['book_id'] . "'");
        } else if ($calldata ['book_type'] == 'Clinics') {
            $calldata ['book_id'] = $PDO->getSingleResult("select pid from #_clinics where pid='" . $calldata ['book_id'] . "'");
            $book_type_name = $PDO->getSingleResult("select name from #_clinics where pid='" . $calldata ['book_id'] . "'");
        } else if ($calldata ['book_type'] == 'Healthcare') {
            $appointmentdata ['appointment_with'] = 'H';
            $title = 'Appointment with health care';
            $sender_id = $calldata ['book_id'];
            $sender_type = 'O';
            $calldata ['book_id'] = $PDO->getSingleResult("select pid from #_healthcare_organization where pid='" . $calldata ['book_id'] . "'");
            $book_type_name = $PDO->getSingleResult("select name from #_healthcare_organization where pid='" . $calldata ['book_id'] . "'");
        }

        $calldata ['patient_id'] = mysql_real_escape_string($_POST ['patient_id']);
        $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        // $patient_id =$PDO->getSingleResult("select pid from #_patients where patient_id='".$calldata['patient_id']."'");

        $calldata ['patient_id'] = $patient_id;

        $call_details ['patient_id'] = $patient_id;
        $call_details ['service_type'] = $_POST ['service_type'];
        $call_details ['status'] = 1;
        $call_details ['call_by'] = 'app';
        $call_details ['created_on'] = @date('Y-m-d H:i:s');
        $call_details ['shortorder'] = $PDO->getSingleresult("select max(shortorder) as shortorder from #_call_details where 1=1 ") + 1;
        $calldata ['call_id'] = $PDO->sqlquery("rs", 'call_details', $call_details);

        $appointmentdata ['call_details_id'] = $calldata ['call_id'];
        $query = "select * from #_booking where book_id ='" . $calldata ['book_id'] . "' and call_id='" . $calldata ['call_id'] . "' and  book_type='" . $calldata ['book_type'] . "' and book_date ='" . $calldata ['book_date'] . "' and book_time ='" . $calldata ['book_time'] . "' ";

        $booking_query = $PDO->db_query($query);
        if (mysql_num_rows($booking_query) == 0) {
            $calldata ['status'] = 1;
            $calldata ['created_on'] = @date('Y-m-d H:i:s');
            $calldata ['create_by'] = $_SESSION ["AMD"] [0];
            $calldata ['shortorder'] = $PDO->getSingleresult("select max(shortorder) as shortorder from #_booking where 1=1 ") + 1;
            $call_id = $PDO->sqlquery("rs", 'booking', $calldata);

            $appointmentdata ['patient_id'] = $patient_id;
            $appointmentdata ['doctor_id'] = $calldata ['book_id'];
            $appointmentdata ['created_on'] = @date('Y-m-d H:i:s');
            $appointmentdata ['modified_on'] = @date('Y-m-d H:i:s');
            $appointmentdata ['status'] = 1;
            $appointment_id = $PDO->sqlquery("rs", 'appointment', $appointmentdata);

            $appointmentBookMapping ['doctor_id'] = $calldata ['book_id'];
            $appointmentBookMapping ['patient_id'] = $patient_id;
            $appointmentBookMapping ['call_details_id'] = $calldata ['call_id'];
            $appointmentBookMapping ['booking_id'] = $call_id;
            $appointmentBookMapping ['appoinment_id'] = $appointment_id;
            $appointmentBookMapping ['created_on'] = @date('Y-m-d H:i:s');
            $appointmentBookMapping ['modified_on'] = @date('Y-m-d H:i:s');

            $appointment_mapping_id = $PDO->sqlquery("rs", 'book_app_mapping', $appointmentBookMapping);

            $patient_gsm = $PDO->getSingleResult("select gsm_tocken from #_patients where pid=" . $patient_id);
            if ($patient_gsm) {

                $message = array(
                    "message" => "Appointment with " . $book_type_name . "(" . $calldata ['book_type'] . ") fixed with reference id(" . $appointment_id . ")",
                    "flag" => 'booking_app'
                );
                $is_ios_device = $PDO->getSingleResult("select is_ios_device from #_patients where pid=" . $patient_id);
                $result = $gcm->send_notification($patient_gsm, $message, $title, $sender_id, $sender_type, $patient_id, 'P', $is_ios_device);

            }

            $json ['status'] = 'true';
        } else {
            $json ['status'] = 'false';
        }

        break;


    default :
        break;
}

/* Output header */
// header('Content-type: application/json');
// echo json_encode($_POST);
echo json_encode($json);
?>