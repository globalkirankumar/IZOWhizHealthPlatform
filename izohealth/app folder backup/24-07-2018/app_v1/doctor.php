<?php
// error_reporting ( - 1 );
include("../lib/open.inc.php");
include 'class/all_class_files.php';
include_once 'notification/GCM.php';
$gcm = new GCM ();
$m = new MyMail ();
$json = array();
$flag = mysql_real_escape_string($_POST ['flag']);
// initiate doctor object
$doctor_function = new doctor ();
date_default_timezone_set('Asia/Kolkata');

switch ($flag) {

    case 'livestatus' :
        $status = mysql_real_escape_string($_POST ['status']);
        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $doctor_query = $PDO->db_query("update #_doctors SET live_status ='" . $status . "' where pid='" . $doctor_id . "'");
        $json ['status'] = 'true';
        break;
    case 'getservicetypelist' :
        $service_type = NULL;
        $service_list = array();

        // $json['msg']='Faied to recieve chat message';

        $service_type = mysql_real_escape_string($_POST ['service_type']);
        $query = $PDO->db_query("select S.pid,S.name,S.name_my from #_" . services_cat . " as S Where S.service_type=" . $service_type . " order by S.name");
        while ($row = $PDO->db_fetch_array($query)) {
            $service_type_data = array();
            $service_type_data = array(
                'pid' => $row ['pid'],
                'name' => $row ['name'],
                'name_my' => $row ['name_my']
            );

            $query1 = $PDO->db_query("select * from #_" . service_subcat . " where service_cat ='" . $row ['pid'] . "' ");
            $sub_service_type_data = array();
            while ($row1 = $PDO->db_fetch_array($query1)) {
                $sub_service_type_data ['sub_service_data'] [] = array(
                    'pid' => $row1 ['pid'],
                    'name' => $row1 ['name']
                );
            }
            $service_list [] = array_merge($service_type_data, $sub_service_type_data);
        }

        $json ['service_list'] = $service_list;
        break;
    case 'getCoreConfig':
        $appointment_version = DOCTOR_APP_VER;
        $json ['status'] = 'true';
        $json ['app_version'] = $appointment_version;

        break;
    case 'livestatus' :
        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $live_status = $PDO->getSingleResult("select live_status from #_doctors where user_id='" . $doctor_id . "'");
        $live_status = ($live_status == 1) ? 'true' : 'false';
        $json ['status'] = $live_status;
        break;
    // infi Doctor sign up form doctor type field
    // Doctor sign up form doctor type field
    case 'type' :

        // initialize arrays
        $json ['type'] = array();
        // Get doctor types from db
        $doctortypes_values = $doctor_function->doctor_type();
        if ($doctortypes_values != 'false') {
            $json ['type'] = $doctortypes_values;
            $json ['status'] = 'true';
        } else {
            $json ['status'] = 'false';
        }

        break;
    /*
	 * case 'insert_locations_from_doctor_table_to_location_table':
	 * $insert_data = $doctor_function->insert_location();
	 * if ($insert_data != 'false') {
	 * $json['status'] = $insert_data;
	 * } else {
	 * $json['status'] = 'false';
	 * }
	 * break;
	 */
    case 'available_unavailable_status_spinner' :

        $data = array();
        $data [0] ['pid'] = 1;
        $data [0] ['name'] = "Active";
        // $data[1]['pid'] = 0;
        // $data[1]['name'] = "In Active";;
        $json ['statusar'] = $data;
        break;
    case 'set_busy_free' :

        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $set_busy_free = mysql_real_escape_string($_POST ['set_busy_free']); // 0 means busy 1 means free

        $set_busy_free_status = $doctor_function->set_busy_free($doctor_id, $set_busy_free);

        if ($set_busy_free_status == 'false') {

            $json ['status'] = 'false';
            $json ['msg'] = 'You cannot able to change the status';
        } else {
            $json ['status'] = 'true';
            $json ['msg'] = 'Your status is changed successfully';
        }

        break;
    case 'set_voip' :

        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $set_voip = mysql_real_escape_string($_POST ['set_avail']); // 0 means busy 1 means free

        $set_voip_status = $doctor_function->set_voip($doctor_id, $set_voip);

        if ($set_voip_status == 'false') {

            $json ['status'] = 'false';
            $json ['msg'] = 'You cannot able to change the status';
        } else {
            $json ['status'] = 'true';
            $json ['msg'] = 'Your status is changed successfully';
        }

        break;
    case 'set_isHomeservice' :

        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $isHomeservice = mysql_real_escape_string($_POST ['set_avail']); // 0 means busy 1 means free

        $set_isHomeservice_status = $doctor_function->set_isHomeservice($doctor_id, $isHomeservice);

        if ($set_isHomeservice_status == 'false') {

            $json ['status'] = 'false';
            $json ['msg'] = 'You cannot able to change the status';
        } else {
            $json ['status'] = 'true';
            $json ['msg'] = 'Your status is changed successfully';
        }

        break;
    case 'set_available_date' :

        $available_unavailable_details = $_POST ['available_unavailable_details'];
        $obj = json_decode($available_unavailable_details);
        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        // print_r($available_unavailable_details);
        $start_date = $obj->{'StartDate'};
        $end_date = $obj->{'EndDate'};
        $locations = $obj->{'Locations'};
        $status = $obj->{'status'};

        $update_available_date = $doctor_function->set_available_date($start_date, $end_date, $locations, $status, $doctor_id);

        if ($update_available_date ['status'] == 'false') {

            $json ['status'] = 'false';
            $json ['msg'] = 'You cannot able to change the status';
            $json ['missing_location_ids'] = $update_available_date ['missing_location_ids'];
        } else {

            $json ['doctors_location_list'] = array();
            $lang_flag = 'en';
            // get the doctor location details from doctor_locations table
            $doctors_location_list_values = $doctor_function->get_doctors_location_list($doctor_id, $lang_flag);
            if ($doctors_location_list_values != 'false') {
                $json ['doctors_location_list'] = $doctors_location_list_values;
            } else {
                $json ['doctors_location_list'] = array();
            }
            $json ['msg'] = 'Your available date is updated successfully.';
            $json ['status'] = 'true';
        }
        break;
    case 'check_available_unavailable' :

        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $location_id = mysql_real_escape_string($_POST ['location_id']);

        $available_unavailable_details = $doctor_function->check_available_unavailable($doctor_id, $location_id);

        if ($available_unavailable_details ['status'] != 'false') {

            $json ['status'] = 'true';
            $json ['msg'] = $available_unavailable_details ['msg'];
        } else {
            $json ['status'] = 'false';
            $json ['msg'] = $available_unavailable_details ['msg'];
        }
        break;
    case 'set_chat_available_unavailable' :

        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $isChatNeeded = mysql_real_escape_string($_POST ['set_avail']);
        $set_chat_available_unavailable = $doctor_function->set_chat_available_unavailable($doctor_id, $isChatNeeded);

        if ($set_chat_available_unavailable == 'false') {
            $json ['status'] = 'false';
            $json ['msg'] = 'You cannot able to change the status';
        } else {
            $json ['status'] = 'true';
            $json ['msg'] = 'Your status is changed successfully';
        }

        break;
    case 'check_chat_available' :

        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        // $today_date = date("Y-m-d");
        // $current_time = date("H:i:s");
        $chat_available_unavailable_details = $doctor_function->check_chat_available($doctor_id);
        if ($chat_available_unavailable_details ['status'] != 'false') {
            $json ['status'] = 'true';
            $json ['msg'] = $chat_available_unavailable_details ['msg'];
        } else {
            $json ['status'] = 'false';
            $json ['msg'] = $chat_available_unavailable_details ['msg'];
        }
        break;
    case 'doctor_profile_location_details' :

        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $lang_flag = mysql_real_escape_string($_POST ['lang_flag']);

        $json ['doctor_details'] = array();
        // $json['doctors_location_list'] = array();
        // get doctor details
        $doctor_details = $doctor_function->doctor_short_details($doctor_id);
        $json ['doctor_details'] = $doctor_details;

        // get the doctor location details from doctor_locations table

        $doctors_location_list_values = $doctor_function->get_doctors_location_verified_list($doctor_id, $lang_flag);
        if ($doctors_location_list_values != 'false') {
            $json ['doctors_location_list'] = $doctors_location_list_values;
        } else {
            $json ['doctors_location_list'] = array();
        }
        if (empty ($doctor_details)) {
            $json ['status'] = 'false';
        } else {
            $json ['status'] = 'true';
        }

        break;
    case 'doctor_profile' :

        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $lang_flag = mysql_real_escape_string($_POST ['lang_flag']);

        $json ['doctor_details'] = array();
        // $json['doctors_location_list'] = array();
        // get doctor details
        $doctor_details = $doctor_function->doctor_details($doctor_id);
        $json ['doctor_details'] = $doctor_details;

        // get the doctor location details from doctor_locations table
        /*
		 * $doctors_location_list_values = $doctor_function->get_doctors_location_list($doctor_id, $lang_flag);
		 * if ($doctors_location_list_values != 'false') {
		 * $json['doctors_location_list'] = $doctors_location_list_values;
		 * } else {
		 * $json['doctors_location_list'] = array();
		 * }
		 */
        if (empty ($doctor_details)) {
            $json ['status'] = 'false';
        } else {
            $json ['status'] = 'true';
        }

        break;
    case 'doctor_profile_edit' :

        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $lang_flag = mysql_real_escape_string($_POST ['lang_flag']);
        $name = mysql_real_escape_string($_POST ['name']);
        $email = mysql_real_escape_string($_POST ['email']);
        $gender = mysql_real_escape_string($_POST ['gender']);
        $education = mysql_real_escape_string($_POST ['education']);
        $rs_number = mysql_real_escape_string($_POST ['rs_number']);
        $speciality = mysql_real_escape_string($_POST ['speciality']);
        $years_of_experience = mysql_real_escape_string($_POST ['years_of_experience']);

        // check user edit email
        $email_changed = 0;
        $email_update = 0;
        $email_from_db = $PDO->getSingleResult("select email from #_doctors where pid='" . $doctor_id . "'");
        $user_id = $PDO->getSingleResult("select user_id from #_doctors where pid='" . $doctor_id . "'");

        if ($email != $email_from_db) {
            $email_changed = 1;
        }

        if ($email_changed == 1) {
            // user edited the email.
            // Check the new email is already checked.
            $count_of_email = $PDO->getSingleResult("select COUNT(email) from #_doctors where email='" . $email . "' and pid!='" . $doctor_id . "' ");

            if ($count_of_email == 0) {
                // updated the email id

                $sql1 = "UPDATE #_users_login SET 
            email='" . $email . "' 
           WHERE  pid='" . $user_id . "'  ";
                $query1 = $PDO->db_query($sql1);

                $sql = "UPDATE #_doctors SET 
            name='" . $name . "' ,  email='" . $email . "' , sex='" . $gender . "' , education='" . $education . "', rs_number='" . $rs_number . "'
            , years_of_experience='" . $years_of_experience . "', speciality='" . $speciality . "', rs_number='" . $rs_number . "'
           WHERE  pid='" . $doctor_id . "'  ";
                $query = $PDO->db_query($sql);

                $json ['doctor_details'] = array();
                $doctor_details = $doctor_function->doctor_details($doctor_id);
                $json ['doctor_details'] = $doctor_details;

                $json ['msg'] = 'Your details are updated successfully';
                $json ['status'] = 'true';
            } else {
                $json ['msg'] = 'Sorry.This email is already exists.Others informations updated successfully';
                $json ['status'] = 'true';
            }
        } else {
            $sql = "UPDATE #_doctors SET 
            name='" . $name . "' ,  email='" . $email . "' , sex='" . $gender . "' , education='" . $education . "', rs_number='" . $rs_number . "'
            , years_of_experience='" . $years_of_experience . "', speciality='" . $speciality . "', rs_number='" . $rs_number . "'
           WHERE  pid='" . $doctor_id . "'  ";
            $query = $PDO->db_query($sql);

            $json ['doctor_details'] = array();
            $doctor_details = $doctor_function->doctor_details($doctor_id);
            $json ['doctor_details'] = $doctor_details;

            $json ['msg'] = 'details are updated successfully';
            $json ['status'] = 'true';
        }

        break;
    case 'upload_doctor_details_image' :
        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $uploaded_profile_name = '';
        $uploaded_id_card_name = '';
        $uploaded_registration_name = '';
		
        $uploaded_profile_details = $doctor_function->upload_images('profile_picture', UP_FILES_FS_PATH . '/doctors/profile/');
        $uploaded_id_card_details = $doctor_function->upload_images('idcard_file', UP_FILES_FS_PATH . '/doctors/idcard/');
        $uploaded_registration_details = $doctor_function->upload_images('registration_documents', UP_FILES_FS_PATH . '/doctors/registration/');
        if ($uploaded_profile_details ['status'] != 'true') {
            $json ['status'] = 'false';
            $json ['msg'] = 'Profile picture ' . $uploaded_profile_details ['msg'];
        } else if ($uploaded_id_card_details ['status'] != 'true') {
            $json ['status'] = 'false';
            $json ['msg'] = 'Id Card picture ' . $uploaded_id_card_details ['msg'];
        } else if ($uploaded_registration_details ['status'] != 'true') {
            $json ['status'] = 'false';
            $json ['msg'] = 'Registeration Documents ' . $uploaded_registration_details ['msg'];
        } else {

            $uploaded_profile_name = $uploaded_profile_details ['doctor_image_name'];
            $uploaded_id_card_name = $uploaded_id_card_details ['doctor_image_name'];
            $uploaded_registration_name = $uploaded_registration_details ['doctor_image_name'];

            if ((!empty ($uploaded_profile_name)) && (!empty ($uploaded_id_card_name)) && (!empty ($uploaded_registration_name))) {
                // update the pic names in to doctor table
                $sql = "UPDATE #_doctors SET 
            	profile_image='" . $uploaded_profile_name . "' ,  id_card='" . $uploaded_id_card_name . "' , registration_image='" . $uploaded_registration_name . "' 
           WHERE  pid='" . $doctor_id . "'  ";
                $query = $PDO->db_query($sql);

                $json ['msg'] = 'Your details are updated successfully';
                $json ['status'] = 'true';
            } else {
                $json ['status'] = 'false';
                $json ['msg'] = 'Sorry. There was an occured while uploading the files';
            }
        }

        break;
    case 'doctor_dashboard' :

        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        if ($doctor_id == '') {
            $json ['msg'] = 'Doctor id is empty ';
            $json ['status'] = 'false';
            $doctor_function->send_res($json);
            exit ();
        }
        $json ['doctor_details'] = array();
        $doctor_details = $doctor_function->doctor_details($doctor_id);
        if ($doctor_details ['doctor_id'] == NULL) {
            $json ['msg'] = 'Doctor is not matched ';
            $json ['status'] = 'false';
            $doctor_function->send_res($json);
            exit ();
        }
        if ($doctor_details ['speciality'] == 0) {
            $speciality_name = 'General Practitioner';

        } else {
            $speciality_name = $PDO->getSingleResult("select name from #_specialities where pid='" . $doctor_details ['speciality'] . "'");
        }
        $notification_count = $PDO->getSingleResult("select count(pid) from #_notification_details where receiver_id='" . $doctor_id . "' and receiver_type='D' and un_read=1");

        $json ['doctor_details'] = array(
            'doctor_id' => $doctor_details ['doctor_id'],
            'unread_appointment_count' => $doctor_details ['unread_appointment_count'],
            'unread_chat_count' => $doctor_details ['unread_chat_count'],
            'unread_wallet_count' => $doctor_details ['unread_wallet_count'],
            'completed_feedback_count' => $doctor_details ['completed_feedback_count'],
            'is_verified' => $doctor_details ['is_verified'],
            'isHomeservice' => $doctor_details ['isHomeservice'],
            'is_voip' => $doctor_details ['is_voip'],
            'busy_free' => $doctor_details ['busy_free'],
            'isChatNeeded' => $doctor_details ['isChatNeeded'],
            'name' => $doctor_details ['name'],
            'education' => $doctor_details ['education'],
            'speciality_name' => $speciality_name,
            'years_of_experience' => $doctor_details ['years_of_experience'],
            'notification_count' => $notification_count,
            'profile_image' => $doctor_details ['profile_image']
        );
        $json ['status'] = 'true';

        break;
    case 'user_email_check' :

        $email = mysql_real_escape_string($_POST ['email']);

        $login_type = 0; // 0 email, 1 phone
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // valid address
            $login_type = 0;
        } else {
            if (is_numeric($email)) {
                $login_type = 1;
            }
        }
        if ($login_type == 1) {
            $phone = $email;
            $email_from_db = $PDO->getSingleResult("select email from #_doctors where phone='" . $email . "'");

            if ($email_from_db != '') {
                $email = $email_from_db;
            } else {
                $json ['status'] = 'false';
                $json ['msg'] = 'New signup';
                $doctor_function->send_res($json);
                exit ();
            }
        } else {
            $phone = $PDO->getSingleResult("select phone from #_doctors where email='" . $email . "'");
        }

        $mobile_number = $phone;

        if ($mobile_number == '') {
            $json ['status'] = 'false';
            $doctor_function->send_res($json);
            exit ();
        } else {
            $json ['signup_step'] = 1;
        }
        $doctor_id = $PDO->getSingleResult("select pid from #_doctors where phone='" . $mobile_number . "'");
        $profile_pic = $PDO->getSingleResult("select profile_image from #_doctors where phone='" . $mobile_number . "'");
        if (($doctor_id != '') || ($doctor_id != null)) {
            $check_location_count = $doctor_function->check_location_count($doctor_id);

            // check the unverified user can add only one location
            $verified_status_from_db = $PDO->getSingleResult("select is_verified from #_doctors where pid='" . $doctor_id . "'");
            if (($check_location_count == 1)) {
                $json ['signup_step'] = 2;
            }
            if (($profile_pic != '') || ($profile_pic != null)) {
                $json ['signup_step'] = 3;
                $json ['status'] = 'false';
                $doctor_function->send_res($json);
                exit ();
            }
        }

        $json ['mobile_number'] = $mobile_number;
        $doctor_query = $PDO->db_query("select * from #_doctors  where  phone='" . $mobile_number . "' ");
        $json ['otp'] = str_pad(( string )rand(0, 999999), 6, '0', STR_PAD_LEFT);
        $signup_otp_details = array(
            'phone' => $mobile_number,
            'otp' => $json ['otp'],
            'status' => 0
        );

        $phone_otp_query = $PDO->db_query("select * from #_signup_otp_details  where  phone='" . $mobile_number . "' ");
        if (mysql_num_rows($phone_otp_query) > 0) {
            $otp_query = $PDO->db_query("update  #_signup_otp_details  SET otp ='" . $json ['otp'] . "' where  phone='" . $mobile_number . "' ");
            $json ['status'] = 'true';
            $json ['msg'] = 'Your OTP is send to you entered phone number.';
        } else {
            $result = $PDO->sqlquery("rs", 'signup_otp_details', $signup_otp_details);
            $mobile = $mobile_number;
            $message = urlencode('OTP(one time password) for signup is :' . $json ['otp']);
            $sendmsg = file_get_contents("http://www.doctoroncall.com.mm/crm/app/sms/send_sms.php?mobile=" . $mobile . "&message=" . $message);
            $json ['status'] = 'true';
            $json ['msg'] = 'Your OTP is send to you entered phone number.';
        }

        break;
    /*
	 * case 'forgot_password' :
	 *
	 * $email = mysql_real_escape_string ( $_POST ['email'] );
	 * $new_password = $doctor_function->random_password ( 8 );
	 * $update_new_password = $doctor_function->update_new_password ( $new_password, $email );
	 * if (($update_new_password == true)) {
	 * $json ['password_update_status'] = 'true';
	 *
	 * // send mail
	 * $table_name = 'doctors';
	 * $doctor_details = $doctor_function->get_doctor_details_by_email ( $table_name, $email );
	 * $doctor_name = $doctor_details ['name'];
	 * $to_email = $email;
	 * $to_name = $doctor_name;
	 * if ($to_name == '') {
	 * $to_name = $to_email;
	 * }
	 * $subject = 'Forgot Password';
	 * $message_content = $doctor_name . 'As per your request your newly generated password is : ' . $new_password . '<br>Please use your email id and with this password to login.';
	 *
	 * $email_details = array (
	 * 'from_email' => 'Doctor@doctoroncall.com.mm',
	 * 'from_name' => 'Admin Doctor CRM',
	 * 'to_email' => $to_email,
	 * 'to_name' => $to_name,
	 * 'subject' => $subject,
	 * 'message_content' => $message_content
	 * );
	 *
	 * $sendEmail = $m->sendMail ( $email_details );
	 *
	 * if ($sendEmail == true) {
	 * $json ['status'] = 'true';
	 * $json ['mail_status'] = 'true';
	 * $json ['msg'] = 'Your Newly Generated Password is sent to your mail Id.Please check';
	 * } else {
	 *
	 * $json ['mail_status'] = 'false';
	 * $json ['mail_status_msg'] = 'Mail is not sent';
	 * $json ['status'] = 'false';
	 * $json ['msg'] = 'There is a problem in email sending process';
	 * }
	 * } else {
	 * $json ['password_update_status'] = 'true';
	 * $json ['status'] = 'false';
	 * $json ['msg'] = 'This email id is not registered with us.';
	 * }
	 *
	 * break;
	 */
    case 'get_address' :

        $filter_name = mysql_real_escape_string($_POST ['filter_name']);
        $id = mysql_real_escape_string($_POST ['id']);
        if ($filter_name == 'doctors') {
            $table_name = 'doctors';
        }
        if ($filter_name == 'hospitals') {
            $table_name = 'hospitals';
        }
        if ($filter_name == 'clinics') {
            $table_name = 'clinics';
        }
        if ($filter_name == 'labs') {
            $table_name = 'labs';
        }

        $address = $doctor_function->get_address($table_name, $id, $filter_name);
        if (!empty ($address)) {
            $json ['address_list'] = $address;
        } else {
            $json ['address_list'] = array();
        }

        break;
    case 'hit_five_sec':

        $json ['status'] = 'true';

        break;
    case 'infitest' :

        $message_content = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt 
                ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi 
                ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit
                in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                ';

        $email_details = array(
            'from_email' => 'Doctor@doctoroncall.com.mm',
            'from_name' => 'Doctor CRM',
            'to_email' => 'ironmaninfi@gmail.com',
            'to_name' => 'Doctor Infi',
            'subject' => 'Infi Test',
            'message_content' => $message_content
        );

        $result = $m->sendMail($email_details);
        break;

    default :
        break;
}

/* Output header */
// header('Content-type: application/json');
// echo json_encode($_POST);
echo json_encode($json);
?>