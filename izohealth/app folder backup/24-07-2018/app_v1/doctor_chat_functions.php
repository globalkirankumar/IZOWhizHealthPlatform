<?php
// error_reporting ( - 1 );
include("../lib/open.inc.php");
include 'class/all_class_files.php';
include_once 'notification/GCM.php';
$gcm = new GCM ();
$m = new MyMail ();
$json = array();
$flag = mysql_real_escape_string($_POST ['flag']);
// initiate doctor object
$doctor_function = new doctor ();
date_default_timezone_set('Asia/Kolkata');

switch ($flag) {
    //Normal Chat functions end
    case 'sendChatMessage' :
        $doctor_id = NULL;
        $patient_id = NULL;
        $chat_from = NULL;
        $chat_message = NULL;
        $json ['status'] = 0;
        $json ['msg'] = '';

        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        $chat_from = mysql_real_escape_string($_POST ['chat_from']);
        $chat_message = mysql_real_escape_string($_POST ['chat_message']);

        if ($doctor_id && $patient_id && $chat_message) {
            if (array_key_exists('appointment_id', $_POST) && $_POST ['appointment_id']) {
                $appointment_id = $_POST ['appointment_id'];
                $query = "insert into  #_chat_details  set  doctor_id ='" . $doctor_id . "',appointment_id ='" . $appointment_id . "', patient_id='" . $patient_id . "',chat_from ='" . $chat_from . "',chat_message='" . $chat_message . "', created_on='" . date('Y-m-d H:i:s') . "', modified_on='" . date('Y-m-d H:i:s') . "'";
            } else {
                $query = "insert into  #_chat_details  set  doctor_id ='" . $doctor_id . "', patient_id='" . $patient_id . "',chat_from ='" . $chat_from . "',chat_message='" . $chat_message . "', created_on='" . date('Y-m-d H:i:s') . "', modified_on='" . date('Y-m-d H:i:s') . "'";
            }
            // echo $query;
            $inserted_chat_details = $PDO->db_query($query);

            $patient_gsm = $PDO->getSingleResult("select gsm_tocken from #_patients where pid=" . $patient_id);
            if ($patient_gsm) {

                // $doctor_name=$PDO->getSingleResult("select name from #_doctors where pid=" . $doctor_id);
                $message = array(
                    'message' => $chat_message,
                    'flag' => 'chat_message'
                );
                $is_ios_device = $PDO->getSingleResult("select is_ios_device from #_patients where pid=" . $patient_id);
                $result = $gcm->send_notification($patient_gsm, $message, "Chat Message", $doctor_id, 'D', $patient_id, 'P', $is_ios_device);
            }

            if ($inserted_chat_details) {
                $json ['status'] = 1;
                $json ['msg'] = 'Message sent successfully';
            }
        } else {
            $json ['msg'] = 'Mandatory data are missed';
        }
        break;
    case 'getChatNotificationList' :
        $doctor_id = NULL;

        $json ['status'] = 'true';
        $json ['msg'] = 'Failed to get Unread Notification Lists, Try again!!!';
        $json ['notify_data'] = array();

        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);

        if ($doctor_id) {
            $query = "select max(cd.pid) as chat_id,cd.patient_id as patient_id,count(cd.pid) as notify_count,SUM((CASE WHEN(cd.chat_from='P' AND cd.un_read=1) THEN 1 ELSE 0
		  END)) as un_read_count,cd.chat_from as chat_from,cd.chat_message as last_chat_message,cd.created_on as last_msg_date_time from #_chat_details as cd where cd.doctor_id ='" . $doctor_id . "' and cd.appointment_id=0  group by cd.patient_id order by chat_id desc";
            //print_r($query);exit;
            $chat_notification_lists = $PDO->db_query($query);
            if (mysql_num_rows($chat_notification_lists) > 0) {
                $i = 0;
                $notify_data = array();
                while ($row = $PDO->db_fetch_array($chat_notification_lists)) {
                    $notify_data [$i] ['patient_id'] = $row ['patient_id'];
                    $notify_data [$i] ['doctor_id'] = $doctor_id;
                    $notify_data [$i] ['patient_name'] = $PDO->getSingleResult("select name from #_patients where pid='" . $row ['patient_id'] . "'");
                    $notify_data [$i] ['patient_profile_image'] = ($PDO->getSingleResult("select profile_pic from #_patients where pid='" . $row ['patient_id'] . "'") == '') ? '' : SITE_PATH . "uploaded_files/profile_pictures/" . $PDO->getSingleResult("select profile_pic from #_patients where pid='" . $row ['patient_id'] . "'");
                    $notify_data [$i] ['doctor_name'] = $PDO->getSingleResult("select name from #_doctors where pid='" . $doctor_id . "'");
                    //$notify_data [$i] ['patient_notify_count'] = $row ['notify_count'];

                    $notify_data [$i] ['patient_notify_count'] = $row['un_read_count'];
                    $notify_data [$i] ['last_chat_message'] = $PDO->getSingleResult("select chat_message from #_chat_details where doctor_id ='" . $doctor_id . "' and patient_id='" . $row ['patient_id'] . "' and appointment_id=0 order by pid desc limit 0,1");
                    //$notify_data [$i] ['last_chat_message'] =$row ['last_chat_message'];
                    $notify_data [$i] ['last_msg_date_time'] = $row ['last_msg_date_time'];
                    $i++;
                }
                $json ['notify_data'] = $notify_data;
            }
            if (count($notify_data) > 0) {
                $json ['status'] = 'true';
                $json ['msg'] = 'Success!! Unread Notification Lists';
            } else {
                $json ['msg'] = 'Unread Notification Lists Not Found';
            }
        } else {
            $json ['msg'] = 'Mandatory data are missed';
        }
        break;
    case 'getChatMessage' :
        $doctor_id = NULL;
        $patient_id = NULL;
        $chat_from = NULL;
        $reponse_data = array();
        $json ['status'] = 0;
        $unread_count = 0;

        $json ['msg'] = 'No Chat available';

        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        $offset = mysql_real_escape_string($_POST ['offset']);
        $chat_from = mysql_real_escape_string($_POST ['chat_from']);

        if ($doctor_id && $patient_id) {
            if (array_key_exists('appointment_id', $_POST) && $_POST ['appointment_id']) {
                $appointment_id = $_POST ['appointment_id'];
                $query = "select * from #_chat_details where doctor_id='" . $doctor_id . "' And patient_id='" . $patient_id . "' And un_read=1 And appointment_id='" . $appointment_id . "' ";
            } else {
                $query = "select * from #_chat_details where doctor_id='" . $doctor_id . "' And patient_id='" . $patient_id . "' And un_read=1 And appointment_id=0";
            }

            $chat_unread_count = $PDO->db_query($query);
            $unread_count = (mysql_num_rows($chat_unread_count));
            if (array_key_exists('appointment_id', $_POST) && $_POST ['appointment_id']) {
                $appointment_id = $_POST ['appointment_id'];
                $unread_count = $PDO->getSingleResult("select (CASE WHEN(STR_TO_DATE(modified_on, '%Y-%m-%d %H:%i:%s')>STR_TO_DATE(created_on, '%Y-%m-%d %H:%i:%s')) THEN 0 ELSE 1 END) as msg_un_read from #_chat_details where doctor_id='" . $doctor_id . "' And patient_id='" . $patient_id . "' And appointment_id='" . $appointment_id . "' order by pid desc Limit 0,1");
            } else {
                $unread_count = $PDO->getSingleResult("select (CASE WHEN(STR_TO_DATE(modified_on, '%Y-%m-%d %H:%i:%s')>STR_TO_DATE(created_on, '%Y-%m-%d %H:%i:%s')) THEN 0 ELSE 1 END) as msg_un_read from #_chat_details where doctor_id='" . $doctor_id . "' And patient_id='" . $patient_id . "' And appointment_id=0 order by pid desc Limit 0,1");
            }

            //$last = $offset + PAGINATION_END_LIMIT;
            $limit = " limit " . $offset . "," . PAGINATION_END_LIMIT;
            if (array_key_exists('appointment_id', $_POST) && $_POST ['appointment_id']) {
                $appointment_id = $_POST ['appointment_id'];
                $query = "select *,(CASE WHEN(chat_from='P' AND un_read=1) THEN 1 ELSE 0
		  END) as msg_un_read from #_chat_details where doctor_id='" . $doctor_id . "' And patient_id='" . $patient_id . "' And appointment_id='" . $appointment_id . "' order by pid desc " . $limit;
            } else {
                $query = "select *,(CASE WHEN(chat_from='P' AND un_read=1) THEN 1 ELSE 0
		        END) as msg_un_read from #_chat_details where doctor_id='" . $doctor_id . "' And patient_id='" . $patient_id . "' And appointment_id=0 order by pid desc " . $limit;

            }
            //print_r($query);exit;
            $chat_unread = $PDO->db_query($query);
            while ($list = $PDO->db_fetch_array($chat_unread)) {
                $reponse_data [] = array(
                    'chat_id' => $list ['pid'],
                    'chat_message' => $list ['chat_message'],
                    'doctor_id' => $list ['doctor_id'],
                    'patient_id' => $list ['patient_id'],
                    'chat_from' => $list ['chat_from'],
                    'un_read' => $list ['msg_un_read'],
                    'sent_date_time' => $list ['created_on']
                );
                if ($list ['pid']) {
                    if ($list ['chat_from'] == 'P') {
                        $query = "update #_chat_details set un_read =0, modified_on='" . date('Y-m-d H:i:s') . "' Where pid='" . $list ['pid'] . "'  AND chat_from='P'";
                        $update_call_details = $PDO->db_query($query);
                    } else {
                        $query = "update #_chat_details set modified_on='" . date('Y-m-d H:i:s') . "' Where pid='" . $list ['pid'] . "'";
                        $update_call_details = $PDO->db_query($query);

                    }
                }

            }
            if ($reponse_data) {
                $json ['msg'] = 'Successfully recieved chat message';
                $json ['details'] = $reponse_data;
                $json ['un_read'] = $unread_count;
                $json ['status'] = 1;
            }
        } else {
            $json ['msg'] = 'Mandatory data are missed';
            $json ['un_read'] = $unread_count;
            $json ['details'] = array();
        }
        break;

    case 'getInboxNotificationList' :
        $doctor_id = NULL;
        $notify_data = array();
        $json ['status'] = 'true';
        $json ['msg'] = 'Failed to get Unread Inbox Notification Lists, Try again!!!';
        $json ['notify_data'] = array();

        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);

        if ($doctor_id) {
            $query = "select nd.pid as notification_id,nd.title as title,nd.description as message,nd.sender_id as sender_id,nd.sender_type as sender_type,nd.receiver_id as receiver_id, nd.receiver_type as receiver_type,nd.notification_type as notification_type,nd.created_on as notified_date from #_notification_details as nd where nd.receiver_id ='" . trim($doctor_id) . "' and nd.receiver_type='D' order by nd.pid desc";
            $inbox_notification_lists = $PDO->db_query($query);
            if (mysql_num_rows($inbox_notification_lists) > 0) {
                $i = 0;

                while ($row = $PDO->db_fetch_array($inbox_notification_lists)) {
                    $notify_data [$i] ['notification_id'] = $row ['notification_id'];
                    $notify_data [$i] ['title'] = $row ['title'];
                    $notify_data [$i] ['message'] = $row ['message'];
                    $notify_data [$i] ['receiver_id'] = $doctor_id;
                    $notify_data [$i] ['sender_id'] = $row ['sender_id'];
                    $notify_data [$i] ['receiver_name'] = ($row ['receiver_type'] = 'D') ? $PDO->getSingleResult("select name from #_doctors where pid='" . $row ['receiver_id'] . "'") : $PDO->getSingleResult("select name from #_patients where pid='" . $row ['receiver_id'] . "'");
                    $notify_data [$i] ['sender_name'] = ($row ['sender_type'] = 'D') ? $PDO->getSingleResult("select name from #_doctors where pid='" . $row ['sender_id'] . "'") : $PDO->getSingleResult("select name from #_patients where pid='" . $row ['sender_id'] . "'");
                    $notify_data [$i] ['notified_type'] = $row ['notification_type'];
                    $notify_data [$i] ['notified_date'] = $row ['notified_date'];
                    $notify_data [$i] ['notify_count'] = mysql_num_rows($inbox_notification_lists);
                    $i++;

                    if ($row ['notification_id']) {
                        $query = "update #_notification_details set un_read ='0', modified_on='" . date('Y-m-d H:i:s') . "' Where pid='" . $row ['notification_id'] . "'";
                        $update_notification_read = $PDO->db_query($query);
                    }
                    /*$query = "update #_notification_details set un_read=0 where pid=" . $row ['notification_id'];
					$update_notification_read = $PDO->db_query ( $query );*/
                }
            }
            if (count($notify_data) > 0) {
                $json ['status'] = 'true';
                $json ['msg'] = 'Success!! Unread Inbox Notification Lists';
                $json ['notify_data'] = $notify_data;
            } else {
                $json ['msg'] = 'Unread Inbox Notification Lists Not Found';
            }
        } else {
            $json ['msg'] = 'Mandatory data are missed';
        }
        break;
    //Normal Chat functions end


    default :
        break;
}

/* Output header */
// header('Content-type: application/json');
// echo json_encode($_POST);
echo json_encode($json);
?>