<?php
// error_reporting ( - 1 );
include("../lib/open.inc.php");
include 'class/all_class_files.php';
include_once 'notification/GCM.php';
$gcm = new GCM ();
$m = new MyMail ();
$json = array();
$flag = mysql_real_escape_string($_POST ['flag']);
// initiate doctor object
$doctor_function = new doctor ();
date_default_timezone_set('Asia/Kolkata');

switch ($flag) {

    case 'doctorLocationDetails' :
        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $appointment_category_lists = array();
        $json ['location_details'] = array();
        $json ['msg'] = 'Doctor location not found';
        $json ['status'] = 'false';
        $json ['appointment_category_list'] = $appointment_category_lists;

        $query = "select dl.location_type As location_type,dl.hospital_id as hospital_id,dl.pid as doctorLocationId,dl.clinic_id as clinic_id,dl.isHomeservice as isHomeservice,dl.latlong as latlong,dl.location as location from #_doctors_locations as dl where dl.doctor_id=" . $doctor_id;
        $doctor_location_details = $PDO->db_query($query);
        if (mysql_num_rows($doctor_location_details) > 0) {
            $i = 0;
            $doctor_location_array = array();
            while ($doctor_data = $PDO->db_fetch_array($doctor_location_details)) {
                // $doctor_location_array[] = $doctor_data['user_id'];
                // $doctor_location_array [$i] ['appointmentId'] = $doctor_data ['appointmentId'];
                $doctor_location_array [$i] ['locationType'] = $doctor_data ['location_type'];
                $doctor_location_array [$i] ['id'] = $doctor_data ['doctorLocationId'];
                $doctor_location_array [$i] ['locationName'] = ($doctor_data ['location_type'] == 1) ? $PDO->getSingleResult("select name from #_clinics where pid='" . $doctor_data ['clinic_id'] . "'") . "(Clinic)" : $PDO->getSingleResult("select name from #_hospitals where pid='" . $doctor_data ['hospital_id'] . "'") . "(Hospital)";
                $doctor_location_array [$i] ['hospitalName'] = $PDO->getSingleResult("select name from #_hospitals where pid='" . $doctor_data ['hospital_id'] . "'");
                $doctor_location_array [$i] ['clinicName'] = $PDO->getSingleResult("select name from #_clinics where pid='" . $doctor_data ['clinic_id'] . "'");
                if ($doctor_location_array [$i] ['hospitalName'] == null) {
                    $doctor_location_array [$i] ['hospitalName'] = '';
                }
                if ($doctor_location_array [$i] ['clinicName'] == null) {
                    $doctor_location_array [$i] ['clinicName'] = '';
                }
                $doctor_location_array [$i] ['isHomeservice'] = $doctor_data ['isHomeservice'];
                $doctor_location_array [$i] ['isAmbulance'] = ($PDO->getSingleResult("select ambulance_service from #_hospitals where pid='" . $doctor_data ['hospital_id'] . "'") == '') ? '0' : '1';
                $doctor_location_array [$i] ['latlong'] = $doctor_data ['latlong'];
                $doctor_location_array [$i] ['location'] = $doctor_data ['location'];
                $doctor_location_array [$i] ['appointmentCount'] = '0';
                $query = "select count(app.pid) As appointment_count from #_appointment as app where app.doctor_id='" . $doctor_id . "' AND app.doctor_location_id='" . $doctor_data ['doctorLocationId'] . "' AND app.status=1 and STR_TO_DATE(app.app_date, '%Y-%m-%d')>='" . date('Y-m-d') . "'";
                //print_r($query); echo "======";print_r($PDO->getSingleResult ($query)); echo "======";

                $doctor_location_array [$i] ['appointmentCount'] = ($PDO->getSingleResult($query) == '') ? '0' : $PDO->getSingleResult($query);
                /*$doctor_appointment_count = $PDO->db_query ( $query );
				if (mysql_num_rows ( $doctor_appointment_count ) > 0)
				{
				while ( $doctor_data_count = $PDO->db_fetch_array ( $doctor_appointment_count ) ) {
					$doctor_location_array [$i] ['appointmentCount'] = ( $doctor_data_count['appointment_count']===NULL)?'0':$doctor_data_count['appointment_count'];

				}
				}*/

                //$doctor_location_array [$i] ['appointmentCount'] = ($doctor_appointment_count===NULL)?'0':$doctor_appointment_count;
                $i++;
            }
            $start_offset = 0;
            $end_limit = 10;
            $is_today = 1;
            $category = 1;
            $appointment_category_lists = $doctor_function->getDoctorAppointmentList($doctor_id, $category, $start_offset, $end_limit, $is_today);
            if ($appointment_category_lists) {
                $json ['appointment_category_list'] = $appointment_category_lists;
            }

            $json ['location_details'] = $doctor_location_array;
            $json ['msg'] = 'Success!!! Doctor location Lists';
            $json ['status'] = 'true';
        }

        break;
    case 'doctors_location_list' :

        $json ['doctors_location_list'] = array();
        $lang_flag = mysql_real_escape_string($_POST ['lang_flag']);
        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        // get the doctor location details from doctor_locations table
        $doctors_location_list_values = $doctor_function->get_doctors_location_list($doctor_id, $lang_flag);
        if ($doctors_location_list_values != 'false') {
            $json ['doctors_location_list'] = $doctors_location_list_values;
            $json ['status'] = 'true';
        } else {
            $json ['status'] = 'false';
        }
        break;
    case 'add_doctor_location' :

        // get the info from request
        $location_data = $_POST ['location_data'];
        $obj = json_decode($location_data);

        // print_r($location_data);
        $lang_flag = $obj->{'lang_flag'};
        // $area_code = $obj->{'area_code'};
        $doctor_id = $obj->{'doctor_id'};

        $isHomeservice = $obj->{'isHomeservice'}; // value 1 means home location
        $isChatNeeded = $obj->{'isChatNeeded'}; // value 1 means need chat
        $location_one = $obj->{'location_one'};
        $location_two = $obj->{'location_two'};
        $latlong = "";
        $status = $obj->{'status'};
        $timeslots = $obj->{'timeslots'};

        if (empty ($doctor_id) || empty ($lang_flag) || empty ($timeslots) || empty ($status)) {
            $error = true; // total_location exceed
            $json ['msg'] = 'Some details are missing.Please send all the required details';
            $json ['status'] = 'false';
            $home_location_count = 1;
            $doctor_function->send_res($json);
            exit ();
        }

        $error = false;
        $home_location_count = 0;
        $work_location_count = 0;
        $clinic_id_exist = array();
        $hospital_id_exist = array();
        $clinic_id = $obj->{'clinic_id'};
        $hospital_id = $obj->{'hospital_id'};
        if ($clinic_id == "0") {
            $isHomeLocation = 0;
        } else {
            $isHomeLocation = 1;
        }

        $check_location_count = $doctor_function->check_location_count($doctor_id);

        // check the unverified user can add only one location
        $verified_status_from_db = $PDO->getSingleResult("select is_verified from #_doctors where pid='" . $doctor_id . "'");
        if (($check_location_count == 1) && ($verified_status_from_db == 0)) {
            $error = true; // total_location exceed
            $json ['msg'] = 'You cannot able to add more than 1 locations untill you verified.';
            $json ['status'] = 'false';
            $json ['signup_step'] = 1;
            $doctor_function->send_res($json);
            exit ();
        }

        // print_r($clinic_id_exist);
        if ($check_location_count >= 4) {
            $error = true; // total_location exceed
            $json ['msg'] = 'You cannot able to add more than 4 locations';
            $json ['status'] = 'false';
            $doctor_function->send_res($json);
            exit ();
        }

        if ($isHomeLocation == 1) {
            $clinic_id = $obj->{'clinic_id'};
            $location_type = 1;
            $clinic_id_exist = $doctor_function->check_clinic_id_exist($doctor_id, $location_type, $clinic_id);
            // print_r($clinic_id_exist);
            if ($clinic_id_exist ['query_status'] == 'true') {
                $error = true; // total_location exceed
                $json ['msg'] = 'This clinic id is already available';
                $json ['status'] = 'false';
                $home_location_count = 1;
                $doctor_function->send_res($json);
                exit ();
            }
            $home_location_count = $doctor_function->check_location_type_count($doctor_id, 1);

            /*
			 * if ($home_location_count >= 1) {
			 * $error = true; //total_location exceed
			 * $json['msg'] = 'Home location count is cannot be more than one. ';
			 * $json['status'] = 'false';
			 * $doctor_function->send_res($json);
			 * exit;
			 * }
			 */
        } else {
            $hospital_id = $obj->{'hospital_id'};
            $location_type = 2;
            $hospital_id_exist = $doctor_function->check_hospital_id_exist($doctor_id, $location_type, $hospital_id);

            if ($hospital_id_exist ['query_status'] == 'true') {
                $error = true; // total_location exceed
                $json ['msg'] = 'This hospital location is already added';
                $json ['status'] = 'false';
                $doctor_function->send_res($json);
                exit ();
            }
            $work_location_count = $doctor_function->check_location_type_count($doctor_id, 2);
            /*
			 * if ($work_location_count >= 3) {
			 * $error = true; //total_location exceed
			 * $json['msg'] = 'work location count is cannot be more than three. ';
			 * $json['status'] = 'false';
			 * $doctor_function->send_res($json);
			 * exit;
			 * }
			 */
        }

        // check condition before insert

        $total_location_count = $work_location_count + $home_location_count;

        // check whether the new location is home location or work location

        /*
		 * if ($isHomeLocation == 1) {
		 * //if home location check the home location count
		 * if ($home_location_count == 1) {
		 * $error = true; //total_location exceed
		 * $json['msg'] = 'you can add only one home location';
		 * $json['status'] = 'false';
		 * $doctor_function->send_res($json);
		 * exit;
		 * }
		 * } else {
		 * //if work location check the work location count
		 * if ($work_location_count == 3) {
		 * $error = true; //total_location exceed
		 * $json['msg'] = 'you can add only three work locations';
		 * $json['status'] = 'false';
		 * $doctor_function->send_res($json);
		 * exit;
		 * }
		 *
		 * }
		 */
        // check total location count is more than 4

        // check whether the location details already exist
        // print_r($timeslots);
        if ($isHomeLocation == 1) {
            // adding home location
            $count_of_location_details = count($clinic_id_exist) - 1;
            // print_r($clinic_id_exist);
            /*
			 * for ($i = 0; $i < $count_of_location_details; $i++) {
			 * // $clinic_id_exist[$i]['pid'];
			 * $location_id = $clinic_id_exist[$i]['pid'];
			 * $doctor_id = $clinic_id_exist[$i]['doctor_id'];
			 * $time_slot_status = $doctor_function->timeslot_loop($timeslots, $doctor_id);
			 * }
			 */
            $time_slot_status = $doctor_function->timeslot_loop($timeslots, $doctor_id);
            $time_slot_exisitng_weekdays = $time_slot_status ['time_slot_is_exist_weedkday'];

            if (!empty ($time_slot_exisitng_weekdays)) {
                $error = true; // total_location exceed
                $json ['msg'] = 'The time slot that you have selected has been blocked by you for one of your Hospital/Clinic. Try a different Time slot.';
                $json ['status'] = 'false';
                $json ['time_slot_exisitng_weekdays'] = array_unique($time_slot_status ['time_slot_is_exist_weedkday']);
                $doctor_function->send_res($json);
                exit ();
            }
        } else {

            $count_of_location_details = count($hospital_id_exist) - 1;
            // print_r($clinic_id_exist);

            $time_slot_status = array();

            /*
			 * for ($i = 0; $i < $count_of_location_details; $i++) {
			 * // $clinic_id_exist[$i]['pid'];
			 * $location_id = $hospital_id_exist[$i]['pid'];
			 * $doctor_id = $hospital_id_exist[$i]['doctor_id'];
			 * $time_slot_status = $doctor_function->timeslot_loop($timeslots, $doctor_id);
			 * }
			 */

            $time_slot_status = $doctor_function->timeslot_loop($timeslots, $doctor_id);
            $time_slot_exisitng_weekdays = $time_slot_status ['time_slot_is_exist_weedkday'];

            if (!empty ($time_slot_exisitng_weekdays)) {
                $error = true; // total_location exceed
                $json ['msg'] = 'The time slot that you have selected has been blocked by you for one of your Hospital/Clinic. Try a different Time slot.';
                $json ['status'] = 'false';
                $json ['time_slot_exisitng_weekdays'] = array_unique($time_slot_status ['time_slot_is_exist_weedkday']);
                $doctor_function->send_res($json);
                exit ();
            }
        }

        // No error
        if ($error == false) {
            // insert location details
            $doctor_table_insert_details = array();
            // default empty values
            $doctor_table_insert_details ['live_status'] = 0;
            $doctor_table_insert_details ['status'] = 0;
            $doctor_table_insert_details ['name_my'] = "NA";
            $doctor_table_insert_details ['location'] = "";
            $doctor_table_insert_details ['dates'] = "";
            $doctor_table_insert_details ['location_my'] = "";
            $doctor_table_insert_details ['latlong'] = "";
            $doctor_table_insert_details ['isHomeservice'] = "";
            $doctor_table_insert_details ['area_code'] = "";
            $doctor_table_insert_details ['address'] = "";
            $doctor_table_insert_details ['tf_1'] = "";
            $doctor_table_insert_details ['tf_2'] = "";
            $doctor_table_insert_details ['fees'] = "NA";
            $doctor_table_insert_details ['date_of_birth'] = "0000-00-00";
            $doctor_table_insert_details ['speciality'] = 0;
            $doctor_table_insert_details ['hospital_id'] = 0;
            $doctor_table_insert_details ['clinic_id'] = 0;
            $location_one = $obj->{'location_one'};
            $location_two = $obj->{'location_two'};
            $location = $location_one . '~' . $location_two;
            // $doctor_table_insert_details['lang_flag']=$obj->{'lang_flag'};
            $doctor_table_insert_details ['clinic_id'] = 0;
            $doctor_table_insert_details ['hospital_id'] = 0;
            $doctor_table_insert_details ['doctor_id'] = $obj->{'doctor_id'};
            if ($location_type == 1) {
                $doctor_table_insert_details ['clinic_id'] = $obj->{'clinic_id'};
            } else {
                $doctor_table_insert_details ['hospital_id'] = $obj->{'hospital_id'};
            }
            $doctor_table_insert_details ['location'] = $location;
            $doctor_table_insert_details ['isChatNeeded'] = $obj->{'isChatNeeded'};
            // $doctor_table_insert_details['status']=$obj->{'status'};
            $doctor_table_insert_details ['dates'] = $obj->{'timeslots'};
            // update details into doctor table
            $location_insert_step1 = $doctor_function->add_location_insert_step1($doctor_table_insert_details);
            $doctor_table_insert_details ['location_type'] = $location_type;
            $doctor_table_insert_details ['latlong'] = $obj->{'latlong'};
            $doctor_table_insert_details ['isHomeservice'] = $obj->{'isHomeservice'};
            $doctor_table_insert_details ['modified_on'] = @date('Y-m-d H:i:s');
            $doctor_table_insert_details ['create_by'] = $obj->{'doctor_id'};
            $doctor_table_insert_details ['dates'] = json_encode($obj->{'timeslots'});
            $doctor_table_insert_details ['status'] = 1;
            // if latlong is empty get lat long from location
            if ($doctor_table_insert_details ['latlong'] == '') {
                $doctor_table_insert_details ['latlong'] = $doctor_function->get_lat_long($location); // create a function with the name "get_lat_long" given as below
            }
            $location_insert_step2 = $doctor_function->add_location_insert_step2($doctor_table_insert_details);
            $location_id_last_inserted = $location_insert_step2;
            $location_insert_step3 = $doctor_function->add_location_insert_step3($timeslots, $doctor_id, $location_id_last_inserted);
            // print_r($location_insert_step3);
            $json ['location_id'] = $location_id_last_inserted;

            // Send Verification mail
            // Send admin verification url link:
            $admin_details = $doctor_function->get_all_admin_user_details();
            $email = $doctor_table_insert_details ['email'];
            foreach ($admin_details as $row) {
                $admin_email = $row ['email'];
                // $admin_email = 'admin@doctoroncall.com';
                $table_name = 'doctors';
                $doctor_details = $doctor_function->get_doctor_details_by_email($table_name, $email);
                $doctor_name = $doctor_details ['name'];
                $reply_email = 'noreply@doctoroncall.com.mm';
                $reply_name = 'noreplydoctorapp';
                $from_email = 'doctorapp@doctoroncall.com.mm';
                $from_name = 'Doctor APP';
                $to_email = $admin_email;
                $to_name = 'Admin';
                $subject = 'Add New Location Request Approval';
                // http://192.168.0.123/DoctorOnCall/crm/home.php?comp=verify_doctors_locations&mode=add&uid=7804
                $verification_crm_url = SITE_PATH_ADM . 'home.php?comp=verify_doctors_locations&mode=add&uid=' . $location_id_last_inserted;
                // Send Mail
                $message_content = "Doctor $doctor_name is added a new location via doctor app. <br>Please verify their details and activate the location.<br>					
				<a href='$verification_crm_url'>Verify</a>";
                $email_details = array(
                    'from_email' => 'doctor@doctoroncall.com.mm',
                    'from_name' => 'Doctor CRM',
                    'to_email' => $to_email,
                    'to_name' => $to_name,
                    'subject' => $subject,
                    'message_content' => $message_content
                );

                $send_admin_Email = $m->sendMail($email_details);
            }

            if ($send_admin_Email == true) {
                $json ['mail_status'] = 'true';
            } else {

                $json ['mail_status'] = 'false';
                $json ['mail_status_msg'] = $new_mail->ErrorInfo;
            }

            $json ['status'] = 'true';
            $json ['msg'] = 'Location Added Successfully';
            $json ['doctors_location_list'] = array();
            $lang_flag = 'en';
            // get the doctor location details from doctor_locations table
            $doctors_location_list_values = $doctor_function->get_doctors_location_list($doctor_id, $lang_flag);
            // print_r($doctors_location_list_values);
            if ($doctors_location_list_values != 'false') {
                $json ['doctors_location_list'] = $doctors_location_list_values;
            } else {
                $json ['doctors_location_list'] = array();
            }
        }

        break;
    case 'edit_doctor_location' :

        $location_data = $_POST ['location_data'];
        $obj = json_decode($location_data);
        // print_r($location_data);
        $lang_flag = $obj->{'lang_flag'};
        // $area_code = $obj->{'area_code'};
        $doctor_id = $obj->{'doctor_id'};
        $location_id = $obj->{'location_id'};
        $isHomeservice = $obj->{'isHomeservice'}; // value 1 means home location
        $isChatNeeded = $obj->{'isChatNeeded'}; // value 1 means need chat
        $location_one = $obj->{'location_one'};
        $location_two = $obj->{'location_two'};
        $status = $obj->{'status'};
        $timeslots = $obj->{'timeslots'};

        if (empty ($doctor_id) || empty ($lang_flag) || empty ($timeslots) || empty ($location_id) || empty ($status)) {
            $error = true; // total_location exceed
            $json ['msg'] = 'Some details are missing.Please send all the required details';
            $json ['status'] = 'false';
            $home_location_count = 1;
            $doctor_function->send_res($json);
            exit ();
        }

        $error = false;
        $home_location_count = 0;
        $work_location_count = 0;
        $clinic_id_exist = array();
        $hospital_id_exist = array();
        $clinic_id = $obj->{'clinic_id'};
        $hospital_id = $obj->{'hospital_id'};
        if ($clinic_id == 0) {
            $isHomeLocation = 0;
        } else {
            $isHomeLocation = 1;
        }

        $old_hospital_id = $PDO->getSingleResult("select hospital_id from #_doctors_locations where pid='" . $location_id . "' and status=1 and isDeleted=0 ");
        $old_clinic_id = $PDO->getSingleResult("select clinic_id from #_doctors_locations where pid='" . $location_id . "' and status=1 and isDeleted=0 ");

        $latlong_from_db = $PDO->getSingleResult("select latlong from #_doctors_locations where pid='" . $location_id . "' and status=1 and isDeleted=0 ");

        if (($old_hospital_id == $hospital_id) && ($old_clinic_id == $clinic_id)) {
            $is_location_type_change = 0;
        } else {
            if (($old_hospital_id != $hospital_id) && ($old_clinic_id == $clinic_id)) {
                $is_location_type_change = 0;
            }
            if (($old_hospital_id == $hospital_id) && ($old_clinic_id != $clinic_id)) {
                $is_location_type_change = 0;
            }
            if (($old_hospital_id != $hospital_id) && ($old_clinic_id != $clinic_id)) {
                $is_location_type_change = 1;
            }
        }

        $is_hospital_clinic_is_changed = 0;
        if ($old_hospital_id != $hospital_id) {
            $is_hospital_clinic_is_changed = 1;
        }
        if ($old_clinic_id != $clinic_id) {
            $is_hospital_clinic_is_changed = 1;
        }

        // if user changed location type details
        if ($is_location_type_change == 1) {
            // 1 means user chnaged the location type
            //

            if ($isHomeLocation == 1) {
                $clinic_id = $obj->{'clinic_id'};
                $location_type = 1;
                $clinic_id_exist = $doctor_function->check_clinic_id_exist($doctor_id, $location_type, $clinic_id);
                // print_r($clinic_id_exist);
                if ($clinic_id_exist ['query_status'] == 'true') {
                    $error = true; // total_location exceed
                    $json ['msg'] = 'This Clinic id is already available';
                    $json ['status'] = 'false';
                    $home_location_count = 1;
                    $doctor_function->send_res($json);
                    exit ();
                }
                $home_location_count = $doctor_function->check_location_type_count($doctor_id, 1);

                /*
				 * if ($home_location_count >= 1) {
				 * $error = true; //total_location exceed
				 * $json['msg'] = 'Home location count is cannot be more than one. ';
				 * $json['status'] = 'false';
				 * $doctor_function->send_res($json);
				 * exit;
				 * }
				 */

                $count_of_location_details = count($clinic_id_exist) - 1;
                // print_r($clinic_id_exist);
                /*
				 * for ($i = 0; $i < $count_of_location_details; $i++) {
				 * // $clinic_id_exist[$i]['pid'];
				 * $location_id = $clinic_id_exist[$i]['pid'];
				 * $doctor_id = $clinic_id_exist[$i]['doctor_id'];
				 * $time_slot_status = $doctor_function->timeslot_loop($timeslots, $doctor_id);
				 * }
				 */
                $time_slot_status = $doctor_function->edit_timeslot_loop($timeslots, $doctor_id, $location_id);
                $time_slot_exisitng_weekdays = $time_slot_status ['time_slot_is_exist_weedkday'];

                if (!empty ($time_slot_exisitng_weekdays)) {
                    $error = true; // total_location exceed
                    $json ['msg'] = 'The time slot that you have selected has been blocked by you for one of your Hospital/Clinic. Try a different Time slot.';
                    $json ['status'] = 'false';
                    $json ['time_slot_exisitng_weekdays'] = array_unique($time_slot_status ['time_slot_is_exist_weedkday']);
                    $doctor_function->send_res($json);
                    exit ();
                }
            } else {
                $hospital_id = $obj->{'hospital_id'};
                $location_type = 2;
                $hospital_id_exist = $doctor_function->check_hospital_id_exist($doctor_id, $location_type, $hospital_id);

                if ($hospital_id_exist ['query_status'] == 'true') {
                    $error = true; // total_location exceed
                    $json ['msg'] = 'This hospital location is already added';
                    $json ['status'] = 'false';
                    $doctor_function->send_res($json);
                    exit ();
                }
                $work_location_count = $doctor_function->check_location_type_count($doctor_id, 2);
                /*
				 * if ($work_location_count >= 3) {
				 * $error = true; //total_location exceed
				 * $json['msg'] = 'work location count is cannot be more than three. ';
				 * $json['status'] = 'false';
				 * $doctor_function->send_res($json);
				 * exit;
				 * }
				 */

                $count_of_location_details = count($hospital_id_exist) - 1;
                // print_r($clinic_id_exist);

                $time_slot_status = array();

                /*
				 * for ($i = 0; $i < $count_of_location_details; $i++) {
				 * // $clinic_id_exist[$i]['pid'];
				 * $location_id = $hospital_id_exist[$i]['pid'];
				 * $doctor_id = $hospital_id_exist[$i]['doctor_id'];
				 * $time_slot_status = $doctor_function->timeslot_loop($timeslots, $doctor_id);
				 * }
				 */

                $time_slot_status = $doctor_function->edit_timeslot_loop($timeslots, $doctor_id, $location_id);
                $time_slot_exisitng_weekdays = $time_slot_status ['time_slot_is_exist_weedkday'];

                if (!empty ($time_slot_exisitng_weekdays)) {
                    $error = true; // total_location exceed
                    $json ['msg'] = 'The time slot that you have selected has been blocked by you for one of your Hospital/Clinic. Try a different Time slot.';
                    $json ['status'] = 'false';
                    $json ['time_slot_exisitng_weekdays'] = array_unique($time_slot_status ['time_slot_is_exist_weedkday']);
                    $doctor_function->send_res($json);
                    exit ();
                }
            }

            if ($error == false) {
                // update location details
                $doctor_table_insert_details = array();
                // default empty values
                $doctor_table_insert_details ['live_status'] = 0;
                $doctor_table_insert_details ['status'] = 0;
                $doctor_table_insert_details ['name_my'] = "NA";
                $doctor_table_insert_details ['location'] = "";
                $doctor_table_insert_details ['dates'] = "";
                $doctor_table_insert_details ['location_my'] = "";
                $doctor_table_insert_details ['latlong'] = "";
                $doctor_table_insert_details ['isHomeservice'] = "";
                $doctor_table_insert_details ['area_code'] = "";
                $doctor_table_insert_details ['address'] = "";
                $doctor_table_insert_details ['tf_1'] = "";
                $doctor_table_insert_details ['tf_2'] = "";
                $doctor_table_insert_details ['fees'] = "NA";
                $doctor_table_insert_details ['date_of_birth'] = "0000-00-00";
                $doctor_table_insert_details ['speciality'] = 0;
                $doctor_table_insert_details ['hospital_id'] = 0;
                $doctor_table_insert_details ['clinic_id'] = 0;
                $location_one = $obj->{'location_one'};
                $location_two = $obj->{'location_two'};
                $latlong = "";
                $location = $location_one . '~' . $location_two;
                // $doctor_table_insert_details['lang_flag']=$obj->{'lang_flag'};
                $doctor_table_insert_details ['doctor_id'] = $obj->{'doctor_id'};
                if ($location_type == 1) {
                    $doctor_table_insert_details ['clinic_id'] = $obj->{'clinic_id'};
                } else {
                    $doctor_table_insert_details ['hospital_id'] = $obj->{'hospital_id'};
                }
                $doctor_table_insert_details ['location'] = $location;
                $doctor_table_insert_details ['isChatNeeded'] = $obj->{'isChatNeeded'};
                $doctor_table_insert_details ['status'] = $obj->{'status'};
                $doctor_table_insert_details ['dates'] = $obj->{'timeslots'};
                // update details into doctor table
                $doctor_table_insert_details ['isHomeservice'] = $obj->{'isHomeservice'};

                $location_insert_step1 = $doctor_function->add_location_insert_step1($doctor_table_insert_details);
                $doctor_table_insert_details ['location_type'] = $location_type;
                $doctor_table_insert_details ['latlong'] = $obj->{'latlong'};
                $doctor_table_insert_details ['modified_on'] = @date('Y-m-d H:i:s');
                $doctor_table_insert_details ['create_by'] = $obj->{'doctor_id'};
                $doctor_table_insert_details ['dates'] = json_encode($obj->{'timeslots'});
                $doctor_table_insert_details ['status'] = 1;
                $doctor_table_insert_details ['is_verified'] = 0;
                if ($doctor_table_insert_details ['latlong'] != $latlong_from_db) {

                    $doctor_table_insert_details ['is_verified'] = 0;
                }
                $doctor_table_insert_details ['location_id'] = $location_id;
                $location_update_step2 = $doctor_function->edit_location_update_step2($doctor_table_insert_details);
                $location_id_last_updated = $location_update_step2;
                $delete_location_time_slots = $doctor_function->delete_location_time_slot_details($doctor_id, $location_id);
                $location_insert_step3 = $doctor_function->add_location_insert_step3($timeslots, $doctor_id, $location_id);
                // print_r($location_insert_step3);
                $json ['location_id'] = $location_id_last_updated;
                $json ['status'] = 'true';
                $json ['msg'] = 'Location Updated Successfully';
                $json ['doctors_location_list'] = array();
                $lang_flag = 'en';
                // get the doctor location details from doctor_locations table
                $doctors_location_list_values = $doctor_function->get_doctors_location_list($doctor_id, $lang_flag);
                // print_r($doctors_location_list_values);
                if ($doctors_location_list_values != 'false') {
                    $json ['doctors_location_list'] = $doctors_location_list_values;
                } else {
                    $json ['doctors_location_list'] = array();
                }

                // Send Mail
                // Send Verification mail
                // Send admin verification url link:
                $admin_details = $doctor_function->get_all_admin_user_details();

                $email = $PDO->getSingleResult("select email from #_doctors where pid='" . $doctor_id . "'");
                foreach ($admin_details as $row) {
                    $admin_email = $row ['email'];
                    // $admin_email = 'admin@doctoroncall.com';
                    $table_name = 'doctors';
                    $doctor_details = $doctor_function->get_doctor_details_by_email($table_name, $email);
                    $doctor_name = $doctor_details ['name'];
                    $reply_email = 'noreply@doctoroncall.com.mm';
                    $reply_name = 'noreplydoctorapp';
                    $from_email = 'doctorapp@doctoroncall.com.mm';
                    $from_name = 'Doctor APP';
                    $to_email = $admin_email;
                    $to_name = 'Admin';
                    $subject = 'Add New Location Request Approval';
                    // http://192.168.0.123/DoctorOnCall/crm/home.php?comp=verify_doctors_locations&mode=add&uid=7804
                    $verification_crm_url = SITE_PATH_ADM . 'home.php?comp=verify_doctors_locations&mode=add&uid=' . $location_id;
                    $message_content = "Doctor $doctor_name is added a new location via doctor app. <br>Please verify their details and activate the location.<br>					
				<a href='$verification_crm_url'>Verify</a>";

                    // Send Mail

                    $email_details = array(
                        'from_email' => 'doctor@doctoroncall.com.mm',
                        'from_name' => 'Doctor CRM',
                        'to_email' => $to_email,
                        'to_name' => $to_name,
                        'subject' => $subject,
                        'message_content' => $message_content
                    );

                    $send_admin_Email = $m->sendMail($email_details);
                }
            }
        } else {

            // update exsiting location details

            if ($isHomeLocation == 1) {

                $clinic_id = $obj->{'clinic_id'};
                $location_type = 1;
                $clinic_id_exist = $doctor_function->edit_check_clinic_id_exist($doctor_id, $location_type, $clinic_id, $location_id);
                // print_r($clinic_id_exist);
                if ($clinic_id_exist ['query_status'] == 'true') {
                    $error = true; // total_location exceed
                    $json ['msg'] = 'This Clinic id is already available';
                    $json ['status'] = 'false';
                    $home_location_count = 1;
                    $doctor_function->send_res($json);
                    exit ();
                }

                $home_location_count = $doctor_function->check_location_type_count($doctor_id, 1);

                /*
				 * if ($home_location_count >= 1) {
				 * $error = true; //total_location exceed
				 * $json['msg'] = 'Home location count is cannot be more than one. ';
				 * $json['status'] = 'false';
				 * $doctor_function->send_res($json);
				 * exit;
				 * }
				 */

                $count_of_location_details = count($clinic_id_exist) - 1;
                // print_r($clinic_id_exist);
                /*
				 * for ($i = 0; $i < $count_of_location_details; $i++) {
				 * // $clinic_id_exist[$i]['pid'];
				 * $location_id = $clinic_id_exist[$i]['pid'];
				 * $doctor_id = $clinic_id_exist[$i]['doctor_id'];
				 * $time_slot_status = $doctor_function->timeslot_loop($timeslots, $doctor_id);
				 * }
				 */

                $time_slot_status = $doctor_function->edit_timeslot_loop($timeslots, $doctor_id, $location_id);
                $time_slot_exisitng_weekdays = $time_slot_status ['time_slot_is_exist_weedkday'];

                if (!empty ($time_slot_exisitng_weekdays)) {
                    $error = true; // total_location exceed
                    $json ['msg'] = 'The time slot that you have selected has been blocked by you for one of your Hospital/Clinic. Try a different Time slot.';
                    $json ['status'] = 'false';
                    $json ['time_slot_exisitng_weekdays'] = array_unique($time_slot_status ['time_slot_is_exist_weedkday']);
                    $doctor_function->send_res($json);
                    exit ();
                }
            } else {

                $hospital_id = $obj->{'hospital_id'};
                $location_type = 2;

                $hospital_id_exist = $doctor_function->edit_check_hospital_id_exist($doctor_id, $location_type, $hospital_id, $location_id);

                if ($hospital_id_exist ['query_status'] == 'true') {
                    $error = true; // total_location exceed
                    $json ['msg'] = 'This hospital location is already added';
                    $json ['status'] = 'false';
                    $doctor_function->send_res($json);
                    exit ();
                }

                $work_location_count = $doctor_function->check_location_type_count($doctor_id, 2);
                /*
				 * if ($work_location_count >= 3) {
				 * $error = true; //total_location exceed
				 * $json['msg'] = 'work location count is cannot be more than three. ';
				 * $json['status'] = 'false';
				 * $doctor_function->send_res($json);
				 * exit;
				 * }
				 */

                $count_of_location_details = count($hospital_id_exist) - 1;
                // print_r($clinic_id_exist);

                $time_slot_status = array();

                /*
				 * for ($i = 0; $i < $count_of_location_details; $i++) {
				 * // $clinic_id_exist[$i]['pid'];
				 * $location_id = $hospital_id_exist[$i]['pid'];
				 * $doctor_id = $hospital_id_exist[$i]['doctor_id'];
				 * $time_slot_status = $doctor_function->timeslot_loop($timeslots, $doctor_id);
				 * }
				 */

                // $time_slot_status = $doctor_function->timeslot_loop($timeslots, $doctor_id);

                $time_slot_status = $doctor_function->edit_timeslot_loop($timeslots, $doctor_id, $location_id);
                $time_slot_exisitng_weekdays = $time_slot_status ['time_slot_is_exist_weedkday'];

                if (!empty ($time_slot_exisitng_weekdays)) {
                    $error = true; // total_location exceed
                    $json ['msg'] = 'The time slot that you have selected has been blocked by you for one of your Hospital/Clinic. Try a different Time slot.';
                    $json ['status'] = 'false';
                    $json ['time_slot_exisitng_weekdays'] = array_unique($time_slot_status ['time_slot_is_exist_weedkday']);
                    $doctor_function->send_res($json);

                    exit ();
                }
            }

            // No error and Update

            if ($error == false) {

                // update location details
                $doctor_table_insert_details = array();
                // default empty values
                $doctor_table_insert_details ['live_status'] = 0;
                $doctor_table_insert_details ['status'] = 0;
                $doctor_table_insert_details ['name_my'] = "NA";
                $doctor_table_insert_details ['location'] = "";
                $doctor_table_insert_details ['dates'] = "";
                $doctor_table_insert_details ['location_my'] = "";
                $doctor_table_insert_details ['latlong'] = "";
                $doctor_table_insert_details ['area_code'] = "";
                $doctor_table_insert_details ['address'] = "";
                $doctor_table_insert_details ['tf_1'] = "";
                $doctor_table_insert_details ['tf_2'] = "";

                $doctor_table_insert_details ['fees'] = "NA";
                $doctor_table_insert_details ['date_of_birth'] = "0000-00-00";
                $doctor_table_insert_details ['speciality'] = 0;
                $doctor_table_insert_details ['hospital_id'] = 0;
                $doctor_table_insert_details ['clinic_id'] = 0;

                $location_one = $obj->{'location_one'};
                $location_two = $obj->{'location_two'};
                $location = $location_one . '~' . $location_two;

                // $doctor_table_insert_details['lang_flag']=$obj->{'lang_flag'};
                $doctor_table_insert_details ['doctor_id'] = $obj->{'doctor_id'};
                if ($location_type == 1) {
                    $doctor_table_insert_details ['clinic_id'] = $obj->{'clinic_id'};
                } else {
                    $doctor_table_insert_details ['hospital_id'] = $obj->{'hospital_id'};
                }
                $doctor_table_insert_details ['location'] = $location;
                $doctor_table_insert_details ['isChatNeeded'] = $obj->{'isChatNeeded'};
                $doctor_table_insert_details ['status'] = $obj->{'status'};
                $doctor_table_insert_details ['dates'] = $obj->{'timeslots'};

                // update details into doctor table
                $location_insert_step1 = $doctor_function->add_location_insert_step1($doctor_table_insert_details);

                $doctor_table_insert_details ['location_type'] = $location_type;
                $doctor_table_insert_details ['modified_on'] = @date('Y-m-d H:i:s');
                $doctor_table_insert_details ['create_by'] = $obj->{'doctor_id'};
                $doctor_table_insert_details ['dates'] = json_encode($obj->{'timeslots'});
                $doctor_table_insert_details ['status'] = 1;
                $doctor_table_insert_details ['location_id'] = $location_id;
                $doctor_table_insert_details ['latlong'] = $obj->{'latlong'};
                $doctor_table_insert_details ['isHomeservice'] = $obj->{'isHomeservice'};

                // echo $is_hospital_clinic_is_changed;

                if ($is_hospital_clinic_is_changed != 0) {
                    $doctor_table_insert_details ['is_verified'] = 0;
                } else {

                    $doctor_table_insert_details ['is_verified'] = $PDO->getSingleResult("select is_verified from #_doctors_locations where pid='" . $location_id . "' and status=1 and isDeleted=0 ");
                }
                if ($doctor_table_insert_details ['latlong'] != $latlong_from_db) {

                    $doctor_table_insert_details ['is_verified'] = 0;
                }

                $location_update_step2 = $doctor_function->edit_location_update_step2($doctor_table_insert_details);
                $location_id_last_updated = $location_update_step2;
                $delete_location_time_slots = $doctor_function->delete_location_time_slot_details($doctor_id, $location_id);

                $location_insert_step3 = $doctor_function->add_location_insert_step3($timeslots, $doctor_id, $location_id);
                // print_r($location_insert_step3);
                $json ['location_id'] = $location_id_last_updated;
                $json ['status'] = 'true';
                $json ['msg'] = 'Location Updated Successfully';
                $json ['doctors_location_list'] = array();
                $lang_flag = 'en';
                // get the doctor location details from doctor_locations table
                $doctors_location_list_values = $doctor_function->get_doctors_location_list($doctor_id, $lang_flag);
                // print_r($doctors_location_list_values);
                if ($doctors_location_list_values != 'false') {
                    $json ['doctors_location_list'] = $doctors_location_list_values;
                } else {
                    $json ['doctors_location_list'] = array();
                }

                // Send Verification mail
                // Send admin verification url link:
                $admin_details = $doctor_function->get_all_admin_user_details();

                $email = $PDO->getSingleResult("select email from #_doctors where pid='" . $doctor_id . "'");
                foreach ($admin_details as $row) {
                    $admin_email = $row ['email'];
                    // $admin_email = 'admin@doctoroncall.com';
                    $table_name = 'doctors';
                    $doctor_details = $doctor_function->get_doctor_details_by_email($table_name, $email);
                    $doctor_name = $doctor_details ['name'];
                    $reply_email = 'noreply@doctoroncall.com.mm';
                    $reply_name = 'noreplydoctorapp';
                    $from_email = 'doctorapp@doctoroncall.com.mm';
                    $from_name = 'Doctor APP';
                    $to_email = $admin_email;
                    $to_name = 'Admin';
                    $subject = 'Add New Location Request Approval';
                    // http://192.168.0.123/DoctorOnCall/crm/home.php?comp=verify_doctors_locations&mode=add&uid=7804
                    $verification_crm_url = SITE_PATH_ADM . 'home.php?comp=verify_doctors_locations&mode=add&uid=' . $location_id;
                    $message_content = "Doctor $doctor_name is added a new location via doctor app. <br>Please verify their details and activate the location.<br>					
				<a href='$verification_crm_url'>Verify</a>";

                    // Send Mail

                    $email_details = array(
                        'from_email' => 'doctor@doctoroncall.com.mm',
                        'from_name' => 'Doctor CRM',
                        'to_email' => $to_email,
                        'to_name' => $to_name,
                        'subject' => $subject,
                        'message_content' => $message_content
                    );

                    $send_admin_Email = $m->sendMail($email_details);
                }
            }
        }

        break;
    case 'doctor_location_delete' :

        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $location_id = mysql_real_escape_string($_POST ['location_id']);
        $delete_status = $doctor_function->delete_doc_location($doctor_id, $location_id);
        if ($delete_status == 'true') {
            $error = true; // total_location exceed
            $json ['msg'] = 'This hospital location is deleted successfully';
            $json ['status'] = 'true';
            $json ['doctors_location_list'] = array();
            $lang_flag = 'en';
            // get the doctor location details from doctor_locations table
            $doctors_location_list_values = $doctor_function->get_doctors_location_list($doctor_id, $lang_flag);
            // print_r($doctors_location_list_values);
            if ($doctors_location_list_values != 'false') {
                $json ['doctors_location_list'] = $doctors_location_list_values;
            } else {
                $json ['doctors_location_list'] = array();
            }
        } else {
            $json ['status'] = 'false';
        }

        break;
    case 'selected_location_details' :
        $json ['doctors_location_list'] = array();
        $location_id = mysql_real_escape_string($_POST ['location_id']);
        $selected_doctor_location_details = $doctor_function->get_selected_doctor_location_details($location_id);
        if ($selected_doctor_location_details != 'false') {
            $json ['doctors_location_list'] = $selected_doctor_location_details;
        } else {
            $json ['status'] = 'false';
        }
        break;
    case 'getDoctorsListOnLocation' :
        $location_id = mysql_real_escape_string ( $_POST ['location_id'] );
        $location_type = mysql_real_escape_string ( $_POST ['location_type'] );

        $doctors_list = array ();

        $json ['status'] = 'false';
        $json ['msg'] = 'Doctor List Not Found';
        $json ['doctors_list'] = $doctors_list;

        $doctors_list = $doctor_function->getDoctorsListOnLocation ( $location_id, $location_type );
        if ($doctors_list) {
            $json ['status'] = 'true';
            $json ['msg'] = 'Doctor List';
            $json ['doctors_list'] = $doctors_list;
        }
        break;

    default :
        break;
}

/* Output header */
// header('Content-type: application/json');
// echo json_encode($_POST);
echo json_encode($json);
?>