<?php
include("../lib/open.inc.php");
$flag = mysql_real_escape_string($_POST ['flag']);
$json = array();
include 'class/all_class_files.php';
$doctor_function = new doctor ();
$patient_function = new patient ();
$m = new MyMail ();
include_once 'notification/GCM.php';
$gcm = new GCM ();
date_default_timezone_set('Asia/Kolkata');

switch ($flag) {

    case 'patienthistorylist' :

        $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        $medication_history_data = array();
        $json ['status'] = 'false';
        $json ['msg'] = 'No medical records found';
        $json ['response'] = $medication_history_data;

        $query = "select ccd.pid As 'callDetailsId',cd.name as 'doctorName',cp.name as 'patientName',ccd.chief_complaint As 'chiefComplaint',ccd.history_of_present_illness As 'chiefComplaintHistory',ccd.modified_on As 'updatedDatetime' from crm_call_details as ccd left join crm_doctor_history as cdh on cdh.call_detal_id=ccd.pid right join crm_doctors as cd on cdh.doctor_id=cd.pid left join crm_patients as cp on cp.pid=ccd.patient_id where ccd.patient_id='" . $patient_id . "' order by ccd.pid desc";
        $medication_history = $PDO->db_query($query);
        if (mysql_num_rows($medication_history) > 0) {
            $i = 0;
            while ($list = $PDO->db_fetch_array($medication_history)) {
                if ($i <= mysql_num_rows($medication_history)) {
                    /*
					 * $reponse_data[$i]['title'] =$list['title'];
					 * $reponse_data[$i]['reports_image']= $_SERVER['SERVER_NAME']."/crm/uploaded_files/reports/".$list['reports'];
					 * $reponse_data[$i]['call_details_id']= $list['call_details_id'];
					 * $reponse_data[$i]['report_id']= $list['pid'];
					 */
                    $medication_history_data [] = array(
                        'callDetailsId' => $list ['callDetailsId'],
                        'doctorName' => $list ['doctorName'],
                        'patientName' => $list ['patientName'],
                        'chiefComplaint' => $list ['chiefComplaint'],
                        'chiefComplaintHistory' => $list ['chiefComplaintHistory'],
                        'updatedDatetime' => $list ['updatedDatetime']
                    );
                }
                $i++;
            }
        }

        if (count($medication_history_data) > 0) {
            $json ['status'] = 'true';
            $json ['msg'] = 'Medical records found';
            $json ['response'] = $medication_history_data;
        }

        break;

    case 'patienthistoryadd' :
        sleep(2);
        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        if (isset($_POST["chief_complaint"]) && !empty($_POST["chief_complaint"])) {
            $chief_complaint = mysql_real_escape_string($_POST ['chief_complaint']);
        } else {
            $chief_complaint = '';
        }
        if (isset($_POST["history_of_present_illness"]) && !empty($_POST["history_of_present_illness"])) {
            $history_of_present_illness = mysql_real_escape_string($_POST ['history_of_present_illness']);
        } else {
            $history_of_present_illness = '';
        }
        if (isset($_POST["past_medical_history"]) && !empty($_POST["past_medical_history"])) {
            $past_medical_history = mysql_real_escape_string($_POST ['past_medical_history']);
        } else {
            $past_medical_history = '';
        }
        if (isset($_POST["past_surgical_history"]) && !empty($_POST["past_surgical_history"])) {
            $past_surgical_history = mysql_real_escape_string($_POST ['past_surgical_history']);
        } else {
            $past_surgical_history = '';
        }
        if (isset($_POST["social_history"]) && !empty($_POST["social_history"])) {
            $social_history = mysql_real_escape_string($_POST ['social_history']);
        } else {
            $social_history = '';
        }
        if (isset($_POST["family_history"]) && !empty($_POST["family_history"])) {
            $family_history = mysql_real_escape_string($_POST ['family_history']);
        } else {
            $family_history = '';
        }
        if (isset($_POST["occupational_history"]) && !empty($_POST["occupational_history"])) {
            $occupational_history = mysql_real_escape_string($_POST ['occupational_history']);
        } else {
            $occupational_history = '';
        }
        if (isset($_POST["history_of_drug_allergy"]) && !empty($_POST["history_of_drug_allergy"])) {
            $history_of_drug_allergy = mysql_real_escape_string($_POST ['history_of_drug_allergy']);
        } else {
            $history_of_drug_allergy = '';
        }
        if (isset($_POST["drug_allergy_comment"]) && !empty($_POST["drug_allergy_comment"])) {
            $drug_allergy_comment = mysql_real_escape_string($_POST ['drug_allergy_comment']);
        } else {
            $drug_allergy_comment = '';
        }
        if (isset($_POST["drug_allergy_regular_taking_medication"]) && !empty($_POST["drug_allergy_regular_taking_medication"])) {
            $drug_allergy_regular_taking_medication = mysql_real_escape_string($_POST ['drug_allergy_regular_taking_medication']);
        } else {
            $drug_allergy_regular_taking_medication = '';
        }
        $bp = mysql_real_escape_string($_POST ['bp']);
        $pr = mysql_real_escape_string($_POST ['pr']);
        $sao = mysql_real_escape_string($_POST ['sao']);
        $rbs = mysql_real_escape_string($_POST ['rbs']);
        $temp = mysql_real_escape_string($_POST ['temp']);
        $call_details_id = mysql_real_escape_string($_POST ['call_details_id']);
        $provisional_diagnosis = mysql_real_escape_string($_POST ['provisional_diagnosis']);

        $json ['status'] = 'false';


        if ($call_details_id) {
            $query = "update #_call_details  set  medicine_id='',caller_id=0,subscription_details_id=0,service_type='Health Consultation',sub_center_check=' ',sub_centers=0,services_comment='',call_type='',service_provided='referral service to',referral_service_to='Specialist',
				follow_up_call_schedule='',follow_up_call_date='',follow_up_call_time='',health_consultation='',shortorder=0,status=1,tf_1='',tf_2='',create_by='" . $doctor_id . "',
				patient_id ='" . $patient_id . "',call_by='crm', chief_complaint ='" . $chief_complaint . "' , history_of_present_illness ='" . $history_of_present_illness . "', past_medical_history='" . $past_medical_history . "', past_surgical_history='" . $past_surgical_history . "',
				social_history='" . $social_history . "', family_history='" . $family_history . "', occupational_history='" . $occupational_history . "' ,history_of_drug_allergy='" . $history_of_drug_allergy . "',drug_allergy_comment='" . $drug_allergy_comment . "' , drug_allergy_regular_taking_medication='" . $drug_allergy_regular_taking_medication . "',
				bp='" . $bp . "',pr='" . $pr . "',sao='" . $sao . "',weight='" . $weight . "',rbs='" . $rbs . "',temp='" . $temp . "',provisional_diagnosis='" . $provisional_diagnosis . "',create_by_type='Doctors',created_on='" . date('Y-m-d H:i:s') . "' WHERE pid='" . $call_details_id . "'";

            $update_call_details = $PDO->db_query($query);

            $history_data_array = array(
                'call_detail_id' => $call_details_id,
                'chief_complaint' => $chief_complaint,
                'history_of_present_illness' => $history_of_present_illness,
                'past_medical_history' => $past_medical_history,
                'past_surgical_history' => $past_surgical_history,
                'social_history' => $social_history,
                'family_history' => $family_history,
                'occupational_history' => $occupational_history,
                'history_of_drug_allergy' => $history_of_drug_allergy,
                'drug_allergy_comment' => $drug_allergy_comment,
                'drug_allergy_regular_taking_medication' => $drug_allergy_regular_taking_medication,
                'bp' => $bp,
                'pr' => $pr,
                'sao' => $sao,
                'rbs' => $rbs,
                'temp' => $temp,
                'provisional_diagnosis' => $provisional_diagnosis
            );
            $history_data_array = json_encode($history_data_array);

            $short_order = $PDO->db_query("select shortorder from #_doctor_history order by pid desc limit 0,1");
            $short_order = $PDO->db_fetch_array($short_order);

            // check history exits or not
            $doctor_history_exists = $PDO->db_query("select * from #_doctor_history where call_detal_id='" . $call_details_id . "'");
            // print_r(mysql_num_rows($doctor_history_exists));exit;
            if (mysql_num_rows($doctor_history_exists) > 0) {
                $query = "update  #_doctor_history  set history_data ='" . $history_data_array . "',doctor_id ='" . $doctor_id . "',modified_on='" . date('Y-m-d H:i:s') . "' where call_detal_id ='" . $call_details_id . "'";
                $doctory_history_id = $PDO->db_query($query);
            } else {
                $query = "insert into #_doctor_history  set  shortorder=" . ++$short_order ['shortorder'] . ",tf_1='',tf_2='',next_follow_up_appointment='',imaging_investigation1='',imaging_investigation2='',imaging_investigation3='',
             imaging_investigation4='',imaging_investigation5='', doctor_id ='" . $doctor_id . "',call_detal_id ='" . $call_details_id . "' , history_data ='" . $history_data_array . "',status=1,created_on='" . date('Y-m-d H:i:s') . "'";
                $doctory_history_id = $PDO->db_query($query);
            }
            if ($doctory_history_id) {
                $json ['status'] = 'true';
                $json['msg'] = 'Medication history succesfully added.';
            }
        }
        break;

    case 'patienthistorydetails_old' :
        $call_details_id = NULL;
        $doctor_id = NULL;
        $patient_id = NULL;
        $reponse_data = array();
        $doctory_history_details = array();
        $patient_details = array();

        $json ['status'] = 'true';
        $json ['msg'] = 'Failed!! To get patiend History Details';
        $json ['details'] = $reponse_data;
        $json ['patient_details'] = $patient_details;

        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        if (array_key_exists('call_details_id', $_POST)) {
            $call_details_id = mysql_real_escape_string($_POST ['call_details_id']);
        }
        if (array_key_exists('doctor_id', $_POST)) {
            $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        }
        if (array_key_exists('patient_id', $_POST)) {
            $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        }

        if ($doctor_id && $patient_id && ($doctor_id != '' || $patient_id != '')) {
            $query = "select ccd.*,dh.history_data as history_data,d.pid as doctor_id,d.name as doctor_name,d.education as doctor_education,d.speciality as doctor_speciality from crm_call_details as ccd left join crm_doctor_history as dh on ccd.pid=dh.call_detal_id left join crm_doctors as d on d.pid=dh.doctor_id where dh.doctor_id='" . $doctor_id . "' and ccd.patient_id ='" . $patient_id . "' order by ccd.pid  limit 0,1";

            $doctory_history_details = $PDO->db_query($query);
            // $doctory_history_details = $PDO->db_fetch_array($doctory_history_details);
        }
        if (mysql_num_rows($doctory_history_details) > 0) {

            $i = 0;
            while ($list = $PDO->db_fetch_array($doctory_history_details)) {
                if ($i < count($doctory_history_details)) {
                    $patient_history = json_decode($list ['history_data']);
                    // print_r($patient_history->chief_complaint);exit();
                    $reponse_data [$i] ['call_detail_id'] = ($patient_history->call_detal_id === NULL) ? '' : $patient_history->call_detal_id;
                    $reponse_data [$i] ['chief_complaint'] = ($patient_history->chief_complaint === NULL) ? '' : $patient_history->chief_complaint;
                    $reponse_data [$i] ['history_of_present_illness'] = ($patient_history->history_of_present_illness === NULL) ? '' : $patient_history->history_of_present_illness;
                    $reponse_data [$i] ['past_medical_history'] = ($patient_history->past_medical_history === NULL) ? '' : $patient_history->past_medical_history;
                    $reponse_data [$i] ['past_surgical_history'] = ($patient_history->past_surgical_histor === NULL) ? '' : $patient_history->past_surgical_histor;
                    $reponse_data [$i] ['social_history'] = ($patient_history->social_history === NULL) ? '' : $patient_history->social_history;
                    $reponse_data [$i] ['family_history'] = ($patient_history->family_history === NULL) ? '' : $patient_history->family_history;
                    $reponse_data [$i] ['occupational_history'] = ($patient_history->occupational_history === NULL) ? '' : $patient_history->occupational_history;
                    $reponse_data [$i] ['history_of_drug_allergy'] = ($patient_history->history_of_drug_allergy === NULL) ? '' : $patient_history->history_of_drug_allergy;
                    $reponse_data [$i] ['drug_allergy_comment'] = ($patient_history->drug_allergy_comment === NULL) ? '' : $patient_history->drug_allergy_comment;
                    $reponse_data [$i] ['drug_allergy_regular_taking_medication'] = ($patient_history->drug_allergy_regular_taking_medication === NULL) ? '' : $patient_history->drug_allergy_regular_taking_medication;
                    $reponse_data [$i] ['bp'] = ($patient_history->bp === NULL) ? '' : $patient_history->bp;
                    $reponse_data [$i] ['pr'] = ($patient_history->pr === NULL) ? '' : $patient_history->pr;
                    $reponse_data [$i] ['sao'] = ($patient_history->sao === NULL) ? '' : $patient_history->sao;
                    $reponse_data [$i] ['rbs'] = ($patient_history->rbs === NULL) ? '' : $patient_history->rbs;
                    $reponse_data [$i] ['temp'] = ($patient_history->temp === NULL) ? '' : $patient_history->temp;
                    $reponse_data [$i] ['provisional_diagnosis'] = ($patient_history->provisional_diagnosis === NULL) ? '' : $patient_history->provisional_diagnosis;
                    $reponse_data [$i] ['is_rated'] = ($patient_history->doctor_rated > 0) ? '1' : '0';
                    $reponse_data [$i] ['doctorName'] = ($patient_history->doctor_name === NULL) ? '' : $patient_history->doctor_name;
                    $reponse_data [$i] ['doctorEducation'] = ($patient_history->doctor_education === NULL) ? '' : $patient_history->doctor_education;
                    $reponse_data [$i] ['doctorSpeciality'] = ($patient_history->doctor_speciality === NULL) ? '' : $PDO->getSingleResult("select name from #_specialities where pid='" . $patient_history ['doctor_speciality'] . "'");;
                }

                $i++;
            }
        }
        if ($patient_id) {
            $patient_details = $doctor_function->getPatientDetails($patient_id);
            {
                $json ['patient_details'] = $patient_details;
                $json ['msg'] = 'Sucess!! To get patiend History Details';
            }
        }
        $json ['details'] = $reponse_data;
        break;

    case 'patienthistorydetails' :

        $call_details_id = NULL;
        $doctor_id = NULL;
        $patient_id = NULL;
        $reponse_data = array();
        $doctory_history_details = array();
        $patient_details = array();

        $json ['status'] = 'true';
        $json ['msg'] = 'Failed!! To get patiend History Details';
        $json ['details'] = $reponse_data;
        $json ['patient_details'] = $patient_details;

        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        if (array_key_exists('call_details_id', $_POST)) {
            $call_details_id = mysql_real_escape_string($_POST ['call_details_id']);
        }
        if (array_key_exists('doctor_id', $_POST)) {
            $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        }
        if (array_key_exists('patient_id', $_POST)) {
            $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        }

        if ($patient_id && $call_details_id && $call_details_id != '' && $patient_id != '') {
            $query = "select ccd.*,d.pid as doctor_id,d.name as doctor_name,d.education as doctor_education,d.speciality as doctor_speciality,app.appointment_type from crm_call_details as ccd left join crm_appointment as app on app.call_details_id=ccd.pid left join crm_doctors as d on d.pid=app.doctor_id
				 where app.call_details_id='" . $call_details_id . "' AND ccd.patient_id ='" . $patient_id . "' order by ccd.pid  limit 0,1";

            $doctory_history_details = $PDO->db_query($query);
            // $doctory_history_details = $PDO->db_fetch_array($doctory_history_details);
        }

        if (mysql_num_rows($doctory_history_details) > 0) {

            $i = 0;
            while ($row = $PDO->db_fetch_array($doctory_history_details)) {
                if ($i < count($doctory_history_details)) {

                    $reponse_data [$i] ['call_detail_id'] = ($row ['pid'] === NULL) ? '' : $row ['pid'];
                    $reponse_data [$i] ['virtual_type'] = ($row ['appointment_type'] === NULL) ? '' : $row ['appointment_type'];
                    $reponse_data [$i] ['chief_complaint'] = ($row ['chief_complaint'] === NULL) ? '' : $row ['chief_complaint'];
                    $reponse_data [$i] ['history_of_present_illness'] = ($row ['history_of_present_illness'] === NULL) ? '' : $row ['history_of_present_illness'];
                    $reponse_data [$i] ['past_medical_history'] = ($row ['past_medical_history'] === NULL) ? '' : $row ['past_medical_history'];
                    $reponse_data [$i] ['past_surgical_history'] = ($row ['past_surgical_history'] === NULL) ? '' : $row ['past_surgical_history'];
                    $reponse_data [$i] ['social_history'] = ($row ['social_history'] === NULL) ? '' : $row ['social_history'];
                    $reponse_data [$i] ['family_history'] = ($row ['family_history'] === NULL) ? '' : $row ['family_history'];
                    $reponse_data [$i] ['occupational_history'] = ($row ['occupational_history'] === NULL) ? '' : $row ['occupational_history'];
                    $reponse_data [$i] ['history_of_drug_allergy'] = ($row ['history_of_drug_allergy'] === NULL) ? '' : $row ['history_of_drug_allergy'];
                    $reponse_data [$i] ['drug_allergy_comment'] = ($row ['drug_allergy_comment'] === NULL) ? '' : $row ['drug_allergy_comment'];
                    $reponse_data [$i] ['drug_allergy_regular_taking_medication'] = ($row ['drug_allergy_regular_taking_medication'] === NULL) ? '' : $row ['drug_allergy_regular_taking_medication'];
                    $reponse_data [$i] ['bp'] = ($row ['bp'] === NULL) ? '' : $row ['bp'];
                    $reponse_data [$i] ['pr'] = ($row ['pr'] === NULL) ? '' : $row ['pr'];
                    $reponse_data [$i] ['sao'] = ($row ['sao'] === NULL) ? '' : $row ['sao'];
                    $reponse_data [$i] ['rbs'] = ($row ['rbs'] === NULL) ? '' : $row ['rbs'];
                    $reponse_data [$i] ['temp'] = ($row ['temp'] === NULL) ? '' : $row ['temp'];
                    $reponse_data [$i] ['provisional_diagnosis'] = ($row ['provisional_diagnosis'] === NULL) ? '' : $row ['provisional_diagnosis'];
                    $reponse_data [$i] ['is_rated'] = ($row ['doctor_rated'] > 0) ? '1' : '0';
                    $reponse_data [$i] ['doctorName'] = ($row ['doctor_name'] === NULL) ? '' : $row ['doctor_name'];
                    $reponse_data [$i] ['doctorEducation'] = ($row ['doctor_education'] === NULL) ? '' : $row ['doctor_education'];
                    if ($row ['doctor_speciality'] == 0) {
                        $reponse_data [$i] ['doctorSpeciality'] = 'General Practitioner';
                    } else {
                        $reponse_data [$i] ['doctorSpeciality'] = ($row ['doctor_speciality'] === NULL) ? '' : $PDO->getSingleResult("select name from #_specialities where pid='" . $row ['doctor_speciality'] . "'");
                    }

                }

                $i++;
            }
        }
        if ($patient_id) {
            $patient_details = $doctor_function->getPatientDetails($patient_id);
            {
                $json ['patient_details'] = $patient_details;
                $json ['msg'] = 'Sucess!! To get patiend History Details';
            }
        }
        $json ['details'] = $reponse_data;
        break;

    case 'uploadPatientRecord_old' :
        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        $report_img = mysql_real_escape_string($_POST ['report_img']);
        $report_title = mysql_real_escape_string($_POST ['report_title']);

        /*
		 * $query="select dh*,ccd.* from #_doctor_history as dh join #_crm_call_details as ccd on dh.caller_id=ccd.call_detal_id
		 * where dh.doctor_id=".$doctor_id." AND dh.call_detal_id ='".$call_details_id."' order by ccd.pid desc limit 0,1";
		 * $doctory_history_details = $PDO->db_query($query);
		 */
        $json ['status'] = 0;

        $file_image = '';
        $img = $report_img;
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);

        $file_name = strtotime(data('Y-m-d H:i:s')) . "-" . $doctor_id . ".jpg";
        $upload_path = "crm/uploades_files/reports/" . $file_name;
        if (file_exists($upload_path)) {
            $upload_path .= "-$patient_id";
            $file_name = strtotime(data('Y-m-d H:i:s')) . "-" . $doctor_id . "-" . $patient_id . ".jpg";
        }

        $success = file_put_contents($upload_path, $data);
        if ($success) {
            $file_image = $file_name;
        } else {
            $file_image = '';
        }

        $query = "insert into crm_call_details  set  medicine_id='',caller_id=0,subscription_details_id=0,service_type='Lab/Imaging/Reports',sub_center_check=' ',sub_centers=0,services_comment='',call_type='',service_provided='referral service to',referral_service_to='Reports',
				follow_up_call_schedule='',follow_up_call_date='',follow_up_call_time='',health_consultation='',shortorder=0,status=1,tf_1='',tf_2='',create_by='" . $doctor_id . "',
				patient_id ='" . $patient_id . "',call_by='crm', chief_complaint ='' , history_of_present_illness ='', past_medical_history='', past_surgical_history='',
				social_history='', family_history='', occupational_history='' ,history_of_drug_allergy='',drug_allergy_comment='' , drug_allergy_regular_taking_medication='',
				bp='',pr='',sao='',weight='',rbs='',temp='',provisional_diagnosis='',create_by_type='Doctors',created_on='" . date('Y-m-d H:i:s') . "'";
        $call_details = $PDO->db_query($query);

        $call_details_id = $PDO->db_query("SELECT LAST_INSERT_ID() as id FROM crm_call_details");
        $call_details_id = $PDO->db_fetch_array($call_details_id);
        if ($call_details_id) {
            $query = "update #_call_details  set  caller_id ='" . $call_details_id ['id'] . "' Where pid='" . $call_details_id ['id'] . "'";

            $update_call_details = $PDO->db_query($query);

            $short_order = $PDO->db_query("SELECT shortorder FROM #_reports order by pid desc limit 0,1");
            $short_order = $PDO->db_fetch_array($short_order);

            $query = "insert into #_reports  set  status=1,shortorder=" . $short_order ['shortorder'] . ", call_details_id='" . $call_details_id ['id'] . "',reports='" . $file_name . "' create_by ='" . $doctor_id . "',user_type='Doctor', service_type ='Lab/Imaging/Reports',user_type='Doctors',created_on='" . date('Y-m-d H:i:s') . "'";

            $report_details = $PDO->db_query($query);
            if ($report_details) {
                $json ['status'] = 1;
            }
        }
        break;

    case 'uploadPatientRecord' :
        sleep(2);
        // $report_file = mysql_real_escape_string($_POST ['report_file']);
        $is_digital_prescription = 0;
        $user_type = 'Patient';
        if (array_key_exists('report_title', $_POST)) {
            $report_title = mysql_real_escape_string($_POST ['report_title']);
        } else {
            $report_title = '';
        }
        if ($report_title == '') {
            $is_digital_prescription = 1;
            $report_title = 'DigitalPrescription';
            $user_type = 'Doctor';
        }
        if (array_key_exists('call_details_id', $_POST)) {
            $call_details_id = mysql_real_escape_string($_POST ['call_details_id']);
        }
        if (array_key_exists('file_ext', $_POST)) {
            $file_ext = mysql_real_escape_string($_POST ['file_ext']);
        }
        if (array_key_exists('doctor_id', $_POST)) {
            $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        }
        if (array_key_exists('patient_id', $_POST)) {
            $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        }
        /*
		 * $query="select dh*,ccd.* from #_doctor_history as dh join #_crm_call_details as ccd on dh.caller_id=ccd.call_detal_id
		 * where dh.doctor_id=".$doctor_id." AND dh.call_detal_id ='".$call_details_id."' order by ccd.pid desc limit 0,1";
		 * $doctory_history_details = $PDO->db_query($query);
		 */
        $json ['status'] = 0;

        // $path = $_SERVER['SERVER_NAME']."/crm/uploaded_files/reports/";
        $path = UP_FILES_FS_PATH . "/reports/";

        $report_file_name = '';
        if ($_FILES ['report_file'] ['name']) {

            $report_file_name = $BSC->uploadFile($path, $_FILES ['report_file'] ['name'], 'report_file');
        }
        if ($report_file_name != '') {
            $short_order = $PDO->db_query("SELECT shortorder FROM #_reports order by pid desc limit 0,1");
            $short_order = $PDO->db_fetch_array($short_order);

            $query = "insert into #_reports  set  title='" . $report_title . "',is_digital_prescription='" . $is_digital_prescription . "',status=1,shortorder=" . $short_order ['shortorder'] . ", call_details_id='" . $call_details_id . "',reports='" . $report_file_name . "', create_by ='" . $doctor_id . "',user_type='" . $user_type . "',created_on='" . date('Y-m-d H:i:s') . "',modified_on='" . date('Y-m-d H:i:s') . "'";

            $report_details = $PDO->db_query($query);
            if ($report_details) {
                $json ['status'] = 1;
            }
        }
        break;

    case 'getUploadedPatientRecords' :
        $call_details_id = NULL;
        $patient_id = NULL;
        $reponse_data = array();
        $uploaded_patients_reports = array();
        $patient_name = '';
        $doctor_name = '';
        $start_offset = 0;
        /*
		 * $doctor_id =mysql_real_escape_string($_POST['doctor_id']);
		 * $patient_id =mysql_real_escape_string($_POST['patient_id']);
		 * $call_details_id =mysql_real_escape_string($_POST['call_details_id']);
		 */

        if (array_key_exists('call_details_id', $_POST)) {
            $call_details_id = mysql_real_escape_string($_POST ['call_details_id']);
        }
        if (array_key_exists('doctor_id', $_POST)) {
            $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        }
        if (array_key_exists('start_offset', $_POST)) {
            $start_offset = mysql_real_escape_string($_POST ['start_offset']);
        }
        //$last = $start_offset + PAGINATION_END_LIMIT;
        $limit = " limit " . $start_offset . "," . PAGINATION_END_LIMIT;
        if ($call_details_id && ($patient_id == '' || $patient_id === null || empty ($patient_id))) {
            $query = "select cr.* ,bam.patient_id as 'patient_id', bam.doctor_id as 'doctor_id' from #_reports as cr left join #_book_app_mapping as bam on bam.call_details_id=cr.call_details_id where cr.call_details_id='" . $call_details_id . "' group by cr.pid desc " . $limit;
        } else if ($patient_id && ($call_details_id == '' || $call_details_id === null || empty ($call_details_id))) {
            $query = "select cr.* ,bam.patient_id as 'patient_id', bam.doctor_id as 'doctor_id' from #_book_app_mapping as bam left join #_reports as cr on bam.call_details_id=cr.call_details_id where bam.patient_id='" . $patient_id . "' group by cr.pid desc " . $limit;
        }

        $uploaded_patients_reports = $PDO->db_query($query);

        if (mysql_num_rows($uploaded_patients_reports) > 0) {
            $i = 0;
            while ($list = $PDO->db_fetch_array($uploaded_patients_reports)) {
                if ($i <= mysql_num_rows($uploaded_patients_reports)) {
                    $patient_name = $PDO->getSingleResult("select name from #_patients where pid='" . $list ['patient_id'] . "'");
                    $doctor_name = $PDO->getSingleResult("select name from #_doctors where pid='" . $list ['doctor_id'] . "'");
                    /*
					 * $reponse_data[$i]['title'] =$list['title'];
					 * $reponse_data[$i]['reports_image']= $_SERVER['SERVER_NAME']."/crm/uploaded_files/reports/".$list['reports'];
					 * $reponse_data[$i]['call_details_id']= $list['call_details_id'];
					 * $reponse_data[$i]['report_id']= $list['pid'];
					 */
                    $reponse_data [] = array(
                        'title' => $list ['title'],
                        'reports_image' => SITE_PATH . "uploaded_files/reports/" . $list ['reports'],
                        'call_details_id' => $list ['call_details_id'],
                        'report_id' => $list ['pid'],
                        'report_date' => $list ['created_on'],
                        'patient_name' => $patient_name,
                        'doctor_name' => $doctor_name
                    );
                }
                $i++;
            }
        }
        $json ['details'] = $reponse_data;

        break;

    case 'updateRatingComments' :
        $call_details_id = NULL;
        $doctor_rated = NULL;
        $doctor_commets = NULL;

        $json ['status'] = 'false';
        $json ['msg'] = 'Failed!! to Rate the Appointment. Try Again!!!!';

        $doctor_rated = mysql_real_escape_string($_POST ['doctor_rated']);
        $doctor_comments = mysql_real_escape_string($_POST ['doctor_comments']);
        $call_details_id = mysql_real_escape_string($_POST ['call_details_id']);

        if ($call_details_id) {
            $query = "update #_call_details  set  doctor_rated ='" . $doctor_rated . "', doctor_rated_comments='" . $doctor_comments . "'  Where pid='" . $call_details_id . "'";

            $update_call_details = $PDO->db_query($query);
            if ($update_call_details) {
                $json ['status'] = 'true';
                $json ['msg'] = 'Success!! Appointment rated.';
            }
        }
        break;

    case 'save_patient_medicine_details':
        $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $appointment_id = mysql_real_escape_string($_POST ['appointment_id']);
        $call_details_id = mysql_real_escape_string($_POST ['call_details_id']);
        $appointment_id = $PDO->getSingleResult("select pid from #_appointment where call_details_id='" . $call_details_id . "'");
        $drugs = json_encode(mysql_real_escape_string($_POST ['drugs']));
        $modified_on = date('Y-m-d H:i:s');
        $patient_medicine_details = array(
            'patient_id' => $patient_id,
            'doctor_id' => $doctor_id,
            'appointment_id' => $appointment_id,
            'call_details_id' => $call_details_id,
            'drugs' => $drugs,
            'modified_on' => $modified_on
        );
        $result = $PDO->sqlquery("rs", 'patient_medicine_details', $patient_medicine_details);
        if (!empty($result)) {
            $json ['status'] = 'true';
            $json ['msg'] = 'Success!! Patient Medicine details added.';
        } else {
            $json ['status'] = 'false';
            $json ['msg'] = 'Failure!! Patient Medicine details not added.';
        }

        break;

    case 'patient_medicine_details_list':
        $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        $wh = "and patient_id='" . $patient_id . "' ";
        if (array_key_exists('doctor_id', $_POST)) {
            $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
            $wh = "and doctor_id='" . $doctor_id . "' ";
        }
        $query = "select * from #_patient_medicine_details where is_deleted=0 AND status=1 " . $wh . " ORDER BY appointment_id desc";
        // print_r($query);exit();
        $result = $PDO->db_query($query);
        $medicine_data_list = array();
        if (mysql_num_rows($result) > 0) {
            $i = 0;
            while ($medicine_data = $PDO->db_fetch_array($result)) {
                $medicine_data_list [$i] ['patient_id'] = $medicine_data ['patient_id'];
                $medicine_data_list [$i] ['doctor_id'] = $medicine_data ['doctor_id'];
                $medicine_data_list [$i] ['appointment_id'] = $medicine_data ['appointment_id'];
                $medicine_data_list [$i] ['call_details_id'] = $medicine_data ['call_details_id'];
                $medicine_data_list [$i] ['drugs'] = json_decode($medicine_data ['drugs']);
                $i++;
            }
        }

        $json ['details'] = $medicine_data_list;


        break;
    case 'medicine_list' :

        // initialize arrays
        $json ['medicine_list'] = array();
        // Get doctor types from db
        $medicine_list_values = $doctor_function->medicine_list();
        if ($medicine_list_values != 'false') {
            $json ['medicine_list'] = $medicine_list_values;
            $json ['status'] = 'true';
        } else {
            $json ['status'] = 'false';
        }

        break;

    default :
        break;
}
/* Output header */
// header('Content-type: application/json');
// echo json_encode($_POST);
echo json_encode($json);
?>