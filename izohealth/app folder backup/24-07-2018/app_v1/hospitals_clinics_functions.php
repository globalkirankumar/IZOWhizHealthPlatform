<?php
// error_reporting ( - 1 );
include("../lib/open.inc.php");
include 'class/all_class_files.php';
include_once 'notification/GCM.php';
$gcm = new GCM ();
$m = new MyMail ();
$json = array();
$flag = mysql_real_escape_string($_POST ['flag']);
// initiate doctor object
$doctor_function = new doctor ();
date_default_timezone_set('Asia/Kolkata');

switch ($flag) {

    case 'addclinics' :

        $email_check = NULL;
        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $name = mysql_real_escape_string($_POST ['name']);
        $email = mysql_real_escape_string($_POST ['email']);
        $password = mysql_real_escape_string($_POST ['password']);
        $address = mysql_real_escape_string($_POST ['address']);
        $latlong = mysql_real_escape_string($_POST ['latlong']);
        $phone = mysql_real_escape_string($_POST ['phone']);
        $nearest_land_mark = mysql_real_escape_string($_POST ['nearest_land_mark']);
        $nearest_bus_stop = mysql_real_escape_string($_POST ['nearest_bus_stop']);
        $postal_code = mysql_real_escape_string($_POST ['postal_code']);
        $township = mysql_real_escape_string($_POST ['township']);
        $division = mysql_real_escape_string($_POST ['division']);

        $contact_person_name = $PDO->getSingleResult("select name from #_doctors where pid='" . $doctor_id . "'");
        $contact_person_email = $PDO->getSingleResult("select email from #_doctors where pid='" . $doctor_id . "'");
        $contact_person_phone = $PDO->getSingleResult("select phone from #_doctors where pid='" . $doctor_id . "'");

        $clinic_open_time = mysql_real_escape_string($_POST ['clinic_open_time']);
        $clinic_close_time = mysql_real_escape_string($_POST ['clinic_close_time']);
        $clinic_condition = 'Good'; // mysql_real_escape_string ( $_POST ['clinic_condition'] );

        $home_visit_service = mysql_real_escape_string($_POST ['home_visit_service']);
        $home_visit_coverage_area = mysql_real_escape_string($_POST ['home_visit_coverage_area']);
        $home_visit_available_start_time = mysql_real_escape_string($_POST ['home_visit_available_start_time']);
        $home_visit_available_end_time = mysql_real_escape_string($_POST ['home_visit_available_end_time']);
        $home_visit_charges = mysql_real_escape_string($_POST ['home_visit_charges']);
        $status = mysql_real_escape_string($_POST ['status']);

        $left_services = '';
        $imagin_services = '';
        $services = '';
        $data = array();

        if ($email && $password) {

            // check wether the email is already there
            $email_check = $doctor_function->email_check('users_login', $email);
        }
        if (!$email_check) {
            // This informations are need to store in the user_login table
            $table_name = 'users_login';
            $password = md5($password);
            $data ['password'] = base64_encode($password);
            $data ['email'] = $email;
            $data ['user_type'] = 'hospital';

            $email_check = $doctor_function->signup_step1($data);

            $services = mysql_real_escape_string($_POST ['services']);
            $services_decoded = json_decode(stripslashes($services));

            if (array_key_exists('leb', $services_decoded) && $services_decoded->leb) {
                $left_services = $doctor_function->getServiceTypeList(1);
            }
            if (array_key_exists('imaging', $services_decoded) && $services_decoded->imaging) {
                $imagin_services = $doctor_function->getServiceTypeList(2);
            }
            $clinic_image = mysql_real_escape_string($_POST ['clinic_image']);

            $json ['status'] = 'false';
            $json ['msg'] = "Failed to add clinic, Please try Again.";
            // $path = $_SERVER['SERVER_NAME']."/crm/uploaded_files/reports/";
            $path = UP_FILES_FS_PATH . "/clinics/";

            $clinic_file_name = '';
            if ($_FILES ['image'] ['name']) {

                $clinic_file_name = $BSC->uploadFile2($path, $_FILES ['image'] ['name'], 'image');
            }

            if ($doctor_id) {
                $query = "Insert into #_clinics  set  user_id='" . $doctor_id . "',home_visit_coverage_area_be='',latlong='" . $latlong . "',name_be='',address_be='',nearest_land_mark_be='',nearest_bus_stop_be='',shortorder=0,tf_1='',tf_2='',create_by='0',
				name ='" . $name . "',image='" . $clinic_file_name . "', email ='" . $email . "' , address ='" . $address . "', phone='" . $phone . "', nearest_land_mark='" . $nearest_land_mark . "',
				nearest_bus_stop='" . $nearest_bus_stop . "', postal_code='" . $postal_code . "', township='" . $township . "' ,division='" . $division . "',contact_person_name='" . $contact_person_name . "' , contact_person_email='" . $contact_person_email . "',
				contact_person_phone='" . $contact_person_phone . "',clinic_open_time='" . $clinic_open_time . "',clinic_close_time='" . $clinic_close_time . "',clinic_condition='" . $clinic_condition . "',
				home_visit_service='" . $home_visit_service . "',home_visit_coverage_area='" . $home_visit_coverage_area . "',home_visit_available_start_time='" . $home_visit_available_start_time . "',home_visit_available_end_time='" . $home_visit_available_end_time . "',
				home_visit_charges='" . $home_visit_charges . "',services='" . $services . "',left_services='" . $left_services . "',imagin_services='" . $imagin_services . "',status='" . $status . "',
				created_on='" . date('Y-m-d H:i:s') . "',modified_on='" . date('Y-m-d H:i:s') . "'";

                $insert_clincs = $PDO->db_query($query);
                $short_order = $PDO->db_fetch_array($insert_clincs);

                $history_data_array = json_encode($history_data_array);

                $short_order = $PDO->db_query("update #_clinics set  shortorder='" . $short_order ['pid'] . "' where pid='" . $short_order ['pid'] . "'");
                $short_order = $PDO->getSingleResult("select max(shortorder) from #_clinics ");
                $short_order = $short_order + 1;
                $last_inserted_id = $PDO->getSingleResult("select max(pid) from #_clinics");

                $short_order = $PDO->db_query("update #_clinics set  shortorder='" . $short_order . "' where pid='" . $last_inserted_id . "'");

                if (!empty ($last_inserted_id)) {
                    $json ['status'] = 'true';
                    $json ['msg'] = "Successfully Added Clinic.Wait untill admin verification process to complete.";

                    // send mail to the user

                    $table_name = 'doctors';
                    $doctor_details = $doctor_function->doctor_details($doctor_id);
                    $doctor_name = $doctor_details ['name'];
                    $doctor_email = $doctor_details ['email'];
                    $reply_email = 'noreply@doctoroncall.com.mm';
                    $reply_name = 'noreplydoc';
                    $from_email = 'doctorapp@doctoroncall.com.mm';
                    $from_name = 'Doctor APP';
                    $to_email = $doctor_email;
                    $to_name = 'Admin';
                    $subject = 'Add Clinic ';

                    // Send Mail
                    $message_content = 'Your Clinic $name is registered. <br>Please wait untill admin verification';
                    $email_details = array(
                        'from_email' => 'doctor@doctoroncall.com.mm',
                        'from_name' => 'Doctor CRM',
                        'to_email' => $to_email,
                        'to_name' => $to_name,
                        'subject' => 'Add Clinic',
                        'message_content' => $message_content
                    );

                    $send_admin_Email = $m->sendMail($email_details);
                    // Send Mail to admin
                    $admin_details = $doctor_function->get_all_admin_user_details();
                    // print_r($admin_details);
                    foreach ($admin_details as $row) {
                        // echo $row['pid'];
                        $admin_email = $row ['email'];
                        $table_name = 'doctors';
                        $doctor_details = $doctor_function->doctor_details($doctor_id);
                        $doctor_name = $doctor_details ['name'];
                        $reply_email = 'noreply@doctoroncall.com.mm';
                        $reply_name = 'noreplydoc';
                        $from_email = 'doctorapp@doctoroncall.com.mm';
                        $from_name = 'Doctor APP';
                        $to_email = $admin_email;
                        $to_name = 'Admin';
                        $subject = 'Add Clinics Verification Request';

                        // http://www.doctoroncall.com.mm/crm/home.php?comp=clinics&mode=add&uid=295
                        $verification_crm_url = SITE_PATH_ADM . 'home.php?comp=clinics&mode=add&uid=' . $last_inserted_id;
                        // Send Mail
                        $message_content = "New Clinic $name is registered via doctor app by $doctor_name. <br>Please verify their details and activate the hospital.<br><a href='$verification_crm_url'>Verify</a>";
                        $email_details = array(
                            'from_email' => 'doctor@doctoroncall.com.mm',
                            'from_name' => 'Doctor CRM',
                            'to_email' => $to_email,
                            'to_name' => $to_name,
                            'subject' => $subject,
                            'message_content' => $message_content
                        );
                    }

                    if ($send_admin_Email == true) {
                        $json ['mail_status'] = 'true';
                    } else {

                        $json ['mail_status'] = 'false';
                        $json ['mail_status_msg'] = $new_mail->ErrorInfo;
                    }
                }
            }
        } else {
            $json ['status'] = 'false';
            $json ['msg'] = "Email already exists.";
        }
        break;
    case 'addhospital' :
        $email_check = NULL;
        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $name = mysql_real_escape_string($_POST ['name']);
        $email = mysql_real_escape_string($_POST ['email']);
        $password = mysql_real_escape_string($_POST ['password']);
        $address = mysql_real_escape_string($_POST ['address']);
        $latlong = mysql_real_escape_string($_POST ['latlong']);
        $phone = mysql_real_escape_string($_POST ['phone']);
        $nearest_land_mark = mysql_real_escape_string($_POST ['nearest_land_mark']);
        $nearest_bus_stop = mysql_real_escape_string($_POST ['nearest_bus_stop']);
        $postal_code = mysql_real_escape_string($_POST ['postal_code']);
        $township = mysql_real_escape_string($_POST ['township']);
        $division = mysql_real_escape_string($_POST ['division']);
        $hospital_image = mysql_real_escape_string($_POST ['hospital_image']);
        $hot_line_number = mysql_real_escape_string($_POST ['hot_line_number']);

        $contact_person_email = mysql_real_escape_string($_POST ['ambulance_service']);
        $contact_person_phone = mysql_real_escape_string($_POST ['medical_check_up_package']);

        $clinic_open_time = mysql_real_escape_string($_POST ['clinic_open_time']);
        $clinic_close_time = mysql_real_escape_string($_POST ['clinic_close_time']);
        $clinic_condition = mysql_real_escape_string($_POST ['clinic_condition']);

        $home_visit_service = mysql_real_escape_string($_POST ['home_visit_service']);
        $home_visit_coverage_area = mysql_real_escape_string($_POST ['home_visit_coverage_area']);
        $home_visit_available_start_time = mysql_real_escape_string($_POST ['home_visit_available_start_time']);
        $home_visit_available_end_time = mysql_real_escape_string($_POST ['home_visit_available_end_time']);
        $home_visit_service_charges = mysql_real_escape_string($_POST ['home_visit_service_charges']);
        $status = mysql_real_escape_string($_POST ['status']);

        $left_services = '';
        $imagin_services = '';
        $services = '';

        $data = array();

        if ($email && $password) {

            // check wether the email is already there
            $email_check = $doctor_function->email_check('users_login', $email);
        }
        if (!$email_check) {
            // This informations are need to store in the user_login table
            $table_name = 'users_login';
            $password = md5($password);
            $data ['password'] = base64_encode($password);
            $data ['email'] = $email;
            $data ['user_type'] = 'hospital';

            $services = mysql_real_escape_string($_POST ['services']);
            $services_decoded = json_decode(stripslashes($services));

            if (array_key_exists('leb', $services_decoded) && $services_decoded->leb) {
                $left_services = $doctor_function->getServiceTypeList(1);
            }
            if (array_key_exists('imaging', $services_decoded) && $services_decoded->imaging) {
                $imagin_services = $doctor_function->getServiceTypeList(2);
            }

            $healthcare_services = mysql_real_escape_string($_POST ['healthcare_services']);

            $json ['status'] = 'false';
            $json ['msg'] = "Failed to add hospital, Please try Again.";
            // $path = $_SERVER['SERVER_NAME']."/crm/uploaded_files/reports/";
            $path = UP_FILES_FS_PATH . "/hospital/";
            $resize = UP_FILES_FS_PATH . "/hospital/217X279";

            $hospital_file_name = '';

            if ($_FILES ['image'] ['name'] != null) {
           // if (isset($_FILES ['image'] ['name']) && ($_FILES ['image'] ['name']) != null) {
                $hospital_file_name = $BSC->uploadFile2($path, $_FILES ['image'] ['name'], 'image');
                $magicianObj = new imageLib ($path . '/' . $hospital_file_name);
                $magicianObj->resizeImage(217, 279, 'landscape');
                $magicianObj->saveImage($resize . '/' . $hospital_file_name, 70);
            } else {
                $hospital_file_name = '';
            }

            if ($doctor_id) {

                $email_check = $doctor_function->signup_step1($data);

                $query = "Insert into #_hospitals  set  user_id='" . $doctor_id . "',medical_check_up_package_be='',latlong='" . $latlong . "',name_be='',home_visit_coverage_area_be='',ambulance_service_be='',address_be='',nearest_land_mark_be='',nearest_bus_stop_be='',shortorder=0,tf_1='',tf_2='',create_by='0',
				name ='" . $name . "',image='" . $hospital_file_name . "', email ='" . $email . "' , address ='" . $address . "', phone='" . $phone . "', nearest_land_mark='" . $nearest_land_mark . "',
				nearest_bus_stop='" . $nearest_bus_stop . "', postal_code='" . $postal_code . "', township='" . $township . "' ,division='" . $division . "',healthcare_services='" . $healthcare_services . "' , 
				home_visit_service='" . $home_visit_service . "',home_visit_coverage_area='" . $home_visit_coverage_area . "',home_visit_available_start_time='" . $home_visit_available_start_time . "',home_visit_available_end_time='" . $home_visit_available_end_time . "',
				home_visit_service_charges='" . $home_visit_service_charges . "',hot_line_number='" . $hot_line_number . "',medical_check_up_package='" . $medical_check_up_package . "',ambulance_service='" . $ambulance_service . "',
				services='" . $services . "',left_services='" . $left_services . "',imagin_services='" . $imagin_services . "',status='" . $status . "',
				created_on='" . date('Y-m-d H:i:s') . "',modified_on='" . date('Y-m-d H:i:s') . "'";

                $insert_hospitals = $PDO->db_query($query);

                // $short_order = $PDO->db_fetch_array ( $insert_hospitals );
                $short_order = $PDO->getSingleResult("select max(shortorder) from #_hospitals ");
                $short_order = $short_order + 1;
                $last_inserted_id = $PDO->getSingleResult("select max(pid) from #_hospitals");

                $short_order = $PDO->db_query("update #_hospitals set  shortorder='" . $short_order . "' where pid='" . $last_inserted_id . "'");

                $json ['status'] = 'true';
                $json ['msg'] = "Successfully Added Hospital.Wait Until admin verification process done.";

                if (!empty ($last_inserted_id)) {

                    // send mail to the user

                    $table_name = 'doctors';
                    $doctor_details = $doctor_function->doctor_details($doctor_id);
                    $doctor_name = $doctor_details ['name'];
                    $doctor_email = $doctor_details ['email'];
                    $reply_email = 'noreply@doctoroncall.com.mm';
                    $reply_name = 'noreplydoc';
                    $from_email = 'doctorapp@doctoroncall.com.mm';
                    $from_name = 'Doctor APP';
                    $to_email = $doctor_email;
                    $to_name = 'Admin';
                    $subject = 'Add Hospital ';

                    // Send Mail
                    $message_content = "Your hospital $name is registered. <br>Please wait untill admin verification";
                    $email_details = array(
                        'from_email' => 'doctor@doctoroncall.com.mm',
                        'from_name' => 'Doctor CRM',
                        'to_email' => $to_email,
                        'to_name' => $to_name,
                        'subject' => $subject,
                        'message_content' => $message_content
                    );

                    $send_admin_Email = $m->sendMail($email_details);

                    // Send Mail to admin
                    $admin_details = $doctor_function->get_all_admin_user_details();
                    // print_r($admin_details);
                    foreach ($admin_details as $row) {
                        // echo $row['pid'];
                        $admin_email = $row ['email'];
                        $table_name = 'doctors';
                        $doctor_details = $doctor_function->doctor_details($doctor_id);
                        $doctor_name = $doctor_details ['name'];
                        $reply_email = 'noreply@doctoroncall.com.mm';
                        $reply_name = 'noreplydoc';
                        $from_email = 'doctorapp@doctoroncall.com.mm';
                        $from_name = 'Doctor APP';
                        $to_email = $admin_email;
                        $to_name = 'Admin';
                        $subject = 'Add Hospital Verification Request';

                        // http://www.doctoroncall.com.mm/crm/home.php?comp=hospitals&mode=add&uid=106295
                        // $verification_crm_url=SITE_PATH_ADM.'home.php?comp=clinics&mode=add&uid='.$insert_hospitals;
                        $verification_crm_url = SITE_PATH_ADM . 'home.php?comp=hospitals&mode=add&uid=' . $last_inserted_id;

                        // Send Mail
                        $message_content = "New hospital $name is registered via doctor app by $doctor_name. <br>Please verify their details and activate the hospital.<br><a href='$verification_crm_url'>Verify</a>";
                        $email_details = array(
                            'from_email' => 'doctor@doctoroncall.com.mm',
                            'from_name' => 'Doctor CRM',
                            'to_email' => $to_email,
                            'to_name' => $to_name,
                            'subject' => $subject,
                            'message_content' => $message_content
                        );

                        $send_admin_Email = $m->sendMail($email_details);
                    }

                    if ($send_admin_Email == true) {
                        $json ['mail_status'] = 'true';
                    } else {

                        $json ['mail_status'] = 'false';
                        $json ['mail_status_msg'] = $new_mail->ErrorInfo;
                    }
                } else {
                    $json ['status'] = 'true';
                    $json ['msg'] = "Successfully Added Hospital.Wait untill admin verification process to complete.";
                }
            }
        } else {
            $json ['status'] = 'false';
            $json ['msg'] = "Email already exists.";
        }
        break;
    case 'specialities' :
        $lang_flag = mysql_real_escape_string($_POST ['lang_flag']);
        $query = $PDO->db_query("select * from #_specialities  where status=1 order by name asc ");
        $json ['specialities'] = array();
        if (mysql_num_rows($query) > 0) {
            $i = 0;
            $data = array();
            while ($row = $PDO->db_fetch_array($query)) {
                $data [$i] ['pid'] = $row ['pid'];
                if ($lang_flag == "en") {
                    $data [$i] ['name'] = $row ['name'];
                }
                if ($lang_flag == "my") {
                    $data [$i] ['name'] = $row ['name_my'];
                }
                $i++;
            }
            $json ['specialities'] = $data;
            $json ['status'] = 'true';
        } else {
            $json ['status'] = 'false';
        }

        break;
    case 'division' :

        // initialize arrays
        $lang_flag = mysql_real_escape_string($_POST ['lang_flag']);
        $json ['division'] = array();
        // Get division list from db
        $division_values = $doctor_function->division($lang_flag);
        if ($division_values != 'false') {
            $json ['division'] = $division_values;
            $json ['status'] = 'true';
        } else {
            $json ['status'] = 'false';
        }

        break;
    case 'township' :
        $lang_flag = mysql_real_escape_string($_POST ['lang_flag']);
        $division_id = mysql_real_escape_string($_POST ['division']);
        $json ['township'] = array();
        $township_values = $doctor_function->township($division_id, $lang_flag);
        if ($township_values != 'false') {
            $json ['township'] = $township_values;
            $json ['status'] = 'true';
        } else {
            $json ['status'] = 'false';
        }

        break;
    case 'hospital_name_list' :

        // include(FS_ADMIN._MODS."/hospitals/class.inc.php");
        $json ['hospital_name_list'] = array();
        $lang_flag = mysql_real_escape_string($_POST ['lang_flag']);
        $hospital_name_list = $doctor_function->hospital_name_list($lang_flag);
        if ($hospital_name_list != 'false') {
            $json ['hospital_name_list'] = $hospital_name_list;
            $json ['status'] = 'true';
        } else {
            $json ['status'] = 'false';
        }
        break;
    case 'clinics_name_list' :

        // include(FS_ADMIN._MODS."/hospitals/class.inc.php");
        $json ['hospital_name_list'] = array();
        $lang_flag = mysql_real_escape_string($_POST ['lang_flag']);
        $hospital_name_list = $doctor_function->clinics_name_list($lang_flag);
        if ($hospital_name_list != 'false') {
            $json ['clinics_name_list'] = $hospital_name_list;
            $json ['status'] = 'true';
        } else {
            $json ['status'] = 'false';
        }
        break;
    case 'hospital_and_clinic_name_list' :

        $json ['total_list'] = array();
        $lang_flag = mysql_real_escape_string($_POST ['lang_flag']);
        $hospital_name_list = $doctor_function->hospital_name_list($lang_flag);
        $clinic_name_list = $doctor_function->clinics_name_list($lang_flag);
        $total_list = array_merge($hospital_name_list, $clinic_name_list);

        if (($hospital_name_list != 'false') && ($clinic_name_list != 'false')) {
            $json ['total_list'] = $total_list;
            $json ['status'] = 'true';
        } else {
            $json ['status'] = 'false';
        }

        break;
    case 'speciality' :

        $lang_flag = mysql_real_escape_string($_POST ['lang_flag']);
        // initialize arrays
        $json ['speciality'] = array();
        // Get doctor types from db
        $specialities_values = $doctor_function->specialities($lang_flag);
        if ($specialities_values != 'false') {
            $json ['speciality'] = $specialities_values;
            $json ['status'] = 'true';
        } else {
            $json ['status'] = 'false';
        }

        break;
    case 'hospital_clinic_list' :
        $distance = 25;
        $latitude = 0;
        $longitude = 0;
        $hospital_list = array ();
        $clinic_list = array ();
        $patient_id = mysql_real_escape_string ( $_POST ['patient_id'] );
        $latitude = mysql_real_escape_string ( $_POST ['latitude'] );
        $longitude = mysql_real_escape_string ( $_POST ['longitude'] );
        if (array_key_exists ( 'distance', $_POST )) {
            $distance = mysql_real_escape_string ( $_POST ['patient_id'] );
        }
        $json ['status'] = 'true';
        $json ['msg'] = 'No hospital & clinics Found. Try Again!!!';
        $json ['hospital_list'] = $hospital_list;
        $json ['clinic_list'] = $clinic_list;

        if ($latitude > 0 && $longitude > 0) {
            $hospital_list = $doctor_function->getHospitalList ( $latitude, $longitude, $distance );
            if ($hospital_list) {
                $json ['hospital_list'] = $hospital_list;
            }
            $clinic_list = $doctor_function->getClinicList ( $latitude, $longitude, $distance );
            if ($clinic_list) {
                $json ['clinic_list'] = $clinic_list;
            }
            $json ['msg'] = 'No hospital & clinics Found. Try Again!!!';
        } else {
            $json ['msg'] = "Mandtory data value are missed";
        }
        break;

    default :
        break;
}

/* Output header */
// header('Content-type: application/json');
// echo json_encode($_POST);
echo json_encode($json);
?>