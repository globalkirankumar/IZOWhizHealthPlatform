<?php
// error_reporting ( - 1 );
include("../lib/open.inc.php");
include 'class/all_class_files.php';
include_once 'notification/GCM.php';
$gcm = new GCM ();
$m = new MyMail ();
$json = array();
$flag = mysql_real_escape_string($_POST ['flag']);
// initiate doctor object
$doctor_function = new doctor ();
date_default_timezone_set('Asia/Kolkata');

switch ($flag) {


    case 'doctor_register_otp_send' :
        $mobile_number = mysql_real_escape_string($_POST ['mobile_number']);
        $json ['mobile_number'] = $mobile_number;
        $country_code = mysql_real_escape_string($_POST ['country_code']);
        if (($mobile_number == '') || (!is_numeric($mobile_number))) {
            $json ['msg'] = 'mobile number  ber is not valid';
            $json ['status'] = 'false';
            $doctor_function->send_res($json);
            exit ();
        }
        $email_from_db = '';
        $phone_check_query = $PDO->db_query("select * from #_doctors  where phone='" . $mobile_number . "' and country_code='" . $country_code . "' ");
        if (mysql_num_rows($phone_check_query) > 0) {

            $email_from_db = $PDO->getSingleResult("select email from #_doctors where phone='" . $mobile_number . "' and country_code='" . $country_code . "' ");
        }
        if ($email_from_db != '') {
            $json ['status'] = 'true';
            $json ['msg'] = 'This number is already registerd. Please try another .';
            $doctor_function->send_res($json);
            exit ();
        }
        $json ['otp'] = str_pad(( string )rand(0, 999999), 6, '0', STR_PAD_LEFT);
        $signup_otp_details = array(
            'phone' => $mobile_number,
            'otp' => $json ['otp'],
            'status' => 0
        );

        $phone_otp_query = $PDO->db_query("select * from #_signup_otp_details  where  phone='" . $mobile_number . "' ");
        if (mysql_num_rows($phone_otp_query) > 0) {
            $otp_query = $PDO->db_query("update  #_signup_otp_details  SET otp ='" . $json ['otp'] . "' where  phone='" . $mobile_number . "' ");
            $json ['status'] = 'true';
            $json ['msg'] = 'Your OTP is send to you entered phone number.';
        } else {
            $result = $PDO->sqlquery("rs", 'signup_otp_details', $signup_otp_details);
            $mobile = $mobile_number;
            $message = urlencode('OTP(one time password) for signup is :' . $json ['otp']);
            $sendmsg = file_get_contents("http://www.doctoroncall.com.mm/crm/app/sms/send_sms.php?mobile=" . $mobile . "&message=" . $message);
            $json ['status'] = 'true';
            $json ['msg'] = 'Your OTP is send to you entered phone number.';
        }

        break;

    case 'doctor_register_otp_verify' :
        $mobile_number = mysql_real_escape_string($_POST ['mobile_number']);
        $country_code = mysql_real_escape_string($_POST ['country_code']);
        $otp = mysql_real_escape_string($_POST ['otp']);
        $json ['doctor_details'] = array();
        $phone_otp_query = $PDO->db_query("select * from #_signup_otp_details  where  phone='" . $mobile_number . "' and otp='" . $otp . "' ");
        if (mysql_num_rows($phone_otp_query) > 0) {
            $json ['status'] = 'true';
            $json ['msg'] = 'Your OTP is Verified successfully';
            $sql = "DELETE FROM `#_signup_otp_details` WHERE `phone`='" . $mobile_number . "' AND `otp`='" . $otp . "' ";
            $query = $PDO->db_query($sql);

            // check signup step

            $login_type = 0; // 0 email, 1 phone
            if (filter_var($mobile_number, FILTER_VALIDATE_EMAIL)) {
                // valid address
                $login_type = 0;
            } else {
                if (is_numeric($mobile_number)) {
                    $login_type = 1;
                }
            }
            if ($login_type == 1) {
                $email_from_db = $PDO->getSingleResult("select email from #_doctors where phone='" . $mobile_number . "' and country_code='" . $country_code . "'");
                if ($email_from_db != '') {
                    $email = $email_from_db;
                } else {
                    $json ['signup_step'] = 0;
                }
            } else {
                $phone = $PDO->getSingleResult("select phone from #_doctors where email='" . $email . "'");
            }
            $email_check_doc = $doctor_function->signup_email_check('doctors', $email);
            $doctor_id = $PDO->getSingleResult("select pid from #_doctors where email='" . $email . "'");
            if ($doctor_id == '') {
                $json ['signup_step'] = 0;
            }
            if (($email_check_doc == true)) {
                $json ['pid'] = '';
                $json ['signup_step'] = 1;
                $check_location_count = $doctor_function->check_location_count($doctor_id);
                // check the unverified user can add only one location
                $verified_status_from_db = $PDO->getSingleResult("select is_verified from #_doctors where pid='" . $doctor_id . "'");
                if (($check_location_count == 1)) {
                    $json ['signup_step'] = 2;
                    $json ['pid'] = $PDO->getSingleResult("select pid from #_doctors_locations where doctor_id='" . $doctor_id . "' and status=1 and isDeleted=0 ");
                }
                if (($check_location_count > 1)) {
                    $json ['signup_step'] = 3;
                }

                $doctor_details = $doctor_function->doctor_details($doctor_id);
                if ($doctor_details ['doctor_id'] == NULL) {
                    $json ['msg'] = 'Doctor is not matched ';
                    $json ['status'] = 'false';
                    $doctor_function->send_res($json);
                    exit ();
                }
                $json ['doctor_details'] = $doctor_details;
            }
        } else {
            $json ['status'] = 'false';
            $json ['msg'] = 'Your OTP does not matched.Please Try Again.';
        }
        break;

    case 'doctor_signup_first_step' :

        $data = array();
        // This informations are need to store in the user_login table
        $password = mysql_real_escape_string($_POST ['password']);
        $password = md5($password);
        $data ['password'] = base64_encode($password);
        $data ['email'] = mysql_real_escape_string($_POST ['email']);
        $data ['user_type'] = 'doctors';
        if (!empty ($data ['email']) || !empty ($_POST ['password'])) {

            $table_name = 'users_login';
            // check wether the email is already there
            $email_check = $doctor_function->email_check($table_name, $data ['email']);

            // $email_check=false
            if ($email_check != true) {
                $user_id = $doctor_function->signup_step1($data);
                $doctor_table_data = array();
                // this informations are need to store in the doctors table
                $doctor_table_data ['hospital_id'] = 0;
                $doctor_table_data ['clinic_id'] = 0;
                $doctor_table_data ['user_id'] = $user_id; // get from the user_login table pid.
                $doctor_table_data ['speciality'] = mysql_real_escape_string($_POST ['speciality']);
                $doctor_table_data ['name'] = mysql_real_escape_string($_POST ['name']);
                $doctor_table_data ['name_my'] = "NA";
                $doctor_table_data ['email'] = mysql_real_escape_string($_POST ['email']);
                $doctor_table_data ['phone'] = mysql_real_escape_string($_POST ['mobile_number']);
                $doctor_table_data ['sex'] = mysql_real_escape_string($_POST ['gender']);
                $doctor_table_data ['years_of_experience'] = mysql_real_escape_string($_POST ['years_of_experience']);
                $doctor_table_data ['date_of_birth'] = "0000-00-00";
                if ($_POST ['speciality'] == 0) {
                    $doctor_table_data ['doctor_type'] = 1;
                } else {
                    $doctor_table_data ['doctor_type'] = 2;
                }
                $rs_number = mysql_real_escape_string($_POST ['rs_number']);
                if (empty ($rs_number))
                    $doctor_table_data ['rs_number'] = '';
                else
                    $doctor_table_data ['rs_number'] = $rs_number;

                $doctor_table_data ['fees'] = "NA";
                $doctor_table_data ['education'] = mysql_real_escape_string($_POST ['education']);
                $doctor_table_data ['location'] = "";
                $doctor_table_data ['location_my'] = "";
                $doctor_table_data ['latlong'] = "";
                $doctor_table_data ['area_code'] = "";
                $doctor_table_data ['dates'] = "";
                $doctor_table_data ['address'] = "";
                $doctor_table_data ['township'] = "";
                if (empty ($doctor_table_data ['township'])) {
                    $doctor_table_data ['township'] = 0;
                }
                $doctor_table_data ['division'] = "";
                $doctor_table_data ['id_card'] = '';
                $doctor_table_data ['live_status'] = 0;
                $doctor_table_data ['status'] = 1;
                $doctor_table_data ['tf_1'] = "";
                $doctor_table_data ['tf_2'] = "";
                $doctor_table_data ['modified_on'] = @date('Y-m-d H:i:s');
                $doctor_table_data ['create_by'] = $user_id; // get from the user_login table pid.
                $doctor_table_data ['country_code'] = mysql_real_escape_string($_POST ['country_code']);
                $doctor_id = $doctor_function->signup_step2($doctor_table_data);
                $doctor_table_data ['doctor_id'] = $doctor_id;

                if (!empty ($doctor_id)) {
                    $json ['signup_step'] = 1;
                    $json ['doctor_detail'] = $doctor_table_data;
                    $json ['status'] = 'true';
                    $json ['msg'] = 'Your details are saved.Please complete the next step.';

                    // Insert GCM Token
                    $gsm_tocken = mysql_real_escape_string($_POST ['gsm_tocken']);
                    $modified_on = @date('Y-m-d H:i:s');
                    $gsm_tocken_details = array(
                        'user_id' => $user_id,
                        'flag' => 1,
                        'user_type' => 'doctor',
                        'modified_on' => $modified_on,
                        'gsm_tocken' => $gsm_tocken
                    );
                    $result = $PDO->sqlquery("rs", 'gcmuser', $gsm_tocken_details);

                    // send Mail to user
                    $email = $data ['email'];
                    $table_name = 'doctors';
                    $doctor_details = $doctor_function->get_doctor_details_by_email($table_name, $email);
                    $doctor_name = $doctor_details ['name'];
                    $reply_email = 'noreply@doctoroncall.com.mm';
                    $reply_name = 'noreplydoc';
                    $from_email = 'doctoroncalladmin@doctoroncall.com.mm';
                    $from_name = 'Izohealth';
                    $to_email = $email;
                    $to_name = $doctor_name;
                    $subject = 'Doctor Sign Up';

                    // Send Mail
                    $message_content = "Thanks For Joining in Izohealth.<br>Your informations are saved.Please complete the next steps";
                    $email_details = array(
                        'from_email' => 'doctor@doctoroncall.com.mm',
                        'from_name' => 'Doctor CRM',
                        'to_email' => $to_email,
                        'to_name' => $to_name,
                        'subject' => $subject,
                        'message_content' => $message_content
                    );

                    $send_user_Email = $m->sendMail($email_details);

                    // Send admin email verification:
                    $admin_details = $doctor_function->get_all_admin_user_details();
                    // print_r($admin_details);
                    foreach ($admin_details as $row) {
                        // echo $row['pid'];
                        $admin_email = $row ['email'];
                        $table_name = 'doctors';
                        $doctor_details = $doctor_function->get_doctor_details_by_email($table_name, $email);
                        $doctor_name = $doctor_details ['name'];
                        $reply_email = 'noreply@doctoroncall.com';
                        $reply_name = 'noreplydoc';
                        $from_email = 'doctorapp@doctoroncall.com';
                        $from_name = 'Doctor APP';
                        $to_email = $admin_email;
                        $to_name = 'Admin';
                        $subject = 'Sign Up';
                        // http://192.168.0.123/DoctorOnCall/crm/home.php?comp=doctors&mode=add&uid=8400&hospital_id=106
                        $hospital_id = $PDO->getSingleResult("select hospital_id from #_doctors where pid='" . $doctor_id . "'");

                        $verification_crm_url = SITE_PATH_ADM . 'home.php?comp=doctors&mode=add&uid=' . $doctor_id . '&hospital_id=' . $hospital_id;

                        $message_content = "New doctor $doctor_name is registered via doctor app. <br>Please verify their details and activate the user.<br><a href='$verification_crm_url'>Verify</a>";;
                        $email_details = array(
                            'from_email' => 'doctor@doctoroncall.com.mm',
                            'from_name' => 'Doctor CRM',
                            'to_email' => $to_email,
                            'to_name' => $to_name,
                            'subject' => $subject,
                            'message_content' => $message_content
                        );

                        $send_admin_Email = $m->sendMail($email_details);
                    }

                    if (($send_user_Email == true) && ($send_admin_Email == true)) {
                        $json ['mail_status'] = 'true';
                    } else {

                        $json ['mail_status'] = 'false';
                        $json ['mail_status_msg'] = 'There is a problem in mail sending.';
                    }
                }
            } else {
                // email check in doctors table
                $table_name = 'doctors';
                // check wether the email is already there
                $doctor_det = $doctor_function->get_doctor_details_by_email($table_name, $data ['email']);

                $email_check_doc = $doctor_function->email_check($table_name, $data ['email']);
                $phone_check_doc = $doctor_function->phone_check($table_name, $doctor_det ['phone']);
                // checking the user signup steps 1
                if (($email_check_doc == false) && ($phone_check_doc == false)) {
                    $json ['signup_step'] = 1;
                }
                $json ['msg'] = 'Email is already there';
                $json ['status'] = 'false';
            }
        } else {
            $json ['msg'] = 'Email or Password is empty';
            $json ['false'] = 'false';
        }

        break;

    default :
        break;
}

/* Output header */
// header('Content-type: application/json');
// echo json_encode($_POST);
echo json_encode($json);
?>