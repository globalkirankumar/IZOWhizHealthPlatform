<?php
include ("../lib/open.inc.php");
$flag = mysql_real_escape_string ( $_POST ['flag'] );
$json = array ();
include 'class/all_class_files.php';
$doctor_function = new doctor ();
$patient_function = new patient ();
$m = new MyMail ();
include_once 'notification/GCM.php';
$gcm = new GCM ();
date_default_timezone_set('Asia/Kolkata');

switch ($flag) {

    case 'patient_register_otp_send' :
        $mobile_number = mysql_real_escape_string ( $_POST ['mobile_number'] );
        if (($mobile_number == '') || (! is_numeric ( $mobile_number ))) {
            $json ['msg'] = 'mobile number  ber is not valid';
            $json ['status'] = 'false';
            $doctor_function->send_res ( $json );
            exit ();
        }

        $table_name = 'patients';
        $phone_check = $patient_function->phone_check ( $table_name, $mobile_number );

        if ($phone_check == 'false') {
            $json ['status'] = 'false';
            $json ['msg'] = 'This Phone Number is already exist.Please try another number';
        } else {
            $json ['otp'] = str_pad ( ( string ) rand ( 0, 999999 ), 6, '0', STR_PAD_LEFT );
            $signup_otp_details = array (
                'phone' => $mobile_number,
                'otp' => $json ['otp'],
                'status' => 0
            );

            $phone_otp_query = $PDO->db_query ( "select * from #_signup_otp_details  where  phone='" . $mobile_number . "' " );
            if (mysql_num_rows ( $phone_otp_query ) > 0) {
                $otp_query = $PDO->db_query ( "update  #_signup_otp_details  SET otp ='" . $json ['otp'] . "' where  phone='" . $mobile_number . "' " );
                $mobile = $mobile_number;
                $message = urlencode ( 'OTP(one time password) for signup is :' . $json ['otp'] );
                $sendmsg = file_get_contents ( "http://www.doctoroncall.com.mm/crm/app/sms/send_sms.php?mobile=" . $mobile . "&message=" . $message );
                $json ['status'] = 'true';
                $json ['msg'] = 'Your one time password is send to you entered phone number.';
            } else {
                $result = $PDO->sqlquery ( "rs", 'signup_otp_details', $signup_otp_details );
                $mobile = $mobile_number;
                $message = urlencode ( 'OTP(one time password) for signup is :' . $json ['otp'] );
                $sendmsg = file_get_contents ( "http://www.doctoroncall.com.mm/crm/app/sms/send_sms.php?mobile=" . $mobile . "&message=" . $message );
                $json ['status'] = 'true';
                $json ['msg'] = 'Your one time password is send to you entered phone number.';
            }
        }

        break;

    case 'patient_register_otp_verify' :
        $mobile_number = mysql_real_escape_string ( $_POST ['mobile_number'] );
        $otp = mysql_real_escape_string ( $_POST ['otp'] );
        $phone_otp_query = $PDO->db_query ( "select * from #_signup_otp_details  where  phone='" . $mobile_number . "' and otp='" . $otp . "' " );
        if (mysql_num_rows ( $phone_otp_query ) > 0) {
            $json ['status'] = 'true';
            $json ['msg'] = 'Your OTP is Verified successfully';
            $sql = "DELETE FROM `#_signup_otp_details` WHERE `phone`='" . $mobile_number . "' AND `otp`='" . $otp . "' ";
            $query = $PDO->db_query ( $sql );

            // check signup step

            $login_type = 0; // 0 email, 1 phone
            if (filter_var ( $mobile_number, FILTER_VALIDATE_EMAIL )) {
                // valid address
                $login_type = 0;
            } else {
                if (is_numeric ( $mobile_number )) {
                    $login_type = 1;
                }
            }
            if ($login_type == 1) {
                $email_from_db = $PDO->getSingleResult ( "select email from #_patients where phone='" . $mobile_number . "'" );

                if ($email_from_db != '') {
                    $email = $email_from_db;
                } else {
                    $json ['signup_step'] = 0;
                }
            } else {
                $phone = $PDO->getSingleResult ( "select phone from #_patients where email='" . $email . "'" );
            }
            $patient_id = '';
            $password_from_db = '';
            $json ['patient_details'] = array ();
            $patient_query = $PDO->db_query ( "select * from #_patients  where  phone='" . $mobile_number . "' " );
            if (mysql_num_rows ( $phone_otp_query ) > 0) {
                $password_from_db = $PDO->getSingleResult ( "select password from #_patients where phone='" . $mobile_number . "'" );
                $patient_id = $PDO->getSingleResult ( "select pid from #_patients where phone='" . $mobile_number . "'" );
            }
            if ($patient_id == '') {
                $json ['signup_step'] = 0;
            }

            if ($password_from_db != '') {
                $json ['signup_step'] = 1;
                if (! empty ( $patient_id )) {
                    $json ['patient_details'] = array ();
                    $patient_id = mysql_real_escape_string ( $patient_id );
                    $patient_details = $patient_function->patient_details ( $patient_id );
                    $json ['patient_details'] = $patient_details;
                }
            } else {
                $json ['signup_step'] = 0;
            }
            $json ['status'] = 'true';
            $json ['msg'] = 'This OTP is matched.';
        } else {
            $json ['status'] = 'false';
            $json ['msg'] = 'This OTP is not matched. Try Again.';
        }
        break;

    case 'patient_signup_step1' :
        $patient_details = array ();
        $name = mysql_real_escape_string ( $_POST ['name'] );
        $email = mysql_real_escape_string ( $_POST ['email'] );
        $phone = mysql_real_escape_string ( $_POST ['phone'] );
        $password = mysql_real_escape_string ( $_POST ['password'] );
        $gender = mysql_real_escape_string ( $_POST ['gender'] );
        $marital_status = mysql_real_escape_string ( $_POST ['marital_status'] );
        $date_of_birth = mysql_real_escape_string ( $_POST ['date_of_birth'] );
        $date_of_birth = date ( $date_of_birth );
        $location = mysql_real_escape_string ( $_POST ['location'] );
        $nrc_passport_number = mysql_real_escape_string ( $_POST ['nrc_passport_number'] );
        $occupation = mysql_real_escape_string ( $_POST ['occupation'] );
        $blood_group = mysql_real_escape_string ( $_POST ['blood_group'] );
        $height = mysql_real_escape_string ( $_POST ['height'] );
        $weight = mysql_real_escape_string ( $_POST ['weight'] );
        $emergency_contact_number = mysql_real_escape_string ( $_POST ['emergency_contact_number'] );
        $gsm_tocken = mysql_real_escape_string ( $_POST ['gsm_tocken'] );
        $password = md5 ( $password );
        $password = base64_encode ( $password );
        $patient_details ['name'] = $name;
        $patient_details ['email'] = $email;
        $patient_details ['phone'] = $phone;
        $patient_details ['password'] = $password;
        $patient_details ['gsm_tocken'] = $gsm_tocken;
        $patient_details ['sex'] = $gender;
        $patient_details ['marital_status'] = $marital_status;
        $patient_details ['date_of_birth'] = $date_of_birth;
        $patient_details ['address'] = $location;
        $patient_details ['nrc_passport_number'] = $nrc_passport_number;
        $patient_details ['occupation'] = $occupation;
        $patient_details ['blood_group'] = $blood_group;
        $patient_details ['height'] = $height;
        $patient_details ['weight'] = $weight;
        $patient_details ['alternate_phone'] = $emergency_contact_number;
        $shortorder = $PDO->getSingleResult ( "select max(shortorder) from #_patients " );
        $shortorder = $shortorder + 1;
        $patient_details ['shortorder'] = $shortorder;
        $patient_details ['status'] = 1;

        // Insert Data
        if (empty ( $phone )) {
            $json ['msg'] = 'Please enter required details.';
            $json ['status'] = 'false';
            $doctor_function->send_res ( $json );
            exit ();
        }
        $table_name = 'patients';
        $phone_check = $patient_function->phone_check ( $table_name, $phone );

        if ($phone_check == 'false') {
            $json ['status'] = 'false';
            $json ['msg'] = 'This Phone Number is already exist.Please try another number';
            $doctor_function->send_res ( $json );
            exit ();
        }

        $pid = $patient_function->signup ( $patient_details );
        $patient_id = 'DOC' . $pid;
        $patient_details ['patient_id'] = $patient_id;
        $sql = "UPDATE #_patients SET patient_id='" . $patient_id . "',create_by='" . $pid . "'  WHERE pid='" . $pid . "'   ";
        $query = $PDO->db_query ( $sql );
        $patient_details ['patient_id'] = $patient_id;
        $patient_details ['pid'] = $pid;
        $patient_details ['gender'] = $gender;
        $patient_details ['emergency_contact_number'] = $emergency_contact_number;
        $patient_details ['location'] = $location;
        if ($patient_id != '') {
            $json ['status'] = 'true';
            $json ['msg'] = 'Your Details stored successfully';
            $json ['patient_details'] = $patient_details;

            // Send Mail
            $message_content = '
            Thanks for registering in Izohealth. Your account is created successfully.
                ';
            $email_details = array (
                'from_email' => 'doctor@doctoroncall.com.mm',
                'from_name' => 'Doctor CRM',
                'to_email' => $email,
                'to_name' => $name,
                'subject' => 'Patient Register',
                'message_content' => $message_content
            );

            $result = $m->sendMail ( $email_details );
        } else {
            $json ['status'] = 'false';
            $json ['msg'] = 'Sorry.There is a problem to save your data.';
        }

        break;

    default :
		break;
}
/* Output header */
// header('Content-type: application/json');
// echo json_encode($_POST);
echo json_encode ( $json );
?>