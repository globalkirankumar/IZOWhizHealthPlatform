<?php
//require_once('../functions/phpmailer/class.phpmailer.php');
/**
 * Created by PhpStorm.
 * User: Lenovo
 * Date: 02-02-2018
 * Time: PM 05:25
 */
class MyMail extends PHPMailer
{
    private $_host = SMTP_HOST;
    private $_user = SMTP_USER;
    private $_password = SMTP_PASSWORD;

    public function __construct($exceptions=true)
    {
        $mail=new PHPMailer();
        $mail->Host = $this->_host;
        $mail->Username = $this->_user;
        $mail->Password = $this->_password;
        $mail->Port = SMTP_PORT;
        $mail->SMTPAuth = true;
        $mail->SMTPSecure = SMTP_SECURE;
        $mail->isSMTP();
        parent::__construct($exceptions);
    }

    public function sendMail($email_details)
    {
		
        $mail=new PHPMailer();

        $from_email=$email_details['from_email'];
        $from_name=$email_details['from_name'];
        $to_email=$email_details['to_email'];
        $to_name=$email_details['to_name'];
        $subject=$email_details['subject'];
        $message_content=$email_details['message_content'];
        $reply_email = 'noreply@doctoroncall.com.mm';
        $reply_name = 'noreplydoctoroncall';
        $logourl = SITE_PATH . 'img/not6_icon75px.png';
        $message = '
		
<html>
<head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
<title>Notify</title>

<style type="text/css">

div, p, a, li, td { -webkit-text-size-adjust:none; }

*{
-webkit-font-smoothing: antialiased;
-moz-osx-font-smoothing: grayscale;
}

.ReadMsgBody
{width: 100%; background-color: #ffffff;}
.ExternalClass
{width: 100%; background-color: #ffffff;}
body{width: 100%; height: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;}
html{width: 100%; background-color: #ffffff;}

<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400i,600,700" rel="stylesheet"> 

p {padding: 0!important; margin-top: 0!important; margin-right: 0!important; margin-bottom: 0!important; margin-left: 0!important; }

.hover:hover {opacity:0.85;filter:alpha(opacity=85);}

.image77 img {width: 77px; height: auto;}
.avatar125 img {width: 125px; height: auto;}
.icon61 img {width: 61px; height: auto;}
.logo img {width: 200px; height: auto;}
.icon18 img {width: 18px; height: auto;}

</style>

<!-- @media only screen and (max-width: 640px) 
		   {*/
		   -->
<style type="text/css"> @media only screen and (max-width: 640px){
		body{width:auto!important;}
		table[class=full2] {width: 100%!important; clear: both; }
		table[class=mobile2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
		table[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
		td[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
		td[class=pad15] {width: 100%!important; padding-left: 15px; padding-right: 15px; clear: both;}
		
} </style>
<!--

@media only screen and (max-width: 479px) 
		   {
		   -->
<style type="text/css"> @media only screen and (max-width: 479px){
		body{width:auto!important;}
		table[class=full2] {width: 100%!important; clear: both; }
		table[class=mobile2] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
		table[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
		td[class=fullCenter2] {width: 100%!important; text-align: center!important; clear: both; }
		table[class=full] {width: 100%!important; clear: both; }
		table[class=mobile] {width: 100%!important; padding-left: 20px; padding-right: 20px; clear: both; }
		table[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
		td[class=fullCenter] {width: 100%!important; text-align: center!important; clear: both; }
		td[class=pad15] {width: 100%!important; padding-left: 15px; padding-right: 15px; clear: both;}
		.erase {display: none;}
				
		}
} </style>

</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">


<!-- Notification 6 -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="full2"  bgcolor="#303030"style="background-color: rgb(48, 48, 48);">
	<tr>
		<td align="center" style="background:#F1F1F1; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; background-position: center center; background-repeat: no-repeat;" id="not6">
		
			
			<!-- Mobile Wrapper -->
			<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2">
				<tr>
					<td width="100%" align="center">
					
						
						<div class="sortable_inner ui-sortable">
						<!-- Space -->
						<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full2" object="drag-module-small">
							<tbody><tr>
								<td width="600" height="50"></td>
							</tr>
						</tbody></table><!-- End Space -->
						
						<!-- Space -->
						<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full2" object="drag-module-small">
							<tbody><tr>
								<td width="600" height="50"></td>
							</tr>
						</tbody></table><!-- End Space -->
			
						<!-- Start Top -->
						<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#fff" style="border-top-left-radius: 5px; border-top-right-radius: 5px; background-color: #fff; border-top: 3px solid rgb(31,174,102);" object="drag-module-small">
							<tbody><tr>
								<td width="600" valign="middle" align="center" class="logo">
									
									<!-- Header Text --> 
									<table width="540" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
										<tbody><tr>
											<td width="100%" height="30"></td>
										</tr>
										<tr>
											<td width="100%"><span ><img src="' . $logourl . '" width="200" alt="" border="0" mc:edit="39"></span></td>
										</tr>
										<tr>
											<td width="100%" height="10"></td>
										</tr>
									</tbody></table>
								</td>
							</tr>
						</tbody></table>
						
						</div>
						
						<!-- Mobile Wrapper -->
						<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full2" object="drag-module-small" style="-webkit-border-bottom-left-radius: 5px; -moz-border-bottom-left-radius: 5px; border-bottom-left-radius: 5px; -webkit-border-bottom-right-radius: 5px; -moz-border-bottom-right-radius: 5px; border-bottom-right-radius: 5px;">
							<tr>
								<td width="600" align="center" style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px; background-color: rgb(255, 255, 255);" bgcolor="#ffffff">
									
									<div class="sortable_inner ui-sortable">
						
									<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
										<tr>
											<td width="600" valign="middle" align="center">
											 
												<table width="540" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
													<tr>
														<td width="100%" height="30"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									
									<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
										<tr>
											<td width="600" valign="middle" align="center">
											 
												<table width="540" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
													<tr>
														<td valign="middle" width="100%" style="text-align: left; font-family: \'Source Sans Pro\', sans-serif; font-size: 23px; color: rgb(63, 67, 69); line-height: 30px; font-weight: 100;" >
															<!--[if !mso]><!--><span style="font-family: \'Source Sans Pro\', sans-serif; font-weight: normal;"><!--<![endif]-->Hi ' . $to_name . ', <!--[if !mso]><!--></span><!--<![endif]-->
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									
									<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
										<tr>
											<td width="600" valign="middle" align="center">
											 
												<table width="540" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
													<tr>
														<td width="100%" height="30"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									
									<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
										<tr>
											<td width="600" valign="middle" align="center">
											 
												<table width="540" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
													<tr>
														<td valign="middle" width="100%" style="text-align: left; font-family: \'Source Sans Pro\', sans-serif; font-size: 14px; color: rgb(63, 67, 69); line-height: 24px;" >
															<!--[if !mso]><!--><span style="font-family: \'Source Sans Pro\', sans-serif; font-weight: normal;"><!--<![endif]-->'.$message_content.' <!--[if !mso]><!--></span><!--<![endif]-->
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									
									<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
										<tr>
											<td width="600" valign="middle" align="center">
											 
												<table width="540" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
													<tr>
														<td width="100%" height="40"></td>
													</tr>
												</table>
											</td>
										</tr>
									</table>								
									
								
									
									<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff"object="drag-module-small" style="background-color: rgb(255, 255, 255);">
										<tr>
											<td width="600" valign="middle" align="center">
											 
												<table width="540" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
													<tr>
														<td valign="middle" width="100%" style="text-align: left; font-family: \'Source Sans Pro\', sans-serif; font-size: 14px; color: rgb(63, 67, 69); line-height: 24px;" >
															<!--[if !mso]><!--><span style="font-family: \'Source Sans Pro\', sans-serif; font-weight: normal;"><!--<![endif]--><!--Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. -->
															<br><br>
															Thank you!
															<br>'.$from_name.'
															<br><strong>Izohealth</strong>
																
															<!--[if !mso]><!--></span><!--<![endif]-->
														</td>
													</tr>
												</table>
											</td>
										</tr>
									</table>
									
									<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="mobile2" bgcolor="#ffffff"object="drag-module-small" style="border-bottom-left-radius: 5px; border-bottom-right-radius: 5px; background-color: rgb(255, 255, 255);">
										<tr>
											<td width="600" valign="middle" align="center" style="-webkit-border-bottom-left-radius: 5px; -moz-border-bottom-left-radius: 5px; border-bottom-left-radius: 5px; -webkit-border-bottom-right-radius: 5px; -moz-border-bottom-right-radius: 5px; border-bottom-right-radius: 5px;">
											 
												<table width="540" border="0" cellpadding="0" cellspacing="0" align="center" style="text-align: center; border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="fullCenter2">
													<tr>
														<td width="100%" height="50"></td>
													</tr>
												</table>
																				
											</td>
										</tr>
									</table>
									
								</div>
								</td>
							</tr>
						</table>
						
						<table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="full2" object="drag-module-small">
							<tr>
								<td width="600" height="30"></td>
							</tr>
						</table>
						
									
						
							<tr>
								<td width="600" height="1" style="font-size: 1px; line-height: 1px;">&nbsp;</td>
							</tr>
						</table>
						
						
					</td>
				</tr>
			</table>
			
		</div>
		</td>
	</tr>
</table><!-- End Notification 6 -->
</div>
</body></html>	<style>body{ background: none !important; } </style>';
        $body=$message;

        $mail->IsHTML();
        $mail->setFrom($from_email, $from_name);
        $mail->AddReplyTo($reply_email, $reply_name);

        $mail->addAddress($to_email, $to_name);
        $mail->Subject = $subject;
        $mail->Body = $body;

        if (!$mail->Send()) {
          //  echo "Mailer Error: " . $mail->ErrorInfo;
            return false;

        } else {
			//echo 'success';
            return true;
        }

    }
}