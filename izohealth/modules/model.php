<!-- Modal popup-->

<div id="myModal" class="modal  modal-timeline<?php echo $k;?> fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close close1" data-dismiss="">&times;</button>
      </div>
      <div class="modal-body" id="append-records">
				<!-- content -->
				   <div class="question_ans">
                  <ul>
                    <li><img src="img/que.jpg"><span class="que_inner">Chief Complaint</span>  </li>
                    <li><img src="img/ans.jpg"><span class="que_inner"><?php echo $chief_complaint;?><!--<input type="text" class="que_input_box" placeholder="<?php echo $chief_complaint;?>" readonly>--></span></li>
                  </ul>
                  </div>
				  <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">History of present illness </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><?php echo $history_of_present_illness;?><!--<textarea class="que_input_box" placeholder="<?php echo $history_of_present_illness;?>" readonly></textarea> --></span></li>
                    </ul>
                  </div>
				  <div class="question_ans">
                  <span class="que_heading_medical">Past history  </span>
                  <div class="nastingul">
                  <ul>
                    <li><img src="img/que.jpg"><span class="que_inner">past medical  history</span></li>
                    <li><img src="img/ans.jpg"><span class="que_inner"><?php echo $past_medical_history;?><!--<input type="text" class="que_input_box" placeholder="<?php echo $past_medical_history;?>" readonly>--></span></li>
                  </ul>
                  <ul>
                    <li><img src="img/que.jpg"><span class="que_inner">Past surgical history</span></li>
                    <li><img src="img/ans.jpg"><span class="que_inner"><?php echo $past_surgical_history;?><!--<input type="text" class="que_input_box" placeholder="<?php echo $past_surgical_history;?>" readonly>--></span></li>
                  </ul>
                  </div>
                  </div>
				   <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Social history  </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><?php echo $social_history;?><!--<input type="text" class="que_input_box" placeholder="<?php echo $social_history;?>" readonly>--> </span></li>
                    </ul>
                  </div>
                  <div class="question_ans">
                    <ul>
                      <li><img src="img/que.jpg"><span class="que_inner">Family history </span>  </li>
                      <li><img src="img/ans.jpg"><span class="que_inner"><?php echo $family_history;?><!--<input type="text" class="que_input_box" placeholder="<?php echo $family_history;?>" readonly>--> 
                      </span></li>
                    </ul>
                  </div>
				  <div class="question_ans">
                      <ul>
                        <li><span class="que_heading_medical">Medication History  </span>  </li>
                        <li class="editli">
							<!-- <img src="img/que.jpg"> -->
							<span class="que_inner">history of drug allergy </span>  
						</li>
                        <li class="editli">
							<!-- <img src="img/ans.jpg"> -->
							<span class="que_inner"> 
								<span class="checkbox_callinfo">
									<input value="2" id="checkbox-4" type="checkbox" <?php if(strtolower($history_of_drug_allergy)=="yes"){ echo 'checked';}?> disabled>
									<label class="check_box">Yes</label>
								</span>
								<span class="checkbox_callinfo">
									<input value="2" id="checkbox-4" type="checkbox" <?php if(strtolower($history_of_drug_allergy)=="no"){ echo 'checked';}?> disabled>
									<label class="check_box">No</label>
								</span>
							</span>
						</li>
                      <br>
                      <li class="editli"><!-- <img src="img/que.jpg"> --><span class="que_inner">if yes : <?php echo $drug_allergy_comment;?><!--<input type="text" class="que_input_box" placeholder="<?php echo $drug_allergy_comment;?>" readonly >--> </span>  </li>
                        <li class="editli"><!-- <img src="img/que.jpg"> --><span class="que_inner">Regular taking medication: <?php echo $drug_allergy_regular_taking_medication;?><!-- <input type="text" class="que_input_box" placeholder="<?php echo $drug_allergy_regular_taking_medication;?>" readonly >--> </span>  </li>
                       
                      </ul>
                    </div>
					  <div class="question_ans">
                      <ul class="libox">

                        <li><span class="text">BP- </span> <span class="input"><input type="text" class="que_input_box" placeholder="<?php echo $bp;?>" readonly></span> </li>
                        <li><span class="text">PR-</span> <span class="input"><input type="text" class="que_input_box" placeholder="<?php echo $pr;?>" readonly></span> </li>
                       <li><span class="text">SaO2-</span> <span class="input"><input type="text" class="que_input_box" placeholder="<?php echo $sao;?>" readonly></span> </li>
                       <li><span class="text">Weight-</span> <span class="input"><input type="text" class="que_input_box" placeholder="<?php echo $weight;?>" readonly></span> </li>
                        <li><span class="text">RBS-</span> <span class="input"><input type="text" class="que_input_box" placeholder="<?php echo $rbs;?>" readonly></span> </li>
                        
                          <li><span class="text">Temp-</span> <span class="input"><input type="text" class="que_input_box" placeholder="<?php echo $temp;?>" readonly></span> </li>
                       
                      </ul>
                    </div>
					 <div class="question_ans">
                      <ul>
                        <li><img src="img/que.jpg"><span class="que_inner">provisional diagnosis   </span>  </li>
                        <li><img src="img/ans.jpg"><span class="que_inner">
                        <?php echo $provisional_diagnosis;?>
                        <!--<input type="text" class="que_input_box" placeholder="<?php echo $provisional_diagnosis;?>" readonly> --></span></li>
                      </ul>
                    </div>
					 <div class="question_ans">
                      <ul>
                        <li><img src="img/que.jpg"><span class="que_inner">Service provided </span>  </li>
                        <li><img src="img/ans.jpg"><span class="que_inner">
                        <div id="email-input-wrapper" class="controls col-sm-6">
                        <div class="vd_radio radio-success">
                        <input  value="personal" id="optionsRadios5" name="member" type="radio" <?php if(strtolower($service_provided)=="medial advice"){ echo 'checked';}?> disabled>
                          <label for="optionsRadios5"> medical advice and counseling </label>
                        </div>

                         <div class="vd_radio radio-success">
                        <input  value="personal" id="optionsRadios4" name="member" type="radio"  <?php if(strtolower($service_provided)=="health education and information"){ echo 'checked';}?>  disabled>
                          <label for="optionsRadios4"> Health education and information </label>
                        </div>

                        <div class="vd_radio radio-success">
                       <input  value="personal" id="track" name="track" type="radio"  <?php if(strtolower($service_provided)=="referral service to"){ echo 'checked';}?> disabled id="optionsRadios6" >
                          <label for="optionsRadios6"> Referral service to </label>                       
                            <span class="checkbox-inline">
								<input type="radio" name="type" id="track" value="track" <?php if(strtolower($referral_service_to)=="gp"){ echo 'checked';}?> disabled />
								<label for="track">GP</label>
							</span>
							<span class="checkbox-inline">
								<input type="radio" name="type" id="track" value="track" <?php if(strtolower($referral_service_to)=="specialist"){ echo 'checked';}?> disabled />
								<label for="track">Specialist</label>
							</span>
							<span class="checkbox-inline">
								<input type="radio" name="type" id="track" value="track" <?php if(strtolower($referral_service_to)=="lab"){ echo 'checked';}?> disabled />
								<label for="track">Lab</label>
							</span>
							<span class="checkbox-inline">
								<input type="radio" name="type" id="track" value="track" <?php if(strtolower($referral_service_to)=="imaging"){ echo 'checked';}?> disabled />
								<label for="track">Imaging</label>
							</span>
							<span class="checkbox-inline">
								<input type="radio" name="type" id="track" value="track" <?php if(strtolower($referral_service_to)=="healthcare"){ echo 'checked';}?>  disabled />
								<label for="track">Home visit</label>
							</span>
                        </div>
                           <div style="margin-top:20px;"><span class="que_inner"><input type="checkbox" name="follow_up_call_schedule"  value="Yes" <?=($follow_up_call_schedule=='Yes')?'checked="checked"':''?> onclick="follow_up_call_date_div()" >&nbsp;Schedule Follow up call </span> 
                       
                       <div id="follow_up_call_date_div" style="display: <?=($follow_up_call_schedule=='Yes')?'block"':'none'?>">
                       Date :  <?=date('d F Y', strtotime($follow_up_call_date))?> <?=$follow_up_call_time?>
                       <!-- <div class="col-md-6 col-sm-6 col-xs-6"><input type="text"  name="follow_up_call_date"  placeholder="Date" value="<?=$follow_up_call_date?>" readonly="readonly" /></div>-->
                       <!-- <div class="col-md-6 col-sm-6 col-xs-6">'<input type="text" name="follow_up_call_time" class="timePicker"  placeholder="Time"  value="<?=$follow_up_call_time?>" readonly="readonly"  /></div>-->
                       
                       </div>
                       </div>

                        </li>
                      </ul>
                    </div>
                  
       
				<!-- end content -->
      </div>

    </div>
  </div>
</div>
<!--close modal popup-->