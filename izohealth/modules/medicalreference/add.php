<?php 
include(FS_ADMIN._MODS."/medicalreference/class.inc.php");


$OP = new Options();

if($BSC->is_post_back())
{
	
	$path = UP_FILES_FS_PATH."/reports";
	   
	$allowed =  array('gif','png' ,'jpg','jpeg','bmp','txt','pdf','doc','docx','xlsx','xls');
	$flag =1;
	
	if($_FILES['reports']['name']!='')
	{
	
		$filename = $_FILES['reports']['name'];
		$size = $_FILES['reports']['size'];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if(!in_array($ext,$allowed) )
		 {
			$flag =0;
            $PDO->sessset('File Type Mismatch', 'e');
		}else if($size>2097152){
			$flag =0;
			$PDO->sessset('Maximum file size is 2M', 'e');
		}else{
			
			 $_POST['file_name'] = $BSC->uploadFile2($path,$_FILES['reports']['name'],'reports');
			
		}	
		
	}
	
	if($flag ==1)
	{
		
	   if($uid)
	   {
		   $_POST['updateid']=$uid;
		   $flag = $OP->update($_POST);
	  
	   }else {
		 
		   $flag = $OP->add($_POST);
		   
			 
	   }
	   
	   if($flag==1)
	   {
	   
		 $BSC->redir($ADMIN->iurl($comp.(($start)?'&start='.$start:'').(($subpage_id)?'&subpage_id='.$subpage_id:'').(($alumniid)?'&alumniid='.$alumniid:'').(($division_id)?'&division_id='.$division_id:'')).$dlr, true);
	   }
	   
	}
	
}


if($uid)
{
    $query =$PDO->db_query("select * from #_".tblName." where pid ='".$uid."' "); 
	$row = $PDO->db_fetch_array($query);
	@extract($row);	
}

?>

<div class="vd_content-section clearfix">
		  	<div class="row" id="form-basic">
			  <?=$ADMIN->alert()?>
              		<div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span> Add/Health <?=$ADMIN->compname($comp)?> </h3>
                </div>
              		
              		<div class="panel-body"> 
				

              			
						
			<!--add-update form-->
                <!--    <form  class="form-horizontal body-gap" action="#" role="form" id="register-form">-->
                
                
                    
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label  ">Patient's  </label>
                        <div id="first-name-input-wrapper" class="controls ">
                         <select name="patient_id" id="patient_id" class="input_box_client_select cat2" class="validate [required]" data-errormessage-value-missing="Patient's is required!">
                                         <option value="" >---select---</option>
                                         <?php 
										        $record=$PDO->db_query("select * from #_patients  where status =1  order by name");
										        while($res=mysql_fetch_array($record))
												{
										  ?>
					                       <option value="<?=$res['pid']?>" <?php if($patient_id==$res['pid']){echo "selected";}?>><?=$res['name']?> (<?=$res['patient_id']?>)</option>
						              <?php } ?>
                                        </select>
                        </div>
                    </div>
                
                
              
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label  ">Title <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls ">
                          <input class="validate[required]" data-errormessage-value-missing="Name of sub-center is required!" name="title" id="title"  value="<?=$title?>" type="text">
                        </div>
                    </div>
                    
                    
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label  ">Upload File <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls ">
                         <input type="file" name="reports" <?=($uid ==0)?'class="validate[required]"':''?> data-errormessage-value-missing="File is required!">
                           <label style="color:#D20000; font-size:11px">NOTE:File type allowed("jpeg,jpg,png,bmp,gif,txt,pdf,doc and docx only") and Maximum file size 2 MB</label>
                        </div>
                    </div>
                    
                    
                   
                    
					
					<!-- End Services -->
					<div class="form-group col-md-6 col-sm-6 col-xs-12">
                      <label class="control-label  ">Status <span class="vd_red">*</span></label>
                      <div id="website-input-wrapper" class="controls ">
                          <select name="status" class="validate [required]" data-errormessage-value-missing="Status is required!">
                              <option  value="">-------Select Status------</option>
								<option value="1" <?=($status==1)?'selected="selected"':''?>  >Active</option>
								<option value="0" <?=(isset($status) && $status==0)?'selected="selected"':''?>>Inactive</option>
                          </select>
                    </div>
                  </div>
                
                  
<!--
                  <div class="form-group form-actions">
                    <div class="col-sm-6"></div>
                    <div class="col-md-6">
					 <input type="hidden" name="division_id" value="<?=$division_id?>" />
                       <button id="submit-register" name="submit-register" class="btn vd_btn vd_bg-green vd_white uibutton loading" type="submit"><i class="icon-ok" ></i> Save</button>
                          <button onclick="location.reload();" class="btn vd_btn" type="button">Cancel</button>
                    </div>
                  </div>
-->
                  
                  
                  
                  
                  
                  	
		<div class="form-group form-actions">
                   
                    <div class="col-md-12 mgbt-xs-10 mgtp-20">
                     
                      
                      <div class="mgtp-10 gp-center">
					   <input type="hidden" name="pid" value="<?=$division_id?>" />
                        <button class="btn vd_bg-green vd_white greenbutton uibutton loading" type="submit" id="submit-register" name="submit-register">Save</button>
                        
                        
                         <button onclick="location.reload();" class="btn  orng-btn" type="button" >Cancel</button>
                      </div>
                    </div>
                   
                  </div>
             <!--   </form>-->
			<!-- close add-update form-->
              		</div>
              </div>
            </div>
		</div>
<script src="<?=SITE_PATH?>validation/js/jquery-1.8.2.min.js"></script>
<script src="<?=SITE_PATH?>validation/js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=SITE_PATH?>validation/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#formID").validationEngine();
		});
		$('.hide_show').change(function(){
			var id = $(this).attr("data-id");
			if($(this).val()=="Available")
			{
				$('#'+id).show();
			}else{
				$('#'+id).hide();
			}
		});
		$('.services').click(function(){
			var id = $(this).attr("data-id");
			if(this.checked)
			{
				$('#'+id).show();
			}else{
				$('#'+id).hide();
			}
		});
</script>