<?php
/*
# @package BSC CMS
# Copy right code 
# The base configurations of the BSC CMS.
# manage admin user add update and delete
# Developer by jugal kishore kiroriwal

*/

class Options extends dbc
{
	
	 public function  add($data)
	 {
		    @extract($data);
			$tblName = "users_login";
	        $query=parent::db_query("select * from #_".$tblName." where email ='".$email."' "); 
	        if(mysql_num_rows($query)==0)
	        {
				$data['password']=$this->password($data['password']);
				$data['user_type'] = "administrator";
				$data[user_id] = parent::sqlquery("rs",'users_login',$data);
				$data['created_on']=@date('Y-m-d H:i:s');
				$data['create_by']=$_SESSION["AMD"][0];
				$data['shortorder']=parent::getSingleresult("select max(shortorder) as shortorder from #_".tblName." where 1=1 ")+1;
				parent::sqlquery("rs",'admin_users',$data);
		        parent::sessset('Record has been added', 's');
	            $flag =1;
		    }else {
			    parent::sessset('Record has already added', 'e');
				$flag =0;
			}
			
			return $flag; 
	 }
	 
	 public function  update($data)
	 {
		   @extract($data);
		   $tblName = "users_login";
		   // pid is a user id  And Update id is  pid of module table
	       $query=parent::db_query("select * from #_".tblName." where pid='".$updateid."' "); 
		   
		   $querydata =mysql_fetch_array($query);
	       if($querydata['email']==$email)
	       {
			   if($data['password']!='' and $data['change_pwd']!='')
			   {
				   $data['password']=$this->password($data['password']);
                   parent::sqlquery("rs",'users_login',array('password'=>$data['password']),'pid',$pid);				   
			   }
				 parent::sqlquery("rs",'users_login',array('user_type'=>$data['user_type']),'pid',$pid); 
		        
				 $data['modified_on']=@date('Y-m-d H:i:s');
			     parent::sqlquery("rs",'admin_users',$data,'pid',$updateid);
		         parent::sessset('Record has been updated', 's');
	             $flag =1;
		    }else {
				$query2 = db_query("select * from #_".$tblName." where email='".$email."' ");
				if(mysql_num_rows($query)==0)
				{
					parent::sqlquery("rs",'users_login',array('email'=>$data['email'],'user_type'=>$data['user_type']),'pid',$pid);
					$flag =1;
				}else{					
			    parent::sessset('Record has already added', 'e');
				$flag =0;
				}
			}
			
			return $flag;  
		 
	 }
	 
	 public function  delete($updateid)
	 {
		   if(is_array($updateid))
		   {
			   $updateid=implode(',',$updateid);
		   }
		   $tblName = "users_login";
		    $id = parent::getSingleresult("select user_id  from #_".tblName." where pid in ($updateid)");
		    parent::db_query("delete from #_".tblName." where pid in ($updateid)");
		    parent::db_query("delete from #_".$tblName." where pid in ($id)");
		   
		   
	 }
	 
	 public function status($updateid,$status)
	 {
		   if(is_array($updateid))
		   {
			   $updateid=implode(',',$updateid);
		   }
		   
		   parent::db_query("update  #_".tblName." set status='".$status."' where pid in ($updateid)");
		   
	 }
	 
	  
	 
	 public function  display($start,$pagesize,$fld,$otype,$search_data)
	 {
		$_SESSION["message_post"]='';
		$start = intval($start); 
	   	$columns = "select * "; 
		
		if(trim($search_data)!='')
		{
		   $wh=" and (username like '%".parent::parse_input($search_data)."%' or email like '%".parent::parse_input($search_data)."%' or user_type like '%".parent::parse_input($search_data)."%') ";	
		}
		
		
		if(!empty($_POST['call_search_type']))
		{
			 
			$shval =array(); 
			if(in_array('follow up call',$_POST['call_search_type']))
			{
				  $shval[]=" call_type ='follow up call' ";
			}
			
			if(in_array('subscription call',$_POST['call_search_type']))
			{
				  $shval[]=" subscription_details_id > 0 ";
			}
			
			if(in_array('schedule follow up call',$_POST['call_search_type']))
			{
				  $shval[]="  follow_up_call_date !=''  ";
			}
			
			 $wh .= " and (";
			  for($i=0;$i < count($shval); $i++)
			  {
				  if($i==0)
				  {
				    $wh .=$shval[$i]; 
				  }else {
					  $wh .=' or '.$shval[$i];   
				  }
			  }
			 $wh .=")";
			
		}
		
		if($_POST['search_doctor_agents']!='')
		{
		  $wh .=" and create_by ='".$_POST['search_doctor_agents']."'  and create_by_type ='doctoragents' ";	
		}
		
		if($_POST['search_date']!='')
		{
		  $wh .=" and created_on like '%".date('Y-m-d', strtotime($_POST['search_date']))."%' ";	
		}
		
		if($_POST['search_patients_id']!='')
		{
		     $patients_id=parent::getSingleresult("select pid from #_patients where patient_id = '".$_POST['search_patients_id']."' ");
			 $wh .=" and patient_id = '".$patients_id."'";
		}
		
		if(($_POST['search_phone_number']!='')&& (is_numeric($_POST['search_phone_number'])))
		{

			 $caller_patients= parent::db_query("select * from #_patients where phone = '".$_POST['search_phone_number']."'  "); 
             
			 $whp ='';
			 while($caller_data =  parent::db_fetch_array($caller_patients))
			 {
				  $whp .= $caller_data['pid'].',';
			 }
			 if($whp!='')
			 {
		         $wh .=" and patient_id in (".trim($whp,',').")";
			 }
		}
		
		
		if($_POST['search_patients_name']!='')
		{
			 
			
			
			 $caller_patients= parent::db_query("select * from #_patients where name like '%".$_POST['search_patients_name']."%'  "); 
             
			 $whp ='';
			 while($caller_data =  parent::db_fetch_array($caller_patients))
			 {
				  $whp .= $caller_data['pid'].',';
			 }
			 if($whp!='')
			 {
		       $wh .=" and patient_id in (".trim($whp,',').")";
			 }
		}
		
		if($_POST['search_sub_centers']!='')
		{
		   $wh .=" and sub_center_check ='Yes'  and sub_centers ='".$_POST['search_sub_centers']."'";
		}
		if($_POST){
			if($wh=='')
			{
				$_SESSION["message_post"]="No Details Available";
			}else{
				$_SESSION["message_post"]="";
			}
		}
		
		
		
		$type=@$_SESSION['AMD'][2];
		if(strtolower($type)!="administrator" and strtolower($type)!="supervisor"){
		$id = @$_SESSION['AMD'][0];
		$wh.=" and create_by='$id'";
		}
		$sql = " from #_".tblName." where 1 ".$zone.$mtype.$extra.$extra1.$extra2.$wh;
		$order_by == '' ? $order_by = (($ord)?'orders':(($fld)?$fld:'shortorder')) : true;
        $order_by2 == '' ? $order_by2 = (($otype)?$otype:'DESC') : true;
		$sql_count = "select count(*) ".$sql; 
		$sql .= "order by $order_by $order_by2 ";
		$sql .= "limit $start, $pagesize ";
		$sql = $columns.$sql;
		$result = parent::db_query($sql);
		$reccnt = parent::db_scalar($sql_count);
		
		if($_POST){
			if($_SESSION["message_post"]!='')
			{
				$reccnt=0;
			}
		}
		return array($result,$reccnt);
	 }
	 
	 public function password($password)
	 {
	        $password=md5($password); 
		    $password=base64_encode($password); 	
		    return $password;
	 }
	 // 13 jan 2016
	 public function get($table=NULL,$pid=NULL,$field='pid',$order='asc')
	 {
		 if($table!=NULL){
			 $sql = "select * from #_".$table." Where 1";
			 if($pid!=NULL){
			 $sql.=" and {$field}='{$pid}'";
			 }
			 $sql.=" order by pid ".$order." ";
			$result = parent::db_query($sql); 
			return $result;
		 }
		 
	 }
	 public function get2($table=NULL,$pid=NULL,$field='pid',$field1=NULL,$field_val1=NULL,$order='asc')
	 {
		 if($table!=NULL){
			 $sql = "select * from #_".$table." Where 1";
			 if($pid!=NULL){
			 $sql.=" and {$field}='{$pid}'";
			 }
			 if($field1!=NULL and $field_val1!=NULL){
				$sql.=" and {$field1}='{$field_val1}'"; 
			 }
			 $sql.=" order by pid ".$order." ";
			$result = parent::db_query($sql); 
			return $result;
		 }
		 
	 }
	 public function count_val($resource=NULL)
	 {
		 if($resource!='')
		 {
			 $tot = mysql_num_rows($resource);
			 return ($tot)?$tot:0;
		 }
	 }
	 public function upload()
	 {
		parent::sqlquery("rs",'reports',$_POST);
		parent::sessset('File has been Uploaded', 's');
	 }
}








?>