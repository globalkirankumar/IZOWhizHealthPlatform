<?php 
include(FS_ADMIN._MODS."/sponsored/class.inc.php");


$OP = new Options();

if($BSC->is_post_back())
{
   if($uid)
   {
	   $_POST['updateid']=$uid;
       $flag = $OP->update($_POST);
  
   }else {
	 
	   $flag = $OP->add($_POST);
	   
	     
   }
   
   if($flag==1)
   {
   
     $BSC->redir($ADMIN->iurl($comp.(($start)?'&start='.$start:'').(($subpage_id)?'&subpage_id='.$subpage_id:'').(($alumniid)?'&alumniid='.$alumniid:'').(($division_id)?'&division_id='.$division_id:'')).$dlr, true);
   }
}else if($uid) {
    $query =$PDO->db_query("select * from #_".tblName." where pid ='".$uid."' "); 
	$row = $PDO->db_fetch_array($query);
	@extract($row);	
}

?>

<div class="vd_content-section clearfix">
		  	<div class="row" id="form-basic">
			  <?=$ADMIN->alert()?>
              		<div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span> Add/Health <?=$ADMIN->compname($comp)?> </h3>
                </div>
              		
              		<div class="panel-body">
				
			<!--add-update form-->
                 <form  class="form-horizontal body-gap" action="#" role="form" id="register-form">
                 
                 
                 	<div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label  ">Specialities <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls">
				<select class="validate[required]" data-errormessage-value-missing="Specialities is required!" name="specialitie" value="<?=$specialitie?>" onChange="return getSp(this.value)">
							<option value="">---Select Specialities---</option>
							<?php $record=$PDO->db_query("select * from #_specialities");?>
						<?php while($res=mysql_fetch_array($record)){?>
						<option value="<?=$res['pid']?>" <?php if($specialitie==$res['pid']){echo "selected";}?>><?=$res['name']?></option>
						<?php } ?>
							
						</select>	
                         
                        </div>
                    </div>
                   
					<div class="form-group col-md-6 col-sm-6 col-xs-12" id="doctor-list">
                        <label class="control-label  "> Doctor Name <span class="vd_red">*</span></label>
                         <div id="first-name-input-wrapper" class="controls " id="doctor-list">
                        <select class="validate[required]" data-errormessage-value-missing="Name is required!" name="drname" value="<?=$drname?>">
                            <option value="">---Select Doctor---</option>
                            <?php 
                               $record=$PDO->db_query("select * from #_doctors");?>
                                  <?php while($res=mysql_fetch_array($record)){?>
                                    <option value="<?=$res['pid']?>" <?php if($drname==$res['pid']){echo "selected";}?>><?=$res['name']?></option>
                            <?php } ?>
                        </select>
                    </div>
				</div>
                    
              
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label  ">Position <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls ">
                          <input class="validate[required]" data-errormessage-value-missing="Position is required!" name="position" id="name" value="<?=$position?>" type="text">
                        </div>
                    </div>
                    
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                         <label class=" control-label"> Date Form <span class="star">*</span></label>
                            <div class=" controls">
                              <div class="vd_input-wrapper light-theme no-icon">
                             <input type="text" name="date_from" id="date_from" value="<?=$date_from?>" class="datepicker">
                            </div>
                          </div>
                    </div>
                    
                    <div class="form-group col-md-6 col-sm-6 col-xs-12">
                         <label class=" control-label"> Date To <span class="star">*</span></label>
                            <div class="controls">
                              <div class="vd_input-wrapper light-theme no-icon">
                             <input type="text" name="date_to" id="date_to" value="<?=$date_to?>" class="datepicker">
                            </div>
                          </div>
                    </div>
                    
					
					<div class="form-group col-md-6 col-sm-6 col-xs-12">
                      <label class="control-label ">Status <span class="vd_red">*</span></label>
                      <div id="website-input-wrapper" class="controls ">
                          <select name="status" class="validate [required]" data-errormessage-value-missing="Status is required!">
                              <option  value="">-------Select Status------</option>
								<option value="1" <?=($status==1)?'selected="selected"':''?>  >Active</option>
								<option value="0" <?=(isset($status) && $status==0)?'selected="selected"':''?>>Inactive</option>
                          </select>
                    </div>
                  </div>
                
<!--
                  
                  <div class="form-group form-actions">
                    <div class="col-sm-6"></div>
                    <div class="col-md-6">
					 <input type="hidden" name="pid" value="<?=$pid?>" />
                       <button id="submit-register" name="submit-register" class="btn vd_btn vd_bg-green vd_white uibutton loading" type="submit"><i class="icon-ok" ></i> Save</button>
                          <button onclick="location.reload();" class="btn vd_btn" type="button">Cancel</button>
                    </div>
                  </div>
-->
                  
                  
                  		<div class="form-group form-actions">
                   
                    <div class="col-md-12 mgbt-xs-10 mgtp-20">
                     
                      
                      <div class="mgtp-10 gp-center">
					   <input type="hidden" name="pid" value="<?=$pid?>" />
                        <button class="btn vd_bg-green vd_white greenbutton uibutton loading" type="submit" id="submit-register" name="submit-register">Save</button>
                        
                        
                         <button onclick="location.reload();" class="btn  orng-btn" type="button" >Cancel</button>
                      </div>
                    </div>
                   
                  </div>	
                  
                  
                  
             <!--   </form>-->
			<!-- close add-update form-->
              		</div>
              </div>
            </div>
		</div>
<script src="<?=SITE_PATH?>validation/js/jquery-1.8.2.min.js"></script>
<script src="<?=SITE_PATH?>validation/js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=SITE_PATH?>validation/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#formID").validationEngine();
		});
		$('.hide_show').change(function(){
			var id = $(this).attr("data-id");
			if($(this).val()=="Available")
			{
				$('#'+id).show();
			}else{
				$('#'+id).hide();
			}
		});
		$('.services').click(function(){
			var id = $(this).attr("data-id");
			if(this.checked)
			{
				$('#'+id).show();
			}else{
				$('#'+id).hide();
			}
		});
	
</script>
<script>
function getSp(val) {
	//alert("test");
	$.ajax({
	type: "POST",
	url: "<?=SITE_PATH_ADM?>modules/sponsored/get_Doctor.php",
	data:'pid='+val,
	success: function(data){
		//alert(data);		
		$("#doctor-list").html(data);
	}
	});
}
</script>