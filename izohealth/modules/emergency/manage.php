<style>

.table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
    
    vertical-align: middle;
   
}

</style>
<?php 
include(FS_ADMIN._MODS."/emergency/class.inc.php");
$OP = new Options();

if($action)
{
  
  if($uid >0  || !empty($arr_ids))
  {
   
	switch($action)
	{
		  case "read":
						 $OP->read($uid,1);
						 $ADMIN->sessset('Record has been deleted', 'e'); 
						 break;
						 
		  case "Delete":
						 $OP->delete($arr_ids);
						 $ADMIN->sessset(count($arr_ids).' Item(s) Deleted', 'e');
						 break;
						 
						 
		  case "Active":
						 $OP->status($arr_ids,1);
						 $ADMIN->sessset(count($arr_ids).' Item(s) Active', 's');
						 break;
						 
		  case "Inactive":
						 $OP->status($arr_ids,0);
						 $ADMIN->sessset(count($arr_ids).' Item(s) Inactive', 's');
						 break;
					 
		  
		  default:
	}
      $BSC->redir($ADMIN->iurl($comp), true);
  }
}




$start = intval($start);




$pagesize = intval($pagesize)==0?(($_SESSION["totpaging"])?$_SESSION["totpaging"]:DEF_PAGE_SIZE):$pagesize;
list($result,$reccnt) = $OP->display($start,$pagesize,$fld,$otype,$search_data,'division_id',$division_id);

?>
<!--right section panel-->
		<div class="vd_content-section clearfix">
		  	<div class="row">
			
              <div class="">
			  <?=$ADMIN->alert()?>
              		<div class="panel-heading vd_bg-green white">
                    <h3 class="panel-title">Labs List </h3>
                  	</div>
              		<div class="section-body">
              	
			<!--edit table-->
                    <div class="table-responsive ">
                                        <table class="table data-tbl custom-style table-striped display nowrap table-hover table-striped table-bordered dataTable" id="sortable">

                    <thead>
                      <tr class="tbl-head">
                       <!-- <th><?=$ADMIN->check_all()?></th>-->
                        <th width="3%">No.</th>
						 <th width="15%">Patients</th>
                          <th width="12%">Mobile</th>
                         <th width="28%">Loaction</th>
                         <th width="10%">Latitude </th>
                         <th width="11%">Longitude </th>
                        <th width="6%">Status</th>
                        <th width="15%">Action</th>												<th width="15%">Date</th>
                      </tr>
               </thead>
					<tbody>
					<!--</table>
					 <ul class="sortable-liststyle" style="list-style:none;">-->
          
          
            <?php if($reccnt)
			      { 
			
			        $nums = (($start)?$start+1:1); 
					$k = 0;
					$division_name = $PDO->getSingleresult("select name from #_division where pid='".$division_id."' ");
				    while ($line = $PDO->db_fetch_array($result))
				    {
					    @extract($line);
						$k++;
						$css =($k%2!=0)?'success':'';
			
			
			?>
          <!--  <li style="list-style:none;" id="recordsArray_<?=$pid?>">
			<table class="table data-tbl custom-style">
			 <thead>-->
				<tr data-item-id=1 class="item <?=$css?>">
                 <!-- <td><?=$ADMIN->check_input($pid)?></td>-->
                  <td><?=$nums?></td>
                  <td><?=$PDO->getSingleResult("select name from #_patients where pid='".$line['patient_id']."'")?></td>
                   <td><?=$PDO->getSingleResult("select phone from #_patients where pid='".$line['patient_id']."'")?></td>                  	
                  <th><?php
                         $location =json_decode($location);
						 echo $location->results[0]->formatted_address;
					  
					 ?></th>
                  <th><?=$attitude?></th>
                  <th><?=$longitude?></th>
                  <td><?=($status==0)?'Unread':'Read'?></td>
                  <td>
                  <?php if($status==0) { ?>
				    <a class="editfile" href="<?=SITE_PATH?>home.php?comp=emergency&uid=<?=$pid?>&action=read">Mark To Read</a>
				 <?php } ?>
                  </td>				  <td><?=date('d M Y h:i a',strtotime($created_on));?></td>
            </tr>
			<!-- </thead>
			</table>-->
            </li>
            <?php $nums++; } ?>
            
            
          
           <?php  }else { echo '<div align="center" class="norecord">No Record Found</div>'; } ?>
           
         <!--  </ul>-->
		 </tbody>
		   </table>
		   <?php include("cuts/paging.inc.php");?>
						<div class="pull-right pagination" style="display:none;">
                        <ul class="pagination">
                        <li class="page-pre"><a href="#">‹</a></li>
                        <li class="page-number active"><a href="#">1</a></li>
                        <li class="page-number"><a href="#">2</a></li>
                        <li class="page-number"><a href="#">3</a></li>
                        <li class="page-number"><a href="#">4</a></li>
                        <li class="page-number"><a href="#">5</a></li>
                        <li class="page-last-separator disabled"><a href="#">...</a></li>
                        <li class="page-last"><a href="#">80</a></li>
                        <li class="page-next"><a href="#">›</a></li>
                        </ul>
                        </div>	
                        </div>
			<!-- close edit table-->
              		</div>

            <!--next button-->
           
			<!--Close next button-->
              </div>
            </div>
		</div>
  <!--Close right section panel-->
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script language="javascript">
jQuery(document).ready(function(){ 
	jQuery(function() {
		jQuery("#ordrz ul").sortable({ opacity: 0.6, cursor: 'move', update: function() {
			var order = jQuery(this).sortable("serialize") + '&tbl=<?=tblName?>&field=pid'; 
			 $.post("<?=SITE_PATH_ADM?>modules/orders.php", order, function(theResponse){ }); 															
		}								  
		});
	});
});	
</script>