<?php 
include(FS_ADMIN._MODS."/admin_users/user.inc.php");
$US = new Users();

if($action)
{
  
  if($uid >0  || !empty($arr_ids))
  {
   
	switch($action)
	{
		  case "del":
						 $US->delete($uid);
						 $ADMIN->sessset('Record has been deleted', 'e'); 
						 break;
						 
		  case "Delete":
						 $US->delete($arr_ids);
						 $ADMIN->sessset(count($arr_ids).' Item(s) Deleted', 'e');
						 break;
						 
						 
		  case "Active":
						 $US->status($arr_ids,1);
						 $ADMIN->sessset(count($arr_ids).' Item(s) Active', 's');
						 break;
						 
		  case "Inactive":
						 $US->status($arr_ids,0);
						 $ADMIN->sessset(count($arr_ids).' Item(s) Inactive', 's');
						 break;
					 
		  
		  default:
	}
    $BSC->redir($ADMIN->iurl($comp), true);
  }
}

$start = intval($start);
$pagesize = intval($pagesize)==0?(($_SESSION["totpaging"])?$_SESSION["totpaging"]:DEF_PAGE_SIZE):$pagesize;
list($result,$reccnt) = $US->display($start,$pagesize,$fld,$otype,$search_data);

?>
<!--right section panel-->
		<div class="vd_content-section clearfix">
		  	<div class="row">
			
              <div class="">
			  <?=$ADMIN->alert()?>
              		<div class="panel-heading vd_bg-green white">
                    <h3 class="panel-title">Patient/Caller </h3>
                  	</div>
              		<div class="section-body">
              			
						
			<!--edit table-->
                    <div class="table-responsive " id="ordrz">
                    <table class="table data-tbl custom-style table-striped display nowrap table-hover table-striped table-bordered dataTable" >
                    <thead>
                      <tr class="tbl-head">
                        <th><?=$ADMIN->check_all()?></th>
                        <th>No.</th>
						 <th><a href="<?=$ADMIN->iurl($comp)?>&fld=name<?=(($otype=='asc')?"&otype=desc":'&otype=asc')?>" <?=(($fld=='name')?'class="selectedTab"':'')?>><span <?=(($otype=='asc')?'class="des"':'class="asc"')?>> Name</span></a></th>
                        <th><a href="<?=$ADMIN->iurl($comp)?>&fld=email<?=(($otype=='asc')?"&otype=desc":'&otype=asc')?>" <?=(($fld=='email')?'class="selectedTab"':'')?>><span <?=(($otype=='asc')?'class="des"':'class="asc"')?>>Email</span></a></th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
               </thead>
					
				<!--</table>
					 <ul class="sortable-liststyle" style="list-style:none;">
          -->
          <tbody>
            <?php if($reccnt)
			      { 
			
			        $nums = (($start)?$start+1:1); 
					$k = 0;
				    while ($line = $PDO->db_fetch_array($result))
				    {
					    @extract($line);
						$k++;
						$css =($k%2!=0)?'success':'';
			
			
			?>
          <!--<li style="list-style:none;" id="recordsArray_<?=$pid?>">
			<table class="table data-tbl custom-style">
			 <thead>-->
				<tr data-item-id=1 class="item <?=$css?>">
                  <th><?=$ADMIN->check_input($pid)?></th>
                  <th><?=$nums?></th>
                  <th><?=ucwords($name)?></th>
                  <th><?=$email?></th>
                  <th><?=$ADMIN->displaystatus($status)?></th>
                  <th><?=$ADMIN->action($comp, $pid)?></th>
            </tr>
			<!-- </thead>
			</table>
            </li>-->
            <?php $nums++; } ?>
            
            
          
           <?php  }else { echo '<div align="center" class="norecord">No Record Found</div>'; } ?>
           
         <!-- </ul>-->
		 </tbody>
		 </table>
		
		   <?php include("cuts/paging.inc.php");?>
						<div class="pull-right pagination" style="display:none;">
                        <ul class="pagination">
                        <li class="page-pre"><a href="#">‹</a></li>
                        <li class="page-number active"><a href="#">1</a></li>
                        <li class="page-number"><a href="#">2</a></li>
                        <li class="page-number"><a href="#">3</a></li>
                        <li class="page-number"><a href="#">4</a></li>
                        <li class="page-number"><a href="#">5</a></li>
                        <li class="page-last-separator disabled"><a href="#">...</a></li>
                        <li class="page-last"><a href="#">80</a></li>
                        <li class="page-next"><a href="#">›</a></li>
                        </ul>
                        </div>	
                        </div>
			<!-- close edit table-->
              		</div>

            <!--next button-->
           
			<!--Close next button-->
              </div>
            </div>
		</div>
  <!--Close right section panel-->
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script language="javascript">
jQuery(document).ready(function(){ 
	jQuery(function() {
		jQuery("#ordrz ul").sortable({ opacity: 0.6, cursor: 'move', update: function() {
			var order = jQuery(this).sortable("serialize") + '&tbl=<?=tblName?>&field=pid'; 
			 $.post("<?=SITE_PATH_ADM?>modules/orders.php", order, function(theResponse){ }); 															
		}								  
		});
	});
});	
</script>