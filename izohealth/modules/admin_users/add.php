<style>

	.form-horizontal .control-label, .form-horizontal .radio, .form-horizontal .checkbox, .form-horizontal .radio-inline, .form-horizontal .checkbox-inline, .form-horizontal .control-value {
    padding-top: 4px;
    padding-left: 9px;
}

</style>


<?php 
include(FS_ADMIN._MODS."/admin_users/user.inc.php");


$US = new Users();

if($BSC->is_post_back())
{
   if($uid)
   {
	   $_POST['updateid']=$uid;
       $flag = $US->update($_POST);
  
   }else {
	 
	   $flag = $US->add($_POST);
	   
	     
   }
   
   if($flag==1)
   {
   
     $BSC->redir($ADMIN->iurl($comp.(($start)?'&start='.$start:'').(($subpage_id)?'&subpage_id='.$subpage_id:'').(($alumniid)?'&alumniid='.$alumniid:'').(($galleryid)?'&galleryid='.$galleryid:'')).$dlr, true);
   }
}


if($uid)
{
    $query =$PDO->db_query("select * from #_".tblName." where pid ='".$uid."' "); 
	$row = $PDO->db_fetch_array($query);
	@extract($row);	
}

?>

<div class="vd_content-section clearfix">
		  	<div class="row" id="form-basic">
			  <?=$ADMIN->alert()?>
              		<div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span> Add/Health <?=$ADMIN->compname($comp)?> </h3>
					</div>
              		
              		<div class="panel-body">
				

              			
						
			<!--add-update form-->
                <!--    <form  class="form-horizontal body-gap" action="#" role="form" id="register-form">-->
              <div class="row">
                    <div class="form-group">
                      <div class="col-md-4">
                        <label class="control-label">Name <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input class="validate[required]" data-errormessage-value-missing="Name is required!" name="name" id="name"  value="<?=$name?>" type="text">
                        </div>
                      </div>
                   
                    <div class="col-md-4">
                      <label class="control-label">User Type <span class="vd_red">*</span></label>
                      <div id="website-input-wrapper" class="controls">
                          <select name="user_type" class="validate[required]" data-errormessage-value-missing="User Type is required!">
                              <option  value="">-------Select Type------</option>
							  <?php $res=""; if(@$user_id){
							  $res = $PDO->getSingleresult("select user_type from #_users_login where pid='{$user_id}'");}?>
								<option value="administrator" <?=(@$res=="administrator")?'selected="selected"':''?> <?=(@$user_type=="administrator")?'selected="selected"':''?>  >Admin</option>
								<option value="supervisor" <?=(@$res=="supervisor")?'selected="selected"':''?> <?=(@$user_type=="supervisor")?'selected="selected"':''?>>Supervisor</option>
                          </select>
                        </div>
                    </div>
                  
                      <div class="col-md-4">
                        <label class="control-label  ">Email <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input class="validate[required]" data-errormessage-value-missing="Email is required!" name="email" id="email"  value="<?=$email?>" type="text">
                        </div>
                      </div>
                    
                    <div class="col-md-4">
                      <label class="control-label">Status <span class="vd_red">*</span></label>
                      <div id="website-input-wrapper" class="controls">
                          <select name="status" class="validate[required]" data-errormessage-value-missing="Status is required!">
                              <option  value="">-------Select Status------</option>
								<option value="1" <?=($status==1)?'selected="selected"':''?>  >Active</option>
								<option value="0" <?=(isset($status) && $status==0)?'selected="selected"':''?>>Inactive</option>
                          </select>
                        </div>
                    </div>
                  
                    
					<?php if($pid!=''){?>
					
                      <div class="col-md-4">
                        <label class="control-label">Change Password <span class="vd_red"></span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input  name="change_pwd" id="change_pwd"  value="1" type="checkbox">
                        </div>
                      </div>
                   
					<?php } ?>
					<div class=" pwd" style="<?php if($pid!=''){ echo 'display:none;';}?>">
                      <div class="col-md-4">
                        <label class="control-label">Password <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input class="validate[required,minSize[6]]" data-errormessage-value-missing="Password is required!" name="password" id="password"  value="" type="password">
                        </div>
                      </div>
                    </div>
                   
					</div>
                
                  <div class="form-group">
                   
                    <div class="col-md-12 mgbt-xs-10 mgtp-20">
                     
                      
                      <div class="mgtp-10 gp-center">
					   <input type="hidden" name="pid" value="<?=$user_id?>" />
                        <button class="btn vd_bg-green vd_white greenbutton uibutton loading" type="submit" id="submit-register" name="submit-register">Submit</button>
                        
                        
                         <button onclick="location.reload();" class="btn  orng-btn" type="submit" id="submit-register" name="submit-register">Clear Form</button>
                      </div>
                    </div>
                   
                  </div>
             <!--   </form>-->
			<!-- close add-update form-->
						</div>
              		</div>
              </div>
            </div>
		</div>
