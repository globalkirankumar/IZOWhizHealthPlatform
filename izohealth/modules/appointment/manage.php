<?php 
include(FS_ADMIN._MODS."/appointment/class.inc.php");
$OP = new Options();

if($action)
{
  
  if($uid >0  || !empty($arr_ids))
  {
   
	switch($action)
	{
		  case "del":
						 $OP->delete($uid);
						 $ADMIN->sessset('Record has been deleted', 'e'); 
						 break;
						 
		  case "Delete":
						 $OP->delete($arr_ids);
						 $ADMIN->sessset(count($arr_ids).' Item(s) Deleted', 'e');
						 break;
						 
						 
		  case "Active":
						 $OP->status($arr_ids,1);
						 $ADMIN->sessset(count($arr_ids).' Item(s) Active', 's');
						 break;
						 
		  case "Inactive":
						 $OP->status($arr_ids,0);
						 $ADMIN->sessset(count($arr_ids).' Item(s) Inactive', 's');
						 break;
					 
		  
		  default:
	}
    $BSC->redir($ADMIN->iurl($comp), true);
  }
}
if($BSC->is_post_back())
{
   $path = UP_FILES_FS_PATH."/reports";
   if($_FILES['reports']['name'])
   {
		$allowed =  array('gif','png' ,'jpg','jpeg','bmp','txt','pdf','doc','docx');
		$filename = $_FILES['reports']['name'];
		$size = $_FILES['reports']['size'];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if(!in_array($ext,$allowed) ) {
			echo "<script>alert('File Type Mismatch')</script>";
		}else if($size>2097152){
			echo "<script>alert('Maximum file size is 2M')</script>";
		}else{
			 $_POST['reports'] = $BSC->uploadFile2($path,$_FILES['reports']['name'],'reports');
			$OP->upload();
			echo "<script>alert('Reports Uploaded')</script>";
		}
	 
   }

}
$start = intval($start);
$pagesize = intval($pagesize)==0?(($_SESSION["totpaging"])?$_SESSION["totpaging"]:DEF_PAGE_SIZE):$pagesize;
list($result,$reccnt) = $OP->display($start,$pagesize,$fld,$otype,$search_data);

?>
<!--right section panel-->
		<div class="vd_content-section clearfix">
		  	<div class="row">
			
              <div class="">
			  
              		<div class="panel-heading vd_bg-green white">
                    <h3 class="panel-title">Appointment List </h3>
                  	</div>
                  	
                  	<?=$ADMIN->alert()?>
              
              <div class="info-call-details ">
					
                
                 <ul>
				  
                  	<?php if(strtolower($user_type)=="hospital" || strtolower($user_type)=="clinics" || strtolower($user_type)=="manager" || strtolower($user_type)=="executive"){?>
	                   <li style="width: 150px;"><select name="doctor_search" id="doctor_search">
					   <option value="">Select Doctor</option>
					   <?php    
					    if($user_type == 'hospital'){
					      $pid = $PDO->getSingleresult("select pid from #_hospitals where user_id ='".$user_id."' ");
					      $wh = ' and hospital_id = "'.$pid.'" ';
					    }else if($user_type=='clinics'){
					      $clinic = $PDO->getSingleresult("select pid from #_clinics where user_id ='".$user_id."' ");
					      $wh = ' and clinic_id = "'.$clinic.'" ';
					    }else{
					      $created_id = $PDO->getSingleresult("select createdby_userid from #_manager_executives where user_id ='".$user_id."' ");
					      $created_type = $PDO->getSingleresult("select createdby_user_type from #_manager_executives where user_id ='".$user_id."' ");
					      if($created_type == "hospital"){
					        $pid = $PDO->getSingleresult("select pid from #_hospitals where user_id ='".$created_id."' ");
					        $wh = ' and hospital_id = "'.$pid.'" ';
					      }else if($created_type == "clinics"){
					        $clinic = $PDO->getSingleresult("select pid from #_clinics where user_id ='".$created_id."' ");
					        $wh = ' and clinic_id = "'.$clinic.'" ';
					      }else{
					        $wh = "";
					      }
					    }
					        $pecialities_query=$PDO->db_query("select * from #_doctors where status='1' ".$wh."  group by name order by name "); 
					     
					        while($pecialities_data= $PDO->db_fetch_array($pecialities_query))
					        {
					   ?>
					             <option value="<?=$pecialities_data['pid']?>"  <?=($doctor_search==$pecialities_data['pid'])?'selected="selected"':''?>  ><?=ucfirst($pecialities_data['name'])?> </option>
					  <?php } ?>
					  </select></li>
                  	<?php }?>
                   
                      <li>  <input type="text"  name="search_patients_name"  value="<?=$search_patients_name?>"  placeholder="Patient Name " /></li>
                      <li>  <input type="text"  name="search_patients_id"  value="<?=$search_patients_id?>"  placeholder="Patient ID " /></li>
                      <li>  <input type="text"  name="search_phone_number"  value="<?=$search_phone_number?>"  placeholder="Phone Number" /></li>
                      <li>  <input type="text" class="datepicker" name="search_date"  value="<?=$search_date?>"  placeholder="Date" /></li>
                     
                     <li><input type="submit" class="records-search greenbutton inputsearch" value="Search"></li>
                      
                   
                      <!--<li><input type="submit" class="records-search greenbutton inputsearch" value="Search"></li>-->
				  </ul>
              </div>
                  	
                  	
                  	
              		<div class="section-body">
              			
						
			<!--edit table-->
                    <div class="table-responsive ">
                    <table class="table data-tbl custom-style table-striped" id="sortable">
                    <thead>
                      <tr class="tbl-head">
                        <th><?=$ADMIN->check_all()?></th>
                        <th>No.</th>
                        <?php if(strtolower($user_type)=="hospital" || strtolower($user_type)=="clinics" || strtolower($user_type)=="manager" || strtolower($user_type)=="executive" || strtolower($user_type)=="doctor" || strtolower($user_type)=="doctors"){?>
						 <th>Token No.</th>
						 <?php }?>
						 <th>Patient Name</th>
						 <th>Patient ID</th>
						 <?php $user_type = @$_SESSION['AMD'][2];?>
						 <?php if(strtolower($user_type)=="doctor" or strtolower($user_type)=="doctors"){?>
						 <th>Appointment Date</th>
						 <th>Appointment Time</th>
						 <?php }else if(strtolower($user_type)=="hospital" || strtolower($user_type)=="clinics" || strtolower($user_type)=="manager" || strtolower($user_type)=="executive"){?>
						 <th>Doctor's Name</th>
						 <th>Appointment Date</th>
						 <th>Appointment Time</th>
						 <th>Change Status</th>						 
						 <?php }else{ ?>
						<!-- <th>Call Type</th>-->
						 <?php } ?>
						<th>Status</th>
                        <th>Action</th>
                      </tr>
               </thead>
					<tbody>
				
          
            <?php if($reccnt)
			      { 
			
			        $nums = (($start)?$start+1:1); 
					$k = 0;
				    while ($line = $PDO->db_fetch_array($result))
				    {
					    @extract($line);
						$k++;
						$css =($k%2!=0)?'success':'';
			
			
			?>
				<tr data-item-id=1 class="item <?=$css?>">
                  <th><?=$ADMIN->check_input($pid)?></th>
                  <th><?=$nums?></th>
                  <?php if(strtolower($user_type)=="hospital" || strtolower($user_type)=="clinics" || strtolower($user_type)=="manager" || strtolower($user_type)=="executive" || strtolower($user_type)=="doctor" || strtolower($user_type)=="doctors"){?>
                  <th><?=$token_no?></th>
                  <?php }?>
				  <th><?=ucwords($PDO->getSingleResult("select name from #_patients where pid='{$patient_id}'"))?></th>
                  <th><?=$PDO->getSingleResult("select patient_id from #_patients where pid='{$patient_id}'")?></th>
				  <?php if(strtolower($user_type)=="doctor" or strtolower($user_type)=="doctors"){?>
				  <th><?=date('d M Y', strtotime($app_date))?></th>
                  <th><?=$app_time?></th>
				  <?php }else if(strtolower($user_type)=="hospital" || strtolower($user_type)=="clinics" || strtolower($user_type)=="manager" || strtolower($user_type)=="executive"){
					$book_name = $PDO->getSingleResult("select name from #_doctors where user_id='{$doctor_id}'");?>		
                  <th><?=$book_name?></th>
                  <th><?=date('d M Y', strtotime($app_date))?></th>
                  <th><?=$app_time?></th>
                  <th><select name="status" id="status" onchange="changeappointmentstatus(this.value,'appointment', '<?=$pid?>')">
                        <option value="" >---Select Status---</option>
                           <option value="1" <?=($line['status']==1)?'selected="selected"':''?>   >Active <?=$result['status']?></option>
                           <option value="0"  <?=($line['status']==0)?'selected="selected"':''?>>Inactive</option>
                           <option value="2" <?=($line['status']==2)?'selected="selected"':''?> >Processing</option>
                           <option value="3" <?=($line['status']==3)?'selected="selected"':''?> >Finished</option>
                    </select></th>
				  <?php }else{?>
				 <!-- <th><?=$PDO->getSingleResult("select call_type from #_call_details where pid='{$call_id}'")?></th>-->
				  <?php }?>
				  <th><?=$ADMIN->displaystatus($status)?></th>
				  <th style="width: 80px;"><a href="<?=$ADMIN->iurl('appointment','view')?>&uid=<?=$pid?>" style="color:red" title="Explore"><i class="fa fa-eye"></i>  </a>&nbsp;&nbsp;&nbsp;
				  <a href="#"  data-toggle="modal" data-target=".modal-edits8">  <i class="fa fa-upload" title="Upload"></i></a></span>
				  </th>
                 
            </tr>
            <?php $nums++; } ?>
            
            
          
           <?php  }else { echo '<tr ><td colspan="8"><div align="center" class="norecord">No Record Found</div></tr></td>'; } ?>
        
		 </tbody>
		   </table>
		   <?php include("cuts/paging.inc.php");?>
						<div class="pull-right pagination" style="display:none;">
                        <ul class="pagination">
                        <li class="page-pre"><a href="#">‹</a></li>
                        <li class="page-number active"><a href="#">1</a></li>
                        <li class="page-number"><a href="#">2</a></li>
                        <li class="page-number"><a href="#">3</a></li>
                        <li class="page-number"><a href="#">4</a></li>
                        <li class="page-number"><a href="#">5</a></li>
                        <li class="page-last-separator disabled"><a href="#">...</a></li>
                        <li class="page-last"><a href="#">80</a></li>
                        <li class="page-next"><a href="#">›</a></li>
                        </ul>
                        </div>	
                        </div>
			<!-- close edit table-->
              		</div>

            <!--next button-->
           
			<!--Close next button-->
              </div>
            </div>
		</div>
  <!--Close right section panel-->
  <?php $call_details_id = $call_id; ?>
  <?php include('upload-model.php');?>
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script language="javascript">
jQuery(document).ready(function(){ 
	jQuery(function() {
		jQuery("#ordrz ul").sortable({ opacity: 0.6, cursor: 'move', update: function() {
			var order = jQuery(this).sortable("serialize") + '&tbl=<?=tblName?>&field=pid'; 
			 $.post("<?=SITE_PATH_ADM?>modules/orders.php", order, function(theResponse){ }); 															
		}								  
		});
	});
});	
</script>
<script>
function changeappointmentstatus(status,table, uid){
         var str ='status='+status+'&table='+table+'&uid='+uid;
		 $.ajax({type: "POST",
				 url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag=status",
				 data: str,
				 cache: false,
				 success: function(html){				 
				   alert('Appointment status is changed');
				   location.reload();
					
				 }
		});
}
</script>