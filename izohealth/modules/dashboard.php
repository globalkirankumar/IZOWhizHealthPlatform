
    <!-- Bootstrap Core CSS -->
    <link href="<?=SITE_PATH?>css/dashboard/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="<?=SITE_PATH?>dashboard_plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- chartist CSS -->
    <link href="<?=SITE_PATH?>dashboard_plugins/bower_components/chartist-js/dist/chartist.min.css" rel="stylesheet">
    <link href="<?=SITE_PATH?>dashboard_plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
    <!--Owl carousel CSS -->
    <link href="<?=SITE_PATH?>dashboard_plugins/bower_components/owl.carousel/owl.carousel.min.css" rel="stylesheet" type="text/css" />
    <link href="<?=SITE_PATH?>dashboard_plugins/bower_components/owl.carousel/owl.theme.default.css" rel="stylesheet" type="text/css" />
    <!--Gauge chart CSS -->
    <link href="<?=SITE_PATH?>dashboard_plugins/bower_components/Minimal-Gauge-chart/css/cmGauge.css" rel="stylesheet" type="text/css" />
    <!-- animation CSS -->
    <link href="<?=SITE_PATH?>css/dashboard/css/animate.css" rel="stylesheet">
    <!-- morris CSS -->
    <link href="<?=SITE_PATH?>dashboard_plugins/bower_components/morrisjs/morris.css" rel="stylesheet">
    <link href="<?=SITE_PATH?>dashboard_plugins/bower_components/css-chart/css-chart.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?=SITE_PATH?>css/dashboard/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?=SITE_PATH?>css/dashboard/css/colors/blue-dark.css" id="theme" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

    <!-- ============================================================== -->
    <!-- Preloader -->
    <!-- ============================================================== -->
<!--
    <div class="preloader">
        <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
        </svg>
    </div>
-->
    <!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
       
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="">
            <div class="container-fluid">
              
              
               <!-- ============================================================== -->
                <!-- analitics widgets -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-md-4 col-sm-12 col-xs-12"> 
                        <div class="white-box">
                            <h3 class="box-title"><small class="pull-right m-t-10 text-success"><i class="fa fa-sort-asc"></i> 18% High then last month</small>Appointment Fixed</h3>
                            <div class="stats-row">
                                <div class="stat-item">
                                    <h6>Overall Appointment</h6> <b>1400</b></div>
                                <div class="stat-item">
                                    <h6>Montly</h6> <b>600</b></div>
                                <div class="stat-item">
                                    <h6>Day</h6> <b>20</b></div>
                            </div>
                            <div id="sparkline8"></div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="white-box">
                            <h3 class="box-title"><small class="pull-right m-t-10 text-danger"><i class="fa fa-sort-desc"></i> 10% High then last month</small>Appointment Completed</h3>
                            <div class="stats-row">
                                <div class="stat-item">
                                    <h6>Overall Appointment</h6> <b>1000</b></div>
                                <div class="stat-item">
                                    <h6>Montly</h6> <b>500</b></div>
                                <div class="stat-item">
                                    <h6>Day</h6> <b>15</b></div>
                            </div>
                            <div id="sparkline9"></div> 
                        </div>
                    </div>
<!--
                    <div class="col-md-3 col-sm-12 col-xs-12">
                        <div class="white-box">
                            <h3 class="box-title"><small class="pull-right m-t-10 text-success"><i class="fa fa-sort-asc"></i> 18% High then last month</small>Transaction Amount</h3>
                            <div class="stats-row">
                                <div class="stat-item">
                                    <h6>Overall Amount</h6> <b>120000 Rs</b></div>
                                <div class="stat-item">
                                    <h6>Montly</h6> <b>40000 Rs</b></div>
                                <div class="stat-item">
                                    <h6>Day</h6> <b>1700 Rs</b></div>
                            </div>
                            <div id="sparkline10"></div>
                        </div>
                    </div>
-->
                    <div class="col-md-4 col-sm-12 col-xs-12">
                        <div class="white-box">
                            <h3 class="box-title"><small class="pull-right m-t-10 text-success"><i class="fa fa-sort-asc"></i> 40% High then last month</small>Patient Activation</h3>
                            <div class="stats-row">
                                <div class="stat-item">
                                    <h6>Overall Activation</h6> <b>2500</b></div>
                                <div class="stat-item">
                                    <h6>Montly</h6> <b>600</b></div>
                                <div class="stat-item">
                                    <h6>Day</h6> <b>20</b></div>
                            </div>
                            <div id="sparkline11"></div> 
                        </div>
                    </div>
                    
                </div>
                
                
                
                  <div class="row">
                   <div class="col-sm-6">
                        <div class="white-box">
                            <h3 class="box-title">Top 5 Doctors Completed Appoinment</h3>
                            <div>
                                <canvas id="chart2" height="150"></canvas> 
                            </div>
                        </div>
                    </div>
                   <div class="col-md-3 col-lg-3 col-xs-12">
                        <div class="white-box gp-height">
                            <h3 class="box-title">Top 5 Practice</h3>
                            <div id="morris-donut-chart"></div>
                        </div>
                    </div>
                    
                    <div class="col-lg-3 col-md-6">
                        <div class="white-box">
                            <h3 class="box-title">Transaction Amount</h3>
                            <ul class="country-state">
                                <li>
                                    <h2>120000</h2> <small>Overall Amount</small>
                                    <div class="pull-right">62% <i class="fa fa-level-up text-success"></i></div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:48%;"> <span class="sr-only">62% Complete</span></div>
                                    </div>
                                </li>
                                <li>
                                    <h2>40000</h2> <small>Montly</small>
                                    <div class="pull-right">98% <i class="fa fa-level-up text-success"></i></div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-inverse" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:98%;"> <span class="sr-only">98% Complete</span></div>
                                    </div>
                                </li>
                                <li>
                                    <h2>1700</h2> <small>Daily</small>
                                    <div class="pull-right">75% <i class="fa fa-level-down text-danger"></i></div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:75%;"> <span class="sr-only">75% Complete</span></div>
                                    </div>
                                </li>
<!--
                                <li>
                                    <h2>1350</h2> <small>From USA</small>
                                    <div class="pull-right">48% <i class="fa fa-level-up text-success"></i></div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:48%;"> <span class="sr-only">48% Complete</span></div>
                                    </div>
                                </li>
-->
                            </ul>
                        </div>
                    </div>
                   
                </div>
                
                
                
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="white-box">
                            <h3 class="box-title">Appointment</h3>
                            <ul class="list-inline text-right">
                                <li>
                                    <h5><i class="fa fa-circle m-r-5" style="color: #00bfc7;"></i>Completed</h5> </li>
                                <li>
                                    <h5><i class="fa fa-circle m-r-5" style="color: #fdc006;"></i>Canceled</h5> </li>
                                <li>
                                    <h5><i class="fa fa-circle m-r-5" style="color: #9675ce;"></i>Booked</h5> </li>
                            </ul>
                            <div id="morris-area-chart"></div>
                        </div>
                    </div>
				</div>
                
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-xs-12">
                        <div class="white-box">
                            <h3 class="box-title">Appointment History</h3>
                            <ul class="list-inline text-right">
                                <li>
                                    <h5><i class="fa fa-circle m-r-5" style="color: #00bfc7;"></i>Completed</h5> </li>
                                <li>
                                    <h5><i class="fa fa-circle m-r-5" style="color: #fdc006;"></i>Booked</h5> </li>
                                <li>
                                    <h5><i class="fa fa-circle m-r-5" style="color: #9675ce;"></i>Canceled</h5> </li>
                            </ul>
                            <div id="morris-bar-chart"></div>
                        </div>
                    </div>
                </div>
                
                
                 
              
        </div>
       
    </div>
    <!-- /#page-wrapper --> 
    </div>
    <!-- /#wrapper -->
   
  <!-- jQuery -->
  <script src="<?=SITE_PATH?>dashboard_plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?=SITE_PATH?>css/dashboard/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Menu Plugin JavaScript -->
   
    <!--slimscroll JavaScript -->
     <script src="js/dashboard/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?=SITE_PATH?>js/dashboard/js/waves.js"></script>
    <!--Morris JavaScript -->
    <script src="<?=SITE_PATH?>dashboard_plugins/bower_components/raphael/raphael-min.js"></script>
    <script src="<?=SITE_PATH?>dashboard_plugins/bower_components/morrisjs/morris.js"></script>
    <!-- jQuery for carousel -->
    <script src="<?=SITE_PATH?>dashboard_plugins/bower_components/owl.carousel/owl.carousel.min.js"></script>
    <script src="<?=SITE_PATH?>dashboard_plugins/bower_components/owl.carousel/owl.custom.js"></script>
    <!-- Flot Charts JavaScript -->
    <script src="<?=SITE_PATH?>dashboard_plugins/bower_components/flot/jquery.flot.js"></script>
    <script src="<?=SITE_PATH?>dashboard_plugins/bower_components/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
    <!-- Animated skill bar -->
    <script src="<?=SITE_PATH?>dashboard_plugins/bower_components/AnimatedSkillsDiagram/js/animated-bar.js"></script>
    <!-- Sparkline chart JavaScript -->
    <script src="<?=SITE_PATH?>dashboard_plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <!-- chartist chart -->
    <script src="<?=SITE_PATH?>dashboard_plugins/bower_components/chartist-js/dist/chartist.min.js"></script>
    <script src="<?=SITE_PATH?>dashboard_plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js"></script>
    <!-- Guage chart -->
    <script src="<?=SITE_PATH?>dashboard_plugins/bower_components/Minimal-Gauge-chart/js/cmGauge.js"></script>
    <!-- Custom Theme JavaScript -->
       
   
    <script src="<?=SITE_PATH?>js/dashboard/js/widget-ext.js"></script>
    
 
    
  
   
     
    <!-- Chart JS -->  <script src="<?=SITE_PATH?>js/dashboard/js/dashboard2.js"></script>
<!--    <script src="<?=SITE_PATH?>dashboard_plugins/bower_components/Chart.js/chartjs.init.js"></script>-->
    <script src="<?=SITE_PATH?>dashboard_plugins/bower_components/Chart.js/Chart.min.js"></script>
  
       <script>

var ctx2 = document.getElementById("chart2").getContext("2d");
    var data2 = {
         labels: ["Doctor 1", "Doctor 2", "Doctor 3", "Doctor 4", "Doctor 5"],
        datasets: [
            {
                label: "My First dataset",
                fillColor: "rgba(83,230,157,0.8)",
                strokeColor: "rgba(83,230,157,0.8)",
                highlightFill: "rgba(83,230,157,0.8)",
                highlightStroke: "rgba(83,230,157,0.8)",
                data: [10, 30, 80, 61, 26]
            },
            {
                label: "My Second dataset",
                fillColor: "rgba(255,118,118,0.8)",
                strokeColor: "rgba(255,118,118,0.8)",
                highlightFill: "rgba(255,118,118,0.8)",
                highlightStroke: "rgba(255,118,118,0.8)",
                data: [28, 48, 40, 19, 86]
            }
        ]
    };
    
    var chart2 = new Chart(ctx2).Bar(data2, {
        scaleBeginAtZero : true,
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.005)",
        scaleGridLineWidth : 0,
        scaleShowHorizontalLines: true,
        scaleShowVerticalLines: true,
        barShowStroke : true,
        barStrokeWidth : 3, 
		barWidth : 0, 
 		tooltipCornerRadius: 2,
        barDatasetSpacing :3,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });
</script>
    <!-- Custom tab JavaScript -->
    
     
    <!--Style Switcher -->
<!--    <script src="<?=SITE_PATH?>dashboard_plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>-->


<style>
	
	@media screen and (min-width: 1300px) and (max-width: 1400px) {

#morris-donut-chart svg {

    top: -80px !important;
    height: auto !important;

}
		.gp-height{
			max-height: 349px !important;
		}
	}
</style>