<?php 
include(FS_ADMIN._MODS."/clinics/class.inc.php");
if(strtolower($user_type)=="clinics"){
  $readonly = 'readonly="readonly"';
}else{
  $readonly = "";
}

$OP = new Options();

if($BSC->is_post_back())
{
	  $path = UP_FILES_FS_PATH."/clinics";
	   $resize = UP_FILES_FS_PATH."/clinics/217X279";
	   if($_FILES['image'][name])
	   {
				$_POST['image'] = $BSC->uploadFile($path,$_FILES['image']['name'],'image');
				//$BSC->make_thumb_gd($path."/".$_POST['image'], $resize."/".$_POST['image'],'217', '279', 'width');	
				
				if($uid>0)
				{
					$delete_image=$PDO->getSingleresult("select image from #_".tblName." where pid='".$uid."'");
					if($delete_image!='')
					{
					  @unlink($path.'/'.$delete_image);
					  @unlink($resize.'/'.$delete_image);
					}
				}
				
				//echo $_POST['image']; exit;
		}
	
	
   if($uid)
   {
	   $_POST['updateid']=$uid;
       $flag = $OP->update($_POST);
  
   }else {
	 
	   $flag = $OP->add($_POST);
	   
	     
   }
   
   if($flag==1)
   {
    if(strtolower($user_type)=="clinics"){
      //$BSC->redir(SITE_PATH."home.php");
      die('<script type="text/javascript">window.location.href="http://doctoroncall.com.mm/crm_new/home.php";</script>');
    }else{
     $BSC->redir($ADMIN->iurl($comp.(($start)?'&start='.$start:'').(($subpage_id)?'&subpage_id='.$subpage_id:'').(($alumniid)?'&alumniid='.$alumniid:'').(($galleryid)?'&galleryid='.$galleryid:'')).$dlr, true);
   }
   }
}


if($uid)
{
    $query =$PDO->db_query("select * from #_".tblName." where pid ='".$uid."' "); 
	$row = $PDO->db_fetch_array($query);
	@extract($row);	
}

?>

<div class="vd_content-section clearfix">
		  	<div class="row" id="form-basic">
               
			  <?=$ADMIN->alert()?>
			   <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span> Add/Health <?=$ADMIN->compname($comp)?> </h3>
                  </div>
              		
              		<div class="panel-body">
					
                    <div class="form-groupcol-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">Clinic Name <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input class="validate[required]" data-errormessage-value-missing="Name is required!" name="name" id="name"  value="<?=$name?>" type="text">
                        </div>
                    </div>
					
					<div class="form-groupcol-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">Clinic Name(MY)<span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input class="validate[required]" data-errormessage-value-missing="Name is required!" name="name_be" id="name_be"  value="<?=$name_be?>" type="text">
                        </div>
                    </div>
					
					<div class="form-groupcol-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">Email <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input class="validate[required,custom[email]]" data-errormessage-value-missing="Email is required!" name="email" id="email"  value="<?=$email?>" type="text" <?=$readonly?>>
                        </div>
                    </div>
                    <div class="form-group col-md-4 col-sm-4 col-xs-12"> 
					<?php if($pid!=''){?>
					<div class="form-group  col-md-6 col-sm-6 col-xs-12">
                        <label class="control-label">Change Password <span class="vd_red"></span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input  name="change_pwd" id="change_pwd"  value="1" type="checkbox">
                        </div>
                    </div>
					<?php } ?>
					<div class=" col-md-6 col-sm-6 col-xs-12 pwd" style="<?php if($pid!=''){ echo 'display:none;';}?>">
                        <label class="control-label">Password <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input  name="password" id="password"  value="" type="password" class="validate[required]" data-errormessage-value-missing="Password is required!">
                        </div>
                    </div>
                    
						</div>
					<div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">Address <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input class="validate[required]" data-errormessage-value-missing="Address is required!" name="address" id="address"  value="<?=$address?>" type="text">
                        </div>
                    </div>
					<div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">Address(MY)<span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input class="validate[required]" data-errormessage-value-missing="Address is required!" name="address_be" id="address_be"  value="<?=$address_be?>" type="text">
                        </div>
                    </div>
					<div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">Phone Number <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input class="validate[required]" data-errormessage-value-missing="Phone Number is required!" name="phone" id="phone"  value="<?=$phone?>" type="text">
                        </div>
                    </div>
					<div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">Nearest Bus Stop <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input class="validate[required]" data-errormessage-value-missing="Nearest Bus Stop is required!" name="nearest_bus_stop" id="nearest_bus_stop"  value="<?=$nearest_bus_stop?>" type="text">
                        </div>
                    </div>
					<div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">Nearest Bus Stop(MY) <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input class="validate[required]" data-errormessage-value-missing="Nearest Bus Stop is required!" name="nearest_bus_stop_be" id="nearest_bus_stop_be"  value="<?=$nearest_bus_stop_be?>" type="text">
                        </div>
                    </div>
					<div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">Nearest Land Mark <span class="vd_red"></span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input  name="nearest_land_mark" id="nearest_land_mark"  value="<?=$nearest_land_mark?>" type="text">
                        </div>
                    </div>
					
					<div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">Nearest Land Mark(MY) <span class="vd_red"></span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input  name="nearest_land_mark_be" id="nearest_land_mark_be"  value="<?=$nearest_land_mark_be?>" type="text">
                        </div>
                    </div>
                    
                    	
					<div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">Division <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls">
						<select name="division" data-name="township" data-folder="doctor_agents" class="validate[required] cat" data-errormessage-value-missing="Division is required!">
							<option value="">Select Division</option>
							<?php $record=$PDO->db_query("select * from #_division");?>
						<?php while($res=mysql_fetch_array($record)){?>
						<option value="<?=$res['pid']?>" <?php if($division==$res['pid']){echo "selected";}?>><?=$res['name']?></option>
						<?php } ?>
							
						</select>	
                         
                        </div>
                    </div>
					
					<div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">Township <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <select name="township"  id="township" class="add_records validate[required]" data-errormessage-value-missing="Township is required!" onchange="getpincode('township','postal_code')">
							<option value="">Select Township</option>
							<?php if($township!='' and $township!=0){?>
							<option value="<?php echo $township;?>" selected><?php echo $PDO->getSingleresult("select name from #_township where pid='".$township."'");?></option>
							<?php }?>
						</select>	
                        </div>
                    </div>
                    
                    <div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">Postal Code<span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input class="validate[required,custome[integer],maxSize[5]]" data-errormessage-value-missing="Postal Code  is required!" name="postal_code" id="postal_code"  value="<?=$postal_code?>" type="text">
                        </div>
                    </div>
                    
					<div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">Clinic Open Time <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input class="validate[required] timePicker" data-errormessage-value-missing="Clinic Open Time is required!" name="clinic_open_time" id="clinic_open_time"  value="<?=$clinic_open_time?>" type="text">
                        </div>
                    </div>
					<div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label ">Clinic Close Time<span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input class="validate[required] timePicker" data-errormessage-value-missing="Clinic Close Time is required!" name="clinic_close_time" id="clinic_close_time"  value="<?=$clinic_close_time?>" type="text">
                        </div>
                    </div>
                    
                    <div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">Home Visit Service <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls">
							<select name="home_visit_service" data-id="home_visit" class="hide_show validate[required]" data-errormessage-value-missing="Home Visit Service is required!">
                                <option  value="">-------Select------</option>
								<option value="Available" <?=($home_visit_service=='Available')?'selected="selected"':''?>  >Available</option>
								<option value="Not Available" <?=(isset($home_visit_service) && $home_visit_service=='Not Available')?'selected="selected"':''?>>Not Available</option>
                          </select>
                        </div>
                    </div>
                    
                    
					<!-- Home Visit Service -->
					<div id="home_visit" style="display:<?php if($home_visit_service=='Not Available'){echo "none;";}?>">
					<div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">Home Visit Coverage Area <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input class="validate[required]" data-errormessage-value-missing="Home Visit Coverage Area is required!" name="home_visit_coverage_area" id="home_visit_coverage_area1"  value="<?=$home_visit_coverage_area?>" type="text">
                        </div>
                    </div>
					
					<div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">Home Visit Coverage Area (MY)<span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input class="validate[required]" data-errormessage-value-missing="Home Visit Coverage Area is required!" name="home_visit_coverage_area_be" id="home_visit_coverage_area_be"  value="<?=$home_visit_coverage_area_be?>" type="text">
                        </div>
                    </div>
					<div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">	Home Visit Available Start Time <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input class="validate[required]" data-errormessage-value-missing="Home Visit Available Start Time is required!" name="home_visit_available_start_time" id="home_visit_coverage_area"  value="<?=$home_visit_available_start_time?>" type="text">
                        </div>
                    </div>
					<div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">	Home Visit Available End Time <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input class="validate[required]" data-errormessage-value-missing="Home Visit Available End Time is required!" name="home_visit_available_end_time" id="home_visit_available_end_time"  value="<?=$home_visit_available_end_time?>" type="text">
                        </div>
                    </div>
					<div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">	Home Visit Charges <span class="vd_red"></span></label>
                        <div id="first-name-input-wrapper" class="controls">
                          <input  name="home_visit_charges" id="home_visit_charges"  value="<?=$home_visit_charges?>" type="text">
                        </div>
                    </div>
					</div>
					<!-- End Home Visit Service -->
					  <div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label class=" control-label">Available Services</label>
                        <div class="controls">
                          <div class="vd_checkbox checkbox-danger checkbox-addcss">
						  <?php $ser = (array)json_decode($services,true); ?>
                            <input type="checkbox" name="services[leb]" <?php if($ser['leb']!=''){echo "checked";}?> class="services" data-id="leb" value="1" id="checkbox-3">
                            <label for="checkbox-3"> Lab </label>
                            <input type="checkbox" class="services" data-id="imaging" <?php if($ser['imaging']!=''){echo "checked";}?>  name="services[imaging]" value="1" id="checkbox-4">
                            <label for="checkbox-4"> Imaging Centre </label>
                          </div>
                        </div>
                      </div>
					<!--  Services -->
					<div id="leb" style="display:<?php if($ser['leb']!=''){echo "block;";}else{?>none;<?php } ?>">
						<?php $left_services = (array)json_decode($left_services, true);?>
						
						<?php include(FS_ADMIN._MODS."/list_laboratory.php");?>
					</div>
					<div id="imaging" style="display:<?php if($ser['imaging']!=''){echo "block;";}else{?>none;<?php } ?>">
						<?php $imagin_services = (array)json_decode($imagin_services,true);?>
						<?php include(FS_ADMIN._MODS."/imaging_services.php");?>
					</div>	
					<!-- End Services -->
                    
                     <div class="form-group col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label ">Clinic Image <span class="vd_red"></span></label>
                        
                     
                        <div id="first-name-input-wrapper" class="controls ">
                         <input type="file" name="image" />
                          <?php if($image!='' && file_exists(UP_FILES_FS_PATH."/clinics/217X279/".$image) ) {?>
                     
                     <div class="image-thm">
                           <a href=""  class="image"> <span></span><img src="<?=SITE_PATH."uploaded_files/clinics/217X279/".$image?>" alt="" width="150"></a> </div>
                        <?php } ?>
                        </div>
                           
                    </div>
				
				<?php if ($is_verified==0){?>
                    <div class="form-group  col-md-4 col-sm-4 col-xs-12">
                      <label class="control-label">Verification Status <span class="vd_red">*</span></label>
                      <div id="website-input-wrapper" class="controls">
                          <select name="is_verified" class="validate[required]" data-errormessage-value-missing="Verificaation Status is required!">
                                <option  value="">-------Select Status------</option>
								<option value="1" <?=($is_verified==1)?'selected="selected"':''?>  >Done</option>
								<option value="0" <?=($is_verified==0)?'selected="selected"':''?>>Not Done</option>
                          </select>
                        </div>
                  </div>
                  <?php } else { ?>
                  <div class="form-group  col-md-4 col-sm-4 col-xs-12">
                        <label class="control-label">Verification Status <span class="vd_red">*</span></label>
                        <div id="website-input-wrapper" class="controls">
                          <input id="is_verified"  value="Done" type="text" readonly>
                        </div>
                    </div>
                  <?php } ?>
                   					<div class="form-group col-md-4 col-sm-4 col-xs-12">
                      <label class="control-label">Status <span class="vd_red">*</span></label>
                      <div id="website-input-wrapper" class="controls">
                          <select name="status" class="validate[required]" data-errormessage-value-missing="Status is required!">
                                <option  value="">-------Select Status------</option>
								<option value="1" <?=($status==1)?'selected="selected"':''?>  >Active</option>
								<option value="0" <?=(isset($status) && $status==0)?'selected="selected"':''?>>Inactive</option>
                          </select>
                        </div>
                  </div>
                
<!--
                  <div class="form-group form-actions">
                    <div class="col-sm-6"></div>
                    <div class="col-md-6">
					 <input type="hidden" name="pid" value="<?=$user_id?>" />
                       <button id="submit-register" name="submit-register" class="btn vd_btn vd_bg-green vd_white uibutton loading" type="submit"><i class="icon-ok" ></i> Save</button>
                         
                         
                          <button onclick="location.reload();" class="btn vd_btn" type="button">Cancel</button>
                    </div>
                  </div>
-->
                  
                    <div class="form-group form-actions">
                   
                    <div class="col-md-12 mgbt-xs-10 mgtp-20">
                     
                      
                      <div class="mgtp-10 gp-center">
					   <input type="hidden" name="pid" value="<?=$user_id?>" />
                        <button class="btn vd_bg-green vd_white greenbutton uibutton loading" type="submit" id="submit-register" name="submit-register">Save</button>
                        
                        
                         <button onclick="location.reload();" class="btn  orng-btn" type="button" >Cancel</button>
                      </div>
                    </div>
                   
                  </div>
                  
                  
                  
                  
                  
             <!--   </form>-->
			<!-- close add-update form-->
              		</div>
              </div>
            </div>
		</div>
<script src="<?=SITE_PATH?>validation/js/jquery-1.8.2.min.js"></script>
<script src="<?=SITE_PATH?>validation/js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=SITE_PATH?>validation/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#formID").validationEngine();
		});
		$('.hide_show').change(function(){
			var id = $(this).attr("data-id");
			if($(this).val()=="Available")
			{
				$('#'+id).show();
			}else{
				$('#'+id).hide();
			}
		});
		$('.services').click(function(){
			var id = $(this).attr("data-id");
			if(this.checked)
			{
				$('#'+id).show();
			}else{
				$('#'+id).hide();
			}
		});
</script>