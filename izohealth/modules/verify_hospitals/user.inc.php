<?php

/*
# @package BSC CMS
# Copy right code 
# The base configurations of the BSC CMS.
# manage admin user add update and delete
# Developer by jugal kishore kiroriwal

*/

class Users extends dbc
{

    public function update($data)
    {
        @extract($data);
        $tblName = "hospitals";

        // pid is a user id  And Update id is  pid of module table
        $query = parent::db_query("select * from #_" . $tblName . " where pid='" . $updateid . "' ");

        $querydata = mysql_fetch_array($query);
        if ($querydata['email'] == $email) {
            $data['modified_on'] = @date('Y-m-d H:i:s');
            parent::sqlquery("rs", $tblName, $data, 'pid', $updateid);
            parent::sessset('Record has been updated', 's');
            $flag = 1;
            //send Mail to user
            // $email = parent::getSingleresult("select email from #_doctors where pid='{$updateid}'");
            $name = parent::getSingleresult("select name from #_doctors where pid='{$pid}'");
            $doctor_name = $name;
            $reply_email = 'noreply@doc.com';
            $reply_name = 'noreplydoc';
            $from_email = 'admin@doctoroncall.com';
            $from_name = 'Izohealth';
            $to_email = $email;
            $to_name = $doctor_name;
            if ($data['status'] == 1) {
                $subject = 'Account Activated';
                $message = "Thanks For Joining in Izohealth.<br>Your Registeration is completed.Your account is activated now.";

            } else {
                $subject = 'Account Deactivated';
                $message = "Your account is deactivated now.Please contact admin";

            }
            $message_content = $message;

            //include(FS_ADMIN . "lib/open.inc.php");
            include(FS_ADMIN . "app/class/all_class_files.php");
            $m = new MyMail ();
            //$doctor_function = new doctor ();
            /*$hospital_name_list = $doctor_function->hospital_name_list('en');
            print_r($hospital_name_list);*/
            $email_details = array(
                'from_email' => 'doctor@doctoroncall.com.mm',
                'from_name' => 'Doctor CRM',
                'to_email' => $to_email,
                'to_name' => $from_name,
                'subject' => $subject,
                'message_content' => $message_content
            );
            $send_user_Email = $m->sendMail($email_details);

        } else {
            parent::sessset('Email is changed', 'e');
            $flag = 0;
        }

        return $flag;

    }


    public function status($updateid, $status)
    {
        $tblName = 'hospitals';
        $updateidarrr = $updateid;
        if (is_array($updateid)) {
            $updateid = implode(',', $updateid);
            foreach ($updateidarrr as $key => $value) {
                // $arr[3] will be updated with each value from $arr...
                $email = parent::getSingleresult("select email from #_hospitals where pid='{$value}'");
                //   $email=$data['email'];
                $name = parent::getSingleresult("select name from #_hospitals where pid='{$value}'");

                $doctor_name = $name;
                $reply_email = 'noreply@doc.com';
                $reply_name = 'noreplydoc';
                $from_email = 'admin@doctoroncall.com';
                $from_name = 'Izohealth';
                $to_email = $email;
                $to_name = $doctor_name;
                if ($status == 1) {
                    $subject = 'Account Activated';
                    $message = "Thanks For Joining in Izohealth.<br>Your hospital is activated now.";

                } else {
                    $subject = 'Account Deactivated';
                    $message = "Your hospital is deactivated now.Please contact admin";

                }
                $message_content = $message;

                //include(FS_ADMIN . "lib/open.inc.php");
                include(FS_ADMIN . "app/class/all_class_files.php");
                $m = new MyMail ();
                //$doctor_function = new doctor ();
                /*$hospital_name_list = $doctor_function->hospital_name_list('en');
                print_r($hospital_name_list);*/
                $email_details = array(
                    'from_email' => 'doctor@doctoroncall.com.mm',
                    'from_name' => 'Doctor CRM',
                    'to_email' => $to_email,
                    'to_name' => $from_name,
                    'subject' => $subject,
                    'message_content' => $message_content
                );
                $send_user_Email = $m->sendMail($email_details);
            }

        }

        parent::db_query("update  #_" . $tblName . " set status='" . $status . "' where pid in ($updateid)");

    }


    public function display($start, $pagesize, $fld, $otype, $search_data)
    {
        $tblName = 'hospitals';
        $start = intval($start);
        $columns = "select * ";

        if (trim($search_data) != '') {
            $wh = " and (name like '%" . parent::parse_input($search_data) . "%' or email like '%" . parent::parse_input($search_data) . "%' or is_verified like '%" . parent::parse_input($search_data) . "%') ";
        }
        $sql = " from #_" . $tblName . " where is_verified=0 and status=1 " . $zone . $mtype . $extra . $extra1 . $extra2 . $wh;
        $order_by == '' ? $order_by = (($ord) ? 'orders' : (($fld) ? $fld : 'shortorder')) : true;
        $order_by2 == '' ? $order_by2 = (($otype) ? $otype : 'DESC') : true;
        $sql_count = "select count(*) " . $sql;
        $sql .= "order by $order_by $order_by2 ";
        $sql .= "limit $start, $pagesize ";
        $sql = $columns . $sql;

        $result = parent::db_query($sql);
        $reccnt = parent::db_scalar($sql_count);
        return array($result, $reccnt);
    }


    public function password($password)
    {
        $password = md5($password);
        $password = base64_encode($password);
        return $password;
    }
}


?>