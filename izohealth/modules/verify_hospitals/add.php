<?php
include(FS_ADMIN . _MODS . "/verify_users/user.inc.php");


$US = new Users();

if ($BSC->is_post_back()) {

    if ($uid) {
        $_POST['updateid'] = $uid;
        $flag = $US->update($_POST);

    }
    if ($flag == 1) {

        $BSC->redir($ADMIN->iurl($comp . (($start) ? '&start=' . $start : '') . (($subpage_id) ? '&subpage_id=' . $subpage_id : '') . (($alumniid) ? '&alumniid=' . $alumniid : '') . (($galleryid) ? '&galleryid=' . $galleryid : '')) . $dlr, true);
    }
}


if ($uid) {
    $tblName = 'doctors';
    $query = $PDO->db_query("select * from #_" . $tblName . " where pid ='" . $uid . "' ");
    $row = $PDO->db_fetch_array($query);
    @extract($row);

}

?>

<div class="vd_content-section clearfix">
    <div class="row" id="form-basic">
        <?= $ADMIN->alert() ?>
        <div class="panel-heading vd_bg-grey">
            <h3 class="panel-title"><span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span>
                Verify/Doctors <?php $ADMIN->compname($comp) ?> </h3>
        </div>

        <div class="panel-body">


            <!--add-update form-->
            <!--    <form  class="form-horizontal body-gap" action="#" role="form" id="register-form">-->

            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label  col-sm-2">Name <span class="vd_red">*</span></label>
                    <div id="first-name-input-wrapper" class="controls col-sm-8">
                        <input class="validate[required]" data-errormessage-value-missing="Name is required!"
                               name="name" id="name" value="<?= $name ?>" type="text" readonly>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label  col-sm-2">Email <span class="vd_red">*</span></label>
                    <div id="first-name-input-wrapper" class="controls col-sm-8">
                        <?php $email = $PDO->getSingleresult("select email from #_doctors where pid='{$pid}'"); ?>
                        <input class="validate[required]" data-errormessage-value-missing="Email is required!"
                               name="email" id="email" value="<?= $email ?>" type="text" readonly>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label  col-sm-2">ID Card <span class="vd_red">*</span></label>
                    <div id="first-name-input-wrapper" class="controls col-sm-8">
                        <?php
                        $currentPath = $_SERVER['PHP_SELF'];
                        // output: Array ( [dirname] => /myproject [basename] => index.php [extension] => php [filename] => index )
                        $pathInfo = pathinfo($currentPath);
                        // output: localhost
                        $hostName = $_SERVER['HTTP_HOST'];
                        // output: http://
                        $protocol = strtolower(substr($_SERVER["SERVER_PROTOCOL"], 0, 5)) == 'https://' ? 'https://' : 'http://';
                        // return: http://localhost/myproject/

                        if(($id_card!= NULL)||($id_card!= ''))
                        {
                            if($hostName=='localhost')
                                $id_card_image = SITE_PATH_ADM. "/uploads/" . $id_card;
                            else
                                $id_card_image = SITE_PATH_ADM. "/uploads/" . $id_card;

                        }else
                        {
                            if($hostName=='localhost')
                                $id_card_image = SITE_PATH_ADM . "/uploads/default_id_card_logo.png";
                            else
                                $id_card_image = SITE_PATH_ADM. "/uploads/default_id_card_logo.png";
                        }

                        ?>
                        <div><img class="img-responsive img-thumbnail" src="<?=$id_card_image?>"</div>
                    </div><br>
                </div>
            </div>


            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label col-sm-2 col-xs-12">Status <span class="vd_red">*</span></label>
                    <div id="website-input-wrapper" class="controls col-sm-4 col-xs-12">
                        <select name="status" class="validate[required]"
                                data-errormessage-value-missing="Status is required!">
                            <option value="">-------Select Status------</option>
                            <option value="1" <?= ($status == 1) ? 'selected="selected"' : '' ?> >Active</option>
                            <option value="0" <?= (isset($status) && $status == 0) ? 'selected="selected"' : '' ?>>
                                Inactive
                            </option>
                        </select>
                    </div>
                </div>
            </div>



            <div class="form-group">
                <div class="col-sm-2"></div>
                <div class="col-md-6 mgbt-xs-10 mgtp-20">


                    <div class="mgtp-10">
                        <input type="hidden" name="pid" value="<?= $pid ?>"/>
                        <button class="btn vd_bg-green vd_white greenbutton uibutton loading" type="submit"
                                id="submit-register" name="submit-register">Submit
                        </button>
                    </div>
                </div>
                <div class="col-md-12 mgbt-xs-5"></div>
            </div>
            <!--   </form>-->
            <!-- close add-update form-->
        </div>
    </div>
</div>
</div>
