<?php
//include(FS_ADMIN ."app/class/doctor.php");
include(FS_ADMIN . _MODS . "/chatbot/user.inc.php");
$US = new Users();
$OP = new Users();
$uid = @$_GET['uid'];

if ($action) {

    if ($uid > 0 || !empty($arr_ids)) {

        switch ($action) {

            case "del":
                $OP->delete($uid);
                $ADMIN->sessset('Record has been deleted', 'e');
                break;

            case "Delete":
                $OP->delete($arr_ids);
                $ADMIN->sessset(count($arr_ids) . ' Item(s) Deleted', 'e');
                break;


            case "Active":
                $US->status($arr_ids, 1);
                $ADMIN->sessset(count($arr_ids) . ' Item(s) Active', 's');
                break;

            case "Inactive":
                $US->status($arr_ids, 0);
                $ADMIN->sessset(count($arr_ids) . ' Item(s) Inactive', 's');
                break;


            default:
        }
        $BSC->redir($ADMIN->iurl($comp), true);
    }
}

$start = intval($start);
$pagesize = intval($pagesize) == 0 ? (($_SESSION["totpaging"]) ? $_SESSION["totpaging"] : DEF_PAGE_SIZE) : $pagesize;

list($result, $reccnt) = $US->display($start, $pagesize, $fld, $otype, $search_data);

?>
<style>
    .controls-button li:first-child {
        display: none;
    }

    .controls-button li:last-child {
        display: none;
    }
</style>
<!--right section panel-->
<div class="vd_content-section clearfix">
    <div class="row">

        <div class="col-md-12">
            <?= $ADMIN->alert() ?>
            <div class="info-call-details">
                <ul>
                    <li><input type="text" name="search_data" value="<?= $search_data ?>" placeholder="Search"/></li>
                    <li><input type="submit" class="records-search greenbutton inputsearch" value="Search"></li>
                </ul>
            </div>

            <div class="panel-heading vd_bg-green white">
                <?php
                $count_of_categories = $PDO->getSingleresult("select count(*) from #_chatbot_options where parent_option_id=0 and is_deleted=0 and status=1 ");
                $count_of_unverified_clinics = $PDO->getSingleresult("select count(*) from #_clinics where status=1 and is_verified= 0 ");
                $category_name_title = '';
                if (isset($_GET['did'])) {
                    $category_id = $_GET['did'];
                    $category_name_title = $PDO->getSingleresult("select options from #_chatbot_options where pid='" . $category_id . "' and is_deleted=0 and status=1 ");
                }
                if (!empty($category_name_title)) {
                    $count_of_categories = $PDO->getSingleresult("select count(*) from #_chatbot_options where parent_option_id='" . $category_id . "' and is_deleted=0 and status=1 ");
                    ?>
                    <h3 class="panel-title"><?php echo $category_name_title; ?> (<?= $count_of_categories; ?>) </h3>

                <?php } else {
                    ?>

                    <h3 class="panel-title">Chat Bot Categories (<?= $count_of_categories; ?>) </h3>
                <?php }
                ?>
            </div>
            <div class="section-body">

                <div class="col-md-12">
                    <!--edit table-->
                    <div class="table-responsive " id="ordrz">
                        <table class="table data-tbl custom-style table-striped">
                            <thead>
                            <tr class="tbl-head">
                                <th><?= $ADMIN->check_all() ?></th>
                                <th>No.</th>
                                <th>
                                    <a href="<?= $ADMIN->iurl($comp) ?>&fld=name<?= (($otype == 'asc') ? "&otype=desc" : '&otype=asc') ?>" <?= (($fld == 'name') ? 'class="selectedTab"' : '') ?>><span <?= (($otype == 'asc') ? 'class="des"' : 'class="asc"') ?>>Category Name</span></a>
                                </th>
                                <th>
                                    Options Count
                                </th>
                                <th>
                                    <a href="<?= $ADMIN->iurl($comp) ?>&fld=status<?= (($otype == 'asc') ? "&otype=desc" : '&otype=asc') ?>" <?= (($fld == 'status') ? 'class="selectedTab"' : '') ?>><span <?= (($otype == 'asc') ? 'class="des"' : 'class="asc"') ?>>Status</span></a>
                                </th>

                                <th>Action</th>
                            </tr>
                            </thead>

                            <!--</table>
                                 <ul class="sortable-liststyle" style="list-style:none;">
                      -->
                            <tbody>
                            <?php if ($reccnt) {

                                $nums = (($start) ? $start + 1 : 1);
                                $k = 0;
                                while ($line = $PDO->db_fetch_array($result)) {
                                    @extract($line);
                                    $k++;
                                    $css = ($k % 2 != 0) ? 'success' : '';


                                    ?>
                                    <!--<li style="list-style:none;" id="recordsArray_<?= $pid ?>">
			<table class="table data-tbl custom-style">
			 <thead>-->
                                    <tr data-item-id=1 class="item <?= $css ?>">
                                        <th><?= $ADMIN->check_input($pid) ?></th>
                                        <th><?= $nums ?></th>
                                        <!--  <th><?= ucwords($name) ?></th>-->
                                        <?php
                                        //http://192.168.0.123/DoctorOnCall/crm/home.php?comp=doctors&mode=add&uid=8400&hospital_id=106
                                        $hospital_id_of_doc = $PDO->getSingleResult("select hospital_id from #_doctors where pid='" . $pid . "'");
                                        $clinic_id_of_doc = $PDO->getSingleResult("select clinic_id from #_doctors where pid='" . $pid . "'");
                                        if ($hospital_id_of_doc == 0) {
                                            $hospital_id_of_doc = $clinic_id_of_doc;
                                        }
                                        $verification_crm_url = SITE_PATH_ADM . 'home.php?comp=doctors&mode=add&uid=' . $pid . '&hospital_id=' . $hospital_id_of_doc;
                                        ?>
                                        <th>
                                            <a href="<?= $ADMIN->iurl('chatbot') ?>&did=<?= $pid ?>"><?= ucwords($options) ?></a>
                                        </th>
                                        <?php
                                        $count_of_questions = $PDO->getSingleresult("select count(*) from #_chatbot_options where parent_option_id=$pid and is_deleted=0 and status=1 ");
                                        ?>
                                        <th><?= $count_of_questions ?></th>
                                        <th><?= $ADMIN->displaystatus($status) ?></th>
                                        <!--<th><?= $ADMIN->verify_user_action($comp, $verification_crm_url) ?></th>-->
                                        <th><?= $ADMIN->verify_user_action_with_del('chatbot', $verification_crm_url, $pid) ?></th>

                                    </tr>
                                    <!-- </thead>
                                    </table>
                                    </li>-->
                                    <?php $nums++;
                                } ?>


                            <?php } else {
                                echo '<div align="center" class="norecord">No Record Found</div>';
                            } ?>

                            <!-- </ul>-->
                            </tbody>
                        </table>

                        <?php include("cuts/paging.inc.php"); ?>
                        <div class="pull-right pagination" style="display:none;">
                            <ul class="pagination">
                                <li class="page-pre"><a href="#">‹</a></li>
                                <li class="page-number active"><a href="#">1</a></li>
                                <li class="page-number"><a href="#">2</a></li>
                                <li class="page-number"><a href="#">3</a></li>
                                <li class="page-number"><a href="#">4</a></li>
                                <li class="page-number"><a href="#">5</a></li>
                                <li class="page-last-separator disabled"><a href="#">...</a></li>
                                <li class="page-last"><a href="#">80</a></li>
                                <li class="page-next"><a href="#">›</a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- close edit table-->
                </div>
            </div>

            <!--next button-->

            <!--Close next button-->
        </div>
    </div>
</div>
<!--Close right section panel-->
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script language="javascript">
    jQuery(document).ready(function () {
        jQuery(function () {
            jQuery("#ordrz ul").sortable({
                opacity: 0.6, cursor: 'move', update: function () {
                    var order = jQuery(this).sortable("serialize") + '&tbl=<?=tblName?>&field=pid';
                    $.post("<?=SITE_PATH_ADM?>modules/orders.php", order, function (theResponse) {
                    });
                }
            });
        });
    });
</script>