<?php
include(FS_ADMIN . _MODS . "/chatbot/user.inc.php");


$US = new Users();

if ($BSC->is_post_back()) {

    if ($uid) {
        $_POST['updateid'] = $uid;
        $flag = $US->update($_POST);

    }
    if ($flag == 1) {

        $BSC->redir($ADMIN->iurl($comp . (($start) ? '&start=' . $start : '') . (($subpage_id) ? '&subpage_id=' . $subpage_id : '') . (($alumniid) ? '&alumniid=' . $alumniid : '') . (($galleryid) ? '&galleryid=' . $galleryid : '')) . $dlr, true);
    }
}


if ($uid) {
    $tblName = 'chatbot_options';
    $query = $PDO->db_query("select * from #_" . $tblName . " where pid ='" . $uid . "' ");
    // $row = $PDO->db_fetch_array($query);
    // @extract($row);

} else {
    //add
    $tblName = 'chatbot_options';
    $query = $PDO->db_query("select * from #_" . $tblName . "  where parent_option_id =0 and status=1 and is_deleted=0 ");
    // $row_categories = $PDO->db_fetch_array($query);
    // @extract($row_categories);

}

?>

<div class="vd_content-section clearfix">
    <div class="row" id="form-basic">
        <?= $ADMIN->alert() ?>
        <div class="panel-heading vd_bg-grey">
            <h3 class="panel-title"><span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span>
                Chatbot/Question And Answers Add <?php $ADMIN->compname($comp) ?> </h3>
        </div>

        <div class="panel-body">

            <!--add-update form-->
            <!--    <form  class="form-horizontal body-gap" action="#" role="form" id="register-form">-->

            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label  col-sm-2">Category <span class="vd_red">*</span></label>
                    <div id="first-name-input-wrapper" class="controls col-sm-8">
                        <select name="status" class="validate[required]"
                                data-errormessage-value-missing="Select A Category ">
                            <option value="">-------Select Category------</option>
                            <?php
                            while ($categories = $PDO->db_fetch_array($query)) {
                                ?>
                                <option value="<?php echo $categories['pid'] ?>" <?= ($status == 1) ? 'selected="selected"' : '' ?> ><?php echo $categories['options']; ?></option>
                            <?php } ?>

                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label  col-sm-2">Question <span class="vd_red">*</span></label>
                    <div id="first-name-input-wrapper" class="controls col-sm-8">
                        <?php // $email = $PDO->getSingleresult("select email from #_doctors where pid='{$pid}'"); ?>
                        <textarea class="validate[required]" data-errormessage-value-missing="Question is required!"
                                  name="question" id="question" ></textarea>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-12">
                    <label class="control-label  col-sm-2">Answer Option <span class="vd_red">*</span></label>
                    <div id="first-name-input-wrapper" class="controls col-sm-8">
                        <textarea class="validate[required]" data-errormessage-value-missing="Answer is required!"
                                  name="answer[]" id="answer" ></textarea>
                    </div>
                    <i class="fa fa-plus-circle"></i>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-2"></div>
                <div class="col-md-6 mgbt-xs-10 mgtp-20">


                    <div class="mgtp-10">
                        <input type="hidden" name="pid" value="<?= $pid ?>"/>
                        <button class="btn vd_bg-green vd_white greenbutton uibutton loading" type="submit"
                                id="submit-register" name="submit-register">Submit
                        </button>
                    </div>
                </div>
                <div class="col-md-12 mgbt-xs-5"></div>
            </div>
            <!--   </form>-->
            <!-- close add-update form-->
        </div>
    </div>
</div>
</div>
