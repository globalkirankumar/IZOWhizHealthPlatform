<?php

/*
# @package BSC CMS
# Copy right code 
# The base configurations of the BSC CMS.
# manage admin user add update and delete
# Developer by jugal kishore kiroriwal

*/

class Users extends dbc
{
    public function  add($data)
    {
        @extract($data);
        $tblName = "chatbot_options";
        $query=parent::db_query("select * from #_".$tblName." where parent_option_id =0 ");
        if(mysql_num_rows($query)==0)
        {
            $data['password']=$this->password($data['password']);
            $data['user_type'] = "administrator";
            $data[user_id] = parent::sqlquery("rs",'users_login',$data);
            $data['created_on']=@date('Y-m-d H:i:s');
            $data['create_by']=$_SESSION["AMD"][0];
            $data['shortorder']=parent::getSingleresult("select max(shortorder) as shortorder from #_".tblName." where 1=1 ")+1;
            parent::sqlquery("rs",'admin_users',$data);
            parent::sessset('Record has been added', 's');
            $flag =1;
        }else {
            parent::sessset('Record has already added', 'e');
            $flag =0;
        }

        return $flag;
    }

    public function update($data)
    {
        @extract($data);
        $tblName = "doctors";

        // pid is a user id  And Update id is  pid of module table
        $query = parent::db_query("select * from #_" . $tblName . " where pid='" . $updateid . "' ");

        $querydata = mysql_fetch_array($query);
        if ($querydata['email'] == $email) {
            $data['modified_on'] = @date('Y-m-d H:i:s');
            parent::sqlquery("rs", $tblName, $data, 'pid', $updateid);
            parent::sessset('Record has been updated', 's');
            $flag = 1;
            //send Mail to user
            // $email = parent::getSingleresult("select email from #_doctors where pid='{$updateid}'");
            $name = parent::getSingleresult("select name from #_doctors where pid='{$pid}'");
            $doctor_name = $name;
            $reply_email = 'noreply@doc.com';
            $reply_name = 'noreplydoc';
            $from_email = 'admin@doctoroncall.com';
            $from_name = 'Izohealth';
            $to_email = $email;
            $to_name = $doctor_name;
            if ($data['status'] == 1) {
                $subject = 'Account Activated';
                $message = "Hi $doctor_name <br>Thanks For Joining in Izohealth.<br>Your Registeration is completed.Your account is activated now.";

            } else {
                $subject = 'Account Deactivated';
                $message = "Hi $doctor_name <br>Your account is deactivated now.Please contact admin";

            }
            $body = $message;

            // require_once('../functions/phpmailer/class.phpmailer.php');

            $new_mail = new PHPMailer();
            $new_mail->SetFrom($from_email, $from_name);
            $new_mail->AddReplyTo($reply_email, $reply_name);
            $new_mail->AddAddress($to_email, $to_name);
            $new_mail->Subject = $subject;
            $new_mail->MsgHTML($body);

            $send_user_Email = $new_mail->Send();

        } else {
            parent::sessset('Email is changed', 'e');
            $flag = 0;
        }

        return $flag;

    }

	public function  delete($updateid)
	 {
		   if(is_array($updateid))
		   {
			   $updateid=implode(',',$updateid);
		   }
		   $tblName = "chatbot_options";
		    parent::db_query("update  #_" . $tblName . " set is_deleted='1' where pid in ($updateid)");
		    parent::db_query("update  #_" . $tblName . " set is_deleted='1' where parent_option_id in ($updateid)");

	 }

    public function status($updateid, $status)
    {
        $did=$_GET['did'];
        $tblName = 'chatbot_options';
        $updateidarrr = $updateid;
        if (is_array($updateid)) {
            $updateid = implode(',', $updateid);
            foreach ($updateidarrr as $key => $value) {
                parent::db_query("update  #_" . $tblName . " set status='" . $status . "' where pid in ($updateid)");
                parent::db_query("update  #_" . $tblName . " set status='" . $status . "' where parent_option_id in ($updateid)");

            }

        }


    }


    public function display($start, $pagesize, $fld, $otype, $search_data)
    {
        $tblName = 'chatbot_options';
        $start = intval($start);
        $columns = "select * ";
		$did=$_GET['did'];
        if (trim($search_data) != '') {
            $wh = " and (options like '%" . parent::parse_input($search_data) . "%' ) ";
        }
		if(!empty($did)){
			$sql = " from #_" . $tblName . " where parent_option_id=$did and is_deleted=0  " . $zone . $mtype . $extra . $extra1 . $extra2 . $wh;

		}else{
			$sql = " from #_" . $tblName . " where parent_option_id=0 and is_deleted=0  " . $zone . $mtype . $extra . $extra1 . $extra2 . $wh;
		}
        $order_by == '' ? $order_by = (($ord) ? 'orders' : (($fld) ? $fld : 'parent_option_id')) : true;
        $order_by2 == '' ? $order_by2 = (($otype) ? $otype : 'DESC') : true;
        $sql_count = "select count(*) " . $sql;
        $sql .= "order by $order_by $order_by2 ";
        $sql .= "limit $start, $pagesize ";
        $sql = $columns . $sql;

        $result = parent::db_query($sql);
        $reccnt = parent::db_scalar($sql_count);
        return array($result, $reccnt);
    }


    public function password($password)
    {
        $password = md5($password);
        $password = base64_encode($password);
        return $password;
    }
}


?>