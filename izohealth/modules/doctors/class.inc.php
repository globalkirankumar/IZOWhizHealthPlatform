<?php

/*
# @package BSC CMS
# Copy right code 
# The base configurations of the BSC CMS.
# manage admin user add update and delete
# Developer by jugal kishore kiroriwal

*/

class Options extends dbc
{

    public function add($data)
    {
        @extract($data);
        $tblName = "users_login";
        $dates = json_encode($data['dates'], true);
        $data['dates'] = $dates;
        $query = parent::db_query("select * from #_" . $tblName . " where email ='" . $email . "' ");
        if (mysql_num_rows($query) == 0) {
            $data['password'] = $this->password($data['password']);
            $data['user_type'] = "doctors";
            $data['user_id'] = parent::sqlquery("rs", 'users_login', $data);
            $data['created_on'] = @date('Y-m-d H:i:s');
            $data['create_by'] = $_SESSION["AMD"][0];
            $data['shortorder'] = parent::getSingleresult("select max(shortorder) as shortorder from #_" . tblName . " where 1=1 ") + 1;
            parent::sqlquery("rs", 'doctors', $data);
            parent::sessset('Record has been added', 's');
            $flag = 1;
        } else {
            parent::sessset('Record has already added', 'e');
            $flag = 0;
        }

        return $flag;
    }

    public function update($data)
    {

        @extract($data);
        $tblName = "users_login";
        $dates = json_encode($data['dates'], true);
        $data['dates'] = $dates;
        // pid is a user id  And Update id is  pid of module table
        $query = parent::db_query("select * from #_" . tblName . " where pid='" . $updateid . "' ");

        $querydata = mysql_fetch_array($query);
        if ($querydata['email'] == $email) {
            if ($data['password'] != '' and $data['change_pwd'] != '') {
                $data['password'] = $this->password($data['password']);
                parent::sqlquery("rs", 'users_login', array('password' => $data['password']), 'pid', $pid);
            }
            $data['modified_on'] = @date('Y-m-d H:i:s');

            parent::sqlquery("rs", 'doctors', $data, 'pid', $updateid);
            parent::sessset('Record has been updated', 's');
            $flag = 1;

            if (array_key_exists('is_verified', $data)) {
                if ($data['is_verified'] == 1) {
                    //send email
                    $doctor_name = $data['name'];
                    $reply_email = 'noreply@doctoroncall.com.mm';
                    $reply_name = 'noreplydoctoroncall.com';
                    $from_email = 'admin@doctoroncall.com';
                    $from_name = 'Izohealth';
                    $to_email = $data['email'];
                    $to_name = $doctor_name;
                    if ($data['is_verified'] == 1) {
                        $subject = 'Account Activated';
                        $message = "Thanks For Joining in Izohealth.<br>Your account is activated now.";

                    } else {
                        $subject = 'Account Deactivated';
                        $message = "Your Clinic is account now.Please contact admin";

                    }
                    $message_content = $message;

                    //include(FS_ADMIN . "lib/open.inc.php");
                    include(FS_ADMIN . "app/class/all_class_files.php");
                    $m = new MyMail ();
                    //$doctor_function = new doctor ();
                    /*$hospital_name_list = $doctor_function->hospital_name_list('en');
                    print_r($hospital_name_list);*/
                    $email_details = array(
                        'from_email' => 'doctor@doctoroncall.com.mm',
                        'from_name' => 'Doctor CRM',
                        'to_email' => $to_email,
                        'to_name' => $from_name,
                        'subject' => $subject,
                        'message_content' => $message_content
                    );
                    $send_user_Email = $m->sendMail($email_details);

                }
            }


        } else {
            $query2 = parent::db_query("select * from #_" . $tblName . " where email='" . $email . "' ");
            if (mysql_num_rows($query2) == 0) {
                parent::sqlquery("rs", 'users_login', array('email' => $data['email']), 'pid', $pid);
                $flag = 1;
            } else {
                parent::sessset('Record has already added', 'e');
                $flag = 0;
            }
        }

        return $flag;

    }

    public function delete($updateid)
    {
        if (is_array($updateid)) {
            $updateid = implode(',', $updateid);
        }
        $tblName = "users_login";
        $id = parent::getSingleresult("select user_id  from #_" . tblName . " where pid in ($updateid)");
        parent::db_query("delete from #_" . tblName . " where pid in ($updateid)");
        parent::db_query("delete from #_" . $tblName . " where pid in ($id)");


    }

    public function status($updateid, $status)
    {
        if (is_array($updateid)) {
            $updateid = implode(',', $updateid);
        }

        parent::db_query("update  #_" . tblName . " set status='" . $status . "' where pid in ($updateid)");

    }


    public function display($start, $pagesize, $fld, $otype, $search_data, $filter_by, $filter_value)
    {
        $start = intval($start);
        $columns = "select * ";


        if (trim($filter_by) != '' and trim($filter_value)) {

            $wh = " and " . $filter_by . "='{$filter_value}'";
        }
        if (trim($search_data) != '') {

            $wh = " and (name like '%" . parent::parse_input($search_data) . "%' or email like '%" . parent::parse_input($search_data) . "%' or is_verified like '%" . parent::parse_input($search_data) . "%') ";
        }

        $sql = " from #_" . tblName . " where 1 " . $zone . $mtype . $extra . $extra1 . $extra2 . $wh;
        $order_by == '' ? $order_by = (($ord) ? 'orders' : (($fld) ? $fld : 'shortorder')) : true;
        $order_by2 == '' ? $order_by2 = (($otype) ? $otype : 'DESC') : true;
        $sql_count = "select count(*) " . $sql;
        $sql .= "order by $order_by $order_by2 ";
        $sql .= "limit $start, $pagesize ";
        $sql = $columns . $sql;

        $result = parent::db_query($sql);
        $reccnt = parent::db_scalar($sql_count);
        return array($result, $reccnt);
    }


    public function password($password)
    {
        $password = md5($password);
        $password = base64_encode($password);
        return $password;
    }

    public function ajax($pid)
    {
        return parent::db_query("select pid,division_id,name from #_township where `division_id`='" . $pid . "' ");

    }

    public function check_available_unavailable($doctor_id, $location_id)
    {

        if (!empty($location_id) && !empty($doctor_id)) {
            // date_default_timezone_set("Asia/Karachi");
            date_default_timezone_set("Asia/Kolkata");
            $today_date = date("Y-m-d");
            $current_time = date("H:i:s");
            $today_weekday = date("l");
            $weekday = self::today_weekday_num_find();

            //check whether the doctor is free or busy
            $query_busy_free = parent::db_query("select * from #_doctors_locations  where `pid`='" . $location_id . "' and doctor_id= '" . $doctor_id . "' and busy_free=1 ");
            if (mysql_num_rows($query_busy_free) > 0) {

                //check whether the location is available
                $query = parent::db_query("select * from #_doctors_locations  where `pid`='" . $location_id . "' and doctor_id= '" . $doctor_id . "' ");
                if (mysql_num_rows($query) > 0) {
                    $available_from_from_db = parent::getSingleResult("select available_from from #_doctors_locations where pid='" . $location_id . "'");
                    $available_to_from_db = parent::getSingleResult("select available_to from #_doctors_locations where pid='" . $location_id . "'");
                    if (($available_from_from_db != NULL) || ($available_to_from_db != NULL)) {
                        //check given time is available in time slot
                        $sql = "SELECT * FROM `#_doctors_locations` WHERE pid='" . $location_id . "' AND doctor_id='" . $doctor_id . "' AND(available_from <= '" . $today_date . "' AND available_to >= '" . $today_date . "') and available_status=0 and status=1 ";
                        $query_2 = parent::db_query($sql);
                        if (mysql_num_rows($query_2) == 0) {

                            //check current date is inbetween the availbele date
                            $sql_2 = "SELECT * FROM `#_doctors_locations_time_slots` WHERE location_id='" . $location_id . "' AND weekdays='" . $weekday . "' AND(start_time <= '" . $current_time . "' AND end_time >= '" . $current_time . "') and available_status=1 and status=1 ";

                            $query_3 = parent::db_query($sql_2);
                            if (mysql_num_rows($query_3) > 0) {

                                $data['msg'] = 'Doctor is available';
                                $data['status'] = 'true';

                            } else {
                                $data['msg'] = 'Doctor is not available in this location now';
                                $data['status'] = 'false';
                            }
                        } else {
                            $data['msg'] = 'Doctor is not available in this location on today';
                            $data['status'] = 'false';
                        }
                    } else {

                        //check current date is inbetween the availbele date
                        $sql_2 = "SELECT * FROM `#_doctors_locations_time_slots` WHERE location_id='" . $location_id . "' AND weekdays='" . $weekday . "' AND(start_time <= '" . $current_time . "' AND end_time >= '" . $current_time . "') and available_status=1 and status=1 ";

                        $query_3 = parent::db_query($sql_2);
                        if (mysql_num_rows($query_3) > 0) {

                            $data['msg'] = 'Doctor is available';
                            $data['status'] = 'true';

                        } else {
                            $data['msg'] = 'Doctor is not available in this location now';
                            $data['status'] = 'false';
                        }

                    }


                } else {
                    $data['msg'] = 'Doctor id and location id not matched';
                    $data['status'] = 'false';

                }


            } else {
                $data['msg'] = 'Doctor is busy now';
                $data['status'] = 'false';
            }

        } else {
            $data['msg'] = 'Doctor id and location id are empty';
            $data['status'] = 'false';
        }

        return $data;


    }

    public function today_weekday_num_find()
    {

        $today_date = date("Y-m-d");
        $current_time = date("H:i:s");
        $today_weekday = date("l");
        $weekday = 0;
        if ($today_weekday == 'Monday') {
            $weekday = 1;
        }
        if ($today_weekday == 'Tuesday') {
            $weekday = 2;
        }
        if ($today_weekday == 'Wednesday') {
            $weekday = 3;
        }
        if ($today_weekday == 'Thursday') {
            $weekday = 4;
        }
        if ($today_weekday == 'Friday') {
            $weekday = 5;
        }
        if ($today_weekday == 'Saturday') {
            $weekday = 6;
        }
        if ($today_weekday == 'Sunday') {
            $weekday = 7;
        }
        return $weekday;


    }
}


?>