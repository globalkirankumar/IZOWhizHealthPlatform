<?php

/*
# @package BSC CMS
# Copy right code 
# The base configurations of the BSC CMS.
# manage admin user add update and delete
# Developer by jugal kishore kiroriwal

*/

class Users extends dbc
{

    public function update($data)
    {
        @extract($data);
        $tblName = "appointment";

        // pid is a user id  And Update id is  pid of module table
        $query = parent::db_query("select * from #_" . $tblName . " where pid='" . $updateid . "' ");

        $querydata = mysql_fetch_array($query);
        if ($querydata['email'] == $email) {
            $data['modified_on'] = @date('Y-m-d H:i:s');
            parent::sqlquery("rs", $tblName, $data, 'pid', $updateid);
            parent::sessset('Record has been updated', 's');
            $flag = 1;


        } else {
            parent::sessset('Email is changed', 'e');
            $flag = 0;
        }

        return $flag;

    }

    public function delete($updateid)
    {
        if (is_array($updateid)) {
            $updateid = implode(',', $updateid);
        }
        $tblName = "appointment";
        $id = parent::getSingleresult("select user_id  from #_" . tblName . " where pid in ($updateid)");
        parent::db_query("delete from #_" . tblName . " where pid in ($updateid)");
        parent::db_query("delete from #_" . $tblName . " where pid in ($id)");


    }

    public function status($updateid, $status)
    {
        $tblName = 'appointment';
        $updateidarrr = $updateid;
        if (is_array($updateid)) {
            $updateid = implode(',', $updateid);
            foreach ($updateidarrr as $key => $value) {
                parent::db_query("update  #_" . $tblName . " set status='" . $status . "' where pid in ($value)");

            }

        }


    }


    public function display($start, $pagesize, $fld, $otype, $search_data)
    {
        $tblName = 'appointment';
        $start = intval($start);
        $wh = '';
        $columns = "SELECT ap.pid,ap.doctor_id, ap.appointment_with,ap.patient_id,ap.doctor_location_id as doctor_location_id,ap.app_date,ap.app_time,	
            ap.status,ap.shortorder,D.name as DoctorName,L.name as LabName,H.name  as healthcareName,dl.location_type as locationType,dl.hospital_id as hospitalId,dl.clinic_id as clinicId,hs.name as hospitalname,hs.phone as hospitalPhone,c.name as clinicName,c.phone as clinicPhone";
        $sql = " FROM crm_appointment as ap 
     LEFT  JOIN crm_doctors as D ON (ap.appointment_with = 'D' and ap.doctor_id=D.pid)
	 LEFT  JOIN crm_doctors_locations as dl ON (ap.appointment_with='D' AND dl.pid =ap.doctor_location_id)
     LEFT  JOIN crm_labs as L ON (ap.appointment_with = 'L' and ap.doctor_id=L.pid)
	 LEFT  JOIN crm_clinics as c ON (ap.appointment_with = 'D' and dl.location_type=1 and dl.clinic_id=c.pid)
     LEFT  JOIN crm_hospitals as hs ON (ap.appointment_with = 'D' and dl.location_type=2 and dl.hospital_id=hs.pid)
     LEFT JOIN crm_healthcare_organization as H ON (ap.appointment_with = 'H' and ap.doctor_id=H.pid) WHERE 1=1 " . $zone . $mtype . $extra . $extra1 . $extra2 . $wh;
        if (trim($search_data) != '') {
            $wh = " and (D.name like '%" . parent::parse_input($search_data) . "%' or H.name like '%" . parent::parse_input($search_data) . "%' or L.name like '%" . parent::parse_input($search_data) . "%' or P.name like '%" . parent::parse_input($search_data) . "%' or P.phone like '%" . parent::parse_input($search_data) . "%') ";
            $columns = "SELECT ap.pid,ap.doctor_id, ap.appointment_with,ap.patient_id,ap.doctor_location_id,ap.app_date,ap.app_time,
            ap.status,ap.shortorder,D.name as DoctorName,L.name as LabName,H.name  as healthcareName,dl.location_type as locationType,dl.hospital_id as hospitalId,dl.clinic_id as clinicId, hs.name as hospitalname,hs.phone as hospitalPhone,c.name as clinicName,c.phone as clinicPhone";
            $sql = " FROM crm_appointment as ap 
			     LEFT  JOIN crm_doctors as D ON (ap.appointment_with = 'D' and ap.doctor_id=D.pid)
     LEFT  JOIN crm_doctors_locations as dl ON (ap.appointment_with='D' AND dl.pid =ap.doctor_location_id)
     LEFT  JOIN crm_labs as L ON (ap.appointment_with = 'L' and ap.doctor_id=L.pid)
     LEFT  JOIN crm_patients as P ON (ap.patient_id=P.pid)
     LEFT  JOIN crm_clinics as c ON (ap.appointment_with = 'D' and dl.location_type=1 and dl.clinic_id=c.pid)
     LEFT  JOIN crm_hospitals as hs ON (ap.appointment_with = 'D' and dl.location_type=2 and dl.hospital_id=hs.pid)
     LEFT JOIN crm_healthcare_organization as H ON (ap.appointment_with = 'H' and ap.doctor_id=H.pid ) WHERE 1=1 " . $zone . $mtype . $extra . $extra1 . $extra2 . $wh;

        }
        $order_by == '' ? $order_by = (($ord) ? 'orders' : (($fld) ? $fld : 'shortorder')) : true;
        $order_by2 == '' ? $order_by2 = (($otype) ? $otype : 'DESC') : true;
        $sql_count = "select count(*) " . $sql;
        $sql .= "order by $order_by $order_by2 ";
        $sql .= "limit $start, $pagesize ";
        $sql = $columns . $sql;
        $result = parent::db_query($sql);
        $reccnt = parent::db_scalar($sql_count);
        return array($result, $reccnt);
    }


}


?>