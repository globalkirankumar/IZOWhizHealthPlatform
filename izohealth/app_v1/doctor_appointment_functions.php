<?php
// error_reporting ( - 1 );
include("../lib/open.inc.php");
include 'class/all_class_files.php';
include_once 'notification/GCM.php';
$gcm = new GCM ();
$m = new MyMail ();
$json = array();
$flag = mysql_real_escape_string($_POST ['flag']);
// initiate doctor object
$doctor_function = new doctor ();
date_default_timezone_set('Asia/Kolkata');

switch ($flag) {

    case 'appointment_all' :
        $pid = mysql_real_escape_string($_POST ['pid']);
        $user_type = mysql_real_escape_string($_POST ['user_type']);

        $json ['appointment'] = array();
        $json ['status'] = 'false';
        $json ['msg'] = 'Appointment list not found';
        $appointment_lists = $doctor_function->getAppointmentList($pid, $user_type);
        if ($appointment_lists) {
            $json ['appointment'] = $appointment_lists;
            $json ['status'] = 'true';
            $json ['msg'] = 'Succes!! Appointment list';
        }
        break;
    case 'appointment' :

        $doctor_pid = mysql_real_escape_string($_POST ['doctor_id']);
        $gsm_tocken = mysql_real_escape_string($_POST ['gsm_tocken']);

        $doctor_userid = $PDO->getSingleResult("select user_id from #_doctors where pid='" . $doctor_pid . "'");

        $gsm_user = $PDO->db_query("select * from #_gcmuser  where  user_id ='" . $doctor_userid . "' and gsm_tocken ='" . $gsm_tocken . "' and user_type ='doctor'");
        if (mysql_num_rows($gsm_user) == 0) {
            $gsm_user = $PDO->db_query("insert into #_gcmuser  set  user_id ='" . $doctor_userid . "', gsm_tocken ='" . $gsm_tocken . "' ,user_type ='doctor',flag = '1',created_on='" . date('Y-m-d H:i:s') . "'");
        } else {

            $gsm_user = $PDO->db_query("Update #_gcmuser  set  flag ='1', modified_on='" . date('Y-m-d H:i:s') . "' where user_id ='" . $doctor_userid . "' and gsm_tocken ='" . $gsm_tocken . "' and user_type ='doctor' ");
        }

        $query = $PDO->db_query("select * from #_appointment  where  doctor_id ='" . $doctor_pid . "' order by pid desc ");
        $json ['appointment'] = array();
        if (mysql_num_rows($query) > 0) {
            $i = 0;
            $data = array();
            while ($row = $PDO->db_fetch_array($query)) {
                $data [$i] ['pid'] = $row ['pid'];
                $data [$i] ['doctor_id'] = $row ['doctor_id'];
                $data [$i] ['patient_id'] = $row ['patient_id'];
                $data [$i] ['call_details_id'] = $row ['call_details_id'];
                $data [$i] ['app_date'] = $row ['app_date'];
                $data [$i] ['app_time'] = $row ['app_time'];
                $data [$i] ['comments'] = ($row ['comments'] != '') ? $row ['comments'] : '';
                $data [$i] ['status'] = $row ['status'];

                $doctor_query = $PDO->db_query("select * from #_doctors where pid='" . $row ['doctor_id'] . "'");
                $doctor_row = $PDO->db_fetch_array($doctor_query);
                if ($doctor_row ['speciality'] == 0) {
                    $speciality = 'General Practitioner';
                } else {

                    $speciality = $PDO->getSingleResult("select name from #_specialities where pid='" . $doctor_row ['speciality'] . "'");
                }
                $clinic = $PDO->getSingleResult("select name from #_clinics where pid='" . $doctor_row ['clinic_id'] . "'");

                $hospital = $PDO->getSingleResult("select name from #_hospitals where pid='" . $doctor_row ['hospital_id'] . "'");

                $patient_query = $PDO->db_query("select * from #_patients where pid='" . $row ['patient_id'] . "'");
                $patient_row = $PDO->db_fetch_array($patient_query);

                $division_query = $PDO->db_query("select * from #_division
                               where pid='" . $patient_row ['division'] . "'");
                $division_row = $PDO->db_fetch_array($division_query);

                $township_query = $PDO->db_query("select * from #_township
                              where division_id='" . $division_row ['pid'] . "'");
                $township_row = $PDO->db_fetch_array($township_query);

                $data [$i] ['doctor_name'] = $doctor_row ['name'];
                $data [$i] ['doctor_speciality'] = $speciality;
                $data [$i] ['doctor_sex'] = $doctor_row ['sex'];
                $data [$i] ['doctor_address'] = $doctor_row ['address'];
                $data [$i] ['clinic_id'] = $doctor_row ['clinic_id'];
                $data [$i] ['hospital_id'] = $doctor_row ['hospital_id'];
                $data [$i] ['clinic'] = ($clinic != '') ? $clinic : '';
                $data [$i] ['patient_id'] = $patient_row ['patient_id'];
                $data [$i] ['patient_name'] = $patient_row ['name'];
                $data [$i] ['patient_age'] = $patient_row ['age'];
                $data [$i] ['patient_sex'] = $patient_row ['sex'];
                $data [$i] ['patient_phone'] = $patient_row ['phone'];
                $data [$i] ['patient_address'] = $patient_row ['address'];
                $data [$i] ['patient_postal_code'] = $patient_row ['postal_code'];
                $data [$i] ['patient_township'] = $township_row ['name'];
                $data [$i] ['patient_division'] = $division_row ['name'];
                $data [$i] ['patient_consent'] = $PDO->getSingleResult("select patient_consent from #_call_details where pid='" . $row ['call_details_id'] . "'");
                $data [$i] ['hospital'] = ($hospital != '') ? $hospital : '';

                $i++;
            }
            $json ['appointment'] = $data;
            $json ['status'] = 'true';
        } else {
            $json ['status'] = 'false';
        }

        break;
    case 'refferal' :
        $call_details_id = NULL;
        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        $referral_id = mysql_real_escape_string($_POST ['referral_id']);
        $referral_type = mysql_real_escape_string($_POST ['referral_type']);

        $service_type = ($referral_type == 'lab') ? 'Lab/Imaging' : 'Health Consultation';
        $json ['status'] = 'false';
        $json ['msg'] = "Please try again...";
        $json ['call_details_id'] = 0;

        $query = "insert into crm_call_details  set  medicine_id='',caller_id=0,subscription_details_id=0,service_type='" . $service_type . "',sub_center_check=' ',sub_centers=0,services_comment='',call_type='',service_provided='referral service to',referral_service_to='" . $referral_type . "',
				follow_up_call_schedule='',follow_up_call_date='',follow_up_call_time='',health_consultation='',shortorder=0,status=1,tf_1='',tf_2='',create_by='" . $doctor_id . "',
				patient_id ='" . $patient_id . "',call_by='App', chief_complaint ='' , history_of_present_illness ='', past_medical_history='', past_surgical_history='',
				social_history='', family_history='', occupational_history='' ,history_of_drug_allergy='No',drug_allergy_comment='' , drug_allergy_regular_taking_medication='',
				bp='',pr='',sao='',weight='',rbs='',temp='',provisional_diagnosis='',create_by_type='Doctor',created_on='" . date('Y-m-d H:i:s') . "'";
        $call_details = $PDO->db_query($query);

        $call_details_id = $PDO->db_query("SELECT LAST_INSERT_ID() as id FROM crm_call_details");
        $call_details_id = $PDO->db_fetch_array($call_details_id);
        if ($call_details_id) {
            $query = "insert into crm_booking  set  service_type='" . $service_type . "',book_type='Doctor',call_id=" . $call_details_id ['id'] . ",book_id=" . $referral_id . ",shortorder=0,status=1,tf_1='',tf_2='',
				patient_id =" . $patient_id . ", booking_complete_flag =0 , booking_complete_comments ='', lab_test='', home_visit_service='',
				booking_comments='', book_date='" . date('Y-m-d') . "', book_time='" . date('h:i:s A') . "',create_by='0',created_on='" . date('Y-m-d H:i:s') . "',modified_on='" . date('Y-m-d H:i:s') . "'";

            $booking_details = $PDO->db_query($query);
            if ($booking_details) {
                $json ['status'] = 'true';
                $json ['msg'] = "Successfully referral";
                $json ['call_details_id'] = $call_details_id ['id'];
            }
            $patient_gsm = $PDO->getSingleResult("select gsm_tocken from #_patients where pid=" . $patient_id);
            if ($patient_gsm) {
                $referral_name = ($referral_type == 'lab') ? $PDO->getSingleResult("select name from #_labs where pid='" . $referral_id . "'") : "Dr." . $PDO->getSingleResult("select name from #_doctors where pid='" . $referral_id . "'");
                $doctor_name = $PDO->getSingleResult("select name from #_doctors where pid='" . $doctor_id . "'");

                $message = array(
                    "message" => "Referral with " . $referral_type . " by Dr." . $doctor_name . " to " . $referral_name,
                    "flag" => 'refferal'
                );
                $is_ios_device = $PDO->getSingleResult("select is_ios_device from #_patients where pid=" . $patient_id);

                $result = $gcm->send_notification($patient_gsm, $message, "Referral", $doctor_id, 'D', $patient_id, 'P', $is_ios_device);
            }
            $doctor_user_id = $PDO->getSingleResult("select user_id from #_doctors where pid=" . $doctor_id);
            $doctor_gsm = $PDO->getSingleResult("select gsm_tocken from #_gcmuser where user_id=" . $doctor_user_id . " AND flag=1 and user_type='doctor'");
            if ($doctor_gsm) {

                // $doctor_name=$PDO->getSingleResult("select name from #_doctors where pid=" . $doctor_id);
                $message = array(
                    "message" => "Referral with " . $referral_type . " by Dr." . $doctor_name . " to " . $referral_name,
                    "flag" => 'refferal'
                );
                $is_ios_device = $PDO->getSingleResult("select is_ios_device from #_doctors where pid=" . $doctor_id);

                $result = $gcm->send_notification($doctor_gsm, $message, "Referral", $doctor_id, 'D', $doctor_id, 'D', $is_ios_device);
            }
            $doctor_user_id = $PDO->getSingleResult("select user_id from #_doctors where pid=" . $referral_id);
            $doctor_gsm = $PDO->getSingleResult("select gsm_tocken from #_gcmuser where user_id=" . $doctor_user_id . " AND flag=1 and 	user_type='doctor'");
            if ($doctor_gsm) {

                // $doctor_name=$PDO->getSingleResult("select name from #_doctors where pid=" . $doctor_id);
                $message = array(
                    "message" => "Referral with " . $referral_type . " by Dr." . $doctor_name . " to " . $referral_name,
                    "flag" => 'refferal'
                );
                $is_ios_device = $PDO->getSingleResult("select is_ios_device from #_doctors where pid=" . $doctor_id);
                $result = $gcm->send_notification($doctor_gsm, $message, "Referral", $doctor_id, 'D', $referral_id, 'D', $is_ios_device);
            }
        }

        break;
    case 'check_available_timeslots_for_given_date':
        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $given_date = mysql_real_escape_string($_POST ['given_date']);
        $json ['doctors_timeslot_details'] = array();
        if (($doctor_id == '') || ($given_date == '')) {
            $json ['status'] = 'false';
            $json ['msg'] = 'Doctor id or given date is empty.';
            $doctor_function->send_res($json);
            exit ();
        }
        //check date
        $timestamp = strtotime($given_date);
        $weekday = date('l', $timestamp);
        $weekday = $doctor_function->given_weekday_num_find($weekday);
        //check whether the doctor timeslot is avaibale for the given date
        $doctor_timeslot_is_available = $doctor_function->check_timeslot_is_available_for_given_date($doctor_id, $weekday);

        if ($doctor_timeslot_is_available != 'false') {
            $json ['doctors_timeslot_details'] = $doctor_timeslot_is_available;
            $json ['status'] = 'true';
            $json ['msg'] = 'Locations are available.';

        } else {
            $json ['status'] = 'false';
            $json ['msg'] = 'Locations are not available.';
        }

        break;
    case 'refferalappoinment' :
        sleep(2);
        $call_details_id = NULL;
        $app_time_start = NULL;
        $app_time_end = NULL;
        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        $call_details_id = mysql_real_escape_string($_POST ['call_details_id']);
        $doctor_location_id = mysql_real_escape_string($_POST ['doctor_location_id']);
        $app_date = mysql_real_escape_string($_POST ['app_date']);
        if (array_key_exists('app_time_start', $_POST)) {
            $app_time_start = mysql_real_escape_string($_POST ['app_time_start']);
        }
        if (array_key_exists('app_time_end', $_POST)) {
            $app_time_end = mysql_real_escape_string($_POST ['app_time_end']);
        }
        $appointment_time = date('H:i', strtotime($app_time_start));
        $appointment_token = 1;
        $json ['status'] = 'false';
        $json ['msg'] = "Please try again...";
        $json ['appointment_id'] = 0;

        if ($app_time_start && $app_time_end) {
            //$booking_query = $PDO->db_query("select * from #_appointment where doctor_id ='" . $calldata ['doctor_id'] . "' and call_details_id='" . $calldata ['call_id'] . "' and  app_date ='" . $calldata ['app_date'] . "'  and  doctor_location_id ='" . $_POST ['doctor_location_id'] . "' and app_time>='" . date('H:i', strtotime($app_time_start)) . " app_time<=" . date('H:i', strtotime($app_time_end)) . " order by pid limit 0,1");

            $booking_query = $PDO->db_query("select * from #_appointment where doctor_id ='" . $doctor_id . "' and call_details_id='" . $call_details_id . "' and  app_date ='" . $app_date . "'  and  doctor_location_id ='" . $doctor_location_id . "' and app_time >='" . date('H:i', strtotime($app_time_start)) . "' and app_time<='" . date('H:i', strtotime($app_time_end)) . "' order by pid limit 0,1");
            if (mysql_num_rows($booking_query) == 0) {
                while ($row_data = $PDO->db_fetch_array($booking_query)) {
                    $appointment_time = date('H:i', strtotime('+".APPOINTMENT_SLOT." minutes', strtotime($row_data ['app_time'])));
                    $appointment_token = ++$row_data ['token_no'];
                }
            }
        }
        $appointment_type = 4; //4 means referal
        //$parent_appointment_id = $PDO->getSingleResult("select pid from #_appointment where call_details_id='" . $call_details_id . " '");
        if (array_key_exists('parent_id', $_POST)) {
            $parent_appointment_id = mysql_real_escape_string($_POST ['parent_id']);
        }else{
            $parent_appointment_id = $PDO->getSingleResult("select pid from #_appointment where call_details_id='" . $call_details_id . " '");
        }
        $query = "insert into crm_appointment  set call_details_id='" . $call_details_id . "'
        ,parent_appointment_id='" . $parent_appointment_id . "',appointment_type='" . $appointment_type . "',
        doctor_location_id='" . $doctor_location_id . "',doctor_id='" . $doctor_id . "',app_time='" . $appointment_time . "',
        token_no='" . $appointment_token . "',
        shortorder=0,status=1,
				patient_id ='" . $patient_id . "',comments ='', app_date='" . $app_date . "',
				 created_by='0',created_on='" . date('Y-m-d H:i:s') . "',modified_on='" . date('Y-m-d H:i:s') . "'";
        $appointment_details = $PDO->db_query($query);

        $appointment_details_id = $PDO->db_query("SELECT LAST_INSERT_ID() as id FROM crm_appointment");
        $appointment_details_id = $PDO->db_fetch_array($appointment_details_id);

        if ($appointment_details) {
            $mapping_data ['doctor_id'] = $doctor_id;
            $mapping_data ['patient_id'] = $patient_id;
            $mapping_data ['call_details_id'] = $call_details_id;
            $mapping_data ['appoinment_id'] = $appointment_details_id;
            $mapping_data ['created_on'] = @date('Y-m-d H:i:s');
            $mapping_data ['modified_on'] = @date('Y-m-d H:i:s');

            $mapping_id = $PDO->sqlquery("rs", 'book_app_mapping', $mapping_data);
            if ($mapping_id) {
                $json ['status'] = 'true';
                $json ['msg'] = "Successfully referral with appointment date";
                $json ['appointment_id'] = $appointment_details_id ['id'];
            }
            $patient_gsm = $PDO->getSingleResult("select gsm_tocken from #_patients where pid=" . $patient_id);
            if ($patient_gsm) {

                $message = array(
                    "message" => "Referral appointment(" . $appointment_details_id . ")is scheduled on" . $app_date . " " . date('h:i A', strtotime($appointment_time)) . " with Dr." . $PDO->getSingleResult("select name from #_doctors where pid=" . $doctor_id),
                    "flag" => 'refferal_appointment'
                );
                $is_ios_device = $PDO->getSingleResult("select is_ios_device from #_patients where pid=" . $patient_id);
                $result = $gcm->send_notification($patient_gsm, $message, "Referral Appointment", 0, 'N', $patient_id, 'P', $is_ios_device);
            }
        }

        break;
    case 'getDoctorQrCode' :

        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $json_data = array();
        $json ['url'] = '';
        $json ['status'] = 'false';
        $json ['msg'] = 'QR Code Generation Failed';
        $doctor_query = $PDO->db_query("select * from #_doctors where pid='" . $doctor_id . "'");
        if (mysql_num_rows($doctor_query) > 0) {
            while ($doctor_row = $PDO->db_fetch_array($doctor_query)) {
                $doctor_location_data = array();
                $doctor_location_query = $PDO->db_query("select * from #_doctors_locations where doctor_id='" . $doctor_row ['pid'] . "' ");
                if (mysql_num_rows($doctor_location_query) > 0) {

                    while ($row = $PDO->db_fetch_array($doctor_location_query)) {
                        $current_available_status = $doctor_function->check_available_unavailable($doctor_id, $row ['pid']);

                        if ($current_available_status ['status'] == 'true') {
                            $doctor_location_data [] = array(
                                'doctor_location_id' => $row ['pid'],
                                'location_type' => ($row ['location_type'] == 1) ? "Home Location" : "Work Location",
                                'isHomeservice' => $row ['isHomeservice'],
                                'available_from' => $row ['available_from'],
                                'available_to' => $row ['available_to']
                            );
                            /*
							 * $doctor_location_data[$i]['doctor_location_id'] = $row['pid'];
							 * $doctor_location_data[$i]['location_type'] = ($row['location_type']==1)?'Home Location':'Work Location';
							 * $doctor_location_data[$i]['isHomeservice'] = $row['isHomeservice'];
							 * $doctor_location_data[$i]['available_from'] = $row['available_from'];
							 * $doctor_location_data[$i]['available_to'] = $row['available_to'];
							 */
                        }
                    }
                }
                $json_data = array(
                    'doctor_id' => $doctor_row ['pid'],
                    'doctor_name' => $doctor_row ['name'],
                    'doctor_email' => $doctor_row ['email'],
                    'doctor_mobile' => $doctor_row ['phone'],
                    'user_id' => $doctor_row ['user_id'],
                    'doctor_location_data' => $doctor_location_data
                );
                $qr_code_url = "https://chart.googleapis.com/chart?cht=qr&chl=" . json_encode($json_data) . "&chs=300x300&choe=UTF-8&chld=L|0";
                $json ['url'] = stripcslashes($qr_code_url);
                $json ['status'] = 'true';
                $json ['msg'] = 'QR Code Generated Successfully';
            }
        }
        break;

    case 'requestPatientConsent' :
        sleep(2);
        $call_details_id = NULL;
        $json ['status'] = 'false';
        $json ['consent_otp'] = '';
        $json ['msg'] = "";

        $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        $call_details_id = mysql_real_escape_string($_POST ['call_details_id']);
        $consent_otp = str_pad(( string )rand(111111, 999999), 6, '0', STR_PAD_LEFT);
        $query = "update #_call_details  set patient_consent_otp='" . $consent_otp . "',modified_on='" . date('Y-m-d H:i:s') . "' WHERE pid=" . $call_details_id . " AND patient_id ='" . $patient_id . "'";
        $patient_consent_details = $PDO->db_query($query);
        if ($patient_consent_details) {
            $patient_mobile = $PDO->getSingleResult("select phone from #_patients where pid=" . $patient_id);
            if ($patient_mobile) {
                $message = urlencode('OTP(one time password) for your appointment Consent is :' . $consent_otp);
                //    $patient_consent_sms = file_get_contents("http://www.doctoroncall.com.mm/crm/app/sms/send_sms.php?mobile=" . $patient_mobile . "&message=" . $message . "&title='OTPConsent'&recevier_id=" . $patient_id . "&recevier_type='P'");
                // send google push notification to patient
                $patient_gsm = $PDO->getSingleResult("select gsm_tocken from #_patients where pid=" . $patient_id);

                if ($patient_gsm) {

                    $message = array(
                        "message" => "Consent OTP is :" . $consent_otp . " for your appointment. Reference id(" . $call_details_id . ")",
                        "flag" => 'otp_consent',
                        "consent_otp" => $consent_otp
                    );
                    $is_ios_device = $PDO->getSingleResult("select is_ios_device from #_patients where pid=" . $patient_id);
                    $result = $gcm->send_notification($patient_gsm, $message, "Consent OTP", 0, 'N', $patient_id, 'P', $is_ios_device);
                    $json ['notification_status']=$result;
                }
                $json ['status'] = 'true';
                $json ['consent_otp'] = $consent_otp;
                $json ['msg'] = "Consent Request sent to Patient";
                // }
            }
        }
        break;

    case 'patientConsentOtpVerify' :
        $json ['status'] = 'false';
        $json ['msg'] = "";
        $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        $call_details_id = mysql_real_escape_string($_POST ['call_details_id']);
        $consent_otp = mysql_real_escape_string($_POST ['consent_otp']);
        $genrated_consent_otp = $PDO->getSingleResult("select patient_consent_otp from #_call_details where pid=" . $call_details_id . " AND patient_id=" . $patient_id);

        if ($genrated_consent_otp == $consent_otp) {
            $query = "update #_call_details  set patient_consent=1,modified_on='" . date('Y-m-d H:i:s') . "' where pid=" . $call_details_id . " AND patient_id ='" . $patient_id . "'";
            $patient_consent_verified = $PDO->db_query($query);
            $appointment_app_start_time_details = array(
                'app_start_time' => date('Y-m-d H:i:s')
            );
            $update_app_start_time = $PDO->sqlquery("rs", 'appointment', $appointment_app_start_time_details, $update = 'call_details_id', $id = $call_details_id);

            // if ($patient_consent_verified)
            // {
            $json ['status'] = 'true';
            $json ['msg'] = "Patient Consent Verified Successfully";
            // }

            //New Eppisode -insert the details into episode details table
            $doctor_id = $PDO->getSingleResult("select doctor_id from #_appointment where call_details_id='" . $call_details_id . "'");
            $appointment_id = $PDO->getSingleResult("select pid from #_appointment where call_details_id='" . $call_details_id . "'");
            $modified_on = date('Y-m-d H:i:s');

            //check exising epsiode or new episode
            $episode_id = $doctor_function->check_episode_exist($patient_id, $doctor_id);
            $appointment_type = $PDO->getSingleResult("select appointment_type from #_appointment where pid='" . $appointment_id . "'");
            $parent_appointment_id = $PDO->getSingleResult("select parent_appointment_id from #_appointment where pid='" . $appointment_id . "'");
            if ($appointment_type == 5) {

                // referal
                //find the exsiting encounter and assign it for this appointment
                if ($parent_appointment_id != 0) {
                    $episode_id_of_parent_appointment = $PDO->getSingleResult("select episode_id from #_appointment where pid='" . $parent_appointment_id . "'");
                    $encounter_id_of_parent_appointment = $PDO->getSingleResult("select encounter_id from #_appointment where pid='" . $parent_appointment_id . "'");
                    //Update episode and encounter details in to the appointment table
                    $appointment_eppisode_encounter_details = array(
                        'episode_id' => $episode_id_of_parent_appointment,
                        'encounter_id' => $encounter_id_of_parent_appointment
                    );
                    $update_id = $PDO->sqlquery("rs", 'appointment', $appointment_eppisode_encounter_details, $update = 'pid', $id = $appointment_id);
                    if (!empty($episode_id)) {
                        $json ['appointment_update_msg'] = "Episode and Encounter details updated on appointment table successfully";
                    }
                }

            } else {
                if ($episode_id == 'false') {
                    //New episode and new encounter creation
                    $episode_details = array(
                        'call_details_id' => $call_details_id,
                        'appointment_id' => $appointment_id,
                        'patient_id' => $patient_id,
                        'doctor_id' => $doctor_id,
                        'modified_on' => $modified_on,
                    );
                    $episode_id = $doctor_function->create_episode($episode_details);
                    if (!empty($episode_id)) {
                        $json ['episode_msg'] = "Episode created successfully";
                        //New Encounter Create
                        $encounter_details = array(
                            'episode_id' => $episode_id,
                            'call_details_id' => $call_details_id,
                            'appointment_id' => $appointment_id,
                            'patient_id' => $patient_id,
                            'doctor_id' => $doctor_id,
                            'modified_on' => $modified_on,
                        );
                        $encounter_id = $doctor_function->create_encounter($encounter_details);
                        if (!empty($episode_id)) {
                            $json ['encounter_msg'] = "Encounter created successfully";
                        }
                        //Update episode and encounter details in to the appointment table
                        $appointment_eppisode_encounter_details = array(
                            'episode_id' => $episode_id,
                            'encounter_id' => $encounter_id
                        );
                        $update_id = $PDO->sqlquery("rs", 'appointment', $appointment_eppisode_encounter_details, $update = 'pid', $id = $appointment_id);
                        if (!empty($episode_id)) {
                            $json ['appointment_update_msg'] = "Episode and Encounter details updated on appointment table successfully";
                        }

                    } else {
                        $json ['episode_msg'] = "Episode Not created successfully";

                    }
                } else {
                    //exisiting episode
                    //check  the appointment is existing encounter?
                    $appointment_type = $PDO->getSingleResult("select appointment_type from #_appointment where pid='" . $appointment_id . "'");

                    //Create new encounter
                    $encounter_details = array(
                        'episode_id' => $episode_id,
                        'call_details_id' => $call_details_id,
                        'appointment_id' => $appointment_id,
                        'patient_id' => $patient_id,
                        'doctor_id' => $doctor_id,
                        'modified_on' => $modified_on,
                    );
                    $encounter_id = $doctor_function->create_encounter($encounter_details);
                    if (!empty($episode_id)) {
                        $json ['encounter_msg'] = "Encounter created successfully";
                    }
                    //Update episode and encounter details in to the appointment table
                    $appointment_eppisode_encounter_details = array(
                        'episode_id' => $episode_id,
                        'encounter_id' => $encounter_id
                    );
                    $update_id = $PDO->sqlquery("rs", 'appointment', $appointment_eppisode_encounter_details, $update = 'pid', $id = $appointment_id);
                    if (!empty($episode_id)) {
                        $json ['appointment_update_msg'] = "Episode and Encounter details updated on appointment table successfully";
                    }


                }

            }

        } else {
            $json['msg'] = 'Consent OTP not matched. ';
        }
        break;

    case 'feedbackAppointmentDetails' :
        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);

        $json ['location_details'] = array();
        $json ['status'] = 'true';
        $json ['msg'] = 'Feedback Appointments Not Found';

        $query = "select app.pid as appointmentId,app.patient_id as patient_id,dl.location_type As location_type,dl.hospital_id as hospital_id,dl.clinic_id as clinic_id,dl.isHomeservice as isHomeservice,dl.latlong as latlong,dl.location as location,cd.patient_rated as patient_rated,cd.patient_rated_comments as patient_rated_comments from #_appointment as app left join #_doctors_locations as dl ON app.doctor_location_id=dl.pid left join #_call_details as cd on cd.pid=app.call_details_id  where app.doctor_id=" . $doctor_id . " AND app.status=2 and cd.patient_rated >0 order by app.pid";
        $doctor_location_details = $PDO->db_query($query);
        if (mysql_num_rows($doctor_location_details) > 0) {
            $i = 0;
            $doctor_location_array = array();
            while ($doctor_data = $PDO->db_fetch_array($doctor_location_details)) {
                // $doctor_location_array[] = $doctor_data['user_id'];
                $doctor_location_array [$i] ['appointmentId'] = $doctor_data ['appointmentId'];
                $doctor_location_array [$i] ['locationType'] = $doctor_data ['location_type'];
                $doctor_location_array [$i] ['locationName'] = ($doctor_data ['location_type'] == 1) ? $PDO->getSingleResult("select name from #_clinics where pid='" . $doctor_data ['clinic_id'] . "'") . "(Clinic)" : $PDO->getSingleResult("select name from #_hospitals where pid='" . $doctor_data ['hospital_id'] . "'") . "(Hospital)";
                $doctor_location_array [$i] ['hospitalName'] = $PDO->getSingleResult("select name from #_hospitals where pid='" . $doctor_data ['hospital_id'] . "'");
                $doctor_location_array [$i] ['clinicName'] = $PDO->getSingleResult("select name from #_clinics where pid='" . $doctor_data ['clinic_id'] . "'");
                $doctor_location_array [$i] ['isHomeservice'] = $doctor_data ['isHomeservice'];
                $doctor_location_array [$i] ['latlong'] = $doctor_data ['latlong'];
                $doctor_location_array [$i] ['location'] = $doctor_data ['location'];
                $doctor_location_array [$i] ['patientRated'] = round($doctor_data ['patient_rated']);
                $doctor_location_array [$i] ['patientRatedComments'] = $doctor_data ['patient_rated_comments'];
                $doctor_location_array [$i] ['patientId'] = $doctor_data ['patient_id'];
                $doctor_location_array [$i] ['patientProfileImage'] = SITE_PATH . "uploaded_files/profile_pictures/" . $PDO->getSingleResult("select profile_pic from #_patients where pid='" . $doctor_data ['patient_id'] . "'");
                $doctor_location_array [$i] ['patientName'] = $PDO->getSingleResult("select name from #_patients where pid='" . $doctor_data ['patient_id'] . "'");
                $i++;
            }
            $json ['location_details'] = $doctor_location_array;
            $json ['status'] = 'true';
            $json ['msg'] = 'Succes!! Feedback Appointments List';
        }

        break;
    case 'refferalLists' :
        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $json ['refferal_details'] = array();
        $json ['status'] = 'true';
        $json ['msg'] = 'Refferal list not found';
        $query = "select cd.patient_id as patient_id,b.book_id as referral_doctor_id,dl.location_type as location_type,dl.hospital_id as hospital_id,dl.clinic_id as clinic_id,d.speciality as speciality,d.education as education,d.profile_image as profile_image from #_call_details as cd left join #_booking as b ON b.call_id=cd.pid left join #_doctors_locations as dl on dl.pid=cd.doctor_location_id left join #_doctors as d on d.pid=b.book_id where cd.create_by=" . $doctor_id . " AND cd.create_by_type='Doctor' >0 order by cd.pid desc";
        $doctor_refferal_details = $PDO->db_query($query);
        if (mysql_num_rows($doctor_refferal_details) > 0) {
            $i = 0;
            $doctor_refferal_array = array();
            while ($doctor_data = $PDO->db_fetch_array($doctor_refferal_details)) {
                // $doctor_refferal_array[] = $doctor_data['user_id'];
                $doctor_refferal_array [$i] ['referralDoctorId'] = $doctor_data ['referral_doctor_id'];
                $doctor_refferal_array [$i] ['locationType'] = $doctor_data ['location_type'];
                $doctor_refferal_array [$i] ['locationName'] = ($doctor_data ['location_type'] == 1) ? $PDO->getSingleResult("select name from #_clinics where pid='" . $doctor_data ['clinic_id'] . "'") . "(Clinic)" : $PDO->getSingleResult("select name from #_hospitals where pid='" . $doctor_data ['hospital_id'] . "'") . "(Hospital)";
                /*
				 * $doctor_refferal_array[$i]['hospitalName'] = $PDO->getSingleResult("select name from #_hospitals where pid='" . $doctor_data['hospital_id'] . "'");
				 * $doctor_refferal_array[$i]['clinicName'] = $PDO->getSingleResult("select name from #_clinics where pid='" . $doctor_data['clinic_id'] . "'");
				 */
                $doctor_refferal_array [$i] ['referralDoctorEducation'] = $doctor_data ['education'];
                $doctor_refferal_array [$i] ['referralDoctorProfileImage'] = $doctor_data ['profile_image'];
                $doctor_refferal_array [$i] ['referralDoctorName'] = $doctor_data ['location'];
                $doctor_refferal_array [$i] ['referralDoctorSpeciality'] = $PDO->getSingleResult("select name from #_specialities where pid='" . $doctor_data ['speciality'] . "'");
                $doctor_refferal_array [$i] ['patientId'] = $doctor_data ['patient_id'];
                $doctor_refferal_array [$i] ['patientName'] = $PDO->getSingleResult("select name from #_patients where pid='" . $doctor_data ['patient_id'] . "'");
                $i++;
            }
            $json ['refferal_details'] = $doctor_refferal_array;
            $json ['status'] = 'true';
            $json ['msg'] = 'Success!! Refferal list';
        }

        break;
    case 'appointmentsCategoryLists' :
        $appointment_category_lists = array();
        $is_today = 0;
        $start_offset = 0;
        $end_limit = 20;

        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $category = mysql_real_escape_string($_POST ['category']); // 1-new ...
        $location_id = mysql_real_escape_string($_POST ['location_id']);
        if (array_key_exists('start_offset', $_POST) && $_POST ['start_offset']) {
            $start_offset = mysql_real_escape_string($_POST ['start_offset']);
            $end_limit = $start_offset + PAGINATION_END_LIMIT;
        }
        if (array_key_exists('is_today', $_POST) && $_POST ['is_today']) {
            $is_today = mysql_real_escape_string($_POST ['is_today']);
        }

        $json ['appointment_category_list'] = $appointment_category_lists;
        $json ['status'] = 'true';
        $json ['msg'] = 'Appointment list not found';
        if (!empty ($location_id)) {
            $appointment_category_lists = $doctor_function->getDoctorAppointmentListBasedLoccation($doctor_id, $category, $start_offset, $end_limit, $is_today, $location_id);
        } else {
            $appointment_category_lists = $doctor_function->getDoctorAppointmentList($doctor_id, $category, $start_offset, $end_limit, $is_today);
        }
        if ($appointment_category_lists) {
            $json ['appointment_category_list'] = $appointment_category_lists;
            $json ['status'] = 'true';
            $json ['msg'] = 'Success!! Appointment list';
        }

        break;

    case 'patientAppointmentAndPatientDetails' :
        //  $patient_details = array();
        $curent_appointment_deatils = array();
        $patient_medication_history = array();
        $appointment_history_lists = array();
        // $json ['patient_details'] = $patient_details;
        //  $json ['curent_appointment_deatils'] = $curent_appointment_deatils;
        $json ['patient_medication_history'] = $patient_medication_history;
        $json ['appointment_history_lists'] = $appointment_history_lists;


        $start_offset = 0;
        $end_limit = 20;

        $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        $call_details_id = mysql_real_escape_string($_POST ['call_details_id']);
        if (array_key_exists('start_offset', $_POST) && $_POST ['start_offset']) {
            $start_offset = mysql_real_escape_string($_POST ['start_offset']);
            $end_limit = $start_offset + PAGINATION_END_LIMIT;
        }
        $json ['status'] = 'true';
        $json ['msg'] = 'Appointment list not found';

        $patient_details = $doctor_function->getPatientDetails($patient_id);

        if ($patient_details) {
            //   $json ['patient_details'] = $patient_details;
        }


        if ($patient_id && $call_details_id && $call_details_id != '' && $patient_id != '') {
            $query = "select ccd.*,d.pid as doctor_id,d.name as doctor_name,d.education as doctor_education,d.speciality as doctor_speciality,app.appointment_type,app.app_time as app_start_time,app.app_completed_time as app_end_time from crm_call_details as ccd left join crm_appointment as app on app.call_details_id=ccd.pid left join crm_doctors as d on d.pid=app.doctor_id
				 where app.call_details_id='" . $call_details_id . "' AND ccd.patient_id ='" . $patient_id . "' order by ccd.pid  limit 0,1";

            $doctory_history_details = $PDO->db_query($query);
            // $doctory_history_details = $PDO->db_fetch_array($doctory_history_details);
            if (mysql_num_rows($doctory_history_details) > 0) {
                $i = 0;
                while ($row = $PDO->db_fetch_array($doctory_history_details)) {
                    if ($i < count($doctory_history_details)) {
                        $reponse_data  ['app_start_time'] = ($row ['app_start_time'] === NULL) ? '' : $row ['app_start_time'];
                        $reponse_data  ['app_end_time'] = ($row ['app_end_time'] === NULL) ? '' : $row ['app_end_time'];
                        $reponse_data  ['drug_allergy_regular_taking_medication'] = ($row ['drug_allergy_regular_taking_medication'] === NULL) ? '' : $row ['drug_allergy_regular_taking_medication'];
                        $reponse_data  ['bp'] = ($row ['bp'] === NULL) ? '' : $row ['bp'];
                        $reponse_data  ['pr'] = ($row ['pr'] === NULL) ? '' : $row ['pr'];
                        $reponse_data  ['sao'] = ($row ['sao'] === NULL) ? '' : $row ['sao'];
                        $reponse_data  ['rbs'] = ($row ['rbs'] === NULL) ? '' : $row ['rbs'];
                        $reponse_data  ['temp'] = ($row ['temp'] === NULL) ? '' : $row ['temp'];
                        $reponse_data  ['provisional_diagnosis'] = ($row ['provisional_diagnosis'] === NULL) ? '' : $row ['provisional_diagnosis'];


                    }

                    $i++;
                }
                $patient_medication_history = $reponse_data;
                $patient_medication_history = (object)$patient_medication_history;
                $json ['patient_medication_history'] = $patient_medication_history;
            }
        }


        ///Get Uploaded Records
        $call_details_id = NULL;
        $patient_id = NULL;
        $reponse_data = array();
        $uploaded_patients_reports = array();
        $patient_name = '';
        $doctor_name = '';
        $start_offset = 0;
        /*
		 * $doctor_id =mysql_real_escape_string($_POST['doctor_id']);
		 * $patient_id =mysql_real_escape_string($_POST['patient_id']);
		 * $call_details_id =mysql_real_escape_string($_POST['call_details_id']);
		 */

        if (array_key_exists('call_details_id', $_POST)) {
            $call_details_id = mysql_real_escape_string($_POST ['call_details_id']);
        }
        if (array_key_exists('doctor_id', $_POST)) {
            $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        }
        if (array_key_exists('start_offset', $_POST)) {
            $start_offset = mysql_real_escape_string($_POST ['start_offset']);
        }
        //$last = $start_offset + PAGINATION_END_LIMIT;
        $limit = " limit " . $start_offset . "," . PAGINATION_END_LIMIT;
        if ($call_details_id && ($patient_id == '' || $patient_id === null || empty ($patient_id))) {
            $query = "select cr.* ,bam.patient_id as 'patient_id', bam.doctor_id as 'doctor_id' from #_reports as cr left join #_book_app_mapping as bam on bam.call_details_id=cr.call_details_id where cr.call_details_id='" . $call_details_id . "' group by cr.pid desc " . $limit;
        } else if ($patient_id && ($call_details_id == '' || $call_details_id === null || empty ($call_details_id))) {
            $query = "select cr.* ,bam.patient_id as 'patient_id', bam.doctor_id as 'doctor_id' from #_book_app_mapping as bam left join #_reports as cr on bam.call_details_id=cr.call_details_id where bam.patient_id='" . $patient_id . "' group by cr.pid desc " . $limit;
        }

        $uploaded_patients_reports = $PDO->db_query($query);

        if (mysql_num_rows($uploaded_patients_reports) > 0) {
            $i = 0;
            while ($list = $PDO->db_fetch_array($uploaded_patients_reports)) {
                if ($i <= mysql_num_rows($uploaded_patients_reports)) {
                    $patient_name = $PDO->getSingleResult("select name from #_patients where pid='" . $list ['patient_id'] . "'");
                    $doctor_name = $PDO->getSingleResult("select name from #_doctors where pid='" . $list ['doctor_id'] . "'");
                    /*
					 * $reponse_data[$i]['title'] =$list['title'];
					 * $reponse_data[$i]['reports_image']= $_SERVER['SERVER_NAME']."/crm/uploaded_files/reports/".$list['reports'];
					 * $reponse_data[$i]['call_details_id']= $list['call_details_id'];
					 * $reponse_data[$i]['report_id']= $list['pid'];
					 */
                    $reponse_data [] = array(
                        'title' => $list ['title'],
                        'reports_image' => SITE_PATH_ADM . "uploaded_files/reports/" . $list ['reports'],
                        'call_details_id' => $list ['call_details_id'],
                        'report_id' => $list ['pid'],
                        'report_date' => $list ['created_on'],
                        'patient_name' => $patient_name,
                        'doctor_name' => $doctor_name
                    );
                }
                $i++;
            }
        }
        $appointment_history_lists = $reponse_data;

        if ($appointment_history_lists) {
            // $json ['patient_details'] = $patient_details;
            // $json ['curent_appointment_deatils'] = $curent_appointment_deatils;
            $json ['appointment_history_lists'] = $appointment_history_lists;
            $json ['status'] = 'true';
            $json ['msg'] = 'Success!! Appointment list';
        }


        break;

    case 'patientAppointmentHistory' :
        $patient_details = array();
        $curent_appointment_deatils = array();
        $appointment_history_lists = array();

        $start_offset = 0;
        $end_limit = 20;

        $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        if (array_key_exists('start_offset', $_POST) && $_POST ['start_offset']) {
            $start_offset = mysql_real_escape_string($_POST ['start_offset']);
            $end_limit = $start_offset + PAGINATION_END_LIMIT;
        }
        $call_details_id = 0;
        if (array_key_exists('call_details_id', $_POST) && $_POST ['call_details_id']) {
            $call_details_id = mysql_real_escape_string($_POST ['call_details_id']);
        }


        $json ['curent_appointment_deatils'] = $curent_appointment_deatils;
        $json ['appointment_history_lists'] = $appointment_history_lists;
        $json ['status'] = 'true';


        $json ['msg'] = 'Appointment list not found';

        $patient_details = $doctor_function->getPatientDetails($patient_id);

        if ($patient_details) {
            $json ['patient_details'] = $patient_details;
        }
        $curent_appointment_deatils = $doctor_function->getPatientAppointmentHistory($patient_id, $category = 1, $start_offset, $end_limit, 1, $call_details_id);
        if ($curent_appointment_deatils) {
            $json ['curent_appointment_deatils'] = $curent_appointment_deatils;
        }
        $appointment_history_lists = $doctor_function->getPatientAppointmentHistory($patient_id, $category = 3, $start_offset, $end_limit, 0);
        $json ['patient_details'] = $patient_details;
        if ($appointment_history_lists) {
            // $json ['patient_details'] = $patient_details;
            // $json ['curent_appointment_deatils'] = $curent_appointment_deatils;
            $json ['appointment_history_lists'] = $appointment_history_lists;
            $json ['status'] = 'true';
            $json ['msg'] = 'Success!! Appointment list';
        }
        break;

    case 'appointmentCompleted' :

        $appointment_id = mysql_real_escape_string($_POST ['appointment_id']);

        $appointment_completed_updated = NULL;
        $json ['status'] = 'false';
        $json ['msg'] = 'Appointment completion failed';
        $completed_time = date('Y-m-d H:i:s');


        $query = "update #_appointment set status=3,app_completed_time='" . $completed_time . "' where pid=" . $appointment_id;

        $appointment_completed_updated = $PDO->db_query($query);
        if ($appointment_completed_updated) {

            $json ['status'] = 'true';
            $json ['msg'] = 'Success!! Appointment completed';
        }
        $patient_id = $PDO->getSingleResult("select patient_id from #_book_app_mapping where appoinment_id=" . $appointment_id);
        $doctor_id = $PDO->getSingleResult("select doctor_id from #_book_app_mapping where appoinment_id=" . $appointment_id);

        $patient_gsm = $PDO->getSingleResult("select gsm_tocken from #_patients where pid=" . $patient_id);
        if ($patient_gsm) {
            $doctor_name = $PDO->getSingleResult("select name from #_doctors where pid=" . $doctor_id);

            $message = array(
                "message" => "Appointment reference id(" . $appointment_id . ")is completed by Dr." . $doctor_name,
                "flag" => 'appointment_completed'
            );
            $is_ios_device = $PDO->getSingleResult("select is_ios_device from #_patients where pid=" . $patient_id);
            $result = $gcm->send_notification($patient_gsm, $message, "Appointment Completed", $doctor_id, 'D', $patient_id, 'P', $is_ios_device);
        }
        break;
    case 'appointmentCancelled' :

        $appointment_id = mysql_real_escape_string($_POST ['appointment_id']);
        $appointment_cancelled_updated = NULL;
        $json ['status'] = 'false';
        $json ['msg'] = 'Appointment cancellation failed';
        $query = "update #_appointment set status=4 where pid=" . $appointment_id;
        $appointment_cancelled_updated = $PDO->db_query($query);
        if ($appointment_cancelled_updated) {

            $json ['status'] = 'true';
            $json ['msg'] = 'Success!! Appointment cancelled';
        }
        $patient_id = $PDO->getSingleResult("select patient_id from #_book_app_mapping where appoinment_id=" . $appointment_id);
        $doctor_id = $PDO->getSingleResult("select doctor_id from #_book_app_mapping where appoinment_id=" . $appointment_id);

        $patient_gsm = $PDO->getSingleResult("select gsm_tocken from #_patients where pid=" . $patient_id);
        if ($patient_gsm) {
            $doctor_name = $PDO->getSingleResult("select name from #_doctors where pid=" . $doctor_id);

            $message = array(
                "message" => "Appointment reference id(" . $appointment_id . ")is cancelled by Dr." . $doctor_name,
                "flag" => 'appointment_cancelled'
            );
            $is_ios_device = $PDO->getSingleResult("select is_ios_device from #_patients where pid=" . $patient_id);
            $result = $gcm->send_notification($patient_gsm, $message, "Appointment Cancelled", $doctor_id, 'D', $patient_id, 'P', $is_ios_device);
        }
        $doctor_user_id = $PDO->getSingleResult("select user_id from #_doctors where pid=" . $doctor_id);
        $doctor_gsm = $PDO->getSingleResult("select gsm_tocken from #_gcmuser where user_id=" . $doctor_user_id . " AND flag=1 and 	user_type='doctor'");
        if ($doctor_gsm) {
            $doctor_name = $PDO->getSingleResult("select name from #_doctors where pid=" . $doctor_id);

            // $doctor_name=$PDO->getSingleResult("select name from #_doctors where pid=" . $doctor_id);
            $message = array(
                'message' => "Appointment reference id(" . $appointment_id . ") is cancelled by Dr." . $doctor_name,
                'flag' => 'appointment_cancelled'
            );
            $is_ios_device = $PDO->getSingleResult("select is_ios_device from #_doctors where pid=" . $doctor_id);
            $result = $gcm->send_notification($doctor_gsm, $message, "Appointment Cancelled", $patient_id, 'P', $doctor_id, 'D', $is_ios_device);
        }
        break;
    case 'paymentTransactionHistory' :

        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $year = mysql_real_escape_string($_POST ['year']);
        $wallet_amount = 0;
        $payment_history_list = '';

        $json ['status'] = 'false';
        $json ['wallet_amount'] = $wallet_amount;
        $json ['msg'] = 'Payment History Not Found';
        $json ['payment_history'] = $payment_history_list;

        $wallet_amount = $PDO->getSingleResult("select wallet_amount from #_doctors where pid='" . $doctor_id . "' order by pid desc");
        $months = array(
            'Jan',
            'Feb',
            'Mar',
            'Apr',
            'May',
            'Jun',
            'Jul',
            'Aug',
            'Sep',
            'Oct',
            'Nov',
            'Dec'
        );
        foreach ($months as $month) {
            $timestamp_date = strtotime($year);
            $year_only = date("Y", $timestamp_date);
            $month_details = $year_only . '-' . $month . '-01';
            $month_details = strtotime($month_details);
            $month_details = date("Y-m-d", $month_details);
            $result = $doctor_function->getPaymentHistoryDetails($doctor_id, 'D', $year, $month_details, $month);
            $transaction_list['transactionList'] = $result['payment_history'];
            $payment_history_list = $transaction_list;
            $payment_history_list['month_name'] = $month;
            $payment_history_list['amount'] = $result['total_amount'];
            $json ['monthWiseIncome'][] = $payment_history_list;


        }
        //  $payment_history_list = $doctor_function->getPaymentHistoryDetails($doctor_id, 'D',$year,$month_details);
        if (!empty($wallet_amount)) {
            $json ['status'] = 'true';
            $json ['current_wallet_amount'] = $PDO->getSingleResult("select wallet_amount from #_doctors where pid='" . $doctor_id . "'");
            $json ['wallet_amount'] = $wallet_amount;
            $json ['msg'] = 'Payment History List';
        } else {
            $json ['status'] = 'false';
            $json ['msg'] = 'No Details Available';
        }
        break;

    case 'walletRefillUpdate' :
        $update_wallet_amount = NULL;
        $payment_history_updated = NULL;
        $wallet_amount = 0;

        $json ['status'] = 'true';
        $json ['current_wallet_amount'] = $wallet_amount;
        $json ['msg'] = 'Success!! Wallet Refill';

        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $amount = mysql_real_escape_string($_POST ['amount']);

        $current_wallet_amount = $PDO->getSingleResult("select wallet_amount from #_doctors where pid='" . $doctor_id . "'");
        $json ['current_wallet_amount'] = $current_wallet_amount;
        $wallet_amount = $current_wallet_amount + $amount;
        $query = "update #_doctors set wallet_amount=" . $wallet_amount . " where pid='" . $doctor_id . "'";
        $update_wallet_amount = $PDO->db_query($query);

        if ($update_wallet_amount) {
            $query = "insert into #_app_payment_history set appointment_id='0',doctor_id='" . $doctor_id . "',patient_id='0',payment_mode='Wallet Refill',doctor_transaction_type='Credit',patient_transaction_type='',payment_amount='" . $amount . "',call_details_id='0',created_on='" . date('Y-m-d H:i:s') . "',modified_on='" . date('Y-m-d H:i:s') . "'";
            $payment_history_updated = $PDO->db_query($query);
            if ($payment_history_updated) {
                $json ['status'] = 'true';
                $json ['current_wallet_amount'] = $wallet_amount;
                $json ['msg'] = 'Success!! Wallet Refill';
            }
        }
        break;

    case 'followup_time_slot':
        $json['doctor_available_date_suggession'] = array();
        $second_day = '';
        $appointment_id = mysql_real_escape_string($_POST ['appointment_id']);
        $doctor_id = $PDO->getSingleResult("select doctor_id from #_appointment where pid='" . $appointment_id . "'");
        $patient_id = $PDO->getSingleResult("select patient_id from #_appointment where pid='" . $appointment_id . "'");
        $doctor_location_id = $PDO->getSingleResult("select doctor_location_id from #_appointment where pid='" . $appointment_id . "'");
        $app_date = $PDO->getSingleResult("select app_date from #_appointment where pid='" . $appointment_id . "'");
        $app_time = $PDO->getSingleResult("select app_time from #_appointment where pid='" . $appointment_id . "'");

        //Next available day after two days.
        $next_week = date('Y-m-d', strtotime($app_date . ' +7 day'));
        // $second_week = date('Y-m-d', strtotime($app_date . ' +14 day'));
        $next_month = date('Y-m-d', strtotime($app_date . ' +28 day'));
        $second_month = date('Y-m-d', strtotime($app_date . ' +56 day'));
        $week_day_of_the_appointment = date('w', strtotime($app_date));
        $query = $PDO->db_query("select * from #_doctors_locations_time_slots where doctor_id='" . $doctor_id . "' and location_id='" . $doctor_location_id . "' and status=1  and isDeleted=0 ");
        $second_day = '';
        $previous_weekday = '';
        if (mysql_num_rows($query) > 0) {
            $i = 1;
            $week_day_count = 0;
            while ($row = $PDO->db_fetch_array($query)) {
                $weekday_from_db = $row ['weekdays'];
                if ($second_day == '') {
                    if ($weekday_from_db > $week_day_of_the_appointment) {
                        if ($previous_weekday != $weekday_from_db) {
                            if ($weekday_from_db == 6) {
                                // echo 'inside the 6 th week day';
                                $query_weekday = $PDO->db_query("select * from #_doctors_locations_time_slots where doctor_id='" . $doctor_id . "' and location_id='" . $doctor_location_id . "'  and weekdays=1 and status=1 and available_status=1 and isDeleted=0 ");
                                if (mysql_num_rows($query_weekday) > 0) {
                                    $second_day = date('Y-m-d', strtotime($app_date . ' +2 day'));
                                }
                            } elseif ($weekday_from_db == 7) {
                                // echo 'inside the 7 th week day';
                                $query_weekday = $PDO->db_query("select * from #_doctors_locations_time_slots where doctor_id='" . $doctor_id . "' and location_id='" . $doctor_location_id . "'  and weekdays=2 and status=1 and available_status=1 and isDeleted=0 ");
                                if (mysql_num_rows($query_weekday) > 0) {
                                    $second_day = date('Y-m-d', strtotime($app_date . ' +2 day'));
                                }
                            } else {
                                //    echo 'inside the 1 to 5 th week day';
                                $weekday_from_db_plus_two = $week_day_of_the_appointment + 2;
                                $query_weekday = $PDO->db_query("select * from #_doctors_locations_time_slots where doctor_id='" . $doctor_id . "' and location_id='" . $doctor_location_id . "'  and weekdays='" . $weekday_from_db_plus_two . "' and status=1 and available_status=1 and isDeleted=0 ");
                                if (mysql_num_rows($query_weekday) > 0) {
                                    $second_day = date('Y-m-d', strtotime($app_date . ' +2 day'));

                                }
                            }


                        }

                    }

                    $i++;
                    $previous_weekday = $weekday_from_db;
                    $second_day = $second_day;
                }


            }            // echo $second_day;
            $json['status'] = 'true';
            $json['msg'] = 'Dates Available';
            $json['doctor_available_date_suggession'] = array('second_day' => $second_day, 'next_week' => $next_week, 'next_month' => $next_month, 'second_month' => $second_month);



        } else {
            $json['msg'] = 'No dates Available';
            $json['status'] = 'false';
        }
        break;

    case 'followup_time_slot_notification':
        $appointment_id = mysql_real_escape_string($_POST ['appointment_id']);
        $follow_up_date = mysql_real_escape_string($_POST ['follow_up_date']);
        if ((empty($appointment_id)) || (empty($follow_up_date))) {
            $json ['status'] = 'false';
            $json ['msg'] = 'Please send the values.';
            $doctor_function->send_res($json);
            exit ();
        }
        $patient_id = $PDO->getSingleResult("select patient_id from #_book_app_mapping where appoinment_id=" . $appointment_id);
        $doctor_id = $PDO->getSingleResult("select doctor_id from #_book_app_mapping where appoinment_id=" . $appointment_id);

        $patient_gsm = $PDO->getSingleResult("select gsm_tocken from #_patients where pid=" . $patient_id);
        if ($patient_gsm) {
            $doctor_name = $PDO->getSingleResult("select name from #_doctors where pid=" . $doctor_id);

            $message = array(
                "message" => "Follow Up Appointment is suggested on (" . $follow_up_date . ") by Dr." . $doctor_name,
                "flag" => 'followup_appointment'
            );
            $is_ios_device = $PDO->getSingleResult("select is_ios_device from #_patients where pid=" . $patient_id);
            $result = $gcm->send_notification($patient_gsm, $message, "Follow Up Appointment", $doctor_id, 'D', $patient_id, 'P', $is_ios_device);
            $json ['status'] = 'true';
            $json ['msg'] = 'Follow Up suggested successfully.';
        }


        break;


    case 'getAppointmentsDetails' :
        $call_details_id = 0;
        $appointment_details = array();
        $mediation_history = array();
        $reports_list = array();
        $call_details_id = mysql_real_escape_string($_POST ['call_details_id']);
        $appointment_id = $PDO->getSingleResult("select pid from #_appointment where call_details_id='" . $call_details_id . "'");
        $json ['medicine_details'] = array();
        $json ['appointment_details'] = $appointment_details;
        $json ['mediation_history'] = $mediation_history;
        $json ['reports_list'] = $reports_list;
        $json ['status'] = 'true';
        $json ['msg'] = 'Appointment list not found';

        $appointment_details = $doctor_function->getAppointmentDetails($appointment_id);
        if ($appointment_details) {
            $json ['appointment_details'] = $appointment_details;
        }

        $call_details_id = $PDO->getSingleResult("select bam.call_details_id from #_book_app_mapping as bam left join #_appointment as app on bam.appoinment_id=app.pid where bam.appoinment_id=" . $appointment_id) . " AND app.status NOT IN(1,4)";

        if ($call_details_id) {
            $query = "select * from #_call_details  where pid ='" . $call_details_id . "'  order by pid desc limit 0,1";
            $doctory_history_details = $PDO->db_query($query);

            if (mysql_num_rows($doctory_history_details) > 0) {

                $i = 0;
                while ($row = $PDO->db_fetch_array($doctory_history_details)) {
                    if ($i < count($doctory_history_details)) {
                        $service_provided = '';
                        // print_r($patient_history->chief_complaint);exit();
                        $service_provided = ($row ['service_provided'] === NULL) ? '' : $row ['service_provided'];
                        if (!empty($service_provided)) {
                            $mediation_history [$i] ['call_detail_id'] = ($row ['pid'] === NULL) ? '' : $row ['pid'];
                            $mediation_history [$i] ['chief_complaint'] = ($row ['chief_complaint'] === NULL) ? '' : $row ['chief_complaint'];
                            $mediation_history [$i] ['history_of_present_illness'] = ($row ['history_of_present_illness'] === NULL) ? '' : $row ['history_of_present_illness'];
                            $mediation_history [$i] ['past_medical_history'] = ($row ['past_medical_history'] === NULL) ? '' : $row ['past_medical_history'];
                            $mediation_history [$i] ['past_surgical_history'] = ($row ['past_surgical_histor'] === NULL) ? '' : $row ['past_surgical_histor'];
                            $mediation_history [$i] ['social_history'] = ($row ['social_history'] === NULL) ? '' : $row ['social_history'];
                            $mediation_history [$i] ['family_history'] = ($row ['family_history'] === NULL) ? '' : $row ['family_history'];
                            $mediation_history [$i] ['occupational_history'] = ($row ['occupational_history'] === NULL) ? '' : $row ['occupational_history'];
                            $mediation_history [$i] ['history_of_drug_allergy'] = ($row ['history_of_drug_allergy'] === NULL) ? '' : $row ['history_of_drug_allergy'];
                            $mediation_history [$i] ['drug_allergy_comment'] = ($row ['drug_allergy_comment'] === NULL) ? '' : $row ['drug_allergy_comment'];
                            $mediation_history [$i] ['drug_allergy_regular_taking_medication'] = ($row ['drug_allergy_regular_taking_medication'] === NULL) ? '' : $row ['drug_allergy_regular_taking_medication'];
                            $mediation_history [$i] ['bp'] = ($row ['bp'] === NULL) ? '' : $row ['bp'];
                            $mediation_history [$i] ['pr'] = ($row ['pr'] === NULL) ? '' : $row ['pr'];
                            $mediation_history [$i] ['sao'] = ($row ['sao'] === NULL) ? '' : $row ['sao'];
                            $mediation_history [$i] ['rbs'] = ($row ['rbs'] === NULL) ? '' : $row ['rds'];
                            $mediation_history [$i] ['temp'] = ($row ['temp'] === NULL) ? '' : $row ['temp'];
                            $mediation_history [$i] ['provisional_diagnosis'] = ($row ['provisional_diagnosis'] === NULL) ? '' : $row ['provisional_diagnosis'];


                        }
                    }

                    $i++;
                }
            }
        }
        if ($mediation_history) {
            $json ['mediation_history'] = $mediation_history;
        }

        $query = "select cr.* from #_reports as cr where cr.call_details_id='" . $call_details_id . "' and is_deleted=0 group by cr.pid";
        $uploaded_patients_reports = $PDO->db_query($query);

        if (mysql_num_rows($uploaded_patients_reports) > 0) {
            $i = 0;
            while ($list = $PDO->db_fetch_array($uploaded_patients_reports)) {
                if ($i <= mysql_num_rows($uploaded_patients_reports)) {

                    $reports_list [] = array(
                        'title' => $list ['title'],
                        'reports_image' => SITE_PATH_ADM . "uploaded_files/reports/" . $list ['reports'],
                        'call_details_id' => $list ['call_details_id'],
                        'report_id' => $list ['pid'],
                        'report_date' => $list ['created_on'],
                        'is_digital_prescription' => $list ['is_digital_prescription'],
                        'uploaded_type' => $list ['user_type']
                    );
                }
                $i++;
            }
        }
        if ($reports_list) {
            $json ['reports_list'] = $reports_list;
        }

        //Medicine List provided by doc $appointment_id
        $doctor_id = $PDO->getSingleResult("select doctor_id from #_appointment where pid='" . $appointment_id . "'");
        $patient_id = $PDO->getSingleResult("select patient_id from #_appointment where pid='" . $appointment_id . "'");
        $wh = "and doctor_id='" . $doctor_id . "' and patient_id='" . $patient_id . "' and appointment_id='" . $appointment_id . "' ";
        $query = "select * from #_patient_medicine_details where is_deleted=0 AND status=1 " . $wh . " ORDER BY appointment_id desc";
        // print_r($query);exit();
        $result = $PDO->db_query($query);
        $medicine_data_list = array();
        if (mysql_num_rows($result) > 0) {
            $i = 0;
            while ($medicine_data = $PDO->db_fetch_array($result)) {

				$medicine_data_list [$i] ['pid'] = $medicine_data ['pid'];
                $medicine_data_list [$i] ['patient_id'] = $medicine_data ['patient_id'];
                $medicine_data_list [$i] ['doctor_id'] = $medicine_data ['doctor_id'];
                $medicine_data_list [$i] ['appointment_id'] = $medicine_data ['appointment_id'];
                $medicine_data_list [$i] ['call_details_id'] = $medicine_data ['call_details_id'];
                $medicine_data_list [$i] ['drugs'] = stripslashes($medicine_data ['drugs']);
                $i++;
            }
        }

        $json ['medicine_details'] = $medicine_data_list;

        //Lab Test Details List
        $lab_test_data_list=array();
        $wh = "and patient_id='" . $patient_id . "'  and doctor_id='" . $doctor_id . "' ";
        $query = "select * from #_patient_lab_test_details where is_deleted=0 AND status=1 " . $wh . " ORDER BY appointment_id desc";
        // print_r($query);exit();
        $result = $PDO->db_query($query);
        $lab_test_data_list = array();
        if (mysql_num_rows($result) > 0) {
            $i = 0;
            while ($lab_test_data = $PDO->db_fetch_array($result)) {
                $lab_test_data_list [$i] ['lab_id'] = $lab_test_data ['lab_id'];
                $lab_test_data_list [$i] ['lab_test_cat_id'] = $lab_test_data ['lab_test_cat_id'];
                $lab_test_data_list [$i] ['lab_test_sub_cat_id'] = $lab_test_data ['lab_test_sub_cat_id'];
                $lab_test_data_list [$i] ['lab_name'] = $PDO->getSingleResult("select name from #_labs where pid='" . $lab_test_data ['lab_id'] . "'");
                $lab_test_data_list [$i] ['lab_test_category_name'] = $PDO->getSingleResult("select name from #_services_cat where pid='" . $lab_test_data ['lab_test_cat_id'] . "'");
                $lab_test_data_list [$i] ['lab_test_sub_category_name'] = $PDO->getSingleResult("select name from #_service_subcat where pid='" . $lab_test_data ['lab_test_sub_cat_id'] . "'");
                $lab_test_data_list [$i] ['created_on'] = $lab_test_data ['created_on'];
                $i++;
            }
        }

        $json ['lab_test_details'] = $lab_test_data_list;




        if ($appointment_details && $mediation_history && $reports_list) {
            $json ['status'] = 'true';
            $json ['msg'] = 'Success!! Appointment details';
        }
        break;

    default :
        break;
}

/* Output header */
// header('Content-type: application/json');
// echo json_encode($_POST);
echo json_encode($json);
?>