<?php
include ("../lib/open.inc.php");
$flag = mysql_real_escape_string ( $_POST ['flag'] );
$json = array ();
include 'class/all_class_files.php';
$doctor_function = new doctor ();
$patient_function = new patient ();
$m = new MyMail ();
include_once 'notification/GCM.php';
$gcm = new GCM ();
date_default_timezone_set('Asia/Kolkata');

switch ($flag) {
	
	case 'updateRatingComments' :
		$call_details_id = NULL;
		$doctor_rated = NULL;
		$doctor_commets = NULL;
		$update_call_details = NULL;
		$json ['status'] = 'false';
		$json ['msg'] = 'Failed to rate a doctor.Try again!!!';
		
		$patient_rated = mysql_real_escape_string ( $_POST ['patient_rated'] );
		$patient_comments = mysql_real_escape_string ( $_POST ['patient_comments'] );
		$call_details_id = mysql_real_escape_string ( $_POST ['call_details_id'] );
		if ($call_details_id) {
			$query = "update #_call_details  set  patient_rated ='" . $patient_rated . "', patient_rated_comments='" . $patient_comments . "' Where pid='" . $call_details_id . "'";
			
			$update_call_details = $PDO->db_query ( $query );
			if ($update_call_details) {
				$json ['status'] = 'true';
				$json ['msg'] = 'Success!!! Rated a Doctor with comments';
			}
		}
		break;
	

		
	case 'set_isChatNeeded' :
		
		$patient_id = mysql_real_escape_string ( $_POST ['patient_id'] );
		$isChatNeeded = mysql_real_escape_string ( $_POST ['isChatNeeded'] ); // 0 means busy 1 means free
		
		$set_isChatNeeded_status = $patient_function->set_isChatNeeded ( $patient_id, $isChatNeeded );
		
		if ($set_isChatNeeded_status == 'false') {
			
			$json ['status'] = 'false';
			$json ['msg'] = 'You cannot able to change the status';
		} else {
			$json ['status'] = 'true';
			$json ['msg'] = 'Your status is changed successfully';
		}
		
		break;

	case 'patient_profile_details' :
		
		$json ['patient_details'] = array ();
		$patient_id = mysql_real_escape_string ( $_POST ['patient_id'] );
		$patient_details = $patient_function->patient_details ( $patient_id );
		
		if ($patient_details ['pid'] == null) {
			$json ['status'] = 'false';
			$json ['msg'] = 'This user details is not available.';
		} else {
			$json ['status'] = 'true';
			$json ['patient_details'] = $patient_details;
		}
		
		break;
	
	case 'patient_profile_edit' :
		
		$patient_details = array ();
		$patient_id = mysql_real_escape_string ( $_POST ['patient_id'] );
		$name = mysql_real_escape_string ( $_POST ['name'] );
		$email = mysql_real_escape_string ( $_POST ['email'] );
		$phone = mysql_real_escape_string ( $_POST ['phone'] );
		$gender = mysql_real_escape_string ( $_POST ['gender'] );
		$marital_status = mysql_real_escape_string ( $_POST ['marital_status'] );
		$date_of_birth = mysql_real_escape_string ( $_POST ['date_of_birth'] );
		$date_of_birth = date ( $date_of_birth );
		$location = mysql_real_escape_string ( $_POST ['location'] );
		$nrc_passport_number = mysql_real_escape_string ( $_POST ['nrc_passport_number'] );
		$occupation = mysql_real_escape_string ( $_POST ['occupation'] );
		$blood_group = mysql_real_escape_string ( $_POST ['blood_group'] );
		$height = mysql_real_escape_string ( $_POST ['height'] );
		$weight = mysql_real_escape_string ( $_POST ['weight'] );
		$emergency_contact_number = mysql_real_escape_string ( $_POST ['emergency_contact_number'] );
		if (isset ( $_POST ['password'] )) {
			$password = $_POST ['password'];
			$password = md5 ( $password );
			$password = base64_encode ( $password );
		}
		// Profile Pic Upload
		
		$uploaded_profile_name = '';
		if (isset ( $_FILES ['profile_picture'] ['name'] )) {
			$filename = $_FILES ['profile_picture'] ['name'];
			$uploaded_profile_details = $doctor_function->upload_images ( 'profile_picture', UP_FILES_FS_PATH . '/profile_pictures/' );
			if ($uploaded_profile_details ['status'] != 'true') {
				$json ['status'] = 'false';
				$json ['msg'] = 'Profile picture ' . $uploaded_profile_details ['msg'];
				$doctor_function->send_res ( $json );
				exit ();
			} else  {
				$uploaded_profile_name = $uploaded_profile_details ['doctor_image_name'];
				
				if ((! empty ( $uploaded_profile_name ))) {
					// update the pic names in to doctor table
					$patient_details ['profile_pic'] = $uploaded_profile_name;
				} else {
					$patient_details ['profile_pic'] = $uploaded_profile_name;
					$json ['status'] = 'false';
					$json ['msg'] = 'Sorry. There was an occurred while uploading the files';
					$doctor_function->send_res ( $json );
					exit ();
				}
			}
		} else {
			$patient_details ['profile_pic'] = $PDO->getSingleResult ( "select profile_pic from #_patients where pid='" . $patient_id . "'" );
		}
		
		$patient_details ['name'] = $name;
		$patient_details ['email'] = $email;
		$patient_details ['phone'] = $phone;
		$patient_details ['sex'] = $gender;
		$patient_details ['marital_status'] = $marital_status;
		$patient_details ['date_of_birth'] = $date_of_birth;
		$patient_details ['address'] = $location;
		$patient_details ['nrc_passport_number'] = $nrc_passport_number;
		$patient_details ['occupation'] = $occupation;
		$patient_details ['blood_group'] = $blood_group;
		$patient_details ['height'] = $height;
		$patient_details ['weight'] = $weight;
		$patient_details ['alternate_phone'] = $emergency_contact_number;
		$patient_details ['status'] = 1;
		if (isset ( $_POST ['password'] )) {
			$patient_details ['password'] = $password;
		}
		
		$update = $patient_function->profile_update ( $patient_details, $patient_id );
		if (! empty ( $update )) {
			$json ['status'] = 'true';
			$json ['alternate_phone'] = $emergency_contact_number;
			$json ['msg'] = 'Your Details Updated successfully.';
		} else {
			$json ['status'] = 'false';
			$json ['msg'] = 'Sorry.There is an error while updating your status.';
		}
		
		break;
	
	case 'updateRatingComments' :
		$call_details_id = NULL;
		$doctor_rated = NULL;
		$doctor_commets = NULL;
		
		$json ['status'] = 0;
		
		$patient_rated = mysql_real_escape_string ( $_POST [' patient_rated'] );
		$patient_commets = mysql_real_escape_string ( $_POST [' patient_commets'] );
		$call_details_id = mysql_real_escape_string ( $_POST ['call_details_id'] );
		
		if ($call_details_id) {
			$query = "update #_call_details  set  patient_rated ='" . $patient_rated . "', patient_rated_comments='" . $patient_commets . "' Where pid='" . $call_details_id . "'";
			
			$update_call_details = $PDO->db_query ( $query );
			if ($update_call_details) {
				$json ['status'] = 1;
			}
		}
		break;
	

	/*
	 * case 'patientlogin':
	 * $mobile_number = mysql_real_escape_string($_POST['mobile_number']);
	 * $otpcode = mysql_real_escape_string($_POST['otpcode']);
	 *
	 *
	 * $patients_query = $PDO->db_query("select * from #_patients where phone='" . $mobile_number . "' and otpcode ='" . $otpcode . "' and status=1 order by name");
	 * $i = 0;
	 * $patient = array();
	 * if (mysql_num_rows($patients_query) > 0) {
	 *
	 * while ($patients_data = $PDO->db_fetch_array($patients_query)) {
	 * $patient[$i]['pid'] = $patients_data['pid'];
	 * $patient[$i]['user_id'] = $patients_data['user_id'];
	 * $patient[$i]['patient_id'] = $patients_data['patient_id'];
	 * $patient[$i]['name'] = $patients_data['name'];
	 * $patient[$i]['email'] = $patients_data['email'];
	 * $patient[$i]['phone'] = $patients_data['phone'];
	 * $patient[$i]['alternate_phone'] = $patients_data['alternate_phone'];
	 * $patient[$i]['sex'] = $patients_data['sex'];
	 * $patient[$i]['age'] = $patients_data['age'];
	 * $patient[$i]['address'] = $patients_data['address'];
	 * $patient[$i]['township'] = $PDO->getSingleResult("select name from #_township where pid='" . $patients_data['township'] . "'");
	 * $patient[$i]['division'] = $PDO->getSingleResult("select name from #_division where pid='" . $patients_data['division'] . "'");
	 * $i++;
	 *
	 * }
	 *
	 *
	 * $query = $PDO->db_query("select * from #_health_tips where status=1 order by shortorder desc limit 0,5 ");
	 * $json['healthtips'] = array();
	 * if (mysql_num_rows($query) > 0) {
	 * $i = 0;
	 * $data = array();
	 * while ($row = $PDO->db_fetch_array($query)) {
	 * $data[$i]['pid'] = $row['pid'];
	 * $data[$i]['name'] = $row['name'];
	 * $i++;
	 * }
	 * $json['healthtips'] = $data;
	 *
	 * }
	 *
	 *
	 * $json['patient'] = $patient;
	 * $json['status'] = 'true';
	 * } else {
	 * $json['patient'] = $patient;
	 * $json['status'] = 'false';
	 * }
	 *
	 * break;
	 */
	
	case 'patientlist' :
		$mobile_number = mysql_real_escape_string ( $_POST ['mobile_number'] );
		$gsm_tocken = mysql_real_escape_string ( $_POST ['gsm_tocken'] );
		
		$gsm_user = $PDO->db_query ( "select * from #_gcmuser  where  user_id ='" . $mobile_number . "' and gsm_tocken ='" . $gsm_tocken . "' and user_type ='patient'" );
		
		if (mysql_num_rows ( $gsm_user ) == 0) {
			$gsm_user = $PDO->db_query ( "insert into #_gcmuser  set  user_id ='" . $mobile_number . "', gsm_tocken ='" . $gsm_tocken . "' ,user_type ='patient',flag = '1',created_on='" . date ( 'Y-m-d H:i:s' ) . "'" );
		} else {
			$gsm_user = $PDO->db_query ( "Update #_gcmuser  set  flag ='1', modified_on='" . date ( 'Y-m-d H:i:s' ) . "' where user_id ='" . $mobile_number . "' and gsm_tocken ='" . $gsm_tocken . "' and user_type ='patient' " );
		}
		
		$patients_query = $PDO->db_query ( "select * from #_patients  where  phone='" . $mobile_number . "' and status=1 order by name" );
		$i = 0;
		$patient = array ();
		if (mysql_num_rows ( $patients_query ) > 0) {
			
			while ( $patients_data = $PDO->db_fetch_array ( $patients_query ) ) {
				$patient [$i] ['pid'] = $patients_data ['pid'];
				$patient [$i] ['user_id'] = $patients_data ['user_id'];
				$patient [$i] ['patient_id'] = $patients_data ['patient_id'];
				$patient [$i] ['name'] = $patients_data ['name'];
				$patient [$i] ['email'] = $patients_data ['email'];
				$patient [$i] ['phone'] = $patients_data ['phone'];
				$patient [$i] ['alternate_phone'] = $patients_data ['alternate_phone'];
				$patient [$i] ['sex'] = $patients_data ['sex'];
				$patient [$i] ['age'] = $patients_data ['age'];
				$patient [$i] ['address'] = $patients_data ['address'];
				$patient [$i] ['township'] = $PDO->getSingleResult ( "select name from #_township where pid='" . $patients_data ['township'] . "'" );
				$patient [$i] ['division'] = $PDO->getSingleResult ( "select name from #_division where pid='" . $patients_data ['division'] . "'" );
				$i ++;
			}
			
			$query = $PDO->db_query ( "select * from #_health_tips  where status=1 order by shortorder desc limit 0,5 " );
			$json ['healthtips'] = array ();
			if (mysql_num_rows ( $query ) > 0) {
				$i = 0;
				$data = array ();
				while ( $row = $PDO->db_fetch_array ( $query ) ) {
					$data [$i] ['pid'] = $row ['pid'];
					$data [$i] ['name'] = $row ['name'];
					$i ++;
				}
				$json ['healthtips'] = $data;
			}
			
			$json ['patient'] = $patient;
			$json ['status'] = 'true';
		} else {
			$json ['patient'] = $patient;
			$json ['status'] = 'false';
		}
		
		break;
	
	case 'patientdetail' :
		
		$patient_id = mysql_real_escape_string ( $_POST ['patient_id'] );
		/*
		 * $doc_string = substr($_POST['patient_id'], 0, 4);
		 *
		 * if($doc_string=='DOC1'){
		 * $patients_query = $PDO->db_query("select * from #_patients where patient_id='".$patient_id."' and status=1");
		 * }else{
		 * $patients_query = $PDO->db_query("select * from #_patients where pid='".$patient_id."' and status=1");
		 * }
		 */
		$patients_query = $PDO->db_query ( "select * from #_patients  where  patient_id='" . $patient_id . "' and status=1" );
		
		$i = 0;
		$patient = array ();
		$medicine = array ();
		$subscription = array ();
		$call_detail = array ();
		$patient_medicine = array ();
		$service = array ();
		
		if (mysql_num_rows ( $patients_query ) > 0) {
			
			while ( $patients_data = $PDO->db_fetch_array ( $patients_query ) ) {
				$patient ['pid'] = $patients_data ['pid'];
				$patient ['user_id'] = $patients_data ['user_id'];
				$patient ['patient_id'] = $patients_data ['patient_id'];
				$patient ['name'] = $patients_data ['name'];
				$patient ['email'] = $patients_data ['email'];
				$patient ['phone'] = $patients_data ['phone'];
				$patient ['alternate_phone'] = $patients_data ['alternate_phone'];
				$patient ['sex'] = $patients_data ['sex'];
				$patient ['age'] = $patients_data ['age'];
				$patient ['address'] = $patients_data ['address'];
				$patient ['township_id'] = $patients_data ['township'];
				// $patient['created_on']=$patients_data['created_on'];
				$patient ['division_id'] = $patients_data ['division'];
				$patient ['path'] = SITE_PATH . "uploaded_files/profile_pictures/" . $patients_data ['profile_pic'];
				$patient ['township'] = $PDO->getSingleResult ( "select name from #_township where pid='" . $patients_data ['township'] . "'" );
				$patient ['division'] = $PDO->getSingleResult ( "select name from #_division where pid='" . $patients_data ['division'] . "'" );
				$i ++;
			}
			
			$medsels = $PDO->db_query ( "select * from inv_sale_subscription where patcient_id='" . $patient ['pid'] . "' order by created_on desc " );
			
			$i = 0;
			while ( $medselsline = $PDO->db_fetch_array ( $medsels ) ) {
				$plan_query = $PDO->db_query ( "select * from inv_subscription_plan where id='" . $medselsline ['subscription_id'] . "' " );
				$plan_data = $PDO->db_fetch_array ( $plan_query );
				$subscription [$i] ['name'] = $plan_data ['name'];
				$subscription [$i] ['validity_duration'] = $plan_data ['validity_duration'];
				$subscription [$i] ['validity_type'] = $plan_data ['validity_type'];
				$subscription [$i] ['description'] = $plan_data ['description'];
				$subscription [$i] ['amount_taken'] = $medselsline ['amount_taken'];
				$subscription [$i] ['created_on'] = $medselsline ['created_on'];
				$i ++;
			}
			
			$medsels = $PDO->db_query ( "select * from inv_sale_service where patcient_id='" . $patient ['pid'] . "' order by created_on desc " );
			
			$i = 0;
			while ( $medselsline = $PDO->db_fetch_array ( $medsels ) ) {
				$plan_query = $PDO->db_query ( "select * from inv_services where id='" . $medselsline ['service_id'] . "' " );
				$plan_data = $PDO->db_fetch_array ( $plan_query );
				$service [$i] ['name'] = $plan_data ['name'];
				$service [$i] ['validity_duration'] = $plan_data ['validity_duration'];
				$service [$i] ['validity_type'] = $plan_data ['validity_type'];
				$service [$i] ['description'] = $plan_data ['description'];
				$service [$i] ['amount_taken'] = $medselsline ['amount_taken'];
				$service [$i] ['created_on'] = $medselsline ['created_on'];
				$i ++;
			}
			
			$medsels_query = $PDO->db_query ( "select * from inv_sales where patient_id='" . $patient ['pid'] . "' order by created_on desc " );
			$i = 0;
			while ( $medsels_data = $PDO->db_fetch_array ( $medsels_query ) ) {
				$medicine [$i] ['created_on'] = $medsels_data ['created_on'];
				$medicine [$i] ['category'] = $PDO->getSingleResult ( "select name from inv_category where id='" . $medsels_data ['category_id'] . "' " );
				$medicine [$i] ['brand'] = $PDO->getSingleResult ( "select name from inv_brand where id ='" . $medsels_data ['brand_id'] . "' " );
				$medicine [$i] ['generic'] = $PDO->getSingleResult ( "select generic_name from inv_generic_name where id='" . $medsels_data ['generic_id'] . "' " );
				$medicine [$i] ['medicine'] = $PDO->getSingleResult ( "select form_of_medicine_name from inv_form_of_medicine where id='" . $medsels_data ['formof_id'] . "' " );
				$medicine [$i] ['strength'] = $PDO->getSingleResult ( "select strength_of_medicine_name from inv_strength_of_medicine where id='" . $medsels_data ['strength_id'] . "' " );
				$medicine [$i] ['usage'] = $PDO->getSingleResult ( "select usage_category_name from inv_usage_category where id='" . $medsels_data ['usage_id'] . "' " );
				$medicine [$i] ['quantity'] = $medsels_data ['qty'];
				$medicine [$i] ['price'] = $medsels_data ['sale_price'];
			}
			
			$result = $PDO->db_query ( "select * from #_call_details where patient_id='" . $patient ['pid'] . "'  order by pid desc" );
			$i = 0;
			while ( $line = $PDO->db_fetch_array ( $result ) ) {
				$call_detail [$i] ['created_on'] = $line ['created_on'];
				$call_detail [$i] ['call_type'] = $line ['call_type'];
				$call_detail [$i] ['service_type'] = $line ['service_type'];
				$call_detail [$i] ['chief_complaint'] = $line ['chief_complaint'];
				$call_detail [$i] ['history_of_present_illness'] = $line ['history_of_present_illness'];
				$call_detail [$i] ['past_medical_history'] = $line ['past_medical_history'];
				$call_detail [$i] ['past_surgical_history'] = $line ['past_surgical_history'];
				$call_detail [$i] ['social_history'] = $line ['social_history'];
				$call_detail [$i] ['family_history'] = $line ['family_history'];
				$call_detail [$i] ['history_of_drug_allergy'] = $line ['history_of_drug_allergy'];
				$call_detail [$i] ['drug_allergy_comment'] = $line ['drug_allergy_comment'];
				$call_detail [$i] ['bp'] = $line ['bp'];
				$call_detail [$i] ['pr'] = $line ['pr'];
				$call_detail [$i] ['sao'] = $line ['sao'];
				$call_detail [$i] ['weight'] = $line ['weight'];
				$call_detail [$i] ['rbs'] = $line ['rbs'];
				$call_detail [$i] ['temp'] = $line ['temp'];
				$call_detail [$i] ['provisional_diagnosis'] = $line ['provisional_diagnosis'];
				$call_detail [$i] ['follow_up_call_schedule'] = $line ['follow_up_call_schedule'];
				$call_detail [$i] ['follow_up_call_date'] = $line ['follow_up_call_date'];
				
				$reports = array ();
				$report_query = $PDO->db_query ( "select * from #_reports Where call_details_id ='" . $line ['pid'] . "'" );
				while ( $report_rows = $PDO->db_fetch_array ( $report_query ) ) {
					$reports [] = SITE_PATH . '/modules/reports/ajax.php?file=' . $report_rows ['reports'];
				}
				$call_detail [$i] ['reports'] = $reports;
				
				/* VOIP CALL */
				
				$voipcalldetails = array ();
				$voip_query = $PDO->db_query ( "select * from #_voip_call_details Where patient_id='" . $patient_id . "' and created_on='" . $line ['created_on'] . "'" );
				// echo "select * from #_voip_call_details Where patient_id='".$patient_id."' and created_on='".$line['created_on']."'";
				$d = 0;
				
				while ( $call_details = $PDO->db_fetch_array ( $voip_query ) ) {
					$voipcalldetails [$d] ['call_id'] = $call_details ['pid'];
					$check1 = $PDO->db_query ( "select * from #_patients where patient_id='" . $call_details ['patient_id'] . "'" );
					$patient_details = $PDO->db_fetch_array ( $check1 );
					
					$check2 = $PDO->db_query ( "select * from #_doctors where user_id='" . $call_details ['doc_id'] . "'" );
					$doc_details = $PDO->db_fetch_array ( $check2 );
					
					$voipcalldetails [$d] ['patient_name'] = $patient_details ['name'];
					$voipcalldetails [$d] ['doctor_name'] = $doc_details ['name'];
					$voipcalldetails [$d] ['call_duration'] = $call_details ['call_duration'];
					$voipcalldetails [$d] ['comments'] = $call_details ['comments'];
					$voipcalldetails [$d] ['created_on'] = $call_details ['created_on'];
					
					$d ++;
				}
				$call_detail [$i] ['voipcalldetails'] = $voipcalldetails;
				/* END */
				$medicalreference = array ();
				$medicalreference_query = $PDO->db_query ( "select * from #_medicalreference where patient_id ='" . $patient ['pid'] . "' and status='1' " );
				
				$m = 0;
				while ( $medicalreference_row = $PDO->db_fetch_array ( $medicalreference_query ) ) {
					$medicalreference [$m] ['title'] = $medicalreference_row ['title'];
					$medicalreference [$m] ['file_name'] = SITE_PATH . '/modules/reports/ajax.php?file=' . $file ['file_name'];
				}
				
				$call_detail [$i] ['medicalreference'] = $medicalreference;
				
				$call_detail [$i] ['services_comment'] = $line ['services_comment'];
				$call_detail [$i] ['health_consultation'] = $line ['health_consultation'];
				
				$medicine = array ();
				if ($line ['service_type'] == 'Medicine') {
					$medicine_id_arr = json_decode ( $line ['medicine_id'] );
					
					if (! empty ( $medicine_id_arr )) {
						$medicine_id = implode ( ',', $medicine_id_arr );
						$medqry = $PDO->db_query ( "select * from inv_sales where id in (" . $medicine_id . ")" );
						$m = 0;
						while ( $medata = $PDO->db_fetch_array ( $medqry ) ) {
							$medicine [$m] ['id'] = $medata ['id'];
							$medicine [$m] ['category'] = $PDO->getSingleResult ( "select name from inv_category where id='" . $medata ['category_id'] . "' " );
							$medicine [$m] ['brand'] = $PDO->getSingleResult ( "select name from inv_brand where id ='" . $medata ['brand_id'] . "' " );
							$medicine [$m] ['generic_name'] = $PDO->getSingleResult ( "select generic_name from inv_generic_name where id='" . $medata ['generic_id'] . "' " );
							$medicine [$m] ['form_of_medicine_name'] = $PDO->getSingleResult ( "select form_of_medicine_name from inv_form_of_medicine where id='" . $medata ['formof_id'] . "' " );
							$medicine [$m] ['strength_of_medicine_name'] = $PDO->getSingleResult ( "select strength_of_medicine_name from inv_strength_of_medicine where id='" . $medata ['strength_id'] . "' " );
							$medicine [$m] ['strength_of_medicine_name'] = $PDO->getSingleResult ( "select strength_of_medicine_name from inv_strength_of_medicine where id='" . $medata ['strength_id'] . "' " );
							$medicine [$m] ['usage_category_name'] = $PDO->getSingleResult ( "select usage_category_name from inv_usage_category where id='" . $medata ['usage_id'] . "' " );
							
							$medicine [$m] ['qty'] = $medata ['qty'];
							$medicine [$m] ['sale_price'] = $medata ['sale_price'];
							$m ++;
						}
					}
				}
				$call_detail [$i] ['medicine'] = $medicine;
				
				$appointment = array ();
				$report_query = $PDO->db_query ( "select AP.doctor_id,AP.call_details_id As 'callDetailsId', AP.status, AP.comments, AP.app_date,AP.app_time,D.hospital_id, D.clinic_id,D.name,D.pid,D.email,D.phone,D.sex,D.date_of_birth,D.doctor_type,D.rs_number,D.fees,D.education,D.location,D.area_code,D.address,D.dates,D.township,D.division from #_appointment as AP left join #_doctors as D ON D.pid =AP.doctor_id  Where AP.call_details_id ='" . $line ['pid'] . "'" );
				$p = 0;
				
				while ( $report_rows = $PDO->db_fetch_array ( $report_query ) ) {
					$appointment [$p] ['doctor_id'] = $report_rows ['doctor_id'];
					$appointment [$p] ['user_id'] = $report_rows ['pid'];
					$appointment [$p] ['name'] = $report_rows ['name'];
					$appointment [$p] ['email'] = $report_rows ['email'];
					$appointment [$p] ['phone'] = $report_rows ['phone'];
					$appointment [$p] ['sex'] = $report_rows ['sex'];
					$appointment [$p] ['date_of_birth'] = $report_rows ['date_of_birth'];
					$appointment [$p] ['doctor_type'] = $report_rows ['doctor_type'];
					$appointment [$p] ['rs_number'] = $report_rows ['rs_number'];
					$appointment [$p] ['fees'] = $report_rows ['fees'];
					$appointment [$p] ['education'] = $report_rows ['education'];
					$appointment [$p] ['location'] = $report_rows ['location'];
					$appointment [$p] ['area_code'] = $report_rows ['area_code'];
					$appointment [$p] ['address'] = $report_rows ['address'];
					$appointment [$p] ['dates'] = $report_rows ['dates'];
					$appointment [$p] ['township'] = $PDO->getSingleResult ( "select name from #_township where pid='" . $report_rows ['township'] . "'" );
					$appointment [$p] ['division'] = $PDO->getSingleResult ( "select name from #_division where pid='" . $report_rows ['division'] . "'" );
					$clinic = $PDO->getSingleResult ( "select name from #_clinics where pid='" . $report_rows ['clinic_id'] . "'" );
					$appointment [$p] ['clinic'] = ($clinic != '') ? $clinic : '';
					$hospital = $PDO->getSingleResult ( "select name from #_hospitals where pid='" . $report_rows ['hospital_id'] . "'" );
					$appointment [$p] ['hospital'] = ($hospital != '') ? $hospital : '';
					$appointment [$p] ['name'] = ($report_rows ['name'] != '') ? $report_rows ['name'] : '';
					$appointment [$p] ['app_date'] = $report_rows ['app_date'];
					$appointment [$p] ['app_time'] = $report_rows ['app_time'];
					$appointment [$p] ['comments'] = ($report_rows ['comments'] != '') ? $report_rows ['comments'] : '';
					$appointment [$p] ['status'] = $report_rows ['status'];
					$appointment [$p] ['call_details_id'] = $report_rows ['callDetailsId'];
					$p ++;
				}
				if (count ( $appointment ) > 0) {
					$call_detail [$i] ['appointment'] = $appointment;
				}
				
				$booking_query = $PDO->db_query ( "select * from #_booking Where  call_id='" . $line ['pid'] . "'  and service_type='Lab' order by pid desc" );
				$booking = array ();
				$b = 0;
				
				while ( $line2 = $PDO->db_fetch_array ( $booking_query ) ) {
					$booking [$b] ['pid'] = $line2 ['pid'];
					$booking [$b] ['book_type'] = $line2 ['book_type'];
					$booking [$b] ['service_type'] = $line2 ['service_type'];
					$booking [$b] ['book_id'] = $line2 ['book_id'];
					$booking [$b] ['call_id'] = $line2 ['call_id'];
					$booking [$b] ['patient_id'] = $line2 ['patient_id'];
					$booking [$b] ['book_date'] = $line2 ['book_date'];
					$booking [$b] ['book_time'] = $line2 ['book_time'];
					$booking [$b] ['booking_comments'] = $line2 ['booking_comments'];
					$booking [$b] ['home_visit_service'] = $line2 ['home_visit_service'];
					$lab_test1 = json_decode ( $line2 ['lab_test'] );
					
					foreach ( $lab_test1 as $lab_id ) {
						$servicequery = $PDO->db_query ( "select name from #_service_subcat Where pid='" . $lab_id . "' order by pid desc" );
						if (mysql_num_rows ( $servicequery ) > 0) {
							while ( $servr = $PDO->db_fetch_array ( $servicequery ) ) {
								$wh .= $servr ['name'] . ',';
							}
							$booking [$b] ['labtest_new'] = trim ( $wh, ',' );
							$booking [$b] ['lab_test'] = $line2 ['lab_test'];
						} else {
							$booking [$b] ['labtest_new'] = "";
							$booking [$b] ['lab_test'] = "";
						}
					}
					
					$booking [$b] ['booking_complete_comments'] = $line2 ['booking_complete_comments'];
					$booking [$b] ['booking_complete_flag'] = $line2 ['booking_complete_flag'];
					
					if (strtolower ( $line2 ['book_type'] ) == "lab") {
						$book_name = $PDO->getSingleResult ( "select name from #_labs where user_id='" . $line2 ['book_id'] . "'" );
					} else if (strtolower ( $line2 ['book_type'] ) == "hospital") {
						$book_name = $PDO->getSingleResult ( "select name from #_hospitals where user_id='" . $line2 ['book_id'] . "'" );
					} else if (strtolower ( $line2 ['book_type'] ) == "clinics") {
						
						$book_name = $PDO->getSingleResult ( "select name from #_clinics where user_id='" . $line2 ['book_id'] . "'" );
					} else if (strtolower ( $line2 ['book_type'] ) == "healthcare") {
						$book_name = $PDO->getSingleResult ( "select name from #_healthcare_organization where user_id='" . $line2 ['book_id'] . "'" );
					}
					
					$booking [$b] ['book_name'] = $line2 ['book_name'];
					$b ++;
				}
				
				$call_detail [$i] ['booking'] = $booking;
				
				$i ++;
			}
			
			$json ['patient'] = $patient;
			$json ['subscription'] = $subscription;
			$json ['service'] = $service;
			$json ['medicine'] = $medicine;
			$json ['call_detail'] = $call_detail;
			
			$json ['patient_medicine'] = $patient_medicine;
			$json ['status'] = 'true';
		} else {
			
			$json ['patient'] = $patient;
			$json ['subscription'] = $subscription;
			$json ['service'] = $service;
			$json ['medicine'] = $medicine;
			
			$json ['call_detail'] = $call_detail;
			
			$json ['patient_medicine'] = $patient_medicine;
			$json ['status'] = 'false';
		}
		
		break;
	


	case 'patient_phone_check':
		$mobile_number = mysql_real_escape_string ( $_POST ['mobile_number'] );
		$table_name = 'patients';
		$phone_check = $patient_function->phone_check ( $table_name, $mobile_number );
		if ($phone_check == 'false') {
			echo 'false'; //phone number exist
		} else {
			echo 'true';
		}
		exit();
		
	break;

	case 'testGooglePush' :
		$gsm_token =  mysql_real_escape_string ( $_POST ['gsm_token'] );
		$doctor_id =  mysql_real_escape_string ( $_POST ['doctor_id'] );
		$patient_id =  mysql_real_escape_string ( $_POST ['patient_id'] );
        $is_ios_device =  mysql_real_escape_string ( $_POST ['is_ios_device'] );
        $from =  mysql_real_escape_string ( $_POST ['from'] );
        $to =  mysql_real_escape_string ( $_POST ['to'] );
        $message = array(
            "message" => "Test Notification",
            "flag" => 'doctor'
        );
        $is_ios_device = $PDO->getSingleResult("select is_ios_device from #_patients where pid=" . $patient_id);
        $result = $gcm->send_notification($gsm_token, $message, "Test", $doctor_id, $from, $patient_id, $to, $is_ios_device);
        $json ['status'] = $result;
		break;

	case 'getCoreConfig':
				$appointment_version=PATIENT_APP_VER;
				$json ['status'] = 'true';
				$json ['app_version'] = $appointment_version;
        $json ['cancel_reason']=$doctor_function->getAppointmentCancelReasonList();
				break;

	default :
		break;
}
/* Output header */
// header('Content-type: application/json');
// echo json_encode($_POST);
echo json_encode ( $json );
?>