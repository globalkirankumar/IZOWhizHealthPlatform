<?php
include("../lib/open.inc.php");
$flag = mysql_real_escape_string($_POST ['flag']);
$json = array();
include 'class/all_class_files.php';
$doctor_function = new doctor ();
$patient_function = new patient ();
$m = new MyMail ();
include_once 'notification/GCM.php';
$gcm = new GCM ();
date_default_timezone_set('Asia/Kolkata');

switch ($flag) {

    case 'appointment' :
        $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        $patient_id = $PDO->getSingleResult("select pid from #_patients where patient_id='" . $patient_id . "'");

        $item = mysql_real_escape_string($_POST ['item']);
        $page = mysql_real_escape_string($_POST ['page']);
        if ($page > 0 || $item > 0) {
            $item = ($item > 0) ? $item : 10;
            $page = ($page > 0) ? $page : 1;
            $start = ($page == 1) ? 0 : ($item * $page - ($item + 1));
            $wh .= " limit $start,$item";
        }

        $report_query = $PDO->db_query("select *  from #_appointment where patient_id ='" . $patient_id . "' order by app_date desc" . $wh);
        $json ['appointment'] = array();
        if (mysql_num_rows($report_query) > 0) {
            $p = 0;
            while ($report_rows = $PDO->db_fetch_array($report_query)) {

                $doctor_query = $PDO->db_query("select * from #_doctors where pid='" . $report_rows ['doctor_id'] . "'");
                $doctor_row = $PDO->db_fetch_array($doctor_query);

                $speciality = $PDO->getSingleResult("select name from #_specialities where pid='" . $doctor_row ['speciality'] . "'");
                $clinic = $PDO->getSingleResult("select name from #_clinics where pid='" . $doctor_row ['clinic_id'] . "'");
                $hospital = $PDO->getSingleResult("select name from #_hospitals where pid='" . $doctor_row ['hospital_id'] . "'");

                $appointment [$p] ['pid'] = $report_rows ['pid'];
                $appointment [$p] ['doctor_id'] = $report_rows ['doctor_id'];
                $appointment [$p] ['doctor_name'] = $doctor_row ['name'];
                $appointment [$p] ['doctor_speciality'] = $speciality;
                $appointment [$p] ['doctor_sex'] = $doctor_row ['sex'];
                $appointment [$p] ['doctor_address'] = $doctor_row ['address'];
                $appointment [$p] ['clinic_id'] = $doctor_row ['clinic_id'];
                $appointment [$p] ['hospital_id'] = $doctor_row ['hospital_id'];
                $appointment [$p] ['app_date'] = $report_rows ['app_date'];
                $appointment [$p] ['app_time'] = $report_rows ['app_time'];
                $appointment [$p] ['comments'] = ($report_rows ['comments'] != '') ? $report_rows ['comments'] : '';
                $appointment [$p] ['clinic'] = ($clinic != '') ? $clinic : '';
                $appointment [$p] ['hospital'] = ($hospital != '') ? $hospital : '';
                $p++;
            }
            $json ['appointment'] = $appointment;
            $json ['status'] = 'true';
        } else {

            $json ['status'] = 'false';
        }

        break;
    case 'appointmentsCategoryLists' :
        $appointment_category_lists = array();
        $is_today = 0;
        $start_offset = 0;
        $end_limit = 20;

        $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        $category = mysql_real_escape_string($_POST ['category']);
        if (array_key_exists('start_offset', $_POST) && $_POST ['start_offset']) {
            $start_offset = mysql_real_escape_string($_POST ['start_offset']);
            $end_limit = $start_offset + PAGINATION_END_LIMIT;
        }
        if (array_key_exists('is_today', $_POST) && $_POST ['is_today']) {
            $is_today = mysql_real_escape_string($_POST ['is_today']);
        }

        $json ['appointment_category_list'] = $appointment_category_lists;
        $json ['status'] = 'true';
        $json ['msg'] = 'Appointment list not found';

        $appointment_category_lists = $doctor_function->getPatientAppointmentList($patient_id, $category, $start_offset, $end_limit, $is_today);

        if ($appointment_category_lists) {
            $json ['appointment_category_list'] = $appointment_category_lists;
            $json ['status'] = 'true';
            $json ['msg'] = 'Success!! Appointment list';
        }

        break;
    case 'uploadPatientRecord' :

        // $report_file = mysql_real_escape_string($_POST ['report_file']);

        $report_title = mysql_real_escape_string($_POST ['report_title']);

        if (array_key_exists('call_details_id', $_POST)) {
            $call_details_id = mysql_real_escape_string($_POST ['call_details_id']);
        }
        if (array_key_exists('file_ext', $_POST)) {
            $file_ext = mysql_real_escape_string($_POST ['file_ext']);
        }

        if (array_key_exists('patient_id', $_POST)) {
            $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        }

        /*
         * $query="select dh*,ccd.* from #_doctor_history as dh join #_crm_call_details as ccd on dh.caller_id=ccd.call_detal_id
         * where dh.doctor_id=".$doctor_id." AND dh.call_detal_id ='".$call_details_id."' order by ccd.pid desc limit 0,1";
         * $doctory_history_details = $PDO->db_query($query);
         */
        $json ['status'] = 'false';
        $json ['msg'] = '';
        // $path = $_SERVER['SERVER_NAME']."/crm/uploaded_files/reports/";
        $path = UP_FILES_FS_PATH . "/reports/";

        $report_file_name = '';
        if ($_FILES ['report_file'] ['name']) {

            $report_file_name = $BSC->uploadFile($path, $_FILES ['report_file'] ['name'], 'report_file');
        }

        if ($report_file_name != '') {
            $short_order = $PDO->db_query("SELECT shortorder FROM #_reports order by pid desc limit 0,1");
            $short_order = $PDO->db_fetch_array($short_order);

            if ($short_order['shortorder'] == '') {
                $short_order = 0;
            } else {

                $short_order = $short_order['shortorder'] + 1;
            }
            $query = "insert into #_reports  set  title='" . $report_title . "',status=1,shortorder=" . $short_order . ", call_details_id='" . $call_details_id . "',reports='" . $report_file_name . "', create_by ='" . $patient_id . "',user_type='Patient',created_on='" . date('Y-m-d H:i:s') . "',modified_on='" . date('Y-m-d H:i:s') . "'";

            $report_details = $PDO->db_query($query);

            if ($report_details) {
                $json ['status'] = 'true';
                $json ['msg'] = 'File Upload Successfully';

            }
        }
        break;
    case 'getUploadedPatientRecords' :
        $call_details_id = NULL;
        $patient_id = NULL;
        $call_details_id = NULL;
        $reponse_data = array();
        $uploaded_patients_reports = array();
        $patient_name = '';
        $doctor_name = '';
        $start_offset = 0;
        /*
         * $doctor_id =mysql_real_escape_string($_POST['doctor_id']);
         * $patient_id =mysql_real_escape_string($_POST['patient_id']);
         * $call_details_id =mysql_real_escape_string($_POST['call_details_id']);
         */
        $json ['details'] = $reponse_data;
        $json ['status'] = 'false';

        if (array_key_exists('start_offset', $_POST)) {
            $start_offset = mysql_real_escape_string($_POST ['start_offset']);
        }

        if (array_key_exists('patient_id', $_POST)) {
            $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        }
        if (array_key_exists('call_details_id', $_POST)) {
            $call_details_id = mysql_real_escape_string($_POST ['call_details_id']);
        }

        //$last = $start_offset + PAGINATION_END_LIMIT;
        $limit = " limit " . $start_offset . "," . PAGINATION_END_LIMIT;

        // $query = "select cr.* ,ccd.patient_id as 'patient_id', dh.doctor_id as 'doctor_id' from #_reports as cr left join #_call_details as ccd on ccd.pid=cr.call_details_id left join #_doctor_history as dh on ccd.pid=dh.call_detal_id where ccd.patient_id='" . $patient_id . "' group by cr.pid " . $limit;
        if ($call_details_id && ($patient_id == '' || $patient_id === null || empty ($patient_id))) {
          /*  $query = "select cr.* ,bam.patient_id as 'patient_id', bam.doctor_id as 'doctor_id',cr.user_type from #_reports as cr left join #_book_app_mapping as bam on bam.call_details_id=cr.call_details_id where cr.call_details_id='" . $call_details_id . "' group by cr.pid desc " . $limit; 
		  */
			$query = "select cr.* ,bam.patient_id as 'patient_id', bam.doctor_id as 'doctor_id',cr.user_type from #_reports as cr left join #_book_app_mapping as bam on bam.call_details_id=cr.call_details_id where cr.call_details_id='" . $call_details_id . "' group by cr.pid desc " . $limit;
        } else if ($patient_id && ($call_details_id == '' || $call_details_id === null || empty ($call_details_id))) {
          /*  $query = "select cr.* ,bam.patient_id as 'patient_id', bam.doctor_id as 'doctor_id',bam.call_details_id as 'call_details_id' from #_book_app_mapping as bam left join #_reports as cr on bam.call_details_id=cr.call_details_id where bam.patient_id='" . $patient_id . "' group by cr.pid desc " . $limit;
		*/
		  $query = "select cr.* ,bam.patient_id as 'patient_id', bam.doctor_id as 'doctor_id',bam.call_details_id as 'call_details_id' from #_book_app_mapping as bam left join #_reports as cr on bam.call_details_id=cr.call_details_id where bam.patient_id='" . $patient_id . "' group by bam.call_details_id desc " . $limit;

	   }
        $uploaded_patients_reports = $PDO->db_query($query);
        $reponse_images_array = array();
		$responce_image_count=0;
        if (mysql_num_rows($uploaded_patients_reports) > 0) {
            $i = 0;
			$reponse_data=array();
            while ($list = $PDO->db_fetch_array($uploaded_patients_reports)) {
                if ($i <= mysql_num_rows($uploaded_patients_reports)) {
                    $patient_name = $PDO->getSingleResult("select name from #_patients where pid='" . $list ['patient_id'] . "'");
                    $doctor_name = $PDO->getSingleResult("select name from #_doctors where pid='" . $list ['doctor_id'] . "'");
                    /*
                     * $reponse_data[$i]['title'] =$list['title'];
                     * $reponse_data[$i]['reports_image']= $_SERVER['SERVER_NAME']."/crm/uploaded_files/reports/".$list['reports'];
                     * $reponse_data[$i]['call_details_id']= $list['call_details_id'];
                     * $reponse_data[$i]['report_id']= $list['pid'];
                     */
                  $query_for_report_image = "select * from #_reports where call_details_id='" . $list ['call_details_id'] . "' and status=1 and is_deleted=0 ";
                  
					$report_images = $PDO->db_query($query_for_report_image);
                    $report_details = array();
					
                    while ($list_of_reports = $PDO->db_fetch_array($report_images)) {
                        $report_details [] = array(
                            'report_id' => $list_of_reports ['pid'],
                            'title' => $list_of_reports ['title'],
                            'call_details_id' => $list_of_reports ['call_details_id'],
                            'report_date' => $list_of_reports ['created_on'],
                            'is_digital_prescription' => $list_of_reports ['is_digital_prescription'],
                            'report_image' => SITE_PATH . "uploaded_files/reports/" . $list_of_reports ['reports']

                        );
						$report_count=1;

                    }
					if(count($report_details) >0){
						 $reponse_data [] = array(
                        'reports_details' => $report_details,
                        'call_details_id' => $list ['call_details_id'],
                        'user_type' => $list ['user_type'],
                        'patient_name' => ($patient_name == NULL) ? '' : $patient_name,
                        'doctor_name' => ($doctor_name == NULL) ? '' : $doctor_name
						);
						
					}
                   
					
                }
                $i++;
            }
			
            if (!empty($reponse_data)) {
                $json ['details'] = $reponse_data;
                $json ['status'] = 'true';
            } else {
                $json ['details'] = array();
                $json ['status'] = 'false';
            }

        }

        break;
    case 'getAppointmentsDetails' :
        $call_details_id = 0;
        $appointment_details = array();
        $mediation_history = array();
        $reports_list = array();
        $appointment_id = mysql_real_escape_string($_POST ['appointment_id']);
        $json ['medicine_details'] = array();
        $json ['appointment_details'] = $appointment_details;
        $json ['mediation_history'] = $mediation_history;
        $json ['reports_list'] = $reports_list;
        $json ['status'] = 'true';
        $json ['msg'] = 'Appointment list not found';
        $appointment_details = $doctor_function->getAppointmentDetails($appointment_id);
        if ($appointment_details) {
            $json ['appointment_details'] = $appointment_details;
        }

        $call_details_id = $PDO->getSingleResult("select bam.call_details_id from #_book_app_mapping as bam left join #_appointment as app on bam.appoinment_id=app.pid where bam.appoinment_id=" . $appointment_id) . " AND app.status NOT IN(1,4)";

        if ($call_details_id) {
            $query = "select * from #_call_details  where pid ='" . $call_details_id . "'  order by pid desc limit 0,1";
            $doctory_history_details = $PDO->db_query($query);

            if (mysql_num_rows($doctory_history_details) > 0) {

                $i = 0;
                while ($row = $PDO->db_fetch_array($doctory_history_details)) {
                    if ($i < count($doctory_history_details)) {
                        $service_provided = '';
                        // print_r($patient_history->chief_complaint);exit();
                        $service_provided = ($row ['service_provided'] === NULL) ? '' : $row ['service_provided'];
                        if (!empty($service_provided)) {
                            $mediation_history [$i] ['call_detail_id'] = ($row ['pid'] === NULL) ? '' : $row ['pid'];
                            $mediation_history [$i] ['chief_complaint'] = ($row ['chief_complaint'] === NULL) ? '' : $row ['chief_complaint'];
                            $mediation_history [$i] ['history_of_present_illness'] = ($row ['history_of_present_illness'] === NULL) ? '' : $row ['history_of_present_illness'];
                            $mediation_history [$i] ['past_medical_history'] = ($row ['past_medical_history'] === NULL) ? '' : $row ['past_medical_history'];
                            $mediation_history [$i] ['past_surgical_history'] = ($row ['past_surgical_histor'] === NULL) ? '' : $row ['past_surgical_histor'];
                            $mediation_history [$i] ['social_history'] = ($row ['social_history'] === NULL) ? '' : $row ['social_history'];
                            $mediation_history [$i] ['family_history'] = ($row ['family_history'] === NULL) ? '' : $row ['family_history'];
                            $mediation_history [$i] ['occupational_history'] = ($row ['occupational_history'] === NULL) ? '' : $row ['occupational_history'];
                            $mediation_history [$i] ['history_of_drug_allergy'] = ($row ['history_of_drug_allergy'] === NULL) ? '' : $row ['history_of_drug_allergy'];
                            $mediation_history [$i] ['drug_allergy_comment'] = ($row ['drug_allergy_comment'] === NULL) ? '' : $row ['drug_allergy_comment'];
                            $mediation_history [$i] ['drug_allergy_regular_taking_medication'] = ($row ['drug_allergy_regular_taking_medication'] === NULL) ? '' : $row ['drug_allergy_regular_taking_medication'];
                            $mediation_history [$i] ['bp'] = ($row ['bp'] === NULL) ? '' : $row ['bp'];
                            $mediation_history [$i] ['pr'] = ($row ['pr'] === NULL) ? '' : $row ['pr'];
                            $mediation_history [$i] ['sao'] = ($row ['sao'] === NULL) ? '' : $row ['sao'];
                            $mediation_history [$i] ['rbs'] = ($row ['rbs'] === NULL) ? '' : $row ['rds'];
                            $mediation_history [$i] ['temp'] = ($row ['temp'] === NULL) ? '' : $row ['temp'];
                            $mediation_history [$i] ['provisional_diagnosis'] = ($row ['provisional_diagnosis'] === NULL) ? '' : $row ['provisional_diagnosis'];


                        }
                    }

                    $i++;
                }
            }
        }
        if ($mediation_history) {
            $json ['mediation_history'] = $mediation_history;
        }

        $query = "select cr.* from #_reports as cr where cr.call_details_id='" . $call_details_id . "' and is_deleted=0 group by cr.pid";
        $uploaded_patients_reports = $PDO->db_query($query);

        if (mysql_num_rows($uploaded_patients_reports) > 0) {
            $i = 0;
            while ($list = $PDO->db_fetch_array($uploaded_patients_reports)) {
                if ($i <= mysql_num_rows($uploaded_patients_reports)) {

                    $reports_list [] = array(
                        'title' => $list ['title'],
                        'reports_image' => SITE_PATH_ADM . "uploaded_files/reports/" . $list ['reports'],
                        'call_details_id' => $list ['call_details_id'],
                        'report_id' => $list ['pid'],
                        'report_date' => $list ['created_on'],
                        'is_digital_prescription' => $list ['is_digital_prescription'],
                        'uploaded_type' => $list ['user_type']
                    );
                }
                $i++;
            }
        }
        if ($reports_list) {
            $json ['reports_list'] = $reports_list;
        }

        //Medicine List provided by doc $appointment_id
        $doctor_id = $PDO->getSingleResult("select doctor_id from #_appointment where pid='" . $appointment_id . "'");
        $patient_id = $PDO->getSingleResult("select patient_id from #_appointment where pid='" . $appointment_id . "'");
        $wh = "and doctor_id='" . $doctor_id . "' and patient_id='" . $patient_id . "' and appointment_id='" . $appointment_id . "' ";
        $query = "select * from #_patient_medicine_details where is_deleted=0 AND status=1 " . $wh . " ORDER BY appointment_id desc";
        // print_r($query);exit();
        $result = $PDO->db_query($query);
        $medicine_data_list = array();
        if (mysql_num_rows($result) > 0) {
            $i = 0;
            while ($medicine_data = $PDO->db_fetch_array($result)) {
                $medicine_data_list [$i] ['patient_id'] = $medicine_data ['patient_id'];
                $medicine_data_list [$i] ['doctor_id'] = $medicine_data ['doctor_id'];
                $medicine_data_list [$i] ['appointment_id'] = $medicine_data ['appointment_id'];
                $medicine_data_list [$i] ['call_details_id'] = $medicine_data ['call_details_id'];
                $medicine_data_list [$i] ['drugs'] = stripslashes($medicine_data ['drugs']);
                $i++;
            }
        }

        $json ['medicine_details'] = $medicine_data_list;

        //Lab Test Details List
        $lab_test_data_list = array();
        $wh = "and patient_id='" . $patient_id . "'  and doctor_id='" . $doctor_id . "' ";
        $query = "select * from #_patient_lab_test_details where is_deleted=0 AND status=1 " . $wh . " ORDER BY appointment_id desc";
        // print_r($query);exit();
        $result = $PDO->db_query($query);
        $lab_test_data_list = array();
        if (mysql_num_rows($result) > 0) {
            $i = 0;
            while ($lab_test_data = $PDO->db_fetch_array($result)) {
                $lab_test_data_list [$i] ['lab_id'] = $lab_test_data ['lab_id'];
                $lab_test_data_list [$i] ['lab_test_cat_id'] = $lab_test_data ['lab_test_cat_id'];
                $lab_test_data_list [$i] ['lab_test_sub_cat_id'] = $lab_test_data ['lab_test_sub_cat_id'];
                $lab_test_data_list [$i] ['lab_name'] = $PDO->getSingleResult("select name from #_labs where pid='" . $lab_test_data ['lab_id'] . "'");
                $lab_test_data_list [$i] ['lab_test_category_name'] = $PDO->getSingleResult("select name from #_services_cat where pid='" . $lab_test_data ['lab_test_cat_id'] . "'");
                $lab_test_data_list [$i] ['lab_test_sub_category_name'] = $PDO->getSingleResult("select name from #_service_subcat where pid='" . $lab_test_data ['lab_test_sub_cat_id'] . "'");
                $lab_test_data_list [$i] ['created_on'] = $lab_test_data ['created_on'];
                $i++;
            }
        }

        $json ['lab_test_details'] = $lab_test_data_list;


        if ($appointment_details && $mediation_history && $reports_list) {
            $json ['status'] = 'true';
            $json ['msg'] = 'Success!! Appointment details';
        }
        break;
    case 'walletRefillUpdate' :
        $update_wallet_amount = NULL;
        $payment_history_updated = NULL;
        $wallet_amount = 0;

        $json ['status'] = 'true';

        $json ['msg'] = 'Success!! Wallet Refill';

        $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        $amount = mysql_real_escape_string($_POST ['amount']);

        $current_wallet_amount = $PDO->getSingleResult("select wallet_amount from #_patients where pid='" . $patient_id . "'");
        $json ['current_wallet_amount'] = $currenet_amount;
        $wallet_amount = $current_wallet_amount + $amount;
        $query = "update #_patients set wallet_amount=" . $wallet_amount . " where pid='" . $patient_id . "'";
        $update_wallet_amount = $PDO->db_query($query);

        if ($update_wallet_amount) {
            $query = "insert into #_app_payment_history set appointment_id='0',doctor_id='0',patient_id='" . $patient_id . "',payment_mode='Wallet Refill',doctor_transaction_type='',patient_transaction_type='Credit',payment_amount='" . $amount . "',call_details_id='0',created_on='" . date('Y-m-d H:i:s') . "',modified_on='" . date('Y-m-d H:i:s') . "'";
            $payment_history_updated = $PDO->db_query($query);
            if ($payment_history_updated) {
                $json ['status'] = 'true';
                $json ['current_wallet_amount'] = $wallet_amount;
                $json ['msg'] = 'Success!! Wallet Refill';
            }
        }
        break;
    case 'appointmentCancelled' :

        $appointment_id = mysql_real_escape_string($_POST ['appointment_id']);
        $cancel_reason = mysql_real_escape_string($_POST ['cancel_reason']);
        if ($cancel_reason == 4) {
            //others
            $other_cancel_reason = mysql_real_escape_string($_POST ['other_cancel_reason']);
            $query_cancel = "update #_appointment set app_cancel_reason_id=$cancel_reason,app_cancel_other_reasons='" . $other_cancel_reason . "' where pid=" . $appointment_id;
            $appointment_cancelled_reason_updated = $PDO->db_query($query_cancel);

        } else {
            $query_cancel = "update #_appointment set app_cancel_reason_id=$cancel_reason where pid=" . $appointment_id;
            $appointment_cancelled_reason_updated = $PDO->db_query($query_cancel);
        }

        $appointment_cancelled_updated = NULL;
        $json ['status'] = 'false';
        $json ['msg'] = 'Appointment cancellation failed';
        $query = "update #_appointment set status=4 where pid=" . $appointment_id;
        $appointment_cancelled_updated = $PDO->db_query($query);
        if ($appointment_cancelled_updated) {

            $json ['status'] = 'true';
            $json ['msg'] = 'Success!! Appointment canceled';
        }
        $patient_id = $PDO->getSingleResult("select patient_id from #_book_app_mapping where appoinment_id=" . $appointment_id);
        $doctor_id = $PDO->getSingleResult("select doctor_id from #_book_app_mapping where appoinment_id=" . $appointment_id);
        $patient_gsm = $PDO->getSingleResult("select gsm_tocken from #_patients where pid=" . $patient_id);
        if ($patient_gsm) {
            $patient_name = $PDO->getSingleResult("select name from #_patients where pid=" . $patient_id);

            $message = array(
                "message" => "Appointment reference id(" . $appointment_id . ") is cancelled by " . $patient_name,
                "flag" => 'appointment_cancelled'
            );
            $is_ios_device = $PDO->getSingleResult("select is_ios_device from #_patients where pid=" . $patient_id);
            $result = $gcm->send_notification($patient_gsm, $message, "Appointment Cancelled", $doctor_id, 'D', $patient_id, 'P', $is_ios_device);
        }

        $doctor_user_id = $PDO->getSingleResult("select user_id from #_doctors where pid=" . $doctor_id);

        $doctor_gsm = $PDO->getSingleResult("select gsm_tocken from #_gcmuser where user_id=" . $doctor_user_id . " AND flag=1 and user_type='doctor' ");
        if ($doctor_gsm) {
            $patient_name = $PDO->getSingleResult("select name from #_patients where pid=" . $patient_id);

            // $doctor_name=$PDO->getSingleResult("select name from #_doctors where pid=" . $doctor_id);
            $message = array(
                'message' => "Appointment reference id(" . $appointment_id . ") is cancelled by " . $patient_name,
                'flag' => 'appointment_cancelled'
            );
            $is_ios_device = $PDO->getSingleResult("select is_ios_device from #_doctors where pid=" . $doctor_id);
            $result = $gcm->send_notification($doctor_gsm, $message, "Appointment Cancelled", $patient_id, 'P', $doctor_id, 'D', $is_ios_device);
        }
        break;
    case 'appointment_fix' :
        sleep(2);
        // $patient_id =$PDO->getSingleResult("select pid from #_patients where pid='".$_POST['patient_id']."'");
        $doctor_location_id = $_POST ['doctor_location_id'];
        $doctor_id = $_POST ['doctor_id'];
        $health_issues = $_POST ['health_issues'];
        $visited_doctor_before = $_POST ['visited_doctor_before'];
        $patient_query = $PDO->db_query("select * from #_patients where pid='" . $_POST ['patient_id'] . "'");
        $patient_data = $PDO->db_fetch_array($patient_query);
        $mapping_data = array();
        $call_details = array();
        $calldata = array();
        $response_data = array();
        $time_availablity = NULL;
        $patient_id = $patient_data ['pid'];
        $time_availablity = NULL;

        $json ['response_data'] = $response_data;
        $json ['status'] = 'false';
        $json ['msg'] = 'Try Again!!! Failed to get appointment';
        $time_availablity = $doctor_function->TimeIsBetweenTwoTimes(date('H:i', strtotime($_POST ['form_time'])), date('H:i', strtotime($_POST ['to_time'])), date('H:i'));
        if ((strtotime(date('Y-m-d', strtotime($_POST ['app_date']))) == strtotime(date('Y-m-d'))) || (strtotime(date('Y-m-d', strtotime($_POST ['app_date']))) > strtotime(date('Y-m-d')))) {
            $query = "SELECT * FROM #_doctors_locations WHERE '" . date('Y-m-d', strtotime($_POST ['app_date'])) . "' BETWEEN available_from and available_to and pid='" . $_POST ['doctor_location_id'] . "' and doctor_id='" . $_POST ['doctor_id'] . "' and is_verified=1 and status=1 and isDeleted=0";
            //print_r($query);exit;
            $check_booking_date = $PDO->db_query($query);
            if (mysql_num_rows($check_booking_date) == 0) {
                $call_details ['patient_id'] = $patient_id;
                $call_details ['doctor_location_id'] = $doctor_location_id;
                $call_details ['status'] = 1;
                $call_details ['call_by'] = 'app';
                $call_details ['create_by'] = $doctor_id;
                $call_details ['create_by_type'] = 'Doctor';
                $call_details ['created_on'] = @date('Y-m-d H:i:s');
                $call_details ['shortorder'] = $PDO->getSingleresult("select max(shortorder) as shortorder from #_call_details where 1=1 ") + 1;
               //Insert details in call details table

                $calldata ['call_details_id'] = $PDO->sqlquery("rs", 'call_details', $call_details);

                $calldata ['doctor_id'] = $doctor_id;
                $calldata ['doctor_location_id'] = $doctor_location_id;
                $calldata ['patient_id'] = $patient_id;
                $calldata ['app_date'] = date('Y-m-d', strtotime($_POST ['app_date']));
                // $calldata['app_time'] =$_POST['form_time'].' TO '.$_POST['to_time'];
                $calldata ['status'] = 1;
                $calldata ['created_on'] = @date('Y-m-d H:i:s');
                $query = "select * from #_appointment where doctor_id ='" . $calldata ['doctor_id'] . "'  and  app_date ='" . $calldata ['app_date'] . "'  and  doctor_location_id ='" . $_POST ['doctor_location_id'] . "' and app_time>='" . date('H:i', strtotime($_POST ['form_time'])) . "' and app_time<='" . date('H:i', strtotime($_POST ['to_time'])) . "' order by pid desc limit 0,1";
                // print_r($query);exit;
                $booking_query = $PDO->db_query($query);
                /*
                 * if (mysql_num_rows ( $booking_query )> 0) {
                 *
                 * while ( $row_data = $PDO->db_fetch_array ( $booking_query ) ) {
                 * $calldata ['app_time'] = $calldata ['app_time'] = date ( 'H:i', strtotime ( '+'.APPOINTMENT_SLOT.' minutes', strtotime ( $row_data ['app_time'] ) ) );
                 * $calldata ['token_no'] = $row_data ['token_no']+1;
                 *
                 * }
                 * }
                 * else if (mysql_num_rows ( $booking_query ) == 0) {
                 * $calldata ['app_time'] = date ( 'H:i',strtotime ( $_POST['form_time'] ) );
                 * $calldata ['token_no'] = 1;
                 * }
                 */
                if (mysql_num_rows($booking_query) > 0) {
                    while ($row_data = $PDO->db_fetch_array($booking_query)) {
                        if (strtotime($_POST ['app_date']) == strtotime(date('Y-m-d')) && strtotime($row_data ['app_time']) <= strtotime(date('H:i'))) {
                            $row_data ['app_time'] = date('H:i', strtotime('+1 Hour', strtotime($row_data ['app_time'])));
                            $calldata ['app_time'] = date('H:i', strtotime('+' . APPOINTMENT_SLOT . ' minutes', strtotime($row_data ['app_time'])));
                            $calldata ['token_no'] = $row_data ['token_no'] + 1;
                        } else {
                            $calldata ['app_time'] = date('H:i', strtotime('+' . APPOINTMENT_SLOT . ' minutes', strtotime($row_data ['app_time'])));
                            $calldata ['token_no'] = $row_data ['token_no'] + 1;
                        }
                    }
                } else if (mysql_num_rows($booking_query) == 0) {
                    if (strtotime($_POST ['app_date']) == strtotime(date('Y-m-d')) && strtotime($_POST ['form_time']) <= strtotime(date('H:i'))) {
                        $calldata ['app_time'] = date('H:i', strtotime('+1 hour', strtotime(date('H:i'))));
                        $calldata ['token_no'] = 1;
                    } else {
                        $calldata ['app_time'] = date('H:i', strtotime($_POST ['form_time']));
                        $calldata ['token_no'] = 1;
                    }
                }

                $calldata ['shortorder'] = $PDO->getSingleresult("select max(shortorder) as shortorder from #_appointment where 1=1 ") + 1;
                if (array_key_exists('appointment_type', $_POST) && $_POST ['appointment_type']) {
                    $calldata ['appointment_type'] = mysql_real_escape_string($_POST ['appointment_type']);
                }
				
                $calldata['health_issues'] = $health_issues;
                $calldata['visited_doctor_before'] = $visited_doctor_before;
				if (array_key_exists('cause', $_POST) && $_POST ['cause']) {
                    $calldata ['cause'] = mysql_real_escape_string($_POST ['cause']);
                }


                //Insert Into appoitment Table
                $call_id = $PDO->sqlquery("rs", 'appointment', $calldata);


                $mapping_data ['doctor_id'] = $_POST ['doctor_id'];
                $mapping_data ['patient_id'] = $_POST ['patient_id'];
                $mapping_data ['call_details_id'] = $calldata ['call_details_id'];
                $mapping_data ['appoinment_id'] = $call_id;
                $mapping_data ['created_on'] = @date('Y-m-d H:i:s');
                $mapping_data ['modified_on'] = @date('Y-m-d H:i:s');
				
                $mapping_id = $PDO->sqlquery("rs", 'book_app_mapping', $mapping_data);

                if ($mapping_id) {
                    $doctor_id = $_POST ['doctor_id'];
                    $response_data ['doctor_id'] = $doctor_id;
                    $app_date = date('D, d F Y', strtotime($calldata ['app_date']));
                    $app_time = date('h:i A', strtotime($calldata ['app_time']));
                    $response_data ['app_date_time'] = $app_date . " - " . $app_time;
                    $doctor_location_id = $_POST ['doctor_location_id'];
                    $response_data ['doctor_location_id'] = $doctor_location_id;

                    $response_data ['doctorId'] = ($doctor_id === NULL) ? '' : $doctor_id;
                    $response_data ['doctorName'] = ($doctor_id === NULL) ? '' : $PDO->getSingleResult("select name from #_doctors where pid='" . $doctor_id . "'");
                    $response_data ['doctorEmail'] = ($doctor_id === NULL || $PDO->getSingleResult("select email from #_doctors where pid='" . $doctor_id . "'") == '') ? '' : $PDO->getSingleResult("select email from #_doctors where pid='" . $doctor_id . "'");
                    $response_data ['doctorEducation'] = ($doctor_id === NULL || $PDO->getSingleResult("select education from #_doctors where pid='" . $doctor_id . "'") == '') ? '' : $PDO->getSingleResult("select education from #_doctors where pid='" . $doctor_id . "'");
                    $response_data ['doctorProfileImage'] = ($doctor_id === NULL || $PDO->getSingleResult("select profile_image from #_doctors where pid='" . $doctor_id . "'") == '') ? '' : SITE_PATH . "uploaded_files/doctors/" . $PDO->getSingleResult("select profile_image from #_doctors where pid='" . $doctor_id . "'");
                    $response_data ['doctorExperience'] = ($doctor_id === NULL || $PDO->getSingleResult("select years_of_experience from #_doctors where pid='" . $doctor_id . "'") == '') ? '' : $PDO->getSingleResult("select years_of_experience from #_doctors where pid='" . $doctor_id . "'");

                    $speciality = $PDO->getSingleResult("select speciality from #_doctors where pid='" . $doctor_id . "'");

                    if ($speciality == 0) {
                        $response_data ['doctorSpeciality'] = 'General Practitioner';

                    } else {
                        $response_data ['doctorSpeciality'] = ($doctor_id === NULL || $PDO->getSingleResult("select name from #_specialities where pid='" . $PDO->getSingleResult("select speciality from #_doctors where pid='" . $doctor_id . "'") . "'") == '') ? '' : $PDO->getSingleResult("select name from #_specialities where pid='" . $PDO->getSingleResult("select speciality from #_doctors where pid='" . $doctor_id . "'") . "'");
                    }
                    /*$response_data ['doctorSpeciality'] = ($doctor_id === NULL || $PDO->getSingleResult ( "select name from #_specialities where pid='" . $PDO->getSingleResult ( "select speciality from #_doctors where pid='" . $doctor_id . "'" ) . "'" ) == '') ? '' : $PDO->getSingleResult ( "select name from #_specialities where pid='" . $PDO->getSingleResult ( "select speciality from #_doctors where pid='" . $doctor_id . "'" ) . "'" );
                    */
                    $location_type = $PDO->getSingleResult("select location_type from #_doctors_locations where pid='" . $doctor_location_id . "'");
                    $clinic_id = $PDO->getSingleResult("select clinic_id from #_doctors_locations where pid='" . $doctor_location_id . "'");
                    $hospital_id = $PDO->getSingleResult("select hospital_id from #_doctors_locations where pid='" . $doctor_location_id . "'");
                    $response_data ['locationName'] = ($location_type === NULL) ? '' : (($location_type == 1) ? $PDO->getSingleResult("select name from #_clinics where pid='" . $clinic_id . "'") . "(Clinic)" : $PDO->getSingleResult("select name from #_hospitals where pid='" . $hospital_id . "'") . "(Hospital)");
                    $response_data ['locationAddress'] = ($doctor_location_id === NULL || $PDO->getSingleResult("select location from #_doctors_locations where pid='" . $doctor_location_id . "'") == '') ? '' : $PDO->getSingleResult("select location from #_doctors_locations where pid='" . $doctor_location_id . "'");
                }
                $user_id = $PDO->getSingleResult("select user_id from #_doctors where pid='" . $doctor_id . "'");
                $user_type = 'doctor';
                $doctor_gcmtocken = $PDO->getSingleResult("select gsm_tocken from #_gcmuser where user_id ='" . $user_id . "'  and user_type='" . $user_type . "' and flag='1' ORDER BY pid DESC LIMIT 0,1");
                // echo "select * from #_gcmuser where user_id ='".$user_id."' and user_type='".$user_type."' and flag='1' ";
                if ($doctor_gcmtocken) {

                    $patient_name = $PDO->getSingleResult("select name from #_patients where pid='" . $patient_id . "'");
                    $message = array(
                        "message" => $patient_name . " booked an appointment with you.",
                        "flag" => 'doctor'
                    );
                    $is_ios_device = $PDO->getSingleResult("select is_ios_device from #_patients where pid=" . $patient_id);
                    $result = $gcm->send_notification($doctor_gcmtocken, $message, "Appointment", $patient_id, 'P', $doctor_id, 'D', $is_ios_device); // print_r($result);

                }

                $mobile = $PDO->getSingleresult("select phone from #_doctors where pid='" . $_POST ['doctor_id'] . "' ");
                $message = urlencode((($patient_data ['sex'] == 'Female') ? 'Ms' : 'Mr') . ' ' . $patient_data ['name'] . ' has fixed an appointment with you at ' . $calldata ['app_time'] . ' form  ' . $calldata ['app_date']);

                // file_get_contents(SITE_SUB_PATH . "app/sms/send_sms.php?mobile=" . $mobile . "&message=" . $message);

                $json ['response_data'] = $response_data;
                $json ['status'] = 'true';
                $json ['msg'] = 'Appointment fixed with doctor';
            } else {
                $json ['msg'] = 'Selected date doctor is unavailable!! Please try with another date';
            }
        } else {
            $json ['msg'] = 'Selected date doctor is unavailable!! Please try with another date';
        }
        break;
    case 'appointment_finish' :
        $appointment_id = mysql_real_escape_string($_POST ['appointment_id']);
        $comments = mysql_real_escape_string($_POST ['comments']);
        $PDO->db_query("update #_appointment  SET  comments ='" . $comments . "', status ='2' where pid ='" . $appointment_id . "' ");
        $json ['status'] = 'true';

        break;
    case 'booking_all' :

        $pid = mysql_real_escape_string($_POST ['pid']);
        $user_type = mysql_real_escape_string($_POST ['user_type']);

        if ($user_type == 'clinics') {
            $wh = " and book_type='Clinics'";
        } else if ($user_type == 'healthcare_organisation') {

            $wh = "  and book_type='Healthcare'";
        } else if ($user_type == 'labs') {
            $wh = " and book_type='Lab'";
        } else if ($user_type == 'hospital') {
            $wh = " and book_type='Hospital'";
        }

        $booking_query = $PDO->db_query("select * from #_booking  where book_id ='" . $pid . "'  " . $wh . " order by pid desc ");

        $json ['detail'] = array();
        if (mysql_num_rows($booking_query) > 0) {
            $i = 0;
            $data = array();
            while ($booking_data = mysql_fetch_assoc($booking_query)) {

                $patient_query = $PDO->db_query("select * from #_patients where pid='" . $booking_data ['patient_id'] . "'");
                $patient_row = $PDO->db_fetch_array($patient_query);

                $division_query = $PDO->db_query("select * from #_division where pid='" . $patient_row ['division'] . "'");
                $division_row = $PDO->db_fetch_array($division_query);

                $township_query = $PDO->db_query("select * from #_township where division_id='" . $division_row ['pid'] . "'");
                $township_row = $PDO->db_fetch_array($township_query);

                $booking_data ['patient_name'] = $patient_row ['name'];
                $booking_data ['patient_id'] = $patient_row ['patient_id'];
                $booking_data ['patient_age'] = $patient_row ['age'];
                $booking_data ['patient_sex'] = $patient_row ['sex'];
                $booking_data ['patient_phone'] = $patient_row ['phone'];
                $booking_data ['patient_address'] = $patient_row ['address'];
                $booking_data ['patient_postal_code'] = $patient_row ['postal_code'];
                $booking_data ['patient_township'] = $township_row ['name'];
                $booking_data ['patient_division'] = $division_row ['name'];

                $data [] = $booking_data;
            }

            $json ['detail'] = $data;
            $json ['status'] = 'true';
        } else {
            $json ['status'] = 'false';
        }

        break;
    case 'booking_finish' :
        $pid = mysql_real_escape_string($_POST ['pid']);
        $comments = mysql_real_escape_string($_POST ['comments']);
        $PDO->db_query("update #_booking  SET  booking_complete_comments ='" . $comments . "', booking_complete_flag ='1' where pid ='" . $pid . "' ");
        $json ['status'] = 'true';

        break;
    case 'LabTest':
        $lang_flag = mysql_real_escape_string($_POST['lang_flag']);
        $service_type = mysql_real_escape_string($_POST['service_type']);
        $book_type = mysql_real_escape_string($_POST['book_type']);
        if ($book_type == 'Hospital') {
            $lab_query = $PDO->db_query("select * from #_hospitals where pid ='" . $_POST['book_id'] . "' ");
            $lab_data = $PDO->db_fetch_array($lab_query);
            //print_r($lab_data);
        } else if ($book_type == 'Lab') {

            $lab_query = $PDO->db_query("select * from #_labs where pid ='" . $_POST['book_id'] . "' ");
            $lab_data = $PDO->db_fetch_array($lab_query);

        } else if ($book_type == 'Clinics') {

            $lab_query = $PDO->db_query("select * from #_clinics where pid ='" . $_POST['book_id'] . "' ");
            $lab_data = $PDO->db_fetch_array($lab_query);
            // print_r($lab_data);

        } else if ($book_type == 'Healthcare') {

            $lab_query = $PDO->db_query("select * from #_healthcare_organization where pid ='" . $_POST['book_id'] . "' ");
            $lab_data = $PDO->db_fetch_array($lab_query);
            // print_r($lab_data);
        }


        if ($service_type == 'Lab') {
            $left_services = (array)json_decode($lab_data['left_services'], true);
            $j = 0;
            $labtest = array();
            foreach ($left_services as $key1 => $values1) {

                $i = 0;
                $ser_date = array();
                foreach ($values1 as $key2 => $values2) {

                    if ($left_services[$key1][$key2] != "") {


                        $catquery = $PDO->db_query("select S.*,SC.name as sc_name,SC.pid as sc_pid from #_service_subcat as S JOIN #_services_cat as SC ON S.service_cat=SC.pid where S.status=1 and S.service_cat='" . $key1 . "' and S.pid='" . $key2 . "' and SC.service_type=1 group by SC.pid");
                        $catrow = $PDO->db_fetch_array($catquery);

                        if ($i == 0) {
                            $labtest[$j]['service_cat'] = $catrow['sc_name'];

                        }
                        $ser_date[$i]['pid'] = $catrow['pid'];
                        if ($lang_flag = 'en') {
                            $ser_date[$i]['name'] = $catrow['name'];
                        }
                        if ($lang_flag = 'my') {
                            $ser_date[$i]['name'] = $catrow['name_my'];
                        }
                        $i++;
                    }

                    if (!empty($ser_date)) {
                        $labtest[$j]['test'] = $ser_date;
                    }
                }

                if (!empty($ser_date)) {
                    $j++;

                }

            }
        }


        $imgtest = array();
        if ($service_type == 'Imaging') {
            $imagin_services = (array)json_decode($lab_data['imagin_services'], true);
            $j = 0;

            //print_r($imagin_services);

            foreach ($imagin_services as $key1 => $values1) {

                $i = 0;

                foreach ($values1 as $key2 => $values2) {

                    if ($imagin_services[$key1][$key2] != "") {


                        $catquery = $PDO->db_query("select S.*,SC.name as sc_name,SC.pid as sc_pid from #_service_subcat as S JOIN #_services_cat as SC ON S.service_cat=SC.pid where S.status=1 and S.service_cat='" . $key1 . "' and S.pid='" . $key2 . "' and SC.service_type=2 group by SC.pid");
                        $catrow = $PDO->db_fetch_array($catquery);

                        if ($i == 0) {
                            $labtest[$j]['service_cat'] = $catrow['sc_name'];

                        }
                        $ser_date[$i]['pid'] = $catrow['pid'];
                        if ($lang_flag = 'en') {
                            $ser_date[$i]['name'] = $catrow['name'];
                        }
                        if ($lang_flag = 'my') {
                            $ser_date[$i]['name'] = $catrow['name_my'];
                        }

                        $i++;
                    }

                }

                $imgtest[$j]['imgtest'] = $ser_date;
                if (!empty($ser_date)) {
                    $j++;
                }

            }
        }

        $json['labtest'] = $labtest;
        $json['imgtest'] = $imgtest;
        $json['status'] = 'true';
        break;
    case 'patientprevioushistorydetails':

        $json ['appointment_history_lists'] = array();
        $appointment_history_lists = array();
        $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        $call_details_id = mysql_real_escape_string($_POST ['call_details_id']);
        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $appointment_history_lists = $doctor_function->getPatientAppointmentHistory($patient_id, $category = 3, $start_offset, $end_limit, 0);
        if ($appointment_history_lists) {
            // $json ['patient_details'] = $patient_details;
            // $json ['curent_appointment_deatils'] = $curent_appointment_deatils;
            $json ['appointment_history_lists'] = $appointment_history_lists;
            $json ['status'] = 'true';
            $json ['msg'] = 'Success!! Appointment list';
        }
        break;
    case 'paymentTransactionHistory' :

        $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        $wallet_amount = 0;
        $payment_history_list = NULL;

        $json ['status'] = 'false';
        $json ['wallet_amount'] = $wallet_amount;
        $json ['msg'] = 'Payment History Not Found';
        $json ['payment_history'] = $payment_history_list;

        $wallet_amount = $PDO->getSingleResult("select wallet_amount from #_patients where pid='" . $patient_id . "'");

        $payment_history_list = $doctor_function->getPaymentHistoryDetailsPatient($patient_id, 'P');

        if ($payment_history_list) {

            $json ['status'] = 'true';
            $json ['wallet_amount'] = $wallet_amount;
            $json ['msg'] = 'Payment History List';
            $json ['payment_history'] = $payment_history_list;
        }
        break;

    case 'delete_records':
        $pid = mysql_real_escape_string($_POST ['pid']);
        $method = mysql_real_escape_string($_POST ['method']);
        if (!empty($method) || !empty($method)) {
            if ($method == 'delete_reports') {
                $query = "update #_reports set is_deleted=1 where pid=" . $pid;
                $json ['status'] = 'true';
                $json ['msg'] = 'Successfully Deleted.';
            } elseif ($method == 'delete_lab_test') {
                $query = "update #_patient_lab_test_details set is_deleted=1 where pid=" . $pid;
                $json ['status'] = 'true';
                $json ['msg'] = 'Successfully Deleted.';
            }elseif ($method == 'delete_medicine') {
                $query = "update #_patient_medicine_details set is_deleted=1 where pid=" . $pid;
                $json ['status'] = 'true';
                $json ['msg'] = 'Successfully Deleted.';
            }else {
                $query = "select * from #_reports";
                $json ['status'] = 'false';
                $json ['msg'] = 'There is an error in delete.';
            }
            $delete_updated = $PDO->db_query($query);
        } else {
            $json ['status'] = 'true';
            $json ['msg'] = 'Please send correct values';
        }

        break;

    case 'timeslots_and_location_details':
        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $appointment_type = mysql_real_escape_string($_POST ['appointment_type']);
        $given_date = mysql_real_escape_string($_POST ['date']);
        $location_id = mysql_real_escape_string($_POST ['location_id']);
        $doctor_timeslot_is_available = array();
        if (empty($doctor_id) || empty($given_date)) {
            $json ['status'] = 'false';
            $json ['msg'] = 'Please send all required Values';
            $doctor_function->send_res($json);
            exit ();
        }
        $timestamp = strtotime($given_date);
        $weekday = date('l', $timestamp);
        $weekday = $doctor_function->given_weekday_num_find($weekday);
        //
        $today_date=date('Y-m-d');
        if($today_date==$given_date){
            $time_curent=date('H:i:s');
        }else{
            $time_curent='';
        }
        /*echo $time_curent;
        exit;*/
        //check whether the doctor timeslot is avaibale for the given date
        $doctor_timeslot_is_available = $doctor_function->check_timeslot_is_available_for_given_date($doctor_id, $weekday,$time_curent);

        if ($doctor_timeslot_is_available != 'false') {
            $json ['status'] = 'true';
            $json ['msg'] = 'Locations are available.';
            if ($appointment_type == 0 && ($json ['status'] == 'true'))                //physical appointment
            {
                $like = $location_id;
                $doctor_timeslot_is_available = array_filter($doctor_timeslot_is_available, function ($item) use ($like) {
                    if (stripos($item['location_id'], $like) !== false) {
                        return true;
                    }
                    return false;
                });
                $doctor_timeslot_is_available = array_values($doctor_timeslot_is_available);
                $json ['doctors_timeslot_details'] = $doctor_timeslot_is_available;

            } elseif ($appointment_type != 0 && ($json ['status'] == 'true')) {                                    //Virtual Appointment
                $json ['doctors_timeslot_details'] = $doctor_timeslot_is_available;
            } else {
                $json ['status'] = 'false';
            }

        } else {
            $json ['status'] = 'false';
            $json ['msg'] = 'Locations are not available.';
        }

        break;
    case 'appointmentPaymentWithCash' :

        $appointment_id = mysql_real_escape_string ( $_POST ['appointment_id'] );
        $cash_payment = mysql_real_escape_string ( $_POST ['amount'] );
        $appointment_history_updated = NULL;
        $appointment_payment_updated = NULL;

        $json ['status'] = 'false';
        $json ['msg'] = 'Appointment payment failed';

        $doctor_id = $PDO->getSingleResult ( "select doctor_id from #_appointment where pid='" . $appointment_id . "'" );
        $patient_id = $PDO->getSingleResult ( "select patient_id from #_appointment where pid='" . $appointment_id . "'" );
        $call_details_id = $PDO->getSingleResult ( "select call_details_id from #_appointment where pid='" . $appointment_id . "'" );

        $query_paid = "update #_appointment set status=2 where pid=" . $appointment_id;
        $appointment_payment_updated = $PDO->db_query ( $query_paid );

        $query = "insert into #_app_payment_history set appointment_id='" . $appointment_id . "',doctor_id='" . $doctor_id . "',patient_id='" . $patient_id . "',payment_mode='Cash',doctor_transaction_type='Credit',patient_transaction_type='Debit',payment_amount='" . $cash_payment . "',call_details_id='" . $call_details_id . "',created_on='" . date ( 'Y-m-d H:i:s' ) . "',modified_on='" . date ( 'Y-m-d H:i:s' ) . "'";
        $appointment_history_updated = $PDO->db_query ( $query );
        $patient_name = $PDO->getSingleResult ( "select name patient_id from #_patients where pid='" . $patient_id . "'" );
        $doctor_name = $PDO->getSingleResult ( "select name from #_doctors where pid=" . $doctor_id );
        if ($appointment_history_updated) {

            $json ['status'] = 'true';
            $json ['msg'] = 'Success!! Appointment Payment Done';

            $patient_gsm = $PDO->getSingleResult ( "select gsm_tocken from #_patients where pid=" . $patient_id );
            if ($patient_gsm) {

                $message = array (
                    'message' => "Cash transaction for payment of " . $cash_payment . "RS paid to " . $doctor_name . ". Appointment Reference Id is (" . $appointment_id . ").",
                    'flag' => 'cash_payment'
                );

                $is_ios_device = $PDO->getSingleResult ( "select is_ios_device from #_patients where pid=" . $patient_id );
                $result = $gcm->send_notification ( $patient_gsm, $message, "Cash Transaction", $doctor_id, 'D', $patient_id, 'P',$is_ios_device );
            }
            $doctor_user_id = $PDO->getSingleResult ( "select user_id from #_doctors where pid=" . $doctor_id );
            $doctor_gsm = $PDO->getSingleResult ( "select gsm_tocken from #_gcmuser where user_id=" . $doctor_user_id . " AND flag=1 and 	user_type='doctor'" );
            if ($doctor_gsm) {

                // $doctor_name=$PDO->getSingleResult("select name from #_doctors where pid=" . $doctor_id);
                $message = array (
                    'message' => "Cash transaction for payment of " . $cash_payment . "RS received from " . $patient_name. ". Appointment Reference Id is (" . $appointment_id . ").",
                    'flag' => 'cash_payment'
                );
                $is_ios_device = $PDO->getSingleResult ( "select is_ios_device from #_doctors where pid=" . $doctor_id );
                $result = $gcm->send_notification ( $doctor_gsm, $message, "Cash Transaction", $patient_id, 'P', $doctor_id, 'D',$is_ios_device );
            }
        }
        break;
    case 'appointmentPaymentWithWallet' :

        $appointment_id = mysql_real_escape_string ( $_POST ['appointment_id'] );
        $wallet_payment = mysql_real_escape_string ( $_POST ['amount'] );
        $appointment_history_updated = NULL;
        $appointment_payment_updated = NULL;
        $lab_wallet_updated = NULL;
        $doctor_wallet_updated = NULL;
        $patient_wallet_updated = NULL;
        $doctor_id = NULL;
        $call_details_id = NULL;
        $patient_id = NULL;

        $json ['status'] = 'false';
        $json ['msg'] = 'Appointment payment failed';

        $doctor_id = $PDO->getSingleResult ( "select doctor_id from #_appointment where pid='" . $appointment_id . "'" );
        $patient_id = $PDO->getSingleResult ( "select patient_id from #_appointment where pid='" . $appointment_id . "'" );
        $call_details_id = $PDO->getSingleResult ( "select call_details_id from #_appointment where pid='" . $appointment_id . "'" );
        $appointment_with = $PDO->getSingleResult ( "select appointment_with from #_appointment where pid='" . $appointment_id . "'" );
        $patient_name = $PDO->getSingleResult ( "select name patient_id from #_patients where pid='" . $patient_id . "'" );
        $doctor_name = $PDO->getSingleResult ( "select name from #_doctors where pid=" . $doctor_id );
        if ($appointment_with == 'D') {
            $doctor_current_wallet = $PDO->getSingleResult ( "select wallet_amount from #_doctors where pid='" . $doctor_id . "'" );
        } else if ($appointment_with == 'L') {
            $lab_current_wallet = $PDO->getSingleResult ( "select wallet_amount from #_doctors where pid='" . $doctor_id . "'" );
        }
        $patient_current_wallet = $PDO->getSingleResult ( "select wallet_amount from #_patients where pid='" . $patient_id . "'" );

        if ($patient_current_wallet < $wallet_payment) {
            $json ['msg'] = 'Insuffient wallet balance';
        } else {
            if ($appointment_with == 'D') {
                $doctor_wallet_amount = $doctor_current_wallet + $wallet_payment;
                $query = "update #_doctors set wallet_amount=" . $doctor_wallet_amount . " where pid='" . $doctor_id . "'";
                $doctor_wallet_updated = $PDO->db_query ( $query );
            } else if ($appointment_with == 'L') {
                $lab_wallet_amount = $lab_current_wallet + $wallet_payment;
                $query = "update #_labs set wallet_amount=" . $lab_wallet_amount . " where pid='" . $doctor_id . "'";
                $lab_wallet_updated = $PDO->db_query ( $query );
            }
            $patient_wallet_amount = $patient_current_wallet - $wallet_payment;
            $query = "update #_patients set wallet_amount=" . $patient_wallet_amount . " where pid='" . $patient_id . "'";
            $patient_wallet_updated = $PDO->db_query ( $query );

            $query_paid = "update #_appointment set status=2 where pid=" . $appointment_id;
            $appointment_payment_updated = $PDO->db_query ( $query_paid );

            $query = "insert into #_app_payment_history set appointment_id='" . $appointment_id . "',doctor_id='" . $doctor_id . "',patient_id='" . $patient_id . "',payment_mode='Wallet',doctor_transaction_type='Credit',patient_transaction_type='Debit',payment_amount='" . $wallet_payment . "',call_details_id='" . $call_details_id . "',created_on='" . date ( 'Y-m-d H:i:s' ) . "',modified_on='" . date ( 'Y-m-d H:i:s' ) . "'";
            $appointment_history_updated = $PDO->db_query ( $query );

            if ($appointment_history_updated) {
                $json ['status'] = 'true';
                $json ['msg'] = 'Success!! Appointment Payment Done';

                $patient_gsm = $PDO->getSingleResult ( "select gsm_tocken from #_patients where pid=" . $patient_id );
                if ($patient_gsm) {

                    //$doctor_name=$PDO->getSingleResult("select name from #_doctors where pid=" . $doctor_id);

                    $message = array (
                        'message' => "Wallet transaction for payment of " . $wallet_payment . "RS paid to " . $doctor_name . ". Appointment Reference Id is(" . $appointment_id . ").",
                        'flag' => 'wallet_payment'
                    );

                    $is_ios_device = $PDO->getSingleResult ( "select is_ios_device from #_patients where pid=" . $patient_id );
                    $result = $gcm->send_notification ( $patient_gsm, $message, "Wallet Transaction", $doctor_id, 'D', $patient_id, 'P',$is_ios_device );
                }
                if ($appointment_with == 'D') {
                    $doctor_user_id = $PDO->getSingleResult ( "select user_id from #_doctors where pid=" . $doctor_id );
                    $doctor_gsm = $PDO->getSingleResult ( "select gsm_tocken from #_gcmuser where user_id=" . $doctor_user_id . " AND flag=1 and 	user_type='doctor'" );
                    if ($doctor_gsm) {

                        //$doctor_name=$PDO->getSingleResult("select name from #_doctors where pid=" . $doctor_id);
                        $message = array (
                            'message' => "Wallet transaction for payment of " . $wallet_payment . "RS received from " . $patient_name . ". Appointment Reference Id is (" . $appointment_id . ").",
                            'flag' => 'wallet_payment'
                        );
                        $is_ios_device = $PDO->getSingleResult ( "select is_ios_device from #_doctors where pid=" . $doctor_id );
                        $result = $gcm->send_notification ( $doctor_gsm, $message, "Wallet Transaction", $patient_id, 'P', $doctor_id, 'D',$is_ios_device);
                    }
                }
            }
        }

        break;

    default :
        break;
}
/* Output header */
// header('Content-type: application/json');
// echo json_encode($_POST);
echo json_encode($json);
?>