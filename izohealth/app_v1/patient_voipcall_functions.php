<?php
include("../lib/open.inc.php");
$flag = mysql_real_escape_string($_POST ['flag']);
$json = array();
include 'class/all_class_files.php';
$doctor_function = new doctor ();
$patient_function = new patient ();
$m = new MyMail ();
include_once 'notification/GCM.php';
$gcm = new GCM ();
date_default_timezone_set('Asia/Kolkata');

switch ($flag) {

    case 'calllog' :
        $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        $patient_id = $PDO->getSingleResult("select pid from #_patients where patient_id='" . $patient_id . "'");
        $item = mysql_real_escape_string($_POST ['item']);
        $page = mysql_real_escape_string($_POST ['page']);
        if ($page > 0 || $item > 0) {
            $item = ($item > 0) ? $item : 10;
            $page = ($page > 0) ? $page : 1;
            $start = ($page == 1) ? 0 : ($item * $page - ($item + 1));
            $wh .= " limit $start,$item";
        }

        $result = $PDO->db_query("select * from #_call_details where patient_id='" . $patient_id . "'  order by pid desc" . $wh);
        $i = 0;
        $json ['call_detail'] = array();
        if (mysql_num_rows($result) > 0) {
            while ($line = $PDO->db_fetch_array($result)) {
                $call_detail [$i] ['created_on'] = $line ['created_on'];
                $call_detail [$i] ['call_type'] = $line ['call_type'];
                $call_detail [$i] ['chief_complaint'] = $line ['chief_complaint'];
                $call_detail [$i] ['history_of_present_illness'] = $line ['history_of_present_illness'];
                $call_detail [$i] ['past_medical_history'] = $line ['past_medical_history'];
                $call_detail [$i] ['past_surgical_history'] = $line ['past_surgical_history'];
                $call_detail [$i] ['social_history'] = $line ['social_history'];
                $call_detail [$i] ['family_history'] = $line ['family_history'];
                $call_detail [$i] ['history_of_drug_allergy'] = $line ['history_of_drug_allergy'];
                $call_detail [$i] ['drug_allergy_comment'] = $line ['drug_allergy_comment'];
                $call_detail [$i] ['bp'] = $line ['bp'];
                $call_detail [$i] ['pr'] = $line ['pr'];
                $call_detail [$i] ['sao'] = $line ['sao'];
                $call_detail [$i] ['weight'] = $line ['weight'];
                $call_detail [$i] ['rbs'] = $line ['rbs'];
                $call_detail [$i] ['temp'] = $line ['temp'];
                $call_detail [$i] ['provisional_diagnosis'] = $line ['provisional_diagnosis'];
                $call_detail [$i] ['follow_up_call_schedule'] = $line ['follow_up_call_schedule'];
                $call_detail [$i] ['follow_up_call_date'] = $line ['follow_up_call_date'];

                $reports = array();
                $report_query = $PDO->db_query("select * from #_reports Where call_details_id ='" . $patient_id . "'");
                while ($report_rows = $PDO->db_fetch_array($report_query)) {
                    $reports [] = SITE_PATH . '/modules/reports/ajax.php?file=' . $report_rows ['reports'];
                }
                $call_detail [$i] ['reports'] = $reports;

                $medicalreference = array();
                $medicalreference_query = $PDO->db_query("select * from #_medicalreference where patient_id ='" . $patient_id . "' and status='1' ");

                $m = 0;
                while ($medicalreference_row = $PDO->db_fetch_array($medicalreference_query)) {
                    $medicalreference [$m] ['title'] = $medicalreference_row ['title'];
                    $medicalreference [$m] ['file_name'] = SITE_PATH . '/modules/reports/ajax.php?file=' . $file ['file_name'];
                }

                $call_detail [$i] ['medicalreference'] = $medicalreference;

                $call_detail [$i] ['services_comment'] = ($services_comment != '') ? $services_comment : '';
                $call_detail [$i] ['health_consultation'] = ($health_consultation != '') ? $health_consultation : '';

                $appointment = array();
                $report_query = $PDO->db_query("select AP.doctor_id, AP.app_date,AP.app_time,D.hospital_id, D.clinic_id,D.name  from #_appointment as AP left join #_doctors as D ON D.pid =AP.doctor_id  Where AP.call_details_id ='" . $patient_id . "'");
                $p = 0;
                while ($report_rows = $PDO->db_fetch_array($report_query)) {
                    $appointment [$p] ['doctor_id'] = $report_rows ['doctor_id'];
                    $clinic = $PDO->getSingleResult("select name from #_clinics where pid='" . $report_rows ['clinic_id'] . "'");
                    $appointment [$p] ['clinic'] = ($clinic != '') ? $clinic : '';
                    $hospital = $PDO->getSingleResult("select name from #_hospitals where pid='" . $report_rows ['hospital_id'] . "'");
                    $appointment [$p] ['hospital'] = ($hospital != '') ? $hospital : '';
                    $appointment [$p] ['name'] = ($report_rows ['name'] != '') ? $report_rows ['name'] : '';
                    $appointment [$p] ['app_date'] = $report_rows ['app_date'];
                    $appointment [$p] ['app_time'] = $report_rows ['app_time'];
                    $p++;
                }
                $call_detail [$i] ['appointment'] = $appointment;
                $i++;
            }

            $json ['call_detail'] = $call_detail;
            $json ['status'] = 'true';
        } else {

            $json ['status'] = 'false';
        }

        break;
    case 'voip_call_details':

        $patient_id = mysql_real_escape_string($_POST['patient_id']);
        $call_duration = mysql_real_escape_string($_POST['call_duration']);
        $doc_id = mysql_real_escape_string($_POST['doc_id']);
        $created_on = date("Y-m-d h:i:sa");
        //$comments =mysql_real_escape_string($_POST['comments']);

        $vcall_query = $PDO->db_query("insert into #_voip_call_details set
                                                         patient_id='$patient_id',
                                                         call_duration='$call_duration',									
														 doc_id='$doc_id',
														 created_on='$created_on'
														 ");
        $call_id = mysql_insert_id();
        if ($vcall_query) {

            $check = $PDO->db_query("select * from #_voip_call_details  where pid='" . $call_id . "'");
            $call_details = $PDO->db_fetch_array($check);

            $check1 = $PDO->db_query("select * from #_patients where patient_id='" . $call_details['patient_id'] . "'");
            $patient_details = $PDO->db_fetch_array($check1);

            $call_query = $PDO->db_query("insert into #_call_details set
															  patient_id='" . $patient_details['pid'] . "',
															  created_on='$created_on'
															  
														 ");

            $check2 = $PDO->db_query("select * from #_doctors where user_id='" . $call_details['doc_id'] . "'");
            $doc_details = $PDO->db_fetch_array($check2);

            $json['call_id'] = $call_details['pid'];
            $json['patient_name'] = $patient_details['name'];
            $json['doctor_name'] = $doc_details['name'];
            $json['call_duration'] = $call_details['call_duration'];
            $json['comments'] = $call_details['comments'];
            $json['status'] = 'true';

        } else {
            $json['status'] = 'false';
        }
        break;

    case 'call_listing':
        $patient_id = mysql_real_escape_string($_POST['patient_id']);
        $json['call'] = array();
        $check = $PDO->db_query("select * from #_voip_call_details where patient_id='" . $patient_id . "' order by pid desc");
        if (mysql_num_rows($check) > 0) {
            $call = array();
            $d = 0;
            while ($call_details = $PDO->db_fetch_array($check)) {
                $call[$d]['call_id'] = $call_details['pid'];
                $check1 = $PDO->db_query("select * from #_patients where patient_id='" . $call_details['patient_id'] . "'");
                $patient_details = $PDO->db_fetch_array($check1);

                $check2 = $PDO->db_query("select * from #_doctors where  pid='" . $call_details['doc_id'] . "'");
                $doc_details = $PDO->db_fetch_array($check2);

                $call[$d]['patient_name'] = $PDO->getSingleResult("select name from #_patients where pid='" . $call_details['patient_id'] . "'");;
                $call[$d]['doctor_name'] = $PDO->getSingleResult("select name from #_doctors where pid='" . $call_details['doc_id'] . "'");;
                $call[$d]['call_duration'] = $call_details['call_duration'];
                $call[$d]['comments'] = $call_details['comments'];
                $call[$d]['created_on'] = $call_details['created_on'];

                $d++;
            }
            $json['call'] = $call;

            $json['status'] = 'true';

        } else {
            $json['status'] = 'false';
        }
        break;
    case 'comment_call':
        $call_id = mysql_real_escape_string($_POST['call_id']);
        $comments = mysql_real_escape_string($_POST['comments']);

        $query = $PDO->db_query("Update #_voip_call_details set
						                        comments='$comments'
												where pid='" . $call_id . "'
						                      ");
        if ($query) {
            $json['status'] = 'true';
        }

    default :
        break;
}
/* Output header */
// header('Content-type: application/json');
// echo json_encode($_POST);
echo json_encode($json);
?>