<?php
include("../lib/open.inc.php");
$flag = mysql_real_escape_string($_POST ['flag']);
$json = array();
include 'class/all_class_files.php';
$doctor_function = new doctor ();
$patient_function = new patient ();
$m = new MyMail ();
include_once 'notification/GCM.php';
$gcm = new GCM ();
date_default_timezone_set('Asia/Kolkata');

switch ($flag) {

    case 'get_all_chatbot_categories':
        $json ['chatbot_categories'] = array();
        // Get doctor types from db
        $chatbot_categories_values = $patient_function->get_all_chatbot_categories();
        if ($chatbot_categories_values != 'false') {
            $json ['chatbot_categories'] = $chatbot_categories_values;
            $json ['status'] = 'true';
        } else {
            $json ['status'] = 'false';
        }

        break;

    case 'option_list_based_on_previous_selected_option':
        $category_id = mysql_real_escape_string($_POST ['category_id']);
        $selected_option_pid = mysql_real_escape_string($_POST ['selected_option_pid']);
        $selected_option_parent_pid = mysql_real_escape_string($_POST ['selected_option_parent_pid']);
        $lang_flag='en';
        if (($selected_option_pid == '') || ($selected_option_parent_pid == '')) {
            $json ['status'] = 'false';
            $json ['msg'] = 'Given API values are empty';
            $doctor_function->send_res($json);
            exit ();
        }
        $json ['chatbot_options'] = array();
        // Get doctor types from db
        $chatbot_chatbot_options_values = $patient_function->option_list_based_on_previous_selected_option($selected_option_pid, $selected_option_parent_pid);
        if ($chatbot_chatbot_options_values != 'false') {
            $json ['chatbot_options'] = $chatbot_chatbot_options_values;
            $json ['status'] = 'true';
        } else {
            //Last child
            //search the doctor
            //find the speciality for the given category
            $speciality_id = $PDO->getSingleResult("select speciality_id from #_chatbot_categories_specialist_mapping where category_id=" . $category_id);
            $wh .= "   and D.speciality=" . $speciality_id . " and D.is_verified=1 and dl.is_verified=1 and dl.status=1 and dl.isDeleted=0 and dl.available_status=1";
            //	echo "select D.*, S.position as sponsored_position  from #_doctors as D  LEFT JOIN #_sponsored as S ON S.drname = D.pid  where  D.status=1 ".$wh." group by D.pid order by  S.position desc, D.name asc limit $start,$pagesize";
            $query = "select D.pid,D.speciality,D.name,D.name_my,D.email,D.phone,D.sex,D.date_of_birth,D.doctor_type,D.rs_number,D.fees,D.education,D.area_code,D.address,D.live_status,D.township,D.division,D.isHomeservice,D.isChatNeeded,D.profile_image, dl.hospital_id,dl.clinic_id,dl.location,dl.location_my,dl.dates ,dl.available_status from #_doctors as D LEFT JOIN #_doctors_locations as dl ON dl.doctor_id = D.pid  where  D.status=1 " . $wh . " group by D.pid order by D.pid, D.name asc ";//limit ". $start.",".$pagesize
            $doctor_query = $PDO->db_query($query);
            if (mysql_num_rows($doctor_query) > 0) {
                $doctor = array();
                $d = 0;
                while ($doctor_rows = $PDO->db_fetch_array($doctor_query)) {
                    // get the doctor location details from doctor_locations table
                    $doctors_location_list_values = $doctor_function->get_doctors_location_list($doctor_rows['pid'], $lang_flag);
                    if ($doctors_location_list_values != 'false') {
                        $doctor[$d]['doctors_location_list'] = $doctors_location_list_values;
                    } else {
                        $doctor[$d]['doctors_location_list'] = array();
                    }
                    if ($lang_flag == "en") {
                        $doctor[$d]['hospital'] = $PDO->getSingleResult("select name from #_hospitals where pid='" . $doctor_rows['hospital_id'] . "'");
                        $doctor[$d]['hospital_phone'] = $PDO->getSingleResult("select phone from #_hospitals where pid='" . $doctor_rows['hospital_id'] . "'");
                        $doctor[$d]['clinic'] = $PDO->getSingleResult("select name from #_clinics where pid='" . $doctor_rows['clinic_id'] . "'");
                        $doctor[$d]['clinic_phone'] = $PDO->getSingleResult("select phone from #_clinics where pid='" . $doctor_rows['clinic_id'] . "'");
                        if ($doctor_rows['speciality'] == 0) {
                            $doctor[$d]['speciality'] = 'General Practitioner';
                        } else {
                            $doctor[$d]['speciality'] = $PDO->getSingleResult("select name from #_specialities where pid='" . $doctor_rows['speciality'] . "'");
                        }
                    }
                    if ($lang_flag == "my") {
                        $doctor[$d]['hospital'] = $PDO->getSingleResult("select name_be from #_hospitals where pid='" . $doctor_rows['hospital_id'] . "'");
                        $doctor[$d]['hospital_phone'] = $PDO->getSingleResult("select name_be from #_hospitals where pid='" . $doctor_rows['hospital_id'] . "'");
                        $doctor[$d]['clinic'] = $PDO->getSingleResult("select name_be from #_clinics where pid='" . $doctor_rows['clinic_id'] . "'");
                        $doctor[$d]['clinic_phone'] = $PDO->getSingleResult("select name_be from #_clinics where pid='" . $doctor_rows['clinic_id'] . "'");
                        if ($doctor_rows['speciality'] == 0) {
                            $doctor[$d]['speciality'] = 'General Practitioner';
                        } else {
                            $doctor[$d]['speciality'] = $PDO->getSingleResult("select name_my from #_specialities where pid='" . $doctor_rows['speciality'] . "'");
                        }
                    }

                    $doctor[$d]['user_id'] = $doctor_rows['pid'];
                    if ($lang_flag == "en") {
                        $doctor[$d]['name'] = $doctor_rows['name'];
                        $doctor[$d]['location'] = $doctor_rows['location'];
                    }
                    if ($lang_flag == "my") {
                        $doctor[$d]['name'] = $doctor_rows['name_my'];
                        $doctor[$d]['location'] = $doctor_rows['location_my'];
                    }
                    $doctor[$d]['email'] = $doctor_rows['email'];
                    $doctor[$d]['phone'] = $doctor_rows['phone'];
                    $doctor[$d]['sex'] = $doctor_rows['sex'];
                    $doctor[$d]['date_of_birth'] = $doctor_rows['date_of_birth'];
                    $doctor[$d]['doctor_type'] = $doctor_rows['doctor_type'];
                    $doctor[$d]['rs_number'] = $doctor_rows['rs_number'];
                    $doctor[$d]['fees'] = $doctor_rows['fees'];
                    $doctor[$d]['education'] = $doctor_rows['education'];
                    $doctor[$d]['area_code'] = $doctor_rows['area_code'];
                    $doctor[$d]['address'] = $doctor_rows['address'];
                    $doctor[$d]['live_status'] = $doctor_rows['live_status'];
                    $doctor[$d]['dates'] = $doctor_rows['dates'];
                    if ($lang_flag == "en") {
                        $doctor[$d]['township'] = $PDO->getSingleResult("select name from #_township where pid='" . $doctor_rows['township'] . "'");
                        $doctor[$d]['division'] = $PDO->getSingleResult("select name from #_division where pid='" . $doctor_rows['division'] . "'");
                    }
                    if ($lang_flag == "my") {
                        $doctor[$d]['township'] = $PDO->getSingleResult("select name_my from #_township where pid='" . $doctor_rows['township'] . "'");
                        $doctor[$d]['division'] = $PDO->getSingleResult("select name_my from #_division where pid='" . $doctor_rows['division'] . "'");

                    }
                    $doctor[$d]['isHomeservice'] = $doctor_rows['isHomeservice'];
                    $doctor[$d]['isChatNeeded'] = $doctor_rows['isChatNeeded'];
                    if (($doctor_rows ['profile_image'] == NULL) || ($doctor_rows ['profile_image'] == '')) {
                        $doctor[$d] ['profile_image'] = '';
                    } else {
                        $doctor[$d] ['profile_image'] = SITE_PATH_ADM . "uploaded_files/doctors/profile/" . $doctor_rows ['profile_image'];
                    }


                    $d++;
                }
                $json['doctor'] = $doctor;

                $json['status'] = 'true';

            } else {
                $json['status'] = 'false';
            }


        }
        break;

    default :
        break;
}
/* Output header */
// header('Content-type: application/json');
// echo json_encode($_POST);
echo json_encode($json);
?>