<?php
// error_reporting ( - 1 );

// initiate doctor object
date_default_timezone_set('Asia/Kolkata');
include("../lib/open.inc.php");

function alexa_functions($param)
{
    include 'class/all_class_files.php';
    include_once 'notification/GCM.php';
    $gcm = new GCM ();
    $m = new MyMail ();
    $json = array();
    $doctor_function = new doctor ();

    switch ($param['flag']) {

        case 'doctors_list_based_on_speciality':

            if (isset($param['problem'])) {
                $sql = "select * from #_cause_speciality_mapping  where  cause like '%" . $param['problem'] . "%' and status=1 limit 0,1 ";
                $speciality_query = $PDO->db_query($sql);
                if (mysql_num_rows($speciality_query) > 0) {
                    $row = $PDO->db_fetch_array($speciality_query);
                    $speciality = '';
                    $wh = '';
                    $speciality = $row ['speciality_id'];
                    $lang_flag = 'en';
                    if ($speciality != '') {
                        $wh .= " and D.speciality =" . $speciality . "";
                    }
                }

            }
            if (isset($param['speciality_search'])) {
                $json['speciality']='';
                $sql = "select * from #_specialities where  name like '%" . $param['speciality_search'] . "%' and status=1 limit 0,1 ";
                $speciality_query = $PDO->db_query($sql);
                if (mysql_num_rows($speciality_query) > 0) {
                    $row = $PDO->db_fetch_array($speciality_query);
                    $speciality = '';
                    $wh = '';
                    $speciality = $row ['pid'];
                    $lang_flag = 'en';
                    if ($speciality != '') {
                        $json['speciality'] = $speciality;
                        $wh .= " and D.speciality =" . $speciality . "";
                    }
                }

            }
            if (isset($param['doctor_name'])) {

                    $wh .= " and D.name like'%" . $param['doctor_name'] . "%' ";
            }
            $json['doctor'] = array();

            date_default_timezone_set("Asia/Kolkata");
            $today_date = date("Y-m-d");

            //$wh .= "   and D.is_verified=1 and dl.is_verified=1 and dl.status=1 and dl.isDeleted=0 and dl.available_status=1";
            $wh .= "   and D.is_verified=1 and dl.is_verified=1 and dl.status=1 and dl.isDeleted=0 ";

           $query = "select D.pid,D.speciality,D.name,D.name_my,D.email,D.phone,D.sex,D.date_of_birth,D.doctor_type,D.rs_number,D.fees,D.education,D.area_code,D.address,D.live_status,D.township,D.division,D.isHomeservice,D.isChatNeeded,D.profile_image, dl.hospital_id,dl.clinic_id,dl.location,dl.location_my,dl.dates ,dl.available_status from #_doctors as D LEFT JOIN #_doctors_locations as dl ON dl.doctor_id = D.pid  where  D.status=1 " . $wh . " group by D.pid order by D.pid, D.name asc limit 0,10";//limit ". $start.",".$pagesize

            $doctor_query = $PDO->db_query($query);

            if (mysql_num_rows($doctor_query) > 0) {
                $doctor = array();
                $d = 0;
                $lang_flag='en';
                $doctor_details_name_location = array();
                while ($doctor_rows = $PDO->db_fetch_array($doctor_query)) {
                    // get the doctor location details from doctor_locations table
                    $doctors_location_list_values = $doctor_function->get_doctors_location_verified_list_custom($doctor_rows['pid'], $lang_flag);
                   // if(!empty($doctors_location_list_values)){
                        // $doctor[$d]['doctors_location_list'] = $doctors_location_list_values;
                        if ($lang_flag == "en") {
                            // $doctor[$d]['hospital'] = $PDO->getSingleResult("select name from #_hospitals where pid='" . $doctor_rows['hospital_id'] . "'");
                            //   $doctor[$d]['hospital_phone'] = $PDO->getSingleResult("select phone from #_hospitals where pid='" . $doctor_rows['hospital_id'] . "'");
                            //   $doctor[$d]['clinic'] = $PDO->getSingleResult("select name from #_clinics where pid='" . $doctor_rows['clinic_id'] . "'");
                            //   $doctor[$d]['clinic_phone'] = $PDO->getSingleResult("select phone from #_clinics where pid='" . $doctor_rows['clinic_id'] . "'");
                            if ($doctor_rows['speciality'] == 0) {
                                $doctor[$d]['doctorSpeciality'] = 'General Practitioner';
                            } else {
                                $doctor[$d]['doctorSpeciality'] = $PDO->getSingleResult("select name from #_specialities where pid='" . $doctor_rows['speciality'] . "'");
                            }
                        }
                        if ($lang_flag == "my") {
                            //  $doctor[$d]['hospital'] = $PDO->getSingleResult("select name_be from #_hospitals where pid='" . $doctor_rows['hospital_id'] . "'");
                            //  $doctor[$d]['hospital_phone'] = $PDO->getSingleResult("select name_be from #_hospitals where pid='" . $doctor_rows['hospital_id'] . "'");
                            //  $doctor[$d]['clinic'] = $PDO->getSingleResult("select name_be from #_clinics where pid='" . $doctor_rows['clinic_id'] . "'");
                            //  $doctor[$d]['clinic_phone'] = $PDO->getSingleResult("select name_be from #_clinics where pid='" . $doctor_rows['clinic_id'] . "'");
                            if ($doctor_rows['speciality'] == 0) {
                                $doctor[$d]['doctorSpeciality'] = 'General Practitioner';
                            } else {
                                $doctor[$d]['doctorSpeciality'] = $PDO->getSingleResult("select name_my from #_specialities where pid='" . $doctor_rows['speciality'] . "'");
                            }
                        }

                        $doctor[$d]['doctorId'] = $doctor_rows['pid'];
                        if ($lang_flag == "en") {
                            $doctor[$d]['doctorName'] = $doctor_rows['name'];
                        }
                        if ($lang_flag == "my") {
                            $doctor[$d]['doctorName'] = $doctor_rows['name_my'];
                        }
                        $doctor[$d]['email'] = $doctor_rows['email'];
                        $doctor[$d]['phone'] = $doctor_rows['phone'];
                        $doctor[$d]['sex'] = $doctor_rows['sex'];

                        $doctor[$d]['doctorEducation'] = $doctor_rows['education'];
                        $doctor[$d]['doctorExperience'] = ($doctor_rows['years_of_experience']==null) ? '' : $doctor_rows['years_of_experience'] ;
                        if ($lang_flag == "en") {
                            //$doctor[$d]['township'] = $PDO->getSingleResult("select name from #_township where pid='" . $doctor_rows['township'] . "'");
                            //$doctor[$d]['division'] = $PDO->getSingleResult("select name from #_division where pid='" . $doctor_rows['division'] . "'");
                        }
                        if ($lang_flag == "my") {
                            //$doctor[$d]['township'] = $PDO->getSingleResult("select name_my from #_township where pid='" . $doctor_rows['township'] . "'");
                            //$doctor[$d]['division'] = $PDO->getSingleResult("select name_my from #_division where pid='" . $doctor_rows['division'] . "'");

                        }

                        if (($doctor_rows ['profile_image'] == NULL) || ($doctor_rows ['profile_image'] == '')) {
                            $doctor[$d] ['doctoImage'] = '';
                        } else {
                            $doctor[$d] ['doctoImage'] = SITE_PATH_ADM . "uploaded_files/doctors/profile/" . $doctor_rows ['profile_image'];
                        }
                        $d++;

                    //}
                    //text creation for amazon alexa
                    $location = explode(",,,", $doctor_rows['location']);
                    if (strpos($location[0], "~") != 0) {
                        $location = explode("~", $location[0]);
                    }
                    $doctor_details_name_location[] = $doctor_rows['name'] . ' From ' . $location[0];


                }
                $json['doctor'] = $doctor;
                $json['doctor_details_name_location'] = $doctor_details_name_location;
                if(empty($json['doctor'])){
                    $json['status'] = 'false';
                    $json['msg'] = 'No Doctors Available';
                }else{
                    $json['status'] = 'true';
                    $json['msg'] = 'Doctors Available';
                }

            } else {
                $json['doctor'] = array();
                $json ['status'] = 'false';

            }


            break;


        default :
            break;
    }


    //  return json_encode($json);
    return $json;
}

/*
$params = array(
            'flag' => 'doctors_list_based_on_speciality',
            'doctor_name' => "raja"
        );
$doctor_results = alexa_functions($params);
print_r($doctor_results);
exit;

*/

?>