<?php
include 'alexa_functions.php';
// Include the library
require __DIR__ . '/alexa-endpoint/autoload.php';

/**
 * Import classes
 * Note, if there is already a class named 'User' in your scripts, use this:
 * use MayBeTall\Alexa\Endpoint\User as AlexaUser;
 * Then use 'AlexaUser' instead of 'User'.
 */
use MayBeTall\Alexa\Endpoint\Alexa;
use MayBeTall\Alexa\Endpoint\User;

// Sets everything up.
Alexa::init();

// User launched the skill.
Alexa::enters(function () {
    // Say something, wait for an answer, ask again if no reply is given
    Alexa::ask('Welcome to Izohealth. May i know your name?', "I'm sorry, what is your name?");
});

// User triggered the 'NameIntent' intent from the Skill Builder
User::triggered('NameIntent', function () {
    // Get the slot named 'name' sent by the Skill Builder
    $name = User::stated('name');

    // If user stated their name, continue
    if ($name) {
        // Remember the user's name
        Alexa::remember('name', $name);

        // Ask about coffee and use SSML to make Alexa sound mad if the user doesn't respond.
        // See https://developer.amazon.com/public/solutions/alexa/alexa-skills-kit/docs/speech-synthesis-markup-language-ssml-reference#emphasis
        Alexa::ask("Hi $name, I'm Alexa. How may i help you?",
            // Say something if the user doesn't respond
            '<speak>
				<say-as interpret-as="interjection">ahem</say-as>,
				I <emphasis level="reduced">said</emphasis>,
				How may i help you?
			</speak>'
        );
    } else {
        // If the user didn't say their name, say something then end the skill.
        Alexa::say("Fine, don't tell me your name.");
    }
});
/*
User::triggered('CoffeeIntent', function () {
    // Get the 'answer' slot.
    $answer = User::stated('answer');

    // Get the name we remembered (if the user didn't skip directly to the coffee intent)
    $name = Alexa::recall('name');

    // If the user never gave a name, handle it by saying something else instead.
    if (!$name) {
        $name = "and I don't even know your name";
    }

    // If answer was provided.
    if ($answer) {
        if ($answer == 'yes') {
            // If user said yes.
            Alexa::say("That's great! I like you already $name.");
        } else {
            // If user said anything else.
            Alexa::say("Alright, I already dislike you $name.");
        }
    } else {
        // If answer was not provided.
        Alexa::say("Okay, bye!");
    }
});

*/
User::triggered('DefaultWelcomeIntent', function () {
    $name = Alexa::recall('name');
    if (!$name) {
        Alexa::say("Hi,$name. How May i help you?");
    }else{
        Alexa::ask('Welcome to Izohealth. May i know your name?', "I'm sorry, what is your name?");
    }

});

User::triggered('CauseIntent', function () {
    $problem = User::stated('problem');
    if ($problem) {
        Alexa::remember('problem', $problem);
    }
    //Alexa::remember('test_result', $doctor_results);
    $params = array(
        'flag' => 'doctors_list_based_on_speciality',
        'problem' => 'fever'
    );
    $doctor_results = alexa_functions($params);
    Alexa::remember('doctor_status', $doctor_results['status']);
    Alexa::remember('doctor_results', $doctor_results);

    if ($doctor_results['status'] == 'true') {
        $doctor_results = implode(" and ", $doctor_results['doctor_details_name_location']);
        $text = 'These are the Best doctors regarding your cause ' . $doctor_results;
        Alexa::say($text);
    } else {
        $text = '<speak>
				<say-as interpret-as="interjection">Hmm</say-as>,
				<emphasis level="reduced">I am really sorry</emphasis>,
				It seems no doctors available now Try search with Izohealth app
			</speak>';
        Alexa::say($text);
    }
    //Search Doctor
});

User::triggered('doctorSearchIntent', function () {
    $doctor_name = Alexa::recall('doctor_name');
    // If the user never gave a name, handle it by saying something else instead.
    if ($doctor_name) {
        // Doctor Name is confirmed. Check the user details
        $phone_number_of_user = Alexa::recall('phone_number_of_user');


    } else {
        //Search the doctor
        // Get the slot named 'name' sent by the Skill Builder
        $doctor_search_name = User::stated('doctor_name');
        // If user stated their name, continue

        if ($doctor_search_name) {
            // Remember the user's name
            Alexa::remember('search_doctor_name', $doctor_search_name);
            //Find the doctor
            $params = array(
                'flag' => 'doctors_list_based_on_speciality',
                'doctor_name' => $doctor_search_name
            );
            $doctor_results = alexa_functions($params);

            Alexa::remember('doctor_results', $doctor_results);
            $count_of_doctors_result = count($doctor_results['doctor_details_name_location']);
            Alexa::remember('doctor_results_count', $count_of_doctors_result);

            if ($doctor_results['status'] == 'true') {
                $doctor_results = implode(" and ", $doctor_results['doctor_details_name_location']);
                $text = 'These are the Best doctors regarding your search ' . $doctor_results;
                Alexa::say($text);
            } else {
                $text = '<speak>
				<say-as interpret-as="interjection">hmm</say-as>,
				<emphasis level="reduced">I am really sorry</emphasis>,
				It seems There is no doctor is there in that name. Try search with Izohealth app
			</speak>';
                Alexa::say($text, 'If you specify any speciality then i can able find doctors for you');
            }

        } else {
            //general search
            //  Alexa::ask("specify the speciality of doctor you want to search?");
            Alexa::say('Please search with a doctor name');
        }

    }


});

User::triggered('doctorSearchSpecialityIntent', function () {
    $speciality_name = Alexa::recall('speciality');

    // If the user never gave a name, handle it by saying something else instead.
    if ($speciality_name) {
        //speciality_name  is confirmed. Check the doctor details

    } else {
        //Search the doctor
        // Get the slot named 'name' sent by the Skill Builder
        $speciality_name = User::stated('speciality_name');
        // If user stated their name, continue

        if ($speciality_name) {
            // Remember the user's name
            Alexa::remember('speciality_search_name', $speciality_name);
            //Find the doctor
            $params = array(
                'flag' => 'doctors_list_based_on_speciality',
                'speciality_search' => $speciality_name
            );
            $doctor_results = alexa_functions($params);

            if (!empty($doctor_results['speciality'])) {
                Alexa::remember('speciality', $doctor_results['speciality']);
            }
            Alexa::remember('doctor_results', $doctor_results);
            $count_of_doctors_result = count($doctor_results['doctor_details_name_location']);
            Alexa::remember('doctor_results_count', $count_of_doctors_result);

            if ($doctor_results['status'] == 'true') {
                $doctor_results = implode(" and ", $doctor_results['doctor_details_name_location']);
                $text = 'These are the Best doctors regarding your search ' . $doctor_results;
                Alexa::say($text);
            } else {
                $text = '<speak>
				<say-as interpret-as="interjection">hmm</say-as>,
				<emphasis level="reduced">I am really sorry</emphasis>,
				It seems There is no doctor is there in that Speciality. Try search with Izohealth app
			</speak>';
                Alexa::say($text, 'If you specify any speciality then i can able find doctors for you');
            }

        } else {
            //general search
            //  Alexa::ask("specify the speciality of doctor you want to search?");
            Alexa::say('Please search with doctor Speciality');
        }

    }

});

User::triggered('userPhoneNumberIntent', function () {
    Alexa::ask("May i know what you are suffering from?");
});
User::triggered('doctorAvailableCheck', function () {
    Alexa::ask("May i know what you are suffering from?");
});
User::triggered('doctorBooking', function () {
    Alexa::ask("Search for a doctor name or speciality");

});
User::triggered('confirmDoctorIntent', function () {
    Alexa::ask("May i know what you are suffering from?");

});
User::triggered('doctorConfirmbookingIntent', function () {
    Alexa::ask("May i know what you are suffering from?");

});

User::triggered('AMAZON.FallbackIntent', function () {
    Alexa::ask("Choose your words related to Izohealth");
});

//Booking Intents and codes
/*User::triggered('userPhoneNumberIntent', function () {
    $user_phone_number = User::stated('{user_phone_number}');
    if ($user_phone_number) {
        Alexa::remember('user_phone_number', $user_phone_number);
        $doctor_name = Alexa::recall('doctor_name');
        if ($doctor_name) {
            Alexa::say('confirm');

        } else {
            Alexa::say('Please search with a doctor name.');

        }
    } else {
        Alexa::say('Please tell me your registered phone number with Izohealth.');
    }

    //Alexa::remember('test_result', $doctor_results);
    $params = array(
        'flag' => 'doctors_list_based_on_speciality',
        'problem' => 'fever'
    );
    $doctor_results = alexa_functions($params);
    Alexa::remember('doctor_status', $doctor_results['status']);
    Alexa::remember('doctor_results', $doctor_results);

    if ($doctor_results['status'] == 'true') {
        $doctor_results = implode(" and ", $doctor_results['doctor_details_name_location']);
        $text = 'These are the Best doctors regarding your cause ' . $doctor_results;
        Alexa::say($text);
    } else {
        $text = '<speak>
				<say-as interpret-as="interjection">Hmm</say-as>,
				<emphasis level="reduced">I am really sorry</emphasis>,
				It seems no doctors available now Try search with Izohealth app
			</speak>';
        Alexa::say($text);
    }

    //Search Doctor
});*/
/*User::triggered('doctorAvailableCheck', function () {
    $doctor_name = Alexa::recall('doctor_name');
    if (!empty($doctor_name)) {
        //check available time slots for the user
        Alexa::say("Doctors available time is monday 10.30 am to 11.30 am and tuesday 10.30 am to 12.30 pm");

    } else {
        Alexa::ask("Please search the doctor and confirm the doctor.");
    }
});*/

/*User::triggered('doctorBooking', function () {
    $doctor_search_name = User::stated('doctor_name');
    if ($doctor_search_name) {
        Alexa::remember('doctor_search_name', $doctor_search_name);

        //Find all the doctor
        $doctor_name = Alexa::recall('doctor_name');
        if (!empty($doctor_name)) {
            //confirm with doctor
            Alexa::ask("do you want to confirm book an appointment with $doctor_name",'are you sure?');
        } else {
            //search the doctor
            //  Alexa::ask("do you want to search doctor with the name $doctor_search_name");
          //  User::callIntent('doctorConfirmbookingIntent');
            Alexa::ask("do you want to search name with doctor $doctor_search_name",'i said, do you want to search name with doctor $doctor_search_name?');
        }


    } else {
        Alexa::ask("Tell me the doctor name i will search for you");
    }


    $text = '<speak>
				<say-as interpret-as="interjection">hmm</say-as>,
				<emphasis level="reduced">I am really sorry</emphasis>,
				It seems no doctors available now. Try search with Izohealth app
			</speak>';
    Alexa::ask($text);

});*/

/*User::triggered('confirmDoctorIntent', function () {
    $confirm_doctor = User::stated('confirm_doctor');
    if ($confirm_doctor == 'yes') {
        Alexa::ask("doctor name is confirmed.","Can i check for the doctor availability?");
    }else{
        Alexa::ask("Please tell me the exact doctor name which is recommanded.","Please search for the exact doctor name?");
    }
});*/

/*User::triggered('doctorConfirmbookingIntent', function () {
    //
    $doctor_id = Alexa::recall('doctor_id');
    if (empty($doctor_id)) {
        Alexa::ask("Doctor is not selected.Please Search a doctor ", "Please search for a doctor.");
    }
    $doctor_location_id = Alexa::recall('doctor_location_id');
    if (empty($doctor_location_id)) {
        Alexa::ask("Doctor Location is not selected.Please confirm doctor location ", "Tell me the doctor location name");
    }
    $health_issues = Alexa::recall('problem');
    if (empty($health_issues)) {
        Alexa::ask("what you are suffering from?", "what is your cause?");
    }
    $visited_doctor_before = Alexa::recall('visited_doctor_before');
    if (empty($visited_doctor_before)) {
        Alexa::ask("Did you visited doctor before", "I said, did you visited doctor before?");
    }
    $patient_id = Alexa::recall('patient_id');
    if (empty($patient_id)) {
        Alexa::ask("What is your phone number which is registered in Izohealth?", "I said, What is your phone number which is registered in Izohealth");
    }
    $app_date = Alexa::recall('app_date');
    if (empty($app_date)) {
        Alexa::ask("what is the date of the appointment you want to book?", "I said, what is the date of the appointment you want to book");
    }
    $form_time = Alexa::recall('form_time');
    if (empty($visited_doctor_before)) {
        Alexa::ask("what is the time of the appointment you want to book?", "I said, what is the time of the appointment you want to book?");
    }
    $appointment_type=6;
    if(!empty($doctor_id) && !empty($doctor_location_id) && !empty($doctor_location_id) && !empty($doctor_location_id)&& !empty($doctor_location_id)&& !empty($doctor_location_id)&& !empty($doctor_location_id)){
        Alexa::say("Booking confirmed");
    }else{
        Alexa::say("Please search doctors first");
    }


});*/


// User exited the skill.
Alexa::exits(function () {
  //  Alexa::ask("No match. May i know what you are suffering from?","Please search the doctors with name or speciality");
    /**
     * Alexa will not say anything you send in reply, but it is important
     * to have this here because she will give an error message if we
     * don't acknowledge the skill's exit.
     */
    Alexa::say('exit from the Izohealth skill');
});


