<?php
include("../lib/open.inc.php");
include 'class/all_class_files.php';
$flag = mysql_real_escape_string($_POST['flag']);
$json = array();
$pagesize = 100;
$doctor_function = new doctor();
switch ($flag) {

    case 'lab_services_category_list':
        $lang_flag = mysql_real_escape_string($_POST['lang_flag']);
        $wh = '';
        $lab_query = $PDO->db_query("select * from #_services_cat where  status=1 and service_type=1 " . $wh . " order by name");
        if (mysql_num_rows($lab_query) > 0) {
            $lab_services_category = array();
            $d = 0;
            while ($lab_rows = $PDO->db_fetch_array($lab_query)) {

                $lab_services_category[$d]['pid'] = $lab_rows['pid'];
                if ($lang_flag == "en") {
                    $lab_services_category[$d]['name'] = $lab_rows['name'];
                }
                if ($lang_flag == "my") {
                    $lab_services_category[$d]['name'] = $lab_rows['name_my'];
                }
                $d++;
            }

            $json['lab_services_category'] = $lab_services_category;

            //fav
            $lab_services_category=13;
            $lab_query = $PDO->db_query("select * from #_service_subcat where  status=1  and service_cat ='" . $lab_services_category . "' order by name limit 0,4 ");
            if (mysql_num_rows($lab_query) > 0) {
                $lab_fav_services = array();
                $d = 0;
                while ($lab_rows = $PDO->db_fetch_array($lab_query)) {

                    $lab_fav_services[$d]['pid'] = $lab_rows['pid'];
                    $lab_fav_services[$d]['service_cat'] = $lab_rows['service_cat'];
                    if ($lang_flag == "en") {
                        $lab_fav_services[$d]['name'] = $lab_rows['name'];
                    }
                    if ($lang_flag == "my") {
                        $lab_fav_services[$d]['name'] = $lab_rows['name_my'];
                    }
                    $d++;
                }
            }

            $json['lab_fav_services'] = $lab_fav_services;
            $json['status'] = 'true';
        } else {
            $json['lab_services_category'] = array();
            $json['status'] = 'false';
        }
        break;
    case 'lab_services_list_based_on_category':
        $lab_services_category = mysql_real_escape_string($_POST['lab_services_category']);
        $lang_flag = mysql_real_escape_string($_POST['lang_flag']);
        $lab_query = $PDO->db_query("select * from #_service_subcat where  status=1  and service_cat ='" . $lab_services_category . "' order by name ");
        if (mysql_num_rows($lab_query) > 0) {
            $lab_services = array();
            $d = 0;
            while ($lab_rows = $PDO->db_fetch_array($lab_query)) {

                $lab_services[$d]['pid'] = $lab_rows['pid'];
                if ($lang_flag == "en") {
                    $lab_services[$d]['name'] = $lab_rows['name'];
                }
                if ($lang_flag == "my") {
                    $lab_services[$d]['name'] = $lab_rows['name_my'];
                }
                $d++;


            }
            $json['lab_services'] = $lab_services;
            $json['status'] = 'true';
        } else {
            $json['lab_services'] = array();
            $json['status'] = 'false';
        }
        break;

    case 'lab_list_based_on_category_and_service':
        $lab_services_category_id = mysql_real_escape_string($_POST['lab_services_category_id']);
        $lab_services_id = mysql_real_escape_string($_POST['lab_services_id']);
        $lang_flag = mysql_real_escape_string($_POST['lang_flag']);
        if (empty($lab_services_category_id) || (empty($lab_services_id)) || (empty($lang_flag))) {
            $json ['status'] = 'false';
            $json ['msg'] = 'Please send the values.';
            $doctor_function->send_res($json);
            exit ();
        }
        //this query runs more than on mysql 5.7
        $sql="SELECT *  FROM `#_labs` WHERE (JSON_CONTAINS(left_services, '\"y\"', '$.\"24\".\"132\"'))=1 and status=1 order by name ";
        $lab_query = $PDO->db_query($sql);
        if (mysql_num_rows($lab_query) > 0) {
            $lab_list = array();
            $d = 0;
            while ($lab_rows = $PDO->db_fetch_array($lab_query)) {

                $lab_list[$d]['lab_id'] = $lab_rows['pid'];
                if ($lang_flag == "en") {
                    $lab_list[$d]['lab_name'] = $lab_rows['name'];
                    $lab_list[$d]['lab_location'] = $lab_rows['address'];
                    $lab_list[$d]['nearest_land_mark'] = $lab_rows['nearest_land_mark'];
                    $lab_list[$d]['nearest_bus_stop'] = $lab_rows['nearest_bus_stop'];
                    $lab_list[$d]['nearest_bus_stop'] = $lab_rows['nearest_bus_stop'];
                }
                if ($lang_flag == "my") {
                    $lab_list[$d]['lab_name'] = $lab_rows['name_my'];
                    $lab_list[$d]['lab_location'] = $lab_rows['address_be'];
                    $lab_list[$d]['nearest_land_mark_be'] = $lab_rows['nearest_land_mark_be'];
                    $lab_list[$d]['nearest_bus_stop_be'] = $lab_rows['nearest_bus_stop_be'];
                }
                $lab_list[$d]['email'] = $lab_rows['email'];
                $lab_list[$d]['image'] = $lab_rows['image'];
                $lab_list[$d]['image'] = $lab_rows['image'];
                $lab_list[$d]['open_time'] = $lab_rows['open_time'];
                $lab_list[$d]['close_time'] = $lab_rows['close_time'];
                $lab_list[$d]['latlong'] = $lab_rows['latlong'];
                $lab_list[$d]['description_of_test'] = '';
                $d++;
            }
            $json['lab_list'] = $lab_list;
            $json['status'] = 'true';
        } else {
            $json['lab_list'] = array();
            $json['status'] = 'false';
        }


        break;

    case 'save_patient_lab_test_details':
        $patient_id = mysql_real_escape_string($_POST ['patient_id']);
        $doctor_id = mysql_real_escape_string($_POST ['doctor_id']);
        $call_details_id = mysql_real_escape_string($_POST ['call_details_id']);
        $lab_id = mysql_real_escape_string($_POST ['lab_id']);
        $lab_test_cat_id = mysql_real_escape_string($_POST ['lab_test_cat_id']);
        $lab_test_sub_cat_id = mysql_real_escape_string($_POST ['lab_test_sub_cat_id']);
        $appointment_id = $PDO->getSingleResult("select pid from #_appointment where call_details_id='" . $call_details_id . "'");
        $modified_on = date('Y-m-d H:i:s');
        if(empty($patient_id) || empty($doctor_id) || empty($call_details_id) || empty($lab_id) || empty($lab_test_sub_cat_id) || empty($appointment_id)){
            $json ['status'] = 'false';
            $json ['msg'] = 'Please send the values.';
            $doctor_function->send_res($json);
            exit ();
        }

        $patient_lab_test_details = array(
            'patient_id' => $patient_id,
            'doctor_id' => $doctor_id,
            'appointment_id' => $appointment_id,
            'call_details_id' => $call_details_id,
            'lab_id' => $lab_id,
            'lab_test_cat_id' => $lab_test_cat_id,
            'lab_test_sub_cat_id' => $lab_test_sub_cat_id,
            'modified_on' => $modified_on
        );
        $result = $PDO->sqlquery("rs", 'patient_lab_test_details', $patient_lab_test_details);
        if (!empty($result)) {
            $json ['status'] = 'true';
            $json ['msg'] = 'Success!! Patient Lab test details added.';
        } else {
            $json ['status'] = 'false';
            $json ['msg'] = 'Failure!! Patient Lab test details not added.';
        }
        break;


    default:
        break;
}


/* Output header */
//header('Content-type: application/json');
//echo json_encode($_POST);
echo json_encode($json);
?>