 
    "use strict";
    // This is for Vertical carousel
    $('.vcarousel').carousel({
        interval: 3000
    })


    //gauge chart
    $(".mtbutton").on("click", function () {
        var randomNum = Math.floor((Math.random() * 100));
        $('#gaugeDemo .gauge-arrow').trigger('updateGauge', randomNum);
    });
    $('#gaugeDemo .gauge-arrow').cmGauge();


    //chartist-chart
    var chart = new Chartist.Line('#ct-sales', {
        labels: ['1', '2', '3', '4', '5', '6'],
        series: [
    [1, -2, 5, 2, 6, 5.5]

  ]
    }, {
        showArea: true,
        showPoint: true,

        chartPadding: {
            left: -40
        },
        axisX: {
            showLabel: false,
            showGrid: false
        },
        axisY: {
            showLabel: false,
            showGrid: true
        },
        fullWidth: true,
        plugins: [
    Chartist.plugins.tooltip()
  ]
    });
    chart.on('draw', function (data) {

        if (data.type === 'line' || data.type === 'area') {
            data.element.animate({

                d: {
                    begin: 2000 * data.index,
                    dur: 2000,
                    from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                    to: data.path.clone().stringify(),
                    easing: Chartist.Svg.Easing.easeOutQuint
                }
            });
        }
    });


    // ct-weather
    var chart = new Chartist.Line('#ct-weather', {
        labels: ['1', '2', '3', '4', '5', '6'],
        series: [
    [1, 0, 5, 3, 2, 2.5]

  ]
    }, {
        showArea: true,
        showPoint: false,

        chartPadding: {
            left: -20
        },
        axisX: {
            showLabel: false,
            showGrid: false
        },
        axisY: {
            showLabel: false,
            showGrid: true
        },
        fullWidth: true,

    });


    //extra-chart
    var chart = new Chartist.Line('#ct-extra', {
        labels: ['1', '2', '3', '4', '5', '6'],
        series: [
    [1, -2, 5, 3, 0, 2.5]

  ]
    }, {
        showArea: true,
        showPoint: true,
        height: 100,
        chartPadding: {
            left: -20,
            top: 10,
        },
        axisX: {
            showLabel: false,
            showGrid: true
        },
        axisY: {
            showLabel: false,
            showGrid: false
        },
        fullWidth: true,
        plugins: [
    Chartist.plugins.tooltip()
  ]
    });


    //ct-main-balance-chart
    var chart = new Chartist.Line('#ct-main-bal', {
        labels: ['1', '2', '3', '4', '5', '6', '7', '8', '9'],
        series: [
    [1, 2, 5, 3, 4, 2.5, 5, 3, 1],
    [1, 4, 2, 5, 2, 5.5, 3, 4, 1]
   ]

    }, {
        showArea: true,
        showPoint: true,
        height: 100,
        chartPadding: {
            left: -20,
            top: 10,
        },
        axisX: {
            showLabel: false,
            showGrid: false
        },
        axisY: {
            showLabel: false,
            showGrid: false
        },
        fullWidth: true,
        plugins: [
    Chartist.plugins.tooltip()
  ]
    });


    //ct-bar-chart
    new Chartist.Bar('#ct-bar-chart', {
        labels: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'],
        series: [
    [5, 4, 3, 7, 5, 2, 3]

  ]
    }, {
        axisX: {
            showLabel: false,
            showGrid: false,
            // On the x-axis start means top and end means bottom
            position: 'start'
        },

        chartPadding: {
            top: -20,
        },
        axisY: {
            showLabel: false,
            showGrid: false,
            // On the y-axis start means left and end means right
            position: 'end'
        },
        height: 65,
        plugins: [
    Chartist.plugins.tooltip()
  ]
    });


    //ct-visits
    new Chartist.Line('#ct-visits', {
        labels: ['12AM', '2AM', '6AM', '9AM', '12AM', '3PM', '6PM', '9PM'],
        series: [
    [5, 2, 7, 4, 5, 3, 5, 4],
    [2, 5, 2, 6, 2, 5, 2, 4]
  ]
    }, {
        top: 0,

        low: 1,
        showPoint: true,
        height: 210,
        fullWidth: true,
        plugins: [
    Chartist.plugins.tooltip()
  ],
        axisY: {
            labelInterpolationFnc: function (value) {
                return (value / 1) + 'k';
            }
        },
        showArea: true
    });


    //ct-weather
    new Chartist.Line('#ct-city-wth', {
        labels: ['12AM', '2AM', '6AM', '9AM', '12AM', '3PM', '6PM', '9PM'],
        series: [
    [5, 2, 7, 4, 5, 3, 5, 4]
  ]
    }, {
        chartPadding: {
            left: -20,
            top: 10,
        },
        low: 1,
        showPoint: true,
        height: 260,
        fullWidth: true,
        plugins: [
    Chartist.plugins.tooltip()
  ],
        axisX: {
            showLabel: true,
            showGrid: false
        },
        axisY: {
            showLabel: false,
            showGrid: false
        },
        showArea: true
    });


    //polar chart
    new Chartist.Line('#ct-polar-chart', {
        labels: [1, 2, 3, 4, 5, 6, 7, 8],
        series: [
    [1, 2, 3, 1, -2, 0, 1, 0],
    [-2, -1, -2, -1, -2.5, -1, -2, -1],
    [0, 0, 0, 1, 2, 2.5, 2, 1],
    [2.5, 2, 1, 0.5, 1, 0.5, -1, -2.5]
  ]
    }, {
        high: 3,
        low: -3,
        chartPadding: {
            left: -20,
            top: 10,
        },
        showArea: true,
        showLine: false,
        showPoint: true,
        fullWidth: true,
        plugins: [
    Chartist.plugins.tooltip()
  ],
        axisX: {
            showLabel: true,
            showGrid: true
        },
        axisY: {
            showLabel: false,
            showGrid: true
        }
    });


   

    // Real Time chart
    var data = [],
        totalPoints = 300;

    function getRandomData() {
        if (data.length > 0) data = data.slice(1);
        // Do a random walk
        while (data.length < totalPoints) {
            var prev = data.length > 0 ? data[data.length - 1] : 50,
                y = prev + Math.random() * 10 - 5;
            if (y < 0) {
                y = 0;
            } else if (y > 100) {
                y = 100;
            }
            data.push(y);
        }
        // Zip the generated y values with the x values
        var res = [];
        for (var i = 0; i < data.length; ++i) {
            res.push([i, data[i]])
        }
        return res;
    }
    // Set up the control widget
   


    // This is for Sparkline-chart

    var sparklineLogin = function () {
        $("#sparkline1dash").sparkline([0, 23, 43, 35, 44, 45, 56, 37, 40, 45, 56, 7, 10], {
            type: 'line',
            width: '100%',
            height: '70',
            lineColor: '#fff',
            fillColor: 'transparent',
            spotColor: '#fff',
            minSpotColor: undefined,
            maxSpotColor: undefined,
            highlightSpotColor: undefined,
            highlightLineColor: undefined
        });
        $('#sparkline2dash').sparkline([10, 12, 9, 6, 10, 9, 11, 9, 10, 12, 9, 11, 9, 10, 12, ], {
            type: 'bar',
            height: '70',
            barWidth: '5',
            resize: true,
            barSpacing: '10',
            barColor: '#fff'
        });
        $("#sparkline3dash").sparkline([0, 23, 43, 35, 44, 45, 56, 37, 40, 45, 56, 7, 10], {
            type: 'line',
            width: '100%',
            height: '70',
            lineColor: '#fff',
            fillColor: 'transparent',
            spotColor: '#fff',
            minSpotColor: undefined,
            maxSpotColor: undefined,
            highlightSpotColor: undefined,
            highlightLineColor: undefined
        });
        $('#sparkline4dash').sparkline([10, 12, 9, 6, 10, 9, 11, 9, 10, 12, 9, 11, 9, 10, 12, ], {
            type: 'bar',
            height: '70',
            barWidth: '5',
            resize: true,
            barSpacing: '10',
            barColor: '#fff'
        });
        $('#sales1').sparkline([20, 40, 30], {
            type: 'pie',
            height: '100',
            resize: true,
            sliceColors: ['#808f8f', '#fecd36', '#f1f2f7']
        });
        $('#sales2').sparkline([6, 10, 9, 11, 9, 10, 12], {
            type: 'bar',
            height: '154',
            barWidth: '4',
            resize: true,
            barSpacing: '10',
            barColor: '#2cabe3'
        });
        $("#sparkline8").sparkline([2, 4, 4, 6, 8, 5, 6, 4, 8, 6, 6, 2], {
            type: 'line',
            width: '100%',
            height: '50',
            lineColor: '#53e69d',
            fillColor: '#53e69d',
            maxSpotColor: '#53e69d',
            highlightLineColor: 'rgba(0, 0, 0, 0.2)',
            highlightSpotColor: '#53e69d'
        });
        $("#sparkline9").sparkline([0, 2, 8, 6, 8, 5, 6, 4, 8, 6, 6, 2], {
            type: 'line',
            width: '100%',
            height: '50',
            lineColor: '#2cabe3',
            fillColor: '#2cabe3',
            minSpotColor: '#2cabe3',
            maxSpotColor: '#2cabe3',
            highlightLineColor: 'rgba(0, 0, 0, 0.2)',
            highlightSpotColor: '#2cabe3'
        });
        $("#sparkline10").sparkline([2, 4, 4, 6, 8, 5, 6, 4, 8, 6, 6, 2], {
            type: 'line',
            width: '100%',
            height: '50',
            lineColor: '#ff754b',
            fillColor: '#ff754b',
            maxSpotColor: '#ff754b',
            highlightLineColor: 'rgba(0, 0, 0, 0.2)',
            highlightSpotColor: '#ff754b'
        });
		$("#sparkline11").sparkline([2, 4, 4, 6, 8, 5, 6, 4, 8, 6, 6, 2], {
            type: 'line',
            width: '100%',
            height: '50',
            lineColor: '#ff754b',
            fillColor: '#ff754b',
            maxSpotColor: '#ff754b',
            highlightLineColor: 'rgba(0, 0, 0, 0.2)',
            highlightSpotColor: '#ff754b'
        });


    }
    var sparkResize;

    $(window).on("resize", function (e) {
        clearTimeout(sparkResize);
        sparkResize = setTimeout(sparklineLogin, 100);
    });
    sparklineLogin();
 
var ctx2 = document.getElementById("chart2").getContext("2d");
    var data2 = {
         labels: ["Doctor 1", "Doctor 2", "Doctor 3", "Doctor 4", "Doctor 5"],
        datasets: [
            {
                label: "My First dataset",
                fillColor: "rgba(83,230,157,0.8)",
                strokeColor: "rgba(83,230,157,0.8)",
                highlightFill: "rgba(83,230,157,0.8)",
                highlightStroke: "rgba(83,230,157,0.8)",
                data: [10, 30, 80, 61, 26]
            },
            {
                label: "My Second dataset",
                fillColor: "rgba(255,118,118,0.8)",
                strokeColor: "rgba(255,118,118,0.8)",
                highlightFill: "rgba(255,118,118,0.8)",
                highlightStroke: "rgba(255,118,118,0.8)",
                data: [28, 48, 40, 19, 86]
            }
        ]
    };
    
    var chart2 = new Chart(ctx2).Bar(data2, {
        scaleBeginAtZero : true,
        scaleShowGridLines : true,
        scaleGridLineColor : "rgba(0,0,0,.005)",
        scaleGridLineWidth : 0,
        scaleShowHorizontalLines: true,
        scaleShowVerticalLines: true,
        barShowStroke : true,
        barStrokeWidth : 3, 
		barWidth : 0, 
 		tooltipCornerRadius: 2,
        barDatasetSpacing :3,
        legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        responsive: true
    });
