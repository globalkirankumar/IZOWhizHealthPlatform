
    "use strict";
    //gauge chart
    $(".mtbutton").on("click", function () {
        var randomNum = Math.floor((Math.random() * 100));
        $('#gaugeDemo .gauge-arrow').trigger('updateGauge', randomNum);
    });
    $('#gaugeDemo .gauge-arrow').cmGauge();
    //extra-chart
   
    // Morris donut chart
    Morris.Donut({
        element: 'morris-donut-chart',
        data: [{
            label: "practice 1",
            value: 15,
    }, {
            label: "practice 2",
            value: 15,
    }, {
            label: "practice 3",
            value: 35,
    }, {
            label: "practice 4",
            value: 105,
    }, {
            label: "practice 5",
            value: 35
        }],
        resize: true,
        colors: ['#ff7676', '#2cabe3', '#53e69d', '#7bcef3', '#ff754b']
		
    });





// Morris bar chart
    Morris.Bar({
        element: 'morris-bar-chart',
        data: [{
            y: 'January',
            a: 100,
            b: 90,
            c: 60
        }, {
            y: 'February',
            a: 75,
            b: 65,
            c: 40
        }, {
            y: 'March',
            a: 50,
            b: 40,
            c: 30
        }, {
            y: 'April',
            a: 75,
            b: 65,
            c: 40
        }, {
            y: 'May',
            a: 50,
            b: 40,
            c: 30
        }, {
            y: 'June',
            a: 75,
            b: 65,
            c: 40
		}, {
            y: 'July',
            a: 75,
            b: 65,
            c: 40
		}, {
            y: 'August',
            a: 75,
            b: 65,
            c: 40
        }, {
            y: 'September',
            a: 100,
            b: 90,
            c: 40
		}, {
            y: 'October',
            a: 50,
            b: 40,
            c: 30
		}, {
            y: 'November',
            a: 75,
            b: 65,
            c: 40
		 }, {
            y: 'December', 
            a: 50,
            b: 40,
            c: 30
        }],
        xkey: 'y',
        ykeys: ['a', 'b', 'c'],
        labels: ['A', 'B', 'C'],
        barColors:['#b8edf0', '#b4c1d7', '#fcc9ba'],
        hideHover: 'auto',
        gridLineColor: '#eef0f2',
        resize: true
    });









// Morris bar chart
    Morris.Area({
        element: 'morris-area-chart',
        data: [{
            y: 'January',
            a: 100,
            b: 90,
            c: 60
        }, {
            y: 'February',
            a: 75,
            b: 65,
            c: 40
        }, {
            y: 'March',
            a: 50,
            b: 40,
            c: 30
        }, {
            y: 'April',
            a: 75,
            b: 65,
            c: 40
        }, {
            y: 'May',
            a: 50,
            b: 40,
            c: 30
        }, {
            y: 'June',
            a: 75,
            b: 65,
            c: 40
		}, {
            y: 'July',
            a: 75,
            b: 65,
            c: 40
		}, {
            y: 'August',
            a: 75,
            b: 65,
            c: 40
        }, {
            y: 'September',
            a: 100,
            b: 90,
            c: 40
		}, {
            y: 'October',
            a: 50,
            b: 40,
            c: 30
		}, {
            y: 'November',
            a: 75,
            b: 65,
            c: 40
		 }, {
            y: 'December', 
            a: 50,
            b: 40,
            c: 30
        }],
        xkey: 'y',
        ykeys: ['a', 'b', 'c'],
        labels: ['A', 'B', 'C'],
         pointSize: 3,
        fillOpacity: 0,
        pointStrokeColors:['#00bfc7', '#fdc006', '#9675ce'],
        behaveLikeLine: true,
        gridLineColor: '#e0e0e0',
        lineWidth: 1,
        hideHover: 'auto',
        lineColors: ['#00bfc7', '#fdc006', '#9675ce'],
        resize: true
    });



















    // Real Time chart
    var data = [],
        totalPoints = 300;

    function getRandomData() {
        if (data.length > 0) data = data.slice(1);
        // Do a random walk
        while (data.length < totalPoints) {
            var prev = data.length > 0 ? data[data.length - 1] : 50,
                y = prev + Math.random() * 10 - 5;
            if (y < 0) {
                y = 0;
            } else if (y > 100) {
                y = 100;
            }
            data.push(y);
        }
        // Zip the generated y values with the x values 
        var res = [];
        for (var i = 0; i < data.length; ++i) {
            res.push([i, data[i]])
        }
        return res;
    }
    // Set up the control widget
    var updateInterval = 30;
    $("#updateInterval").val(updateInterval).change(function () {
        var v = $(this).val();
        if (v && !isNaN(+v)) {
            updateInterval = +v;
            if (updateInterval < 1) {
                updateInterval = 1;
            } else if (updateInterval > 3000) {
                updateInterval = 3000;
            }
            $(this).val("" + updateInterval);
        }
    });
    var plot = $.plot("#placeholder", [getRandomData()], {
        series: {
            shadowSize: 0 // Drawing is faster without shadows
        },
        yaxis: {
            min: 0,
            max: 100
        },
        xaxis: {
            show: false
        },
        colors: ["#fff"],
        grid: {
            color: "rgba(255, 255, 255, 0.3)",
            hoverable: true,
            borderWidth: 0

        },
        tooltip: true,
        tooltipOpts: {
            content: "Y: %y",
            defaultTheme: false
        }
    });



		 



    function update() {
        plot.setData([getRandomData()]);
        // Since the axes don't change, we don't need to call plot.setupGrid()
        plot.draw();
        setTimeout(update, updateInterval);
    }
    $(window).resize(function () {
        $.plot($('#placeholder'), [getRandomData()]);
    });
    update();

