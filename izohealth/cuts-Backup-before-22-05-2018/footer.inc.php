</div>
  <!-- .vd_content --> 
       </div>
      <!-- .vd_container --> 
    </div>
    <!-- .vd_content-wrapper --> 
    
  
    
  </div>
    <!-- Middle Content End --> 
 <!-- .container --> 
</div>
<!-- .content -->

<!-- Footer Start -->
  <footer class="footer-1"  id="footer">      
    <div class="vd_bottom ">
        <div class="container">
            <div class="row">
              <div class=" col-xs-12">
                <div class="copyright">
                  	Copyright &copy;2014 Venmond Inc. All Rights Reserved 
                </div>
              </div>
            </div><!-- row -->
        </div><!-- container -->
    </div>
  </footer>
<!-- Footer END -->
  

</div>

<!-- Modal popup-->
<div id="timelineModal" class="modal fade" role="dialog">
<div class="modal-dialog modal-dialog-timeline-php">
<div class="modal-content">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal">&times;</button>
  </div>
    <div id="timelineModalDate"></div>
  
  <!-- <div class="modal-footer">
     <a class="btn nextbutton  next pull-right greenbutton" data-dismiss="modal" href="screen2.html">Close </a>
  </div> -->
</div>
</div>
</div>
<!--close modal popup-->
<!-- .vd_body END  -->
<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>

<!--
<a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->

<!-- Javascript =============================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script type="text/javascript" src="js/jquery.js"></script> 
<!--[if lt IE 9]>
  <script type="text/javascript" src="js/excanvas.js"></script>      
<![endif]-->

<script type="text/javascript" src="js/bootstrap.min.js"></script> 
<script type="text/javascript" src='plugins/jquery-ui/jquery-ui.custom.min.js'></script>
<script type="text/javascript" src="plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="js/caroufredsel.js"></script> 
<script type="text/javascript" src="js/plugins.js"></script>

<script type="text/javascript" src="plugins/breakpoints/breakpoints.js"></script>
<script type="text/javascript" src="plugins/dataTables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="plugins/prettyPhoto-plugin/js/jquery.prettyPhoto.js"></script> 

<script type="text/javascript" src="plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="plugins/tagsInput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="plugins/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="plugins/pnotify/js/jquery.pnotify.min.js"></script>

<script type="text/javascript" src="js/theme.js"></script>
<script type="text/javascript" src="custom/custom.js"></script>
 
<!-- Specific Page Scripts Put Here -->
<script type="text/javascript" src='plugins/tagsInput/jquery.tagsinput.min.js'></script>

<script type="text/javascript" src='<?=SITE_PATH;?>plugins/bootstrap-timepicker/bootstrap-timepicker.min.js'></script>
<script type="text/javascript" src='plugins/daterangepicker/moment.min.js'></script>
<script type="text/javascript" src='plugins/daterangepicker/daterangepicker.js'></script>
<script type="text/javascript" src='plugins/colorpicker/colorpicker.js'></script>
<script type="text/javascript" src='plugins/ckeditor/ckeditor.js'></script>
<script type="text/javascript" src='plugins/ckeditor/adapters/jquery.js'></script>

<script type="text/javascript" src="plugins/bootstrap-wysiwyg/js/wysihtml5-0.3.0.min.js"></script>
<script type="text/javascript" src="plugins/bootstrap-wysiwyg/js/bootstrap-wysihtml5-0.0.2.js"></script>



<script type="text/javascript">
function patient_timeline(patient_id)
{
	
	var str ='patient_id='+patient_id;
	
	$.ajax({
		           type: "POST",
		           url: "<?=SITE_PATH_ADM?>/modules/timeline.php",
		           data: str,
		           cache: false,
		           success: function(html){
					      // alert(html)
						  $('#timelineModalDate').html(html);
			           
						
	}});	
	
 	
}

$(window).load(function() 
{
	"use strict";
	$('#goto-menu a').click(function(e) {
        e.preventDefault();
		scrollTo($(this).attr('href'),-80);
    });
	
	
	$('#input-autocomplete').tagsInput({
		width: 'auto',

		//autocomplete_url:'test/fake_plaintext_endpoint.html' //jquery.autocomplete (not jquery ui)
		autocomplete_url:'templates/files/fake_json_endpoint.html', // jquery ui autocomplete requires a json endpoint
/*		autocomplete:{
		source: function(request, response) {
		  $.ajax({
			 url: "templates/files/fake_json_endpoint.html",
			 dataType: "json",
			 data: {
				postalcode_startsWith: request.term
			 },
			 success: function(data) {
				response( $.map( data.postalCodes, function( item ) {
								return {
									label: item.countryCode + "-" + item.placeName,
									value: item.postalCode
								}
							}));
			 }
		  })	
		}} */
	});
	

	var availableTags = [
	"ActionScript",
	"AppleScript",
	"Asp",
	"BASIC",
	"C",
	"C++",
	"Clojure",
	"COBOL",
	"ColdFusion",
	"Erlang",
	"Fortran",
	"Groovy",
	"Haskell",
	"Java",
	"JavaScript",
	"Lisp",
	"Perl",
	"PHP",
	"Python",
	"Ruby",
	"Scala",
	"Scheme"
	];
	
	$( "#normal-autocomplete" ).autocomplete({
		source: availableTags
	});
	


	 $.widget( "custom.catcomplete", $.ui.autocomplete, {
		_renderMenu: function( ul, items ) {
			var that = this,
			currentCategory = "";
			$.each( items, function( index, item ) {
				if ( item.category != currentCategory ) {
					ul.append( "<li class='ui-autocomplete-category'>" + item.category + "</li>" );
					currentCategory = item.category;
				}
					that._renderItemData( ul, item );
			});
		}
	});

	var data = [
	{ label: "anders", category: "" },
	{ label: "andreas", category: "" },
	{ label: "antal", category: "" },
	{ label: "annhhx10", category: "Products" },
	{ label: "annk K12", category: "Products" },
	{ label: "annttop C13", category: "Products" },
	{ label: "anders andersson", category: "People" },
	{ label: "andreas andersson", category: "People" },
	{ label: "andreas johnson", category: "People" }
	];
	$( "#category-autocomplete" ).catcomplete({
		delay: 0,
		source: data
	});

		
	var data_image = [
		{ label: "anders", desc:"Lorem ipsum doler sit amet.", icon: "img/avatar/avatar.jpg" },
		{ label: "andreas", desc:"Lorem ipsum doler sit amet.", icon: "img/avatar/avatar-2.jpg" },
		{ label: "antal", desc:"Lorem ipsum doler sit amet.", icon: "img/avatar/avatar-3.jpg" },
		{ label: "annhhx10", desc:"Lorem ipsum doler sit amet.", icon: "img/avatar/avatar-4.jpg" },
		{ label: "annk K12", desc:"Lorem ipsum doler sit amet.", icon: "img/avatar/avatar-5.jpg" },
		{ label: "annttop C13", desc:"Lorem ipsum doler sit amet.", icon: "img/avatar/avatar-6.jpg" },
		{ label: "anders andersson", desc:"Lorem ipsum doler sit amet.", icon: "img/avatar/avatar-7.jpg"},
		{ label: "andreas andersson", desc:"Lorem ipsum doler sit amet.", icon: "img/avatar/avatar-8.jpg" },
		{ label: "andreas johnson", desc:"Lorem ipsum doler sit amet.", icon: "img/avatar/avatar-9.jpg" }
	];	
	$( "#image-autocomplete" ).autocomplete({
		minLength: 0,
		source: data_image,
		focus: function( event, ui ) {
		$( "#image-autocomplete" ).val( ui.item.label );
			return false;
		}
	})
	.data( "ui-autocomplete" )._renderItem = function( ul, item ) {
		return $( "<li>" )
		.append( "<a href='javascript:void(0)'><span class='menu-icon'><img src='" + item.icon + "' alt='"+ item.icon +"'></span><span class='menu-text'>" + item.label + "<span class='menu-info'>" + item.desc + "</span></span></a>" )
		.appendTo( ul );
	};


	/* Multiple Values */
	function split( val ) {
		return val.split( /,\s*/ );
	}
	function extractLast( term ) {
		return split( term ).pop();
	}
	$( "#multiple-autocomplete" )
	// don't navigate away from the field on tab when selecting an item
	.bind( "keydown", function( event ) {
		if ( event.keyCode === $.ui.keyCode.TAB &&
		$( this ).data( "ui-autocomplete" ).menu.active ) {
		event.preventDefault();
	}
	})
	.autocomplete({
		minLength: 0,
		source: function( request, response ) {
		// delegate back to autocomplete, but extract the last term
		response( $.ui.autocomplete.filter(
		availableTags, extractLast( request.term ) ) );
		},
		focus: function() {
		// prevent value inserted on focus
		return false;
		},
		select: function( event, ui ) {
		var terms = split( this.value );
		// remove the current input
		terms.pop();
		// add the selected item
		terms.push( ui.item.value );
		// add placeholder to get the comma-and-space at the end
		terms.push( "" );
		this.value = terms.join( ", " );
		return false;
		}
	});
	
	$( "#datepicker-normal" ).datepicker({ dateFormat: 'dd M yy'});
	$( "#datepicker-multiple" ).datepicker({
		numberOfMonths: 3,
		showButtonPanel: true,
		dateFormat: 'dd M yy'
	});	
	$( "#datepicker-from" ).datepicker({
		defaultDate: "+1w",
		dateFormat: 'dd M yy',
		changeMonth: true,
		numberOfMonths: 3,
		onClose: function( selectedDate ) {
		$( "#to" ).datepicker( "option", "minDate", selectedDate );
		}
	});
	$( "#datepicker-to" ).datepicker({
		defaultDate: "+1w",
		dateFormat: 'dd M yy',
		changeMonth: true,
		numberOfMonths: 3,
		onClose: function( selectedDate ) {
		$( "#from" ).datepicker( "option", "maxDate", selectedDate );
		}
	});	
	$( "#datepicker-icon" ).datepicker({ dateFormat: 'dd M yy'});
	$( '[data-datepicker]' ).click(function(e){ 
		var data=$(this).data('datepicker');
		$(data).focus();
	});
	$( "#datepicker-restrict" ).datepicker({ minDate: -20, maxDate: "+1M +10D" });	
	$( "#datepicker-widget" ).datepicker();	
	
	
	$('#datepicker-daterangepicker').daterangepicker(
		{
		  ranges: {
			 'Today': [moment(), moment()],
			 'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
			 'Last 7 Days': [moment().subtract('days', 6), moment()],
			 'Last 30 Days': [moment().subtract('days', 29), moment()],
			 'This Month': [moment().startOf('month'), moment().endOf('month')],
			 'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
		  },
		  startDate: moment().subtract('days', 29),
		  endDate: moment()
		},
		function(start, end) {
			$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		}
	);	
	
	$('#datepicker-datetime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' });	

    $('#timepicker-default').timepicker();
	$('#timepicker-full').timepicker({
		minuteStep: 1,
		template: false,
		showSeconds: true,
		showMeridian: false,
	});		

	$('#colorpicker-hex').ColorPicker({
		color: '#ff00ff',
		onSubmit: function(hsb, hex, rgb, el) {
			$(el).val(hex);
			$(el).ColorPickerHide();
		},
		onBeforeShow: function () {
			$(this).ColorPickerSetColor(this.value);
		},
		onChange: function (hsb, hex, rgb) {
			$('#colorpicker-hex').val('#' + hex);
			$('#colorpicker-hex').siblings().css({'color' : '#' + hex});
		}			
	})
	.bind('keyup', function(){
		$(this).ColorPickerSetColor(this.value);
	}).siblings().click(function(e){ 
		$(this).siblings().click();
	});	
	

	
	
	$('#colorpicker-rgba').ColorPicker({
		color: '#ff00ff',
		onSubmit: function(hsb, hex, rgb, el) {
			$(el).val(hex);
			$(el).ColorPickerHide();
		},
		onBeforeShow: function () {
			$(this).ColorPickerSetColor(this.value);
		},
		onChange: function (hsb, hex, rgb) {
			$('#colorpicker-rgba').val('rgb(' + rgb['r'] +',' + rgb['g']+',' + rgb['b'] + ')');
			$('#colorpicker-rgba').siblings().css({'color' : 'rgb(' + rgb['r'] +',' + rgb['g']+',' + rgb['b'] + ')'});
		}			
	})
	.bind('keyup', function(){
		$(this).ColorPickerSetColor(this.value);
	}).siblings().click(function(e){ 
		$(this).siblings().click();
	});	

	//CKEDITOR.replace( $('[data-rel^="ckeditor"]') );
	$( '[data-rel^="ckeditor"]' ).ckeditor();



	
})
</script>

<!-- The Load Image plugin is included for the preview images and image resizing functionality -->
<script src="http://blueimp.github.io/JavaScript-Load-Image/js/load-image.min.js"></script>
<!-- The Canvas to Blob plugin is included for image resizing functionality -->
<script src="http://blueimp.github.io/JavaScript-Canvas-to-Blob/js/canvas-to-blob.min.js"></script>
<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
<script src="plugins/jquery-file-upload/js/jquery.iframe-transport.js"></script>
<!-- The basic File Upload plugin -->
<script src="plugins/jquery-file-upload/js/jquery.fileupload.js"></script>
<!-- The File Upload processing plugin -->
<script src="plugins/jquery-file-upload/js/jquery.fileupload-process.js"></script>
<!-- The File Upload image preview & resize plugin -->
<script src="plugins/jquery-file-upload/js/jquery.fileupload-image.js"></script>
<!-- The File Upload audio preview plugin -->
<script src="plugins/jquery-file-upload/js/jquery.fileupload-audio.js"></script>
<!-- The File Upload video preview plugin -->
<script src="plugins/jquery-file-upload/js/jquery.fileupload-video.js"></script>
<!-- The File Upload validation plugin -->
<script src="plugins/jquery-file-upload/js/jquery.fileupload-validate.js"></script>
<script>
/*jslint unparam: true, regexp: true */
/*global window, $ */
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = window.location.hostname === 'blueimp.github.io' ?
                '//jquery-file-upload.appspot.com/' : 'plugins/jquery-file-upload/server/php/',
        uploadButton = $('<button/>')
            .addClass('btn vd_btn vd_bg-blue')
            .prop('disabled', true)
            .text('Processing...')
            .on('click', function () {
                var $this = $(this),
                    data = $this.data();
                $this
                    .off('click')
                    .text('Abort')
                    .on('click', function () {
                        $this.remove();
                        data.abort();
                    });
                data.submit().always(function () {
                    $this.remove();
                });
            });
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        autoUpload: false,
        acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
        maxFileSize: 5000000, // 5 MB
        // Enable image resizing, except for Android and Opera,
        // which actually support image resizing, but fail to
        // send Blob objects via XHR requests:
        disableImageResize: /Android(?!.*Chrome)|Opera/
            .test(window.navigator.userAgent),
        previewMaxWidth: 100,
        previewMaxHeight: 100,
        previewCrop: true
    }).on('fileuploadadd', function (e, data) {
        data.context = $('<div/>').appendTo('#files');
        $.each(data.files, function (index, file) {
            var node = $('<p/>')
                    .append($('<span/>').text(file.name));
            if (!index) {
                node
                    .append('<br>')
                    .append(uploadButton.clone(true).data(data));
            }
            node.appendTo(data.context);
        });
    }).on('fileuploadprocessalways', function (e, data) {
        var index = data.index,
            file = data.files[index],
            node = $(data.context.children()[index]);
        if (file.preview) {
            node
                .prepend('<br>')
                .prepend(file.preview);
        }
        if (file.error) {
            node
                .append('<br>')
                .append($('<span class="text-danger"/>').text(file.error));
        }
        if (index + 1 === data.files.length) {
            data.context.find('button')
                .text('Upload')
                .prop('disabled', !!data.files.error);
        }
    }).on('fileuploadprogressall', function (e, data) {
        var progress = parseInt(data.loaded / data.total * 100, 10);
        $('#progress .progress-bar').css(
            'width',
            progress + '%'
        );
    }).on('fileuploaddone', function (e, data) {
        $.each(data.result.files, function (index, file) {
            if (file.url) {
                var link = $('<a>')
                    .attr('target', '_blank')
                    .prop('href', file.url);
                $(data.context.children()[index])
                    .wrap(link);
            } else if (file.error) {
                var error = $('<span class="text-danger"/>').text(file.error);
                $(data.context.children()[index])
                    .append('<br>')
                    .append(error);
            }
        });
    }).on('fileuploadfail', function (e, data) {
        $.each(data.files, function (index, file) {
            var error = $('<span class="text-danger"/>').text('File upload failed.');
            $(data.context.children()[index])
                .append('<br>')
                .append(error);
        });
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
		

	$('#wysiwyghtml').wysihtml5();		
});
</script>
<!-- Specific Page Scripts END -->




<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information. -->

<script>
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-XXXXX-X']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script> 
 <script src="<?=SITE_PATH?>validation/js/jquery-1.8.2.min.js"></script>
<script src="<?=SITE_PATH?>validation/js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=SITE_PATH?>validation/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#formID").validationEngine();
		});
		$('#change_pwd').click(function(){
			if(this.checked)
			{
				$('.pwd').show();
			}else{
				$('.pwd').hide();
			}
		});
		$('.change_pwd').click(function(){
			if(this.checked)
			{
				$('.pwd').show();
			}else{
				$('.pwd').hide();
			}
		});
</script>

<!--   Datepicker -->
<link href="<?=SITE_PATH?>plugins/datepicker/jquery.datepick.css" rel="stylesheet" type="text/css">
<script src="<?=SITE_PATH?>plugins/datepicker/jquery.plugin.js" type="text/javascript" charset="utf-8"></script>
 <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script>
$(document).ready(function () {
$('.datepicker').datepicker({
      changeMonth: true,//this option for allowing user to select month
      changeYear: true //this option for allowing user to select from year range
    });
});


$('.timePicker').timepicker({
		minuteStep: 1,
		template: false,
		showSeconds: true,
		showMeridian: false,
	});	
</script>
<!-- End Datepicker -->

<!-- change township by division -->

<script>
$(function(){
	$('.cat').change(function(){
		var value = $(this).val();
		var folder = $(this).attr('data-folder');
		var name = $(this).attr('data-name');
		var url = "<?=SITE_PATH_ADM?>modules/"+folder+"/ajax.php";
		var data = "pid="+value;
		
		
		$.ajax({
			url:url,
			type:"post",
			data:data,
			dataType:"text",
			success:function(response)
			{
				if(response!=0)
				{
					
						$('.add_records').empty().append("<option value=''>------Select "+name+"------</option>");
						$('.add_records').append(response);
					
				}else{
					$('.add_records').empty().append("<option value=''>------Select "+name+"------</option>");
				}
			}
		});
	})
	
});
</script>
<script>
$(function(){
	$('.cat2').change(function(){
		var value = $(this).val();
		var folder = $(this).attr('data-folder');
		var id = $(this).attr('data-name');
		var url = "<?=SITE_PATH_ADM?>modules/"+folder+"/ajax.php";
		var data = "pid="+value;
		
		
		$.ajax({
			url:url,
			type:"post",
			data:data,
			dataType:"text",
			success:function(response)
			{
				if(response!=0)
				{
					
						$('#'+id).empty().append("<option value=''>------Select "+name+"------</option>");
						$('#'+id).append(response);
					
				}else{
					$('#'+id).empty().append("<option value=''>------Select "+name+"------</option>");
				}
			}
		});
	})
	
});

function getpincode(id,sid)
{
	var township =$('#'+id).val();
	
		var str ='township='+township;
	
	$.ajax({
		           type: "POST",
		           url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag=getpincode",
		           data: str,
		           cache: false,
		           success: function(html){
					    
						$('#'+sid).val(html);
						
			           
						
	}});	
}
</script>
<?php if($comp=="doctors"){?>
<!-- End township -->
<script>
// for doctor add time
jQuery.noConflict();
$('.timepicker-default').timepicker({defaultTime: ''});
    $('.add-field').click( function(){
        var value = $(this).attr("data-value");
		if(value<5){ 
		value = parseInt(value)+1;
		$(this).attr("data-value",value);	
		$(this).parent().find('.hidden-val').val(value);
		var day = $(this).parent().find('.hidden-val1').val();
		
        $(this).closest('div.cols-kk2').append('<div class="cols-kk3"><div class="col-sm-4  controls">	<label class="control-label">Time From</label><div class="input-group bootstrap-timepicker">	<input name="dates['+day+'][from][]" value="" class="timepicker-default" placeholder="Time From" type="text">	<span id="timepicker-default-span" class="input-group-addon"><i class="fa fa-clock-o"></i></span></div></div><div class="col-sm-4  controls"><label class="control-label">Time To</label><div class="input-group bootstrap-timepicker"><input name="dates['+day+'][to][]" value="" class="timepicker-default" placeholder="Time To" type="text"><span id="timepicker-default-span" class="input-group-addon"><i class="fa fa-clock-o"></i></span></div></div><div class="col-sm-3"><input value="4" class="hidden-val" name="tot-time[]" type="hidden"><button style="display: inline-block;" data-value="4" type="button" class="btn vd_btn vd_bg-green vd_white btn-2" onclick="remove_field(this)"><i class="fa fa-minus-square"></i></button></div></div>');
		   $('.timepicker-default').timepicker({defaultTime: ''});
		    if(value==5){$(this).hide();}
		}

			

    });
	
   function remove_field(str)
   {		
	   var id = $(str).closest('div.cols-kk2').find('div.cols-kk3').first().find('.add-field');
	   var value = $(id).attr("data-value");
	   value = parseInt(value)-1;	
	   $(id).attr("data-value",value);
	   $(str).closest('div.cols-kk2').find('div.cols-kk3').first().find('.hidden-val').val(value);
	   $(id).show();
	   $(str).closest('div.cols-kk3').remove();
   }
    </script>
	<?php }?>
	
</body>
</html>