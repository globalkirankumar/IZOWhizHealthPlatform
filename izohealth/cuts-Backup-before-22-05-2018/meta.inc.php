<?php $ADMIN->secure();?>
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html><!--<![endif]-->

<!-- Specific Page Data -->

<!-- End of Data -->

<head>
    <meta charset="utf-8" />
    <link rel="icon" href="<?=SITE_PATH?>img/favicon.png"/>

    <title>Izohealth-CRM</title>
    <meta name="keywords" content="HTML5 Template, CSS3, Mega Menu, Admin Template, Elegant HTML Theme, Vendroid" />
    <meta name="description" content="Form Elements - Responsive Admin HTML Template">
    <meta name="author" content="Venmond">
    
    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    
    
   
    
    
    <!-- CSS -->
       
    <!-- Bootstrap & FontAwesome & Entypo CSS -->
    <link href="<?=SITE_PATH?>css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?=SITE_PATH?>css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if IE 7]><link type="text/css" rel="stylesheet" href="css/font-awesome-ie7.min.css"><![endif]-->
    <link href="<?=SITE_PATH?>css/font-entypo.css" rel="stylesheet" type="text/css">    

    <!-- Fonts CSS -->
    <link href="<?=SITE_PATH?>css/fonts.css"  rel="stylesheet" type="text/css">
               
    <!-- Plugin CSS -->
    <link href="<?=SITE_PATH?>plugins/jquery-ui/jquery-ui.custom.min.css" rel="stylesheet" type="text/css">    
    <link href="<?=SITE_PATH?>plugins/prettyPhoto-plugin/css/prettyPhoto.css" rel="stylesheet" type="text/css">
    <link href="<?=SITE_PATH?>plugins/isotope/css/isotope.css" rel="stylesheet" type="text/css">
    <link href="<?=SITE_PATH?>plugins/pnotify/css/jquery.pnotify.css" media="screen" rel="stylesheet" type="text/css">    
	<link href="<?=SITE_PATH?>plugins/google-code-prettify/prettify.css" rel="stylesheet" type="text/css"> 
   
         
    <link href="<?=SITE_PATH?>plugins/mCustomScrollbar/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css">
    <link href="<?=SITE_PATH?>plugins/tagsInput/jquery.tagsinput.css" rel="stylesheet" type="text/css">
    <link href="<?=SITE_PATH?>plugins/bootstrap-switch/bootstrap-switch.css" rel="stylesheet" type="text/css">    
    <link href="<?=SITE_PATH?>plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css">    
    <link href="<?=SITE_PATH?>plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">
    <link href="<?=SITE_PATH?>plugins/colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css">            

	<!-- Specific CSS -->
	<link href="<?=SITE_PATH?>plugins/jquery-file-upload/css/jquery.fileupload.css" rel="stylesheet" type="text/css"><link href="<?=SITE_PATH?>plugins/jquery-file-upload/css/jquery.fileupload-ui.css" rel="stylesheet" type="text/css">
	<link href="<?=SITE_PATH?>plugins/bootstrap-wysiwyg/css/bootstrap-wysihtml5-0.0.2.css" rel="stylesheet" type="text/css">    
     <!----extra------------------------------------------------------>
	 <link href="<?=SITE_PATH?>css/style.css" rel="stylesheet" type="text/css">
    <!-- Theme CSS -->
    <link href="<?=SITE_PATH?>css/theme.min.css" rel="stylesheet" type="text/css">
    <!--[if IE]> <link href="css/ie.css" rel="stylesheet" > <![endif]-->
    <link href="<?=SITE_PATH?>css/chrome.css" rel="stylesheet" type="text/chrome"> <!-- chrome only css -->    


        
    <!-- Responsive CSS -->
        	<link href="<?=SITE_PATH?>css/theme-responsive.min.css" rel="stylesheet" type="text/css"> 

	   <link href="<?=SITE_PATH?>validation/css/validationEngine.jquery.css" rel="stylesheet" type="text/css">
 
 
    <!-- for specific page in style css -->
        
    <!-- for specific page responsive in style css -->
        
    
    <!-- Custom CSS -->
    <link href="<?=SITE_PATH?>custom/custom.css" rel="stylesheet" type="text/css">

    <script src="<?=SITE_PATH_ADM?>js/jquery-1.11.2.min.js"></script>
    <!-- Head SCRIPTS -->
    <script type="text/javascript" src="<?=SITE_PATH?>js/modernizr.js"></script> 
    <script type="text/javascript" src="<?=SITE_PATH?>js/mobile-detect.min.js"></script> 
    <script type="text/javascript" src="<?=SITE_PATH?>js/mobile-detect-modernizr.js"></script> 
    <script type="text/javascript" src="<?=SITE_PATH?>validation/validate.js"></script> 
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type="text/javascript" src="js/html5shiv.js"></script>
      <script type="text/javascript" src="js/respond.min.js"></script>     
    <![endif]-->
    
    <link href="<?=SITE_PATH_ADM?>SpryAssets/SpryTabbedPanels.css" rel="stylesheet" type="text/css">
    <link href="<?=SITE_PATH_ADM?>SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css">
    <script src="<?=SITE_PATH_ADM?>SpryAssets/SpryTabbedPanels.js" type="text/javascript"></script>
    <script src="<?=SITE_PATH_ADM?>SpryAssets/SpryAccordion.js" type="text/javascript"></script>
</head>