<style>
    .sub-menu li a, .nav-side-menu ul .sub-menu li a {
        display: block;
        padding: 0px 0px 0px 46px;
    }

    .nav-left-medium .sub-menu li a, .nav-side-menu ul .sub-menu li a {
        display: block;
        padding: 0px 0px 0px 0px;
        text-align: center;
    }

    .nav-left-medium .small-menu {
        font-size: 36px;
        margin-right: 0;
        width: 100%;
        height: 40px;
        line-height: 38px;
        float: none;
        display: block;
        text-align: center;
    }

    .sub-menu li a i, .nav-side-menu li .sub-menu li a i {
        padding-right: 10px !important;
    }

    .nav-side-menu li a i {
        padding-left: 10px;
        width: 20px;
        padding-right: 20px;
    }

    .sub-menu li, .nav-side-menu li .sub-menu li {
        padding: 10px 0px;
        border-top: 1px solid rgba(255, 255, 255, 0.2);
        /*border-bottom: 1px solid rgba(0,0,0,0.15);*/
        position: relative;
    }

    .sub-menu li:hover {
        background: rgba(255, 255, 255, 0.15);
    }

    .fa.arrow {
        float: right;
        font-size: 18px;
        font-weight: bold;
    }

    .fa.arrow::before {
        content: "\f105";
        display: inline-block;
        -webkit-transform: rotate(90deg);
        -ms-transform: rotate(90deg);
        transform: rotate(90deg);
    }

    .collapsed > a > .fa.arrow::before {

        -webkit-transform: rotate(0deg);
        -ms-transform: rotate(0deg);
        transform: rotate(0deg);
    }

    .nav-left-medium .fa.arrow {
        display: none;
    }

    .nav-left-medium .collapse.in {
        display: block;
        margin-left: 141px;
        width: 147px;
        background: #1FAE66 none repeat scroll 0% 0%;
        margin-top: -89px;
        position: absolute;
        z-index: 111;
        padding: 0px 0px;
        transition-duration: 1s;
        transition-timing-function: cubic-bezier(0.2, 0.5, 0.3, 1);
    }

    .nav-left-medium .collapsing {
        display: none;
    }

</style>

<div class="content">
    <div class="container">
        <div class="vd_navbar vd_nav-width vd_navbar-tabs-menu vd_navbar-left  ">

            <div class="navbar-menu clearfix">
                <div class="vd_panel-menu hidden-xs">
                    <!--
                                <span data-original-title="Expand All" data-toggle="tooltip" data-placement="bottom"
                                      data-action="expand-all" class="menu"
                                      data-intro="<strong>Expand Button</strong><br/>To expand all menu on left navigation menu."
                                      data-step=4>
                                    <i class="fa fa-sort-amount-asc"></i>
                                </span>
                    -->
                </div>
                <!--                <h3 class="menu-title hide-nav-medium hide-nav-small">Menu</h3>-->
                <div class="vd_menu">
                    <ul>
                        <?php $user_type = @$_SESSION['AMD'][2]; ?>
                        <?php if (strtolower($user_type) == "doctors") { ?>
                            <!--    Doctor    -->
                           <!-- <li>
                                <a href="<?/*= $ADMIN->iurl('appointment') */?>">
                                    <span class="menu-icon"><i class="fa fa-dashboard"></i></span>
                                    <span class="menu-text">Appointment</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?/*= $ADMIN->iurl('reports') */?>">
                                    <span class="menu-icon"><i class="fa fa-hospital-o"></i></span>
                                    <span class="menu-text">Upload Reports</span>
                                </a>
                            </li>-->
                        <?php } else if (strtolower($user_type) == "doctoragents") { ?>
                            <!--    Doctor Agents    -->
                           <!-- <li>
                                <a href="<?/*= $ADMIN->iurl('patient_registration') */?>">
                                    <span class="menu-icon"><i class="fa fa-dashboard"></i></span>
                                    <span class="menu-text">Call</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?/*= $ADMIN->iurl('call_details') */?>">
                                    <span class="menu-icon"><i class="fa fa-hospital-o"></i></span>
                                    <span class="menu-text">MY Call Details</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?/*= $ADMIN->iurl('medicalreference') */?>">
                                    <span class="menu-icon"><i class="fa fa-phone-square"></i></span>
                                    <span class="menu-text">Download Medical Reference</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?/*= $ADMIN->iurl('emergency') */?>">
                                    <span class="menu-icon"><i class="fa fa-phone-square"></i></span>
                                    <span class="menu-text">Emergency <span
                                                id="emergency_location">(<?/*= $PDO->getSingleResult("select count(*) from #_emergency  where status=0") */?>
                                            )</span></span>
                                </a>
                            </li>-->

                            <!--<li>
			<a href="<?= $ADMIN->iurl('reports') ?>">
				<span class="menu-icon"><i class="fa fa-hospital-o"></i></span> 
				<span class="menu-text">Upload Reports</span>  
			</a>
		</li>-->


                        <?php } else if (strtolower($user_type) == "administrator" or strtolower($user_type) == "supervisor") { ?>
                            <!--    Administrator And Supervisor    -->

                            <li>
                                <a href="<?= $ADMIN->iurl('patient_registration') ?>">
                                    <span class="menu-icon"><i class="fa fa-phone"></i></span>
                                    <span class="menu-text">Book Appoitment </span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= $ADMIN->iurl('admin_users') ?>">
                                    <span class="menu-icon"><i class="fa fa-user"></i></span>
                                    <span class="menu-text">Admin Users</span>
                                </a>
                            </li>


                            <li data-toggle="collapse" data-target="#manage" class="collapsed">
                                <a href="#"> <span class="menu-icon"><i
                                                class="fa fa-puzzle-piece fa-lg fa-fw sidebar-icon"></i></span>
                                    Unverified <i class="fa arrow"></i></a>
                            </li>


                            <ul class="sub-menu collapse" id="manage">
                                <li>
                                    <a href="<?= $ADMIN->iurl('verify_users') ?>">
                                        <span class="small-menu"><i class="fa fa-check"></i></span>
                                        <span class="menu-text">Unverified Doctors</span>
                                    </a>

                                </li>
                                <li>
                                    <a href="<?= $ADMIN->iurl('verify_hospitals') ?>">
                                        <span class="small-menu"><i class="fa fa-check"></i></span>
                                        <span class="menu-text">Unverified Hospitals</span>
                                    </a>

                                </li>
                                <li>
                                    <a href="<?= $ADMIN->iurl('verify_clinics') ?>">
                                        <span class="small-menu"><i class="fa fa-check"></i></span>
                                        <span class="menu-text">Unverified Clinics</span>
                                    </a>

                                </li>
                            </ul>



                            <!--<li>
                                <a href="<?/*= $ADMIN->iurl('appointment_tracking') */?>">
                                    <span class="menu-icon"><i class="fa fa-calendar"></i></span>
                                    <span class="menu-text">Appointment Tracking</span>
                                </a>

                            </li>-->
                            <li>
                                <a href="<?= $ADMIN->iurl('doctors') ?>">
                                    <span class="menu-icon"><i class="fa fa-user-md"></i></span>
                                    <span class="menu-text">Doctors</span>
                                </a>

                            </li>

                            <li>
                                <a href="<?= $ADMIN->iurl('clinics') ?>">
                                    <span class="menu-icon"><i class="fa fa-stethoscope"></i></span>
                                    <span class="menu-text">Clinics</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?= $ADMIN->iurl('hospitals') ?>">
                                    <span class="menu-icon"><i class="fa fa-hospital-o"></i></span>
                                    <span class="menu-text">Hospital</span>
                                </a>
                            </li>
                           <!-- <li>
                                <a href="<?/*= $ADMIN->iurl('labs') */?>">
                                    <span class="menu-icon"><i class="fa fa-flask"></i></span>
                                    <span class="menu-text">Labs</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?/*= $ADMIN->iurl('services_cat') */?>">
                                    <span class="menu-icon"><i class="fa fa-flask"></i></span>
                                    <span class="menu-text">Lab Services</span>
                                </a>
                            </li>-->
                            <!--<li>
                                <a href="<?/*= $ADMIN->iurl('healthcare_organization') */?>">
                                    <span class="menu-icon"><i class="fa fa-ambulance"></i></span>
                                    <span class="menu-text">Home Healthcare</span>
                                </a>
                            </li>-->
                           <!-- <li>
                                <a href="<?/*= $ADMIN->iurl('doctor_agents') */?>">
                                    <span class="menu-icon"><i class="fa fa-user-md"></i></span>
                                    <span class="menu-text">Doctor Agents</span>
                                </a>
                            </li>-->

                            <li><a href="<?= $ADMIN->iurl('custom_notification') ?>"> <span class="menu-icon"><i
                                                class="fa fa-bell"></i></span> <span
                                            class="menu-text">Notification</span> </a></li>

                            <li>
                                <a href="<?= $ADMIN->iurl('voip_call_details') ?>">
                                    <span class="menu-icon"><i class="fa fa-phone-square"></i></span>
                                    <span class="menu-text">VOIP Call Details</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= $ADMIN->iurl('healthguide') ?>">
                                    <span class="menu-icon"><i class="fa fa-phone-square"></i></span>
                                    <span class="menu-text">Health Guide</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= $ADMIN->iurl('call_details') ?>">
                                    <span class="menu-icon"><i class="fa fa-phone-square"></i></span>
                                    <span class="menu-text">Call Details</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?= $ADMIN->iurl('appointment') ?>">
                                    <span class="menu-icon"><i class="fa fa-clock-o"></i></span>
                                    <span class="menu-text">Doctor's Appointment</span>
                                </a>
                            </li>


                           <!-- <li>
                                <a href="<?/*= $ADMIN->iurl('appointment&mode=schedule') */?>">
                                    <span class="menu-icon"><i class="fa fa-phone-square"></i></span>
                                    <span class="menu-text">Lab/Imaging Appointment</span>
                                </a>
                            </li>-->


                           <!-- <li>
                                <a href="<?/*= $ADMIN->iurl('add_banner') */?>">
                                    <span class="menu-icon"><i class="fa fa-phone-square"></i></span>
                                    <span class="menu-text">Add Banner</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?/*= $ADMIN->iurl('sponsored') */?>">
                                    <span class="menu-icon"><i class="fa fa-phone-square"></i></span>
                                    <span class="menu-text">Sponsored Doctor</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?/*= $ADMIN->iurl('sponsore_clinics') */?>">
                                    <span class="menu-icon"><i class="fa fa-phone-square"></i></span>
                                    <span class="menu-text">Sponsored Clinics</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?/*= $ADMIN->iurl('sponsore_hospital') */?>">
                                    <span class="menu-icon"><i class="fa fa-phone-square"></i></span>
                                    <span class="menu-text">Sponsored Hospital</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?/*= $ADMIN->iurl('sponsore_healthcare') */?>">
                                    <span class="menu-icon"><i class="fa fa-phone-square"></i></span>
                                    <span class="menu-text">Sponsored HealthCare</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?/*= $ADMIN->iurl('sub_centers') */?>">
                                    <span class="menu-icon"><i class="fa fa-phone-square"></i></span>
                                    <span class="menu-text">Sub Centers List</span>
                                </a>
                            </li>

                            <li>
                                <a href="<?/*= $ADMIN->iurl('medicalreference') */?>">
                                    <span class="menu-icon"><i class="fa fa-phone-square"></i></span>
                                    <span class="menu-text">Uload Medical Reference</span>
                                </a>
                            </li>-->

                            <li>
                                <a href="<?= $ADMIN->iurl('health_tips') ?>">
                                    <span class="menu-icon"><i class="fa fa-phone-square"></i></span>
                                    <span class="menu-text">Health Tips</span>
                                </a>
                            </li>

                            <li data-toggle="collapse" data-target="#settings" class="collapsed">
                                <a href="#"> <span class="menu-icon"><i
                                                class="fa fa-cogs fa-lg fa-fw sidebar-icon"></i></span>
                                    Settings <i class="fa arrow"></i></a>
                            </li>
                            <ul class="sub-menu collapse" id="settings">
                                <li>
                                    <a href="<?= $ADMIN->iurl('specialities') ?>">
                                        <span class="small-menu"><i class="fa fa-phone-square"></i></span>
                                        <span class="menu-text">Specialities</span>
                                    </a>
                                </li>

                                <li>
                                    <a href="<?= $ADMIN->iurl('division') ?>">
                                        <span class="small-menu"><i class="fa fa-phone-square"></i></span>
                                        <span class="menu-text">Division</span>
                                    </a>
                                </li>

                            </ul>


                            <!-- <li>
                                 <a href="<?/*= $ADMIN->iurl('emergency') */?>">
                                     <span class="menu-icon"><i class="fa fa-phone-square"></i></span>
                                     <span class="menu-text">Emergency <span
                                                 id="emergency_location">(<?/*= $PDO->getSingleResult("select count(*) from #_emergency  where status=0") */?>
                                             )</span></span>
                                 </a>
                             </li>-->

                        <?php } else { ?>
                            <!--   Clinics,Labs,Hospital And Healthcare     -->
                            <?php if (strtolower($user_type) == "clinics" or strtolower($user_type) == "hospital") { ?>
                               <!-- <li>
                                    <a href="<?/*= $ADMIN->iurl('appointment') */?>">
                                        <span class="menu-icon"><i class="fa fa-dashboard"></i></span>
                                        <span class="menu-text">Doctor's Appointment</span>
                                    </a>
                                </li>-->
                            <?php } ?>
                           <!-- <li>
                                <a href="<?/*= $ADMIN->iurl('appointment', 'schedule') */?>">
                                    <span class="menu-icon"><i class="fa fa-hospital-o"></i></span>
                                    <span class="menu-text">Lab/Imaging Appointment</span>
                                </a>
                            </li>
                            <li>
                                <a href="<?/*= $ADMIN->iurl('reports') */?>">
                                    <span class="menu-icon"><i class="fa fa-hospital-o"></i></span>
                                    <span class="menu-text">Upload Reports</span>
                                </a>
                            </li>-->


                        <?php } ?>


                        <li style="display:none;">
                            <a href="javascript:void(0);" data-action="click-trigger">
                                <span class="menu-icon"><i class="fa fa-dashboard"></i></span>
                                <span class="menu-text">Menu</span>
                                <span class="menu-badge"><span class="badge vd_bg-black-30"><i
                                                class="fa fa-angle-down"></i></span></span>
                            </a>
                            <div class="child-menu" data-action="click-target">
                                <ul>
                                    <li>
                                        <a href="<?= $ADMIN->iurl('admin_users') ?>">
                                            <span class="menu-text">Admin Users</span>
                                        </a>
                                    </li>
                                    <!-- <ul>
                                        <li>
                                            <a href="index.html">
                                                <span class="menu-text">Default Dashboard</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="index-ecommerce.html">
                                                <span class="menu-text">E-Commerce Dashboard</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="index-analytics.html">
                                                <span class="menu-text">Analytics Dashboard</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="index-blogging.html">
                                                <span class="menu-text">Blogging Dashboard</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="index-event-management.html">
                                                <span class="menu-text">Event Management Dashboard</span>
                                                <span class="menu-badge"><span class="badge vd_bg-yellow">NEW</span></span>
                                            </a>
                                        </li>
                                    </ul>  -->
                            </div>
                        </li>


                    </ul>
                    <!-- Head menu search form ends -->         </div>
            </div>
            <div class="navbar-spacing clearfix">
            </div>
            <div class="vd_menu vd_navbar-bottom-widget">
                <ul>
                    <li>
                        <a href="<?= SITE_PATH ?>logout.php">
                            <span class="menu-icon"><i class="fa fa-sign-out"></i></span>
                            <span class="menu-text">Logout</span>
                        </a>

                    </li>
                </ul>
            </div>
        </div>
   