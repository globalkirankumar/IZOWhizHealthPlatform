<!---Change your preferences-->
  <!--<div class="col-md-4 col-sm-offset-4 col-sm-4 col-xs-12">
    <div class="panel-heading vd_bg-green padding_bottom">
      <h3 class="panel-title panel-title-custom"> <span class="menu-text2">Change your preferences</span> </h3>
          <div class="vd_panel-menu">
          <div data-original-title="Config" data-toggle="tooltip" data-placement="bottom" class=" menu entypo-icon smaller-font">
          <div class="menu-trigger" data-action="click-trigger"> <span class="fa_icon"><i class="fa fa-plus-circle"></i></span> </div>
          <div style="display: none;" data-action="click-target" class="vd_mega-menu-content  width-xs-2  left-xs">
            <div class="child-menu">
            <div class="content-list content-menu">
              <ul class="list-wrapper pd-lr-10">
              <li> <a href="#">  <div class="menu-text">Hospital</div> </a> </li>
              <li class="line"></li>
              <li> <a href="#">  <div class="menu-text">Lab</div> </a> </li>
              <li class="line"></li>
              <li> <a href="#">  <div class="menu-text">Imaging Centre</div> </a> </li>
              <li class="line"></li>
              <li> <a href="#">  <div class="menu-text">Healthcare Organization</div> </a> </li>
             
              </ul>
            </div>
            </div>
          </div>
          </div>
        </div>
        </div>
      </div>-->
<!---Close Change your preferences-->


<!---filters-->
<?php 
$user_type = @$_SESSION['AMD'][2];
$user_id = @$_SESSION['AMD'][0];
?>
<div class="search_formbox">
<div class="col-md-3 col-sm-3 col-xs-12">
<div class="form-group form-group2">
<div class="controls">
  <select name="services_search" id="services_search" >
  <option value="">Change your preferences</option>
  <option value="Doctors"  <?=($services_search=='Doctors')?'selected="selected"':''?>>Doctors</option>
  <option value="Lab"  <?=($services_search=='Lab')?'selected="selected"':''?>>Lab</option>
  <option value="Imaging"  <?=($services_search=='Imaging')?'selected="selected"':''?>>Imaging Centre</option>
  <option value="Healthcare" <?=($services_search=='Healthcare')?'selected="selected"':''?> >Home Healthcare Organization</option>
  </select>
 </div>
</div>
</div>
<div class="col-md-3 col-sm-3 col-xs-12">
<div class="form-group form-group2">
<div class="controls">
  <select name="home_services_search" id="home_services_search" >
  <option value="">Select Home Services</option>
  <option value="Available" <?=($home_services_search=='Available')?'selected="selected"':''?>  >Available</option>
  <option value="Not Available" <?=($home_services_search=='Not Available')?'selected="selected"':''?>>Not Available</option>
  
  </select>
 </div>
</div>
</div>
<?php if($user_type!='hospital' && $user_type!='clinics' && $user_type!='manager' && $user_type!='executive'){?>
<div class="col-md-3 col-sm-3 col-xs-12">
<div class="form-group form-group2">
<div class="controls">
  <select name="hospital_services_search"  id="hospital_services_search">
   <option value="">Select Hospital</option>
    <?php
        $hospital_query=$PDO->db_query("select * from #_hospitals where status='1'  order by name "); 
        while($hospital_data= $PDO->db_fetch_array($hospital_query))
        {
   ?>
            <option value="<?=$hospital_data['pid']?>"  <?=($hospital_services_search==$hospital_data['pid'])?'selected="selected"':''?> ><?=ucfirst($hospital_data['name'])?></option>
  <?php } ?>
  
  </select>
 </div>
</div>
</div>


<?php } if($services_search !='Healthcare') {
  if($user_type!='hospital' && $user_type!='clinics' && $user_type!='manager' && $user_type!='executive'){?>
<div class="col-md-3 col-sm-3 col-xs-12">
<div class="form-group form-group2">
<div class="controls">
  <select name="clinic_services_search"  id="clinic_services_search">
   <option value="">Select Clinic</option>
    <?php
        $clinic_query=$PDO->db_query("select * from #_clinics where status='1'  order by name "); 
        while($clinic_data= $PDO->db_fetch_array($clinic_query))
        {
   ?>
            <option value="<?=$clinic_data['pid']?>"  <?=($clinic_services_search==$clinic_data['pid'])?'selected="selected"':''?> ><?=ucfirst($clinic_data['name'])?></option>
  <?php } ?>
  
  </select>
 </div>
</div>
</div>
<?php } }?>

<?php if($services_search !='Lab' && $services_search !='Healthcare' && $services_search !='Imaging') {?>
<div class="col-md-3 col-sm-3 col-xs-12">
<div class="form-group form-group2">
<div class="controls">
  <select name="speciality_search" id="speciality_search">
   <option value="">Select Speciality</option>
   <?php
        $pecialities_query=$PDO->db_query("select * from #_specialities where status='1'  order by name "); 
     
        while($pecialities_data= $PDO->db_fetch_array($pecialities_query))
        {
   ?>
            <option value="<?=$pecialities_data['pid']?>"  <?=($speciality_search==$pecialities_data['pid'])?'selected="selected"':''?>  ><?=ucfirst($pecialities_data['name'])?> </option>
  <?php } ?>
  </select>
 </div>
</div>
</div>


<div class="col-md-3 col-sm-3 col-xs-12">
<div class="form-group form-group2">
<div class="controls">

  <select name="doctor_search" id="doctor_search">
   <option value="">Select Doctor</option>
   <?php    
    if($user_type == 'hospital'){
      $pid = $PDO->getSingleresult("select pid from #_hospitals where user_id ='".$user_id."' ");
      $wh = ' and hospital_id = "'.$pid.'" ';
    }else if($user_type=='clinics'){
      $clinic = $PDO->getSingleresult("select pid from #_clinics where user_id ='".$user_id."' ");
      $wh = ' and clinic_id = "'.$clinic.'" ';
    }else{
      $created_id = $PDO->getSingleresult("select createdby_userid from #_manager_executives where user_id ='".$user_id."' ");
      $created_type = $PDO->getSingleresult("select createdby_user_type from #_manager_executives where user_id ='".$user_id."' ");
      if($created_type == "hospital"){
        $pid = $PDO->getSingleresult("select pid from #_hospitals where user_id ='".$created_id."' ");
        $wh = ' and hospital_id = "'.$pid.'" ';
      }else if($created_type == "clinics"){
        $clinic = $PDO->getSingleresult("select pid from #_clinics where user_id ='".$created_id."' ");
        $wh = ' and clinic_id = "'.$clinic.'" ';
      }else{
        $wh = "";
      }
    }
        $pecialities_query=$PDO->db_query("select * from #_doctors where status='1' ".$wh."  group by name order by name "); 
     
        while($pecialities_data= $PDO->db_fetch_array($pecialities_query))
        {
   ?>
            <option value="<?=$pecialities_data['pid']?>"  <?=($doctor_search==$pecialities_data['pid'])?'selected="selected"':''?>  ><?=ucfirst($pecialities_data['name'])?> </option>
  <?php } ?>
  </select>
 </div>
</div>
</div>

<div class="col-md-3 col-sm-3 col-xs-12">
<div class="form-group form-group2">
<div class="controls">
<input type="text" class="apptdatepicker" placeholder="Appointment Date" name="appointment_date" id="appointment_date" value="<?=$appointment_date?>" readonly>
</div>
</div>
</div>
<?php } if($user_type!='hospital' && $user_type!='clinics' && $user_type!='manager' && $user_type!='executive'){?>


<div class="col-md-3 col-sm-3 col-xs-12">
<div class="form-group form-group2">
<div class="controls">

  <select name="search_division" id="search_division" class="input_box_client_select"  data-name="search_township" data-folder="doctor_agents">
                                         <option value="" >---Division---</option>
                                         <?php 
										        $record=$PDO->db_query("select * from #_division where status =1  order by name");
										        while($res=mysql_fetch_array($record))
												{
										  ?>
					                 <option value="<?=$res['pid']?>" <?php if($search_division==$res['pid']){echo "selected";}?>><?=$res['name']?></option>
						              <?php } ?>
                                        </select>
 </div>
</div>
</div>

<div class="col-md-3 col-sm-3 col-xs-12">
<div class="form-group form-group2">
<div class="controls">

  <select name="search_township" id="search_township" class="input_box_client_select add_records">
                                         <option value="" >---select---</option>
                                        <?php $record=$PDO->db_query("select * from #_township where status =1 and division_id='".$search_division."' order by name");?>
						<?php while($res=mysql_fetch_array($record)){?>
						<option value="<?=$res['pid']?>" <?php if($search_township==$res['pid']){echo "selected";}?>><?=$res['name']?></option>
						<?php } ?>
                                        </select>
 </div>
</div>
</div>
<?php }?>

<div class="col-md-12 col-sm-12 col-xs-12"><button class="btn greenbutton fright" type="button"  onclick="searchdata()"><i class="icon-ok"></i> Search</button></div>
</div>
<script  type="application/javascript">
$(function(){
	$('#search_division').change(function(){
		var division = $(this).val();
		var url = "<?=SITE_PATH_ADM?>/modules/doctor_agents/ajax.php";
		var data = "pid="+division;
		$.ajax({
			url:url,
			type:"post",
			data:data,
			dataType:"text",
			success:function(response)
			{
				if(response!=0)
				{
					
						$('#search_township').empty().append("<option value=''>---Township---</option>");
						$('#search_township').append(response);
					
				}else{
					$('#search_township').empty().append("<option value=''>---Township---</option>");
				}
			}
		});
	})
	
});

</script>
<!--close filters-->