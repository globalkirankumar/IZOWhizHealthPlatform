<?php
if($services_like_data!='')
{
  $services_like_data =	" and services like '%".$services_like_data."%' ";
}

//echo "select * from #_healthcare_organization where status='1'   ".$home_services_search_wh.$services_like_data.$township_wh;

$lab_query=$PDO->db_query("select * from #_healthcare_organization where status='1'   ".$home_services_search_wh.$services_like_data.$township_wh); 
while($lab_data = $PDO->db_fetch_array($lab_query))
{
?>
<!--info box-->
<div class="info-part">
<div class="top-part">
<div class="info-icon">
<i class="fa fa-ambulance addstyle"></i>	
</div>
<div class="map-btn">

<!--<a href="javascript:;"><span data-id="dc-list" class="label vd_bg-green map-list">List</span></a>-->
<a href="#" data-toggle="modal" data-target=".myModa2"><span data-id="map-list" class="label vd_bg-yellow map-list">Map</span></a>
</div>
</div>
<div class="panel widget" id="dc-list">
<div class="info-body">  
<div class="content-list content-image menu-action-right"><div>
<div class="info-category">
<div class="col-sm-6">
<div class="category-text"><span>Healthcare Name :</span> <?=$lab_data['name']?></div>
<div class="category-text"><span>Address :</span> <?=$lab_data['address']?></div>
<div class="category-text"><span>Phone No. :</span> <?=$lab_data['phone']?></div>
<div class="category-text"><span>Postal Code  :</span> <?=$lab_data['postal_code']?> </div>
</div>  
<div class="col-sm-6">
<div class="category-text"><span>Land Mark Near by :</span> <?=$lab_data['nearest_land_mark']?></div>
<div class="category-text"><span>Nearest Bus Stop :</span> <?=$lab_data['nearest_bus_stop']?> </div>
 
<div class="category-text"><span>Division   :</span> <?=$PDO->getSingleresult("select name from #_division where pid='".$lab_data['division']."' ")?> </div>
<div class="category-text"><span>Township   :</span> <?=$PDO->getSingleresult("select name from #_township where pid='".$lab_data['township']."' ")?> </div>


 <div class="col-md-12 col-sm-12 col-xs-12"><button class="btn greenbutton fright" type="button"  data-toggle="modal" data-target="#bookedDiv"  onclick="schedulebox('Healthcare','<?=$services_search?>','<?=$lab_data['user_id']?>')" ><i class="icon-ok"></i> <span  id="scheduleHealthcare<?=$lab_data['user_id']?>">Book</span></button></div>
 
 <!--<div class="col-md-12 col-sm-12 col-xs-12"><button class="btn greenbutton fright" type="button" onclick="bookedleb('Healthcare','<?=$lab_data['pid']?>','<?=$_SESSION['call_id']?>')" ><i class="icon-ok"></i> <span  id="Healthcare<?=$lab_data['pid']?>">Book</span></button></div>-->
</div>
</div>


<div class="panel-group" id="accordion">
<?php if($lab_data['home_visit_service']=='Available'){?>
<div class="panel panel-default" id="panel1">
<div class="custom-heading"><a data-toggle="collapse" data-target="#collapseOne" href="#collapse-1"></a>Home Visit Service</div>
<div id="collapseOne" class="panel-collapse collapse in">
    <div class="panel-body" id="#collapse-1">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th><span>Visit Charges :</span> <?=$lab_data['home_visit_service_charges']?></th>
                    <th><span>Coverage Area :</span> <?=$lab_data['home_visit_coverage_area']?></th>
                    <th><span>Available Time :</span> <?=$lab_data['home_visit_available_start_time']?> TO <?=$lab_data['home_visit_available_end_time']?></th>
                </tr>
            </thead>
        </table>
    </div>
</div>
</div>

<?php } ?>
<div class="panel panel-default" id="panel2">
<div class="custom-heading"><a data-toggle="collapse" data-target="#collapseTwo"  href="#collapse-1"></a> Other Services
</div>
<div id="collapseTwo" class="panel-collapse collapse in">
    <div class="panel-body" id="collapse-2">
    <?=$lab_data['healthcare_services']?>
    <table class="table table-bordered">
            <thead>
                <?php if($lab_data['medical_checkup_plan']!=''){?>
                <tr>
                    <th><span>Medical Checkup Plan</span></th>
                    <th><?=$lab_data['medical_checkup_plan']?></th>
                </tr>
                <?php } 
                
                $services = (array)json_decode($lab_data['services'], true);
                
                $labdiv ='careLab'.$lab_data['pid'];
                $imagingdiv ='careImaging'.$lab_data['pid'];
                
                if($services['leb']==1) {
                ?>
                <tr>
                    <th><span>Available Lab Test List</span> </th>
                    <th><a href="javascript:void(0)" data-toggle="modal" onclick="showslider('<?=$labdiv?>','<?=$imagingdiv?>')" id="see<?=$labdiv?>">See/Close Here</a>           
                 </th>
                </tr>
                <?php } if($services['imaging']==1) {?>
                <tr>
                    <th><span>Available Imaging Test List</span></th>
                    <th><a href="javascript:void(0)" data-toggle="modal" onclick="showslider('<?=$imagingdiv?>','<?=$labdiv?>')"  id="see<?=$imagingdiv?>">See/Close Here </a> </th>
                </tr>
               <?php } ?>
            </thead>
        </table>
           <?php include('lab-data.php');?>
    </div>
</div>
</div>
</div>		
</div>
</div>
</div>
</div>
</div>
<!--CLOSE info box-->
<?php } ?>