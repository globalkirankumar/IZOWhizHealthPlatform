<style>
*::-moz-placeholder {
   color: #333 !important;
    opacity: 1 !important;
}
*::-webkit-placeholder {
   color: #333 !important;
    opacity: 1 !important;
}
</style>
<?php 
if(@$_GET['sc']!="schedule"){
	
	
	include('view-appointment.php');
}else{
	
	
	
 include(FS_ADMIN._MODS."/appointment/class.inc.php");
$OP = new Options();
$uid = @$_GET['uid'];
$user_type = @$_SESSION['AMD'][2];
$result = $OP->get('booking',$uid);	
if($BSC->is_post_back())
{
   $path = UP_FILES_FS_PATH."/reports";
   if($_FILES['reports']['name'])
   {
		$allowed =  array('gif','png' ,'jpg','jpeg','bmp','txt','pdf','doc','docx');
		$filename = $_FILES['reports']['name'];
		$size = $_FILES['reports']['size'];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if(!in_array($ext,$allowed) ) {
			echo "<script>alert('File Type Mismatch')</script>";
		}else if($size>2097152){
			echo "<script>alert('Maximum file size is 2M')</script>";
		}else{
			 $_POST['reports'] = $BSC->uploadFile2($path,$_FILES['reports']['name'],'reports');
			$OP->upload();
			echo "<script>alert('Reports Uploaded')</script>";
		}
	 
   }

}

?>

<!--right section panel-->
	<div class="vd_content-section clearfix">
		  	<div class="row">
              <div class="col-md-12">
             <?php $k=0; while ($line = $PDO->db_fetch_array($result))
				    {
					    @extract($line);
						$k++;	
						
						$patient = $PDO->db_fetch_array($PDO->db_query("select * from #_patients where pid='{$patient_id}'"));
				?>	
                 
              		<select name="status" id="status" class="input_box_client_select cat2 cat3 validate [required] " data-errormessage-value-missing="status's is required!" onchange="changestatus(this.value,'booking')"  style="width:150px;" >
                        <option value="" >---Select Status---</option>
                           <option value="1" <?=($line['status']==1)?'selected="selected"':''?>   >Active <?=$result['status']?></option>
                           <option value="0"  <?=($line['status']==0)?'selected="selected"':''?>>Inactive</option>
                           <option value="2" <?=($line['status']==2)?'selected="selected"':''?> >Processing</option>
                           <option value="3" <?=($line['status']==3)?'selected="selected"':''?> >Finished</option>
                    </select>
                    
               
              		<div class="section-body">
                    
                        <section >
                <!--TIMELINE FIRST SECTION START-->
					
                      <article>
                        <div class="inner">
                          <!--<span class="step"><?=$k?></span>-->
                          <div style="text-align:left;">
                          <div class="paneledit widget">
                              <div class="panel-heading vd_bg-green">
                                <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-info-circle"></i> </span>Patient / Caller Information</h3>
                                <div class="vd_panel-menu">
                                    <div data-original-title="Config" data-toggle="tooltip" data-placement="bottom" class="nav-medium-button menu entypo-icon smaller-font">
                                    </div>
                                </div>
                              </div>
                              <div class="panel-body2 overflow-hidden">
                                <div class="row mgbt-xs-0">
                                  <div class="col-md-4 col-sm-4 col-xs-12">
                                    <ul class="information">
                                    <li><span>Patient ID:</span><?=$patient['patient_id']?></li>
                                    <li><span>Age:</span><?=$patient['age']?></li>
                                    </ul>
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12">
                                    <ul class="information">
                                    <li><span>Name:</span><?=$patient['name']?></li>
                                    <li><span>Sex:</span><?=$patient['sex']?></li>
                                    </ul>
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12">
                                    <ul class="information">
                                    <li><span>Phone No:</span><?=$patient['phone']?></li>
									<?php $township = $patient['township']; ?>
                                    <li><span>Township:</span><?=$PDO->getSingleResult("select name from #_township where pid='{$township}'")?></li>
                                    </ul>
                                  </div>
                                </div>
								<?php $health_consultation = $PDO->getSingleResult("select health_consultation from #_call_details where pid='{$call_id}'");?>
								<?php if($health_consultation!=''){?>
								<div class="row mgbt-xs-0">
                                  <div class="col-md-4 col-sm-4 col-xs-12">
									<p><?=$health_consultation?></p>
								  </div>
								 </div> 
								<?php } ?>
								<?php $user_type= @$_SESSION['AMD'][2];?>
								
								<?php if(strtolower($user_type)!='healthcare_organization'){?>
									
                                <div class="history-btn">
								<?php if(strtolower($user_type)!='labs' and strtolower($user_type)!='imagine'){?>
								<?php $call_type = $PDO->getSingleResult("select call_type from #_call_details where pid='{$call_details_id}'");?>
								<?php if($call_type!=''){?>
                                    <span class="patient-history"><?php echo $call_type;?></span>
								<?php } ?>
									<?php include('model.php');?>
                                    <span class="patient-history"><a href="javascript:;"  data-toggle="modal" data-target=".modal-edit<?php echo $i;?>">Medical History</a></span>	
								<?php } ?>	
                                 	
                                  <?php $files = $OP->get('reports',$call_details_id,'call_details_id');
									if($OP->count_val($files)>0){
									?>
									<?php include('download-model.php');?>
                                    <span class="patient-history"><a href="#"  data-toggle="modal" data-target=".modal-download">Download Reports</a></span>
									<?php } ?>
									 <span class="patient-history"><a href="#"  data-toggle="modal" data-target=".modal-edits8">Upload Reports</a></span>
                                     <span class="patient-history"><a href="javascript:;"  data-toggle="modal" data-target=".modal-edit<?php echo $i;?>">Add Medical History</a></span>	
                                </div>
								<?php } ?>
							  
								
								
								<!--    Hospital and Clinic -->
								<?php   if(@$_GET['sc']='schedule'){ ?>
                                <?php  
										if(strtolower($book_type)=="clinics"){
										$book_name = $PDO->getSingleResult("select name from #_clinics where user_id='{$book_id}'");
										
									}else if(strtolower($book_type)=="hospital"){
										$book_name = $PDO->getSingleResult("select name from #_hospitals where user_id='{$book_id}'");
									}else if(strtolower($book_type)=="lab"){
										$book_name = $PDO->getSingleResult("select name from #_labs where user_id='{$book_id}'");
										
									}else if(strtolower($book_type)=="healthcare"){
										$book_name = $PDO->getSingleResult("select name from #_healthcare_organization where user_id='{$book_id}'");
									} ?>
                                
                                 <div class="app">
                                  <span class="doctor-heading">Appointment(s)</span>
                                  <div class="table-responsive">
                                 <table class="table table-bordered align-left">
                                    <thead>
                                        <tr>
											<td><span>Name</td>
                                            <td><span>Appointment Date</td>
                                            <td><span>Appointment Time</span></td>
											
                                           
                                        </tr>
                                    </thead>
                                      <tr>
                                           <td><?=$book_name?></td>
                                            <td><?=$book_date?></td>
                                            <td><?=$book_time?></td>
                                          
                                        </tr>
                                         </table>
                                 <div class="clearfix"></div>
									</div>
                                    </div>
                                
                                
								
								
								<?php }else{ ?>
								<?php $doc_id=$PDO->getSingleResult("select doctor_id from #_appointment where call_details_id='{$call_details_id}'");
								if($doc_id!=''){?>
								<div class="table-responsive">
                                 <table class="table table-bordered align-left">
                                    <thead>
                                        <tr>
                                            <td><span>Name</span></td>
                                            <td><span>Appointment Date</span></td>
                                            <td><span>Appointment Time</span></td>
                                           
                                        </tr>
                                    </thead>
									<tr>
									<td><?php 
											echo $PDO->getSingleResult("select name from #_doctors where pid='{$doc_id}'");?></td>
									<td><?=$PDO->getSingleResult("select app_date from #_appointment where call_details_id='{$call_details_id}'")?></td>
                                        <td><?=$PDO->getSingleResult("select app_time from #_appointment where call_details_id='{$call_details_id}'")?></td>
                                        </tr>
                                   </table></div> 
								<?php }} ?>
								 
								<!-- end Appointment List -->	
                                
                              </div>
                            </div>
                          </div>
                        </div>
                      </article>
					<?php } ?>
  <!--TIMELINE FIRST SECTION CLOSE-->
						
</section>
              		</div>
              </div>
            </div>
		</div>
 <?php include('upload-model.php');?>
<?php } ?>
<script>
function changestatus(status,table)
{
         var str ='status='+status+'&table='+table+'&uid=<?=$uid?>';
		 $.ajax({type: "POST",
				 url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag=status",
				 data: str,
				 cache: false,
				 success: function(html){ 
				 
				   alert('Appointment status is changed');
				   // $('#PatientDiv').html(html);	
					
				 }
		});
}
</script>