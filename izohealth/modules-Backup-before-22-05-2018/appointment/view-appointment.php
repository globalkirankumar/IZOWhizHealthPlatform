<?php 
include(FS_ADMIN._MODS."/appointment/class.inc.php");
$OP = new Options();
$uid = @$_GET['uid'];
$user_type = @$_SESSION['AMD'][2];
$result = $OP->get('appointment',$uid);

if($BSC->is_post_back())
{
// add medical history
//$PDO->db_query("update  #_appointment set status ='".$_POST['status']."' where pid ='".$uid."'  ");

$calldata['doctor_id']=$_SESSION["AMD"][0];
$calldata['call_detal_id']=$_POST["call_detal_id"];
$calldata['next_follow_up_appointment']=$_POST["next_follow_up_appointment"];
$calldata['history_data'] = json_encode($_POST);
$calldata['status'] =1;
$calldata['created_on']=@date('Y-m-d H:i:s');
$calldata['create_by']=$_SESSION["AMD"][0];
$calldata['shortorder']=$PDO->getSingleresult("select max(shortorder) as shortorder from #_doctor_history
where 1=1 ")+1;

	$path = UP_FILES_FS_PATH."/history";
	$allowed =  array('gif','png' ,'jpg','jpeg','bmp','txt','pdf','doc','docx');
   if($_FILES['imaging_investigation1']['name'])
   {
		$filename = $_FILES['imaging_investigation1']['name'];
		$size = $_FILES['imaging_investigation1']['size'];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if(!in_array($ext,$allowed) ) {
			echo "<script>alert('File Type Mismatch')</script>";
		}else if($size>2097152){
			echo "<script>alert('Maximum file size is 2M')</script>";
		}else{
			 $calldata['imaging_investigation1'] = $BSC->uploadFile2($path,$_FILES['imaging_investigation1']['name'],'imaging_investigation1');
			
		}
	 
	}
	  if($_FILES['imaging_investigation2']['name'])
   {
		$filename = $_FILES['imaging_investigation2']['name'];
		$size = $_FILES['imaging_investigation2']['size'];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if(!in_array($ext,$allowed) ) {
			echo "<script>alert('File Type Mismatch')</script>";
		}else if($size>2097152){
			echo "<script>alert('Maximum file size is 2M')</script>";
		}else{
			 $calldata['imaging_investigation2'] = $BSC->uploadFile2($path,$_FILES['imaging_investigation2']['name'],'imaging_investigation2');
			
		}
	 
	}
	  if($_FILES['imaging_investigation3']['name'])
   {
		$filename = $_FILES['imaging_investigation3']['name'];
		$size = $_FILES['imaging_investigation3']['size'];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if(!in_array($ext,$allowed) ) {
			echo "<script>alert('File Type Mismatch')</script>";
		}else if($size>2097152){
			echo "<script>alert('Maximum file size is 2M')</script>";
		}else{
			 $calldata['imaging_investigation3'] = $BSC->uploadFile2($path,$_FILES['imaging_investigation3']['name'],'imaging_investigation3');
			
		}
	 
	}
	  if($_FILES['imaging_investigation4']['name'])
   {
		$filename = $_FILES['imaging_investigation4']['name'];
		$size = $_FILES['imaging_investigation4']['size'];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if(!in_array($ext,$allowed) ) {
			echo "<script>alert('File Type Mismatch')</script>";
		}else if($size>2097152){
			echo "<script>alert('Maximum file size is 2M')</script>";
		}else{
			 $calldata['imaging_investigation4'] = $BSC->uploadFile2($path,$_FILES['imaging_investigation4']['name'],'imaging_investigation4');
			
		}
	 
	}
	if($_FILES['imaging_investigation5']['name'])
   {
		$filename = $_FILES['imaging_investigation5']['name'];
		$size = $_FILES['imaging_investigation5']['size'];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if(!in_array($ext,$allowed) ) {
			echo "<script>alert('File Type Mismatch')</script>";
		}else if($size>2097152){
			echo "<script>alert('Maximum file size is 2M')</script>";
		}else{
			$calldata['imaging_investigation5'] = $BSC->uploadFile2($path,$_FILES['imaging_investigation5']['name'],'imaging_investigation5');
			
		}
	 
	}

	$PDO->sqlquery("rs",'doctor_history',$calldata);
		echo "<script>alert('Doctor history added successfully.')</script>";
// end medical history	
   $path = UP_FILES_FS_PATH."/reports";
   if($_FILES['reports']['name'])
   {
		$allowed =  array('gif','png' ,'jpg','jpeg','bmp','txt','pdf','doc','docx');
		$filename = $_FILES['reports']['name'];
		$size = $_FILES['reports']['size'];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if(!in_array($ext,$allowed) ) {
			echo "<script>alert('File Type Mismatch')</script>";
		}else if($size>2097152){
			echo "<script>alert('Maximum file size is 2M')</script>";
		}else{
			 $_POST['reports'] = $BSC->uploadFile2($path,$_FILES['reports']['name'],'reports');
			$OP->upload();
			echo "<script>alert('Reports Uploaded')</script>";
		}
	 
   }

}

?>

<!--right section panel-->
	<div class="vd_content-section clearfix">
		  	<div class="row">
            <?php $k=0; while ($line = $PDO->db_fetch_array($result))
				    {
					    @extract($line);
						$k++;
						$patient = $PDO->db_fetch_array($PDO->db_query("select * from #_patients where pid='{$patient_id}'"));
				?>	
              <div class="col-md-12">
              		<select name="status" id="status" class="input_box_client_select cat2 cat3 validate [required]" data-errormessage-value-missing="status's is required!" onchange="changestatus(this.value,'appointment')"  style="width:150px;" >
                        <option value="" >---Select Status---</option>
                           <option value="1" <?=($line['status']==1)?'selected="selected"':''?>   >Active <?=$result['status']?></option>
                           <option value="0"  <?=($line['status']==0)?'selected="selected"':''?>>Inactive</option>
                           <option value="2" <?=($line['status']==2)?'selected="selected"':''?> >Processing</option>
                           <option value="3" <?=($line['status']==3)?'selected="selected"':''?> >Finished</option>
                    </select>
                    
              		<div class="section-body">
                        <section>
                <!--TIMELINE FIRST SECTION START-->
					
                      <article>
                        <div class="inner">
                       <!--   <span class="step"><?=$k?></span>-->
                          <div style="text-align:left;">
                          <div class="paneledit widget">
                              <div class="panel-heading vd_bg-green">
                                <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-info-circle"></i> </span>Patient / Caller Information</h3>
                                <div class="vd_panel-menu">
                                    <div data-original-title="Config" data-toggle="tooltip" data-placement="bottom" class="nav-medium-button menu entypo-icon smaller-font">
                                    </div>
                                </div>
                              </div>
                              <div class="panel-body2 overflow-hidden">
                                <div class="row mgbt-xs-0">
                                  <div class="col-md-4 col-sm-4 col-xs-12">
                                    <ul class="information">
                                    <li><span>Patient ID:</span><?=$patient['patient_id']?></li>
                                    <li><span>Age:</span><?=$patient['age']?></li>
                                    </ul>
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12">
                                    <ul class="information">
                                    <li><span>Name:</span><?=$patient['name']?></li>
                                    <li><span>Sex:</span><?=$patient['sex']?></li>
                                    </ul>
                                  </div>
                                  <div class="col-md-4 col-sm-4 col-xs-12">
                                    <ul class="information">
                                    <li><span>Phone No:</span><?=$patient['phone']?></li>
									<?php $township = $patient['township']; ?>
                                    <li><span>Township:</span><?=$PDO->getSingleResult("select name from #_township where pid='{$township}'")?></li>
                                    </ul>
                                  </div>
                                </div>
								<?php $user_type= @$_SESSION['AMD'][2];?>
								
								<?php if(strtolower($user_type)!='healthcare_organization'){?>
                                <div class="history-btn">
								<?php if(strtolower($user_type)!='labs' and strtolower($user_type)!='imagine'){?>
								<?php $call_type = $PDO->getSingleResult("select call_type from #_call_details where pid='{$call_details_id}'");?>
								<?php if($call_type!=''){?>
                                    <span class="patient-history"><?php echo $call_type;?></span>
								<?php } ?>
								<?php $medical = $PDO->db_fetch_array($OP->get('call_details',$call_details_id));?>
								
									<?php include('model.php');?>
                                    <span class="patient-history"><a href="javascript:;"  data-toggle="modal" data-target=".modal-edit<?php echo $i;?>">Medical History</a></span>	
								<?php } ?>
                                 
                                 <?php if($user_type=='doctors') {
									 
									 $doctors_history_query = $PDO->db_query("select * from #_doctor_history
 where call_detal_id='{$call_details_id}' and  doctor_id='".$_SESSION["AMD"][0]."'");
                                     if(mysql_num_rows($doctors_history_query)>0)
									 {
										  $doctors_history_data = $PDO->db_fetch_array($doctors_history_query);
										  $history_data=(array) json_decode($doctors_history_data['history_data']);
										  include('doctorhistory.php');
									 ?>
                                           <span class="patient-history"><a href="javascript:;"  data-toggle="modal" data-target="#doctorhistoryview" >Doctor Medical History </a></span>
                                           
                                    <?php }else{ ?>
                                 <span class="patient-history"><a href="javascript:;"  data-toggle="modal" data-target="#doctorHistory" onclick="$('input[name=call_detal_id]').val('<?=$call_details_id?>')">Medical Record  </a></span>
                                 <?php } } ?>	
                                	
                                  <?php $files = $OP->get('reports',$call_details_id,'call_details_id');
									if($OP->count_val($files)>0){
									?>
									<?php include('download-model.php');?>
                                    <span class="patient-history"><a href="#"  data-toggle="modal" data-target=".modal-download">Download Reports</a></span>
									<?php } ?>
									 <span class="patient-history"><a href="#"  data-toggle="modal" data-target=".modal-edits8">Upload Reports</a></span>
                                </div>
								<?php } ?>
							  
								 </div>
								<!--    Appointment List -->
								
								<?php if(strtolower($user_type)=="doctor" or strtolower($user_type)=="doctors")
								{
									
									$clinic_id = $PDO->getSingleResult("select clinic_id from #_doctors where user_id='{$doctor_id}' and clinic_id>0");
									
									if($clinic_id>0){
										$book_name = $PDO->getSingleResult("select name from #_clinics where pid='{$clinic_id}'");
									}else{
									$hospital_id = $PDO->getSingleResult("select hospital_id from #_doctors where user_id='{$doctor_id}' and hospital_id>0");	
									$book_name = $PDO->getSingleResult("select name from #_hospitals where user_id='{$hospital_id}'");
									}
									//$n = $book_name.' '.$book_name1;
									?>
                               
                                   
								 <div class="app">
                                 
                                 
                                   
                                 
                                <span class="doctor-heading">Appointment(s)</span>
                                
                                   <div class="table-responsive">
                                 <table class="table table-bordered align-left">
                              
                                    <thead>
                                        <tr>
											<td>Name</td>
                                            <td><span>Appointment Date</span></td>
                                            <td><span>Appointment Time</span></td>
                                        </tr>
                                    </thead>
                                     <tr>
                                           <td><?=ucfirst($book_name)?></td>
                                            <td><?=date('d F Y', strtotime($app_date))?></td>
                                            <td><?=$app_time?></td>
                                        </tr>
                                        
                                        </table>
                                 <div class="clearfix"></div>
									</div>
								
								</div>	
								
								<?php }else{?>
								
								<?php //$doc_id=$PDO->getSingleResult("select doctor_id from #_appointment where call_details_id='{$call_details_id}'");
								$doc_id = $doctor_id;
								if($doc_id!=''){?>
								 <div class="app">
                                <span class="doctor-heading">Appointment(s)</span>
								<div class="table-responsive">
                                 <table class="table table-bordered align-left">
                                    <thead>
                                        <tr>
                                            <td><span>Name</span></td>
                                            <td><span>Appointment Date</span></td>
                                            <td><span>Appointment Time</span></td>
                                           
                                        </tr>
                                    </thead>
									<tr><td><?php 
											echo $PDO->getSingleResult("select name from #_doctors where user_id='{$doc_id}'");?></td>
                                   <td><?=$app_date?></td>
                                      <td><?=$app_time?></td>
                                       </tr>
									   </table>
                                    <div class="clearfix"></div>
									</div>
                                  </div>
								<?php }}?>
								
								<!-- end Appointment List -->	
                              
                              </div>
                            </div>
                          </div>
                        </div>
                      </article>
					<?php } ?>
  <!--TIMELINE FIRST SECTION CLOSE-->
						
</section>
              		</div>
              </div>
            </div>
		</div>
<div id="loader">
<img src="<?=SITE_PATH?>img/gif-load.gif">
</div>  

<script>


function doctorHistory(type)
{
	 var str = $("#doctorHistoryfrom").serialize(); 
	$("#doctorHistoryfrom").submit();
	$('#loader').show();
	
	/* $.ajax({
		           type: "POST",
		           url: "<?=SITE_PATH_ADM?>/modules/patient_ajax.php?flag=doctorHistory",
		           data: str,
		           cache: false,
		           success: function(html){
					  // alert(html)
						 //$('#doctorhistoryfromerrormsg').html('Doctor history added successfully.');
						  alert('Doctor history added successfully.')
						 window.location.href='<?=SITE_PATH?>home.php?comp=appointment&mode=view&uid=<?=$uid?>';
			            // $("#property_type").html(html);
						
	}});*/
}

</script>
 <?php include('upload-model.php');?>