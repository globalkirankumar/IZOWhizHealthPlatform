<?php 

include(FS_ADMIN._MODS."/add_banner/class.inc.php");

$OP = new Options();

if($BSC->is_post_back())
{
	
	$path = UP_FILES_FS_PATH."/banner";
	   
	$allowed =  array('gif','png','jpg','jpeg');
	$flag =1;
	
	if($_FILES['reports']['name']!='')
	{
	
		$filename = $_FILES['reports']['name'];
		$size = $_FILES['reports']['size'];
		$ext = pathinfo($filename, PATHINFO_EXTENSION);
		if(!in_array($ext,$allowed) )
		 {
			$flag =0;
            $PDO->sessset('File Type Mismatch', 'e');
		}else if($size>2097152){
			$flag =0;
			$PDO->sessset('Maximum file size is 2M', 'e');
		}else{
			
			 $_POST['file_name'] = $BSC->uploadFile2($path,$_FILES['reports']['name'],'banner');
			
		}	
		
	}
	
	if($flag ==1)
	{
		
	   if($uid)
	   {
		   $_POST['updateid']=$uid;
		   $flag = $OP->update($_POST);
	  
	   }else {
		 
		   $flag = $OP->add($_POST);
		   
			 
	   }
	   
	   if($flag==1)
	   {
	   
		 $BSC->redir($ADMIN->iurl($comp.(($start)?'&start='.$start:'').(($subpage_id)?'&subpage_id='.$subpage_id:'').(($alumniid)?'&alumniid='.$alumniid:'').(($division_id)?'&division_id='.$division_id:'')).$dlr, true);
	   }
	   
	}
	
}


if($uid)
{
    $query =$PDO->db_query("select * from #_".tblName." where pid ='".$uid."' "); 
	$row = $PDO->db_fetch_array($query);
	@extract($row);	
}

?>

<div class="vd_content-section clearfix">
		  	<div class="row" id="form-basic">
			  <?=$ADMIN->alert()?>
              		<div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span> Add Banner <?=$ADMIN->compname($comp)?> </h3>
                </div>
              		
              		<div class="panel-body">
				<!-- End Services 		
                <form  class="form-horizontal body-gap" action="#" role="form" id="register-form" enctype="multipart/form-data">
                    -->               
                    <div class="form-group">
                        <label class="control-label  col-sm-4"> Add Banner  </label>
                        <div id="first-name-input-wrapper" class="controls col-sm-4">
                         <select name="banner_page" class="input_box_client_select cat2" class="validate [required]" data-errormessage-value-missing="Page Type's is required!" id="discount_type">
                              <option value="" >--select--</option>
                              <option value="Home Page" <?php if($banner_page=='Home Page'){echo "selected";}?>>Home Page</option>
                              <option value="Inner Page" <?php if($banner_page=='Inner Page'){echo "selected";}?>>Inner Page</option>
                         </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label  col-sm-4"> Banner Type </label>
                        <div id="first-name-input-wrapper" class="controls col-sm-4">
                         <select name="banner" class="input_box_client_select cat2" class="validate [required]" data-errormessage-value-missing="Banner Type's is required!">
                              <option value="">--select--</option>
                              <option value="Right Top" <?php if($banner_type=='Right Top'){echo "selected";}?>>Right Top</option>
                              <option value="Right Bottom" <?php if($banner_type=='Right Bottom'){echo "selected";}?>>Right Bottom</option>
                              <option value="Bottom Up" <?php if($banner_type=='Bottom Up'){echo "selected";}?>>Bottom Up</option>
                              <option value="Bottom Down" <?php if($banner_type=='Bottom Down'){echo "selected";}?>>Bottom Down</option>
                         </select>
                        </div>
                    </div>
                
                   <div class="form-group">
                      <label class="control-label  col-sm-4"> Title <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-3">
                         <input class="validate[required]" data-errormessage-value-missing="Title is required!" name="title" value="<?=$title?>" type="text">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label  col-sm-4">Upload File <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-5">
                         <?php if($reports!='' && file_exists(UP_FILES_FS_PATH."/banner/".$reports) ) {?>
                  					 <div class="tool"><a href="<?=SITE_PATH."uploaded_files/banner/".$reports?>" target="_blank">View</a>
                                     </div>
                   					  <a href="" class="image"> <span></span>
                                      <img src="<?=SITE_PATH."uploaded_files/banner/".$reports?>" alt="" width="200" height="100"></a>
              					<?php } ?>
                         <input type="file" name="reports" <?=($uid==0)?'class="validate[required]"':''?> data-errormessage-value-missing="File is required!">
                           <label style="color:#D20000; font-size:11px">NOTE:File type allowed("jpeg,jpg,png only") and Maximum file size 2 MB</label>
           				   <label style="color:#D20000; font-size:11px">Image Size Home Page --</label><br/>
                           <label style="color:#D20000; font-size:11px">NOTE:Right Top & Right Bottom Size: Width:255px, Height:255px</label><br/>
                           <label style="color:#D20000; font-size:11px">NOTE:Bottom Up and Bottom Down Size: Width:825px, Height:180px</label><br/>
                           <label style="color:#D20000; font-size:11px">Image Size Inner Page --</label><br/>
                           <label style="color:#D20000; font-size:11px">NOTE:Right Top and Righit Bottom Size: Width:255px, Height:255px</label><br/>
                            <label style="color:#D20000; font-size:11px">NOTE:Bottom Up and Bottom Down Size: Width:825px, Height:180px</label>
                        </div>
                    </div>
                    
					<div class="form-group">
                         <label class="col-sm-4 control-label"> Date Form <span class="star">*</span></label>
                            <div class="col-sm-3 controls">
                              <div class="vd_input-wrapper light-theme no-icon">
                             <input type="text" name="date_from" id="date_from" value="<?=$date_from?>" class="input_box_client_select cat2 datepicker">
                            </div>
                          </div>
                    </div>
                    
                    <div class="form-group">
                         <label class="col-sm-4 control-label"> Date To <span class="star">*</span></label>
                            <div class="col-sm-3 controls">
                              <div class="vd_input-wrapper light-theme no-icon">
                             <input type="text" name="date_to" id="date_to" value="<?=$date_to?>" class="input_box_client_select cat2 datepicker">
                            </div>
                          </div>
                    </div>
            
					
					<!-- End Services -->
					<div class="form-group">
                      <label class="control-label col-sm-4 col-xs-12">Status <span class="vd_red">*</span></label>
                      <div id="website-input-wrapper" class="controls col-sm-3 col-xs-6">
                          <select name="status" class="validate [required]" data-errormessage-value-missing="Status is required!">
                              <option  value="">-------Select Status------</option>
								<option value="1" <?=($status==1)?'selected="selected"':''?> >Active</option>
								<option value="0" <?=(isset($status) && $status==0)?'selected="selected"':''?>>Inactive</option>
                          </select>
                    </div>
                  </div>
                
                  
                  <div class="form-group form-actions">
                    <div class="col-sm-6"></div>
                    <div class="col-md-6">
					 <input type="hidden" name="pid" value="<?=$division_id?>" />
                       <button id="submit-register" name="submit-register" class="btn vd_btn vd_bg-green vd_white uibutton loading" type="submit">
                       <i class="icon-ok" ></i> Save</button>
                          <button onclick="location.reload();" class="btn vd_btn" type="button">Cancel</button>
                    </div>
                  </div>
             <!--   </form>-->
			<!-- close add-update form-->
              		</div>
              </div>
            </div>
		</div>
<script src="<?=SITE_PATH?>validation/js/jquery-1.8.2.min.js"></script>
<script src="<?=SITE_PATH?>validation/js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=SITE_PATH?>validation/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>

<script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#formID").validationEngine();
		});
		$('.hide_show').change(function(){
			var id = $(this).attr("data-id");
			if($(this).val()=="Available")
			{
				$('#'+id).show();
			}else{
				$('#'+id).hide();
			}
		});
		$('.services').click(function(){
			var id = $(this).attr("data-id");
			if(this.checked)
			{
				$('#'+id).show();
			}else{
				$('#'+id).hide();
			}
		});
</script>
