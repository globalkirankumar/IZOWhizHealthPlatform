<?php 
include(FS_ADMIN._MODS."/doctors/class.inc.php");
$OP = new Options();

if($action)
{
  
  if($uid >0  || !empty($arr_ids))
  {
   
	switch($action)
	{
		  case "del":
						 $OP->delete($uid);
						 $ADMIN->sessset('Record has been deleted', 'e'); 
						 break;
						 
		  case "Delete":
						 $OP->delete($arr_ids);
						 $ADMIN->sessset(count($arr_ids).' Item(s) Deleted', 'e');
						 break;
						 
						 
		  case "Active":
						 $OP->status($arr_ids,1);
						 $ADMIN->sessset(count($arr_ids).' Item(s) Active', 's');
						 break;
						 
		  case "Inactive":
						 $OP->status($arr_ids,0);
						 $ADMIN->sessset(count($arr_ids).' Item(s) Inactive', 's');
						 break;
					 
		  
		  default:
	}
    $BSC->redir($ADMIN->iurl($comp), true);
  }
}

$start = intval($start);
$pagesize = intval($pagesize)==0?(($_SESSION["totpaging"])?$_SESSION["totpaging"]:DEF_PAGE_SIZE):$pagesize;
if(@$_GET['hospital_id']!='')
{
	$filter_by = "hospital_id";
	$filter_value = @$_GET['hospital_id'];
}else if(@$_GET['clinic_id']!=''){
	$filter_by = "clinic_id";
	$filter_value = @$_GET['clinic_id'];
}
else{
	$filter_by = "";
	$filter_value = "";
}
list($result,$reccnt) = $OP->display($start,$pagesize,$fld,$otype,$search_data,$filter_by,$filter_value);

?>
<!--right section panel-->
		<div class="vd_content-section clearfix">
		  	<div class="row">
			
              <div class="col-md-12">
			  <?=$ADMIN->alert()?>
			   <div class="info-call-details">
                <ul>
                    <li>  <input type="text"  name="search_data"  value="<?=$search_data?>"  placeholder="Search"/></li>
                    <li><input type="submit" class="records-search greenbutton inputsearch" value="Search"></li>
                </ul>
            </div>
			
              		<div class="panel-heading vd_bg-green white">
                    <h3 class="panel-title">Doctors List </h3>
                  	</div>
              		<div class="section-body">
              			
						
			<!--edit table-->
                    <div class="table-responsive " id="ordrz">
                    <table class="table data-tbl custom-style table-striped" >
                    <thead>
                      <tr class="tbl-head">
                        <th><?=$ADMIN->check_all()?></th>
                        <th>No.</th>
						 <th><a href="<?=$ADMIN->iurl($comp)?>&fld=name<?=(($otype=='asc')?"&otype=desc":'&otype=asc')?>" <?=(($fld=='name')?'class="selectedTab"':'')?>><span <?=(($otype=='asc')?'class="des"':'class="asc"')?>> Name</span></a></th>
                        <th><a href="<?=$ADMIN->iurl($comp)?>&fld=email<?=(($otype=='asc')?"&otype=desc":'&otype=asc')?>" <?=(($fld=='email')?'class="selectedTab"':'')?>><span <?=(($otype=='asc')?'class="des"':'class="asc"')?>> Email</span></a></th>
                        <th>QRCode</th>
			   <th>Verification Status</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
               </thead>
				<tbody>	
				<!--</table>
					 <ul class="sortable-liststyle" style="list-style:none;">-->
          
          
            <?php if($reccnt)
			      { 
			
			        $nums = (($start)?$start+1:1); 
					$k = 0;
				    while ($line = $PDO->db_fetch_array($result))
				    {
					    @extract($line);
						$k++;
						$css =($k%2!=0)?'success':'';
			
			
			?>
         <!-- <li style="list-style:none;" id="recordsArray_<?=$pid?>">
			<table class="table data-tbl custom-style">
			 <thead>-->
				<tr data-item-id=1 class="item <?=$css?>">
                  <th><?=$ADMIN->check_input($pid)?></th>
                  <th><?=$nums?></th>
                  <th><a href="<?=$ADMIN->iurl('verify_doctors_locations')?>&did=<?=$pid?>"><?=ucwords($name)?></span></a></th>                  <th><?=$email?></th>
                 <?php 
                  $doctor_location_data=array();
                  $doctor_location_query = $PDO->db_query("select * from #_doctors_locations where doctor_id='" . $pid . "'");
                  if (mysql_num_rows($doctor_location_query) > 0) {
                  	$i = 0;
                  	while ($row = $PDO->db_fetch_array($doctor_location_query)) {
                  		$current_available_status=$OP->check_available_unavailable($pid,$row['pid']);
                  		if ($current_available_status['status']=='true')
                  		{
				$doctor_location_data[]=array('doctor_location_id'=> $row['pid'],'location_type'=>($row['location_type']==1)?"Home Location":"Work Location",'isHomeservice'=>$row['isHomeservice'],'available_from'=>$row['available_from'],'available_to'=>$row['available_to']);

                  		/*$doctor_location_data[$i]['doctor_location_id'] = $row['pid'];
                  		$doctor_location_data[$i]['location_type'] = ($row['location_type']==1)?'Home Location':'Work Location';
                  		$doctor_location_data[$i]['isHomeservice'] = $row['isHomeservice'];
                  		$doctor_location_data[$i]['available_from'] = $row['available_from'];
                  		$doctor_location_data[$i]['available_to'] = $row['available_to'];*/
                  		}
                  		$i++;
                  	}
                  }
                  $json_data=array('doctor_id'=>$pid,
                  		'doctor_name'=>$name,
                  		'doctor_email'=>$email,
                  		'doctor_mobile'=>$phone,
                  		'user_id'=>$user_id,
                  		'doctor_location_data'=>$doctor_location_data
                  );

		    $json=json_encode($json_data); 
			//print_r( $json);exit;
		    ?>         
		   <th><a href='<?php  echo "https://chart.googleapis.com/chart?cht=qr&chl=".$json."&chs=300x300&choe=UTF-8&chld=L|0"; ?>' target="_blank" > QrCode Link</a></th>
                 <th><?=($is_verified)?'Done':'Not Done' ?></th>
		<th><?=$ADMIN->displaystatus($status)?></th>
                  <th><?=$ADMIN->action($comp, $pid.($clinic_id?'&clinic_id='.$clinic_id:'').($hospital_id?'&hospital_id='.$hospital_id:''))?></th>
            </tr>
			<!-- </thead>
			</table>
            </li>-->
            <?php $nums++; } ?>
            
            
          
           <?php  }else { echo '<div align="center" class="norecord">No Record Found</div>'; } ?>
           </tbody></table>
         <!-- </ul>-->
		
		   <?php include("cuts/paging.inc.php");?>
						<div class="pull-right pagination" style="display:none;">
                        <ul class="pagination">
                        <li class="page-pre"><a href="#">‹</a></li>
                        <li class="page-number active"><a href="#">1</a></li>
                        <li class="page-number"><a href="#">2</a></li>
                        <li class="page-number"><a href="#">3</a></li>
                        <li class="page-number"><a href="#">4</a></li>
                        <li class="page-number"><a href="#">5</a></li>
                        <li class="page-last-separator disabled"><a href="#">...</a></li>
                        <li class="page-last"><a href="#">80</a></li>
                        <li class="page-next"><a href="#">›</a></li>
                        </ul>
                        </div>	
                        </div>
			<!-- close edit table-->
              		</div>

            <!--next button-->
           
			<!--Close next button-->
              </div>
            </div>
		</div>
  <!--Close right section panel-->
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script language="javascript">
jQuery(document).ready(function(){ 
	jQuery(function() {
		jQuery("#ordrz ul").sortable({ opacity: 0.6, cursor: 'move', update: function() {
			var order = jQuery(this).sortable("serialize") + '&tbl=<?=tblName?>&field=pid'; 
			 $.post("<?=SITE_PATH_ADM?>modules/orders.php", order, function(theResponse){ }); 															
		}								  
		});
	});
});	
</script>