<?php 
include(FS_ADMIN._MODS."/voip_call_details/class.inc.php");


$OP = new Options();

if($BSC->is_post_back())
{
   if($uid)
   {
	   $_POST['updateid']=$uid;
       $flag = $OP->update($_POST);
  
   }else {
	 
	   $flag = $OP->add($_POST);
	   
	     
   }
   
   if($flag==1)
   {
   
     $BSC->redir($ADMIN->iurl($comp.(($start)?'&start='.$start:'').(($subpage_id)?'&subpage_id='.$subpage_id:'').(($alumniid)?'&alumniid='.$alumniid:'').(($division_id)?'&division_id='.$division_id:'')).$dlr, true);
   }
}


if($uid)
{
    $query =$PDO->db_query("select * from #_".tblName." where pid ='".$uid."' "); 
	$row = $PDO->db_fetch_array($query);
	@extract($row);	
}

?>

<div class="vd_content-section clearfix">
		  	<div class="row" id="form-basic">
			  <?=$ADMIN->alert()?>
              		<div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span> Add/Health <?=$ADMIN->compname($comp)?> </h3>
                </div>
              		
              		<div class="panel-body">
				

              			
						
			<!--add-update form-->
                <!--    <form  class="form-horizontal body-gap" action="#" role="form" id="register-form">-->
              
                    <div class="form-group">
                        <label class="control-label  col-sm-4">Patient Name <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
                          <input class="validate[required]" name="name" id="name"  value="<?=$PDO->getSingleresult("select name from #_patients where patient_id='".$patient_id."' ");?>" type="text" disabled="">
                        </div>
                    </div>	
					
					<div class="form-group">
                        <label class="control-label  col-sm-4">Doctor Name <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
                          <input class="validate[required]" name="name" id="name"  value="<?=$PDO->getSingleresult("select name from #_doctors where pid='".$doc_id."' ");?>" type="text" disabled="">
                        </div>
                    </div>	
					
					<div class="form-group">
                        <label class="control-label  col-sm-4">Call Duration<span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
                          <input class="validate[required]" name="name" id="name"  value="<?=$call_duration;?>" type="text" disabled="">
                        </div>
                    </div>
					
					<div class="form-group">
                        <label class="control-label  col-sm-4">Call Time<span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
                          <input class="validate[required]" name="name" id="name"  value="<?=$created_on;?>" type="text" disabled="">
                        </div>
                    </div>
					<!--
					<div class="form-group">
                        <label class="control-label  col-sm-4">Comments<span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
                          <input class="validate[required]" name="name" id="name"  value="<?=$comments;?>" type="text" disabled="">
                        </div>
                    </div>-->
					<!-- End Services -->
					
             <!--   </form>-->
			<!-- close add-update form-->
              		</div>
              </div>
            </div>
		</div>
<script src="<?=SITE_PATH?>validation/js/jquery-1.8.2.min.js"></script>
<script src="<?=SITE_PATH?>validation/js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=SITE_PATH?>validation/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#formID").validationEngine();
		});
		$('.hide_show').change(function(){
			var id = $(this).attr("data-id");
			if($(this).val()=="Available")
			{
				$('#'+id).show();
			}else{
				$('#'+id).hide();
			}
		});
		$('.services').click(function(){
			var id = $(this).attr("data-id");
			if(this.checked)
			{
				$('#'+id).show();
			}else{
				$('#'+id).hide();
			}
		});
</script>