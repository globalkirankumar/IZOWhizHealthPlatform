<?php 
include(FS_ADMIN._MODS."/sub_centers/class.inc.php");


$OP = new Options();

if($BSC->is_post_back())
{
   if($uid)
   {
	   $_POST['updateid']=$uid;
       $flag = $OP->update($_POST);
  
   }else {
	 
	   $flag = $OP->add($_POST);
	   
	     
   }
   
   if($flag==1)
   {
   
     $BSC->redir($ADMIN->iurl($comp.(($start)?'&start='.$start:'').(($subpage_id)?'&subpage_id='.$subpage_id:'').(($alumniid)?'&alumniid='.$alumniid:'').(($division_id)?'&division_id='.$division_id:'')).$dlr, true);
   }
}


if($uid)
{
    $query =$PDO->db_query("select * from #_".tblName." where pid ='".$uid."' "); 
	$row = $PDO->db_fetch_array($query);
	@extract($row);	
}

?>

<div class="vd_content-section clearfix">
		  	<div class="row" id="form-basic">
			  <?=$ADMIN->alert()?>
              		<div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> <span class="menu-icon"> <i class="fa fa-bar-chart-o"></i> </span> Add/Health <?=$ADMIN->compname($comp)?> </h3>
                </div>
              		
              		<div class="panel-body">
				

              			
						
			<!--add-update form-->
                <!--    <form  class="form-horizontal body-gap" action="#" role="form" id="register-form">-->
              
                    <div class="form-group">
                        <label class="control-label  col-sm-4">Name of sub-center <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
                          <input class="validate[required]" data-errormessage-value-missing="Name of sub-center is required!" name="name" id="name"  value="<?=$name?>" type="text">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label  col-sm-4">Phone number <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
                          <input class="validate[required]" data-errormessage-value-missing="Phone number is required!" name="phone_number" id="phone_number"  value="<?=$phone_number?>" type="text">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label  col-sm-4">Hotline number <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
                          <input class="validate[required]" data-errormessage-value-missing="Hotline number is required!" name="hotline_number" id="hotline_number"  value="<?=$hotline_number?>" type="text">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label  col-sm-4">Address <span class="vd_red">*</span></label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
                          <input class="validate[required]" data-errormessage-value-missing="Address is required!" name="address" id="address"  value="<?=$address?>" type="text">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label  col-sm-4">Division </label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
                         <select name="division" id="division" class="input_box_client_select cat2"  data-name="township" data-folder="doctor_agents">
                                         <option value="" >---select---</option>
                                         <?php 
										        $record=$PDO->db_query("select * from #_division where status =1  order by name");
										        while($res=mysql_fetch_array($record))
												{
										  ?>
					                 <option value="<?=$res['pid']?>" <?php if($division==$res['pid']){echo "selected";}?>><?=$res['name']?></option>
						              <?php } ?>
                                        </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label  col-sm-4">Township </label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
                       
                          <select name="township" id="township" class="input_box_client_select add_records" >
                                         <option value="" >---select---</option>
                                        <?php $record=$PDO->db_query("select * from #_township where status =1 and division_id='".$division."' order by name");?>
						<?php while($res=mysql_fetch_array($record)){?>
						<option value="<?=$res['pid']?>" <?php if($township==$res['pid']){echo "selected";}?>><?=$res['name']?></option>
						<?php } ?>
                                        </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label  col-sm-4">Contact Person Name</label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
                          <input data-errormessage-value-missing="contact person name is required!" name="contact_person_name" id="contact_person_name"  value="<?=$contact_person_name?>" type="text">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label  col-sm-4">Contact Person number</label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
                          <input data-errormessage-value-missing="Contact Person number is required!" name="contact_person_phone_number" id="contact_person_phone_number"  value="<?=$contact_person_phone_number?>" type="text">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label  col-sm-4">Supervisor Name</label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
                          <input  data-errormessage-value-missing="Supervisor Name is required!" name="supervisor_name" id="supervisor_name"  value="<?=$supervisor_name?>" type="text">
                        </div>
                    </div>
                    
                     <div class="form-group">
                        <label class="control-label  col-sm-4">Supervisor Phone Number</label>
                        <div id="first-name-input-wrapper" class="controls col-sm-7">
                          <input  data-errormessage-value-missing="Supervisor Phone Number is required!" name="supervisor_phone_number" id="supervisor_phone_number"  value="<?=$supervisor_phone_number?>" type="text">
                        </div>
                    </div>
                    
					
					<!-- End Services -->
					<div class="form-group">
                      <label class="control-label col-sm-4 col-xs-12">Status <span class="vd_red">*</span></label>
                      <div id="website-input-wrapper" class="controls col-sm-6 col-xs-12">
                          <select name="status" class="validate [required]" data-errormessage-value-missing="Status is required!">
                              <option  value="">-------Select Status------</option>
								<option value="1" <?=($status==1)?'selected="selected"':''?>  >Active</option>
								<option value="0" <?=(isset($status) && $status==0)?'selected="selected"':''?>>Inactive</option>
                          </select>
                    </div>
                  </div>
                
                  
                  <div class="form-group form-actions">
                    <div class="col-sm-6"></div>
                    <div class="col-md-6">
					 <input type="hidden" name="division_id" value="<?=$division_id?>" />
                       <button id="submit-register" name="submit-register" class="btn vd_btn vd_bg-green vd_white uibutton loading" type="submit"><i class="icon-ok" ></i> Save</button>
                          <button onclick="location.reload();" class="btn vd_btn" type="button">Cancel</button>
                    </div>
                  </div>
             <!--   </form>-->
			<!-- close add-update form-->
              		</div>
              </div>
            </div>
		</div>
<script src="<?=SITE_PATH?>validation/js/jquery-1.8.2.min.js"></script>
<script src="<?=SITE_PATH?>validation/js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8"></script>
<script src="<?=SITE_PATH?>validation/js/jquery.validationEngine.js" type="text/javascript" charset="utf-8"></script>
<script>
		jQuery(document).ready(function(){
			// binds form submission and fields to the validation engine
			jQuery("#formID").validationEngine();
		});
		$('.hide_show').change(function(){
			var id = $(this).attr("data-id");
			if($(this).val()=="Available")
			{
				$('#'+id).show();
			}else{
				$('#'+id).hide();
			}
		});
		$('.services').click(function(){
			var id = $(this).attr("data-id");
			if(this.checked)
			{
				$('#'+id).show();
			}else{
				$('#'+id).hide();
			}
		});
</script>